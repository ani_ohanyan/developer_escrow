-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Фев 19 2016 г., 15:33
-- Версия сервера: 5.6.26
-- Версия PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `descrow`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `profile_image_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `admins`
--

INSERT INTO `admins` (`id`, `username`, `email`, `password`, `type`, `deleted`, `profile_image_id`) VALUES
(0, 'admin', 'admin@admin.com', '202cb962ac59075b964b07152d234b70', 1, 0, 634);

-- --------------------------------------------------------

--
-- Структура таблицы `banks`
--

CREATE TABLE IF NOT EXISTS `banks` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `account_number` varchar(100) NOT NULL,
  `routing_number` varchar(100) NOT NULL,
  `currency` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `banks`
--

INSERT INTO `banks` (`id`, `user_id`, `bank_name`, `account_number`, `routing_number`, `currency`, `status`) VALUES
(2, 1, 'c', 'c', 'c', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `cards`
--

CREATE TABLE IF NOT EXISTS `cards` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `card_number` varchar(255) NOT NULL,
  `cvc` int(11) NOT NULL,
  `expire_month` int(11) NOT NULL,
  `expire_year` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cards`
--

INSERT INTO `cards` (`id`, `user_id`, `card_number`, `cvc`, `expire_month`, `expire_year`, `status`) VALUES
(13, 5, '4242424242424242', 111, 11, 16, 0),
(16, 0, '43453543453', 353, 30, 22, 0),
(26, 1, '4242424242424242', 111, 10, 2016, 0),
(27, 2, '4242424242424242', 111, 10, 2016, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `type` int(1) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `project_id`, `user_id`, `message`, `status`, `type`, `date`) VALUES
(87, 3, 1, 'dsfs', 1, 0, '2016-01-27 16:09:48'),
(88, 3, 1, '', 0, 0, '2016-01-27 16:10:04'),
(144, 7, 2, 'lalala', 1, 0, '2016-01-28 15:12:44'),
(145, 7, 2, '5441', 1, 0, '2016-01-28 15:25:34'),
(146, 7, 2, 'vxd', 1, 0, '2016-01-28 15:25:51'),
(147, 7, 2, 'vgjmgh ', 0, 0, '2016-01-28 15:26:00'),
(148, 7, 2, '5', 1, 0, '2016-01-28 15:26:47'),
(149, 7, 2, 'cvbhdfhf', 1, 0, '2016-01-28 16:11:30'),
(150, 1, 2, '555', 1, 0, '2016-01-28 16:22:56'),
(151, 7, 2, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n', 0, 0, '2016-01-29 11:43:28'),
(152, 7, 2, '', 0, 0, '2016-01-29 11:47:14'),
(153, 6, 1, '66955', 1, 0, '2016-01-29 12:30:26'),
(176, 7, 1, 'h', 1, 0, '2016-02-03 07:06:04'),
(177, 7, 1, '2562', 1, 0, '2016-02-03 07:35:00'),
(178, 7, 1, '85225252525', 1, 0, '2016-02-03 07:35:10'),
(179, 7, 1, '4444', 1, 0, '2016-02-03 07:37:10'),
(180, 7, 1, '', 1, 0, '2016-02-03 07:42:25'),
(181, 7, 1, 'g', 1, 0, '2016-02-03 09:02:28'),
(182, 7, 1, 't', 1, 0, '2016-02-03 09:02:54'),
(183, 7, 1, 'cbxcb', 1, 0, '2016-02-03 10:34:39'),
(184, 7, 2, '5', 1, 0, '2016-02-03 10:53:03'),
(185, 7, 2, '9', 1, 0, '2016-02-03 10:53:22'),
(186, 7, 1, 'Please, enter the comment text - es errori tex@ Melinein asa tox chisht dzevakerpi naxadasutyun@ ))', 1, 0, '2016-02-03 12:15:16'),
(187, 7, 1, 'jn', 1, 0, '2016-02-03 12:15:28'),
(188, 7, 1, 'fgnxfgbn', 1, 0, '2016-02-05 14:57:21');

-- --------------------------------------------------------

--
-- Структура таблицы `diaries`
--

CREATE TABLE IF NOT EXISTS `diaries` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `time` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `diaries`
--

INSERT INTO `diaries` (`id`, `description`, `time`, `project_id`, `user_id`, `milestone_id`, `date`) VALUES
(86, 'test 1', 242, 14, 8, NULL, '2016-02-12 07:11:03'),
(87, 'test 2', 120, 13, 2, 16, '2016-02-12 07:11:52'),
(88, '555', 120, 15, 2, NULL, '2016-02-12 07:18:09'),
(89, 'fff', 61, 13, 2, 17, '2016-02-12 07:27:44'),
(90, '9', 60, 13, 2, 16, '2016-02-12 07:29:52');

-- --------------------------------------------------------

--
-- Структура таблицы `disputes`
--

CREATE TABLE IF NOT EXISTS `disputes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `status` int(1) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `disputes`
--

INSERT INTO `disputes` (`id`, `user_id`, `project_id`, `description`, `receiver_id`, `phone_number`, `status`, `date`) VALUES
(6, 1, '2 ', 'jhy', 2, '', 1, '2016-01-20 12:41:30'),
(7, 2, '1 ', 'kkkk', 1, '777', 0, '2016-01-20 12:43:39'),
(8, 1, '2 ', ' check', 2, '', 0, '2016-01-20 13:07:04'),
(9, 1, '1 ', 'dzgvsdh tdhf ftgh fj', 3, '', 0, '2016-01-20 13:07:53'),
(10, 1, '1 ', 'klbkhb igb jibhihkb', 2, '', 0, '2016-01-20 14:06:28'),
(11, 1, '1 ', 'check last', 2, '', 0, '2016-01-20 14:50:51'),
(12, 2, '2 ', 'cfdsfs', 1, '', 0, '2016-01-21 07:18:56'),
(13, 2, '2 ', 'aaa', 1, '', 0, '2016-01-21 07:20:06'),
(14, 2, '2 ', '7', 1, '', 0, '2016-01-21 07:21:49'),
(15, 2, '2 ', 'jkkk', 1, 'dxd', 0, '2016-01-21 07:24:53'),
(24, 1, '4 ', 'kkk', 2, '', 0, '2016-01-25 10:15:00'),
(25, 1, '4 ', 'kkk', 2, '', 1, '2016-01-25 10:15:01'),
(26, 1, '4 ', 'kkk', 2, '', 0, '2016-01-25 10:15:02'),
(27, 1, '4 ', 'iiki', 2, '', 0, '2016-01-25 10:15:34'),
(28, 1, '4 ', '.;', 2, '', 0, '2016-01-25 10:16:56'),
(29, 1, '2 ', 'kk', 1, '', 0, '2016-01-25 10:17:11');

-- --------------------------------------------------------

--
-- Структура таблицы `dispute_comments`
--

CREATE TABLE IF NOT EXISTS `dispute_comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dispute_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dispute_comments`
--

INSERT INTO `dispute_comments` (`id`, `user_id`, `message`, `status`, `date`, `dispute_id`) VALUES
(1, 1, 'uihnijnkj\r\n', 1, '2016-01-20 12:04:18', 4),
(2, 2, 'cvbcvb', 1, '2016-01-20 12:04:30', 1),
(3, 2, 'i\r\n', 1, '2016-01-20 12:04:49', 1),
(4, 3, 'htfgh tfbh', 0, '2016-01-20 13:08:33', 9),
(5, 3, 'eryg ersybeyr', 0, '2016-01-20 13:11:00', 9),
(6, 3, 'rey5yy', 1, '2016-01-20 13:11:20', 9),
(7, 1, 'dvsdgds', 1, '2016-01-20 13:50:55', 8),
(8, 1, 'lalala\r\n', 1, '2016-01-20 13:52:06', 9),
(9, 1, 'puplic', 1, '2016-01-20 13:54:17', 8),
(10, 1, 'kl', 0, '2016-01-20 13:58:26', 8),
(11, 1, 't', 1, '2016-01-20 14:02:07', 8),
(12, 1, 'r', 0, '2016-01-20 14:02:12', 8),
(13, 1, 'b jhbkjbnkj hbjkbnkjnlkjn\r\n', 1, '2016-01-20 14:04:15', 8),
(14, 1, 'iuhihniknjkjniluniunpinkjniniunhin', 1, '2016-01-20 14:04:43', 8),
(15, 1, 'gbkyvg', 1, '2016-01-20 14:06:44', 10),
(16, 1, 'nhn', 1, '2016-01-20 14:25:36', 7),
(17, 1, 'lk', 1, '2016-01-20 14:32:26', 7),
(18, 1, ', ', 1, '2016-01-20 14:32:35', 7),
(19, 1, 'o', 1, '2016-01-20 14:35:38', 7),
(20, 1, 'u', 1, '2016-01-20 14:35:49', 7),
(21, 1, '555', 1, '2016-01-20 14:36:38', 7),
(22, 2, '999', 1, '2016-01-20 14:36:51', 8),
(23, 2, 'eereter', 1, '2016-01-20 14:37:58', 8),
(24, 2, 'dsnthdrtnh', 0, '2016-01-20 14:49:46', 10),
(25, 2, 'rydbhryn', 0, '2016-01-20 14:50:01', 10),
(26, 1, '111111', 1, '2016-01-20 14:51:21', 11),
(27, 1, '2222', 0, '2016-01-20 14:51:29', 11),
(28, 2, 'zfx vzsf', 0, '2016-01-20 14:51:39', 11),
(29, 1, 'll', 1, '2016-01-25 10:18:53', 7),
(30, 0, 'hggjjhgjtr', 0, '2016-01-25 15:32:09', 14),
(31, 0, 'wer5twert', 1, '2016-01-25 15:32:16', 14),
(32, 1, '555', 1, '2016-01-26 06:20:01', 29),
(33, 1, '4', 1, '2016-01-26 06:20:18', 14),
(34, 2, 'gjf', 1, '2016-01-26 06:22:42', 14),
(35, 2, 'ooooo', 1, '2016-01-26 06:22:55', 14),
(36, 2, 'gfgf', 1, '2016-01-26 06:24:34', 28),
(37, 1, 'h', 1, '2016-01-26 12:32:07', 24),
(38, 1, 'i', 1, '2016-01-26 12:32:47', 24),
(39, 1, 'i', 0, '2016-01-26 12:33:00', 24),
(40, 1, 'ghjgh', 1, '2016-01-26 12:34:17', 24),
(41, 1, ' rert  eta a tert tet4et4e tetga t t4t a', 1, '2016-01-26 12:35:03', 24),
(42, 2, 'lll', 1, '2016-01-26 12:45:18', 28),
(43, 2, 'uku', 1, '2016-01-26 12:45:40', 28),
(44, 2, 'fchfchb dfh h', 0, '2016-01-29 11:50:51', 28),
(45, 2, '44', 1, '2016-01-29 11:52:23', 28),
(46, 2, 'gf', 1, '2016-01-29 11:55:22', 28),
(47, 2, 'a', 1, '2016-01-29 11:55:25', 28),
(48, 2, 'lalalal 6666989498498', 1, '2016-01-29 11:56:57', 28);

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `deleted` int(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) CHARACTER SET latin1 NOT NULL,
  `size` float(11,0) NOT NULL,
  `path` varchar(255) NOT NULL,
  `full_path` varchar(255) NOT NULL,
  `date` int(11) NOT NULL,
  `attachment_id` int(11) DEFAULT NULL,
  `class` enum('diary','project','milestone','project_comment','diary') DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=682 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `files`
--

INSERT INTO `files` (`id`, `user_id`, `deleted`, `name`, `type`, `size`, `path`, `full_path`, `date`, `attachment_id`, `class`, `status`) VALUES
(1, 1, 0, 'Chrysanthemum.jpg', 'image/jpeg', 859, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Chrysanthemum.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Chrysanthemum.jpg', 1454080115, NULL, NULL, 0),
(2, 0, 0, 'Penguins.jpg', 'image/jpeg', 17, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Penguins.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Penguins3.jpg', 1453789295, NULL, NULL, 0),
(3, 3, 0, '', '', 0, 'assets/images/user_avatar.png', '', 0, NULL, NULL, 0),
(4, 2, 0, 'Chrysanthemum.jpg', 'image/jpeg', 859, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum1.jpg', 1453707972, 1, 'diary', 0),
(5, 2, 0, 'Desert.jpg', 'image/jpeg', 826, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Desert3.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Desert3.jpg', 1453707972, 1, 'diary', 0),
(6, 2, 0, 'Jellyfish.jpg', 'image/jpeg', 758, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Jellyfish12.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Jellyfish12.jpg', 1453707972, 1, 'diary', 0),
(7, 2, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala11.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala11.jpg', 1453707972, 1, 'diary', 0),
(222, 1, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Koala63.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Koala63.jpg', 1453910995, 1, 'project_comment', 0),
(223, 1, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Lighthouse89.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Lighthouse89.jpg', 1453910995, 1, 'project_comment', 0),
(224, 1, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Koala64.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Koala64.jpg', 1453911001, 88, 'project_comment', 0),
(339, 2, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala31.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala31.jpg', 1453996242, 7, 'project', 1),
(340, 2, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala32.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala32.jpg', 1453996526, 7, 'project', 0),
(341, 2, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala33.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala33.jpg', 1453996549, 7, 'project', 0),
(372, 2, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse27.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse27.jpg', 1454075354, 2, 'project_comment', 0),
(373, 2, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala49.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala49.jpg', 1454075370, 2, 'project_comment', 0),
(378, 2, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala52.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala52.jpg', 1454075447, 156, 'project_comment', 0),
(379, 2, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala53.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala53.jpg', 1454075469, 2, 'project_comment', 0),
(380, 2, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse30.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse30.jpg', 1454075469, 2, 'project_comment', 0),
(384, 2, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala55.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala55.jpg', 1454075825, 2, 'project_comment', 0),
(385, 2, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse32.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse32.jpg', 1454075825, 2, 'project_comment', 0),
(386, 2, 0, 'Jellyfish.jpg', 'image/jpeg', 758, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Jellyfish29.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Jellyfish29.jpg', 1454075849, 2, 'project_comment', 0),
(387, 2, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala56.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala56.jpg', 1454075849, 2, 'project_comment', 0),
(388, 2, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse33.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse33.jpg', 1454075849, 2, 'project_comment', 0),
(398, 2, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse36.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse36.jpg', 1454076039, 2, 'project_comment', 0),
(406, 2, 0, 'Jellyfish.jpg', 'image/jpeg', 758, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Jellyfish34.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Jellyfish34.jpg', 1454076282, 157, 'project_comment', 0),
(407, 2, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala62.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala62.jpg', 1454076282, 157, 'project_comment', 0),
(408, 2, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse39.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse39.jpg', 1454076282, 157, 'project_comment', 0),
(458, 1, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/qt8YdgnOvSI.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/qt8YdgnOvSI.jpg', 1454081047, 161, 'project_comment', 0),
(459, 1, 0, 'RglNWD-0mf0.jpg', 'image/jpeg', 49, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/RglNWD-0mf0.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/RglNWD-0mf0.jpg', 1454081047, 161, 'project_comment', 0),
(460, 1, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU.jpg', 1454081047, 161, 'project_comment', 0),
(462, 1, 0, 'RglNWD-0mf0.jpg', 'image/jpeg', 49, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/RglNWD-0mf01.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/RglNWD-0mf01.jpg', 1454081063, 162, 'project_comment', 0),
(463, 1, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU1.jpg', 1454081063, 162, 'project_comment', 0),
(464, 1, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/qt8YdgnOvSI2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/qt8YdgnOvSI2.jpg', 1454081068, 162, 'project_comment', 0),
(465, 1, 0, 'RglNWD-0mf0.jpg', 'image/jpeg', 49, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/RglNWD-0mf02.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/RglNWD-0mf02.jpg', 1454081068, 162, 'project_comment', 0),
(466, 1, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU2.jpg', 1454081068, 162, 'project_comment', 0),
(468, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn8.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn8.jpg', 1454081474, 1, 'project_comment', 0),
(470, 1, 0, 'Desert.jpg', 'image/jpeg', 826, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert.jpg', 1454081552, 1, 'project_comment', 0),
(471, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn81.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn81.jpg', 1454081570, 1, 'project_comment', 0),
(472, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn82.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn82.jpg', 1454081751, 1, 'project_comment', 0),
(473, 1, 0, 'Desert.jpg', 'image/jpeg', 826, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert1.jpg', 1454081878, 1, 'project_comment', 0),
(474, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn83.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn83.jpg', 1454081931, 1, 'project_comment', 0),
(475, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn84.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn84.jpg', 1454082313, 1, 'project_comment', 0),
(476, 1, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU3.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU3.jpg', 1454082368, 1, 'project_comment', 0),
(477, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn85.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn85.jpg', 1454082581, 1, 'project_comment', 0),
(478, 1, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg.jpg', 1454082791, 1, 'project_comment', 0),
(480, 1, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/QMR7RuLMJZ8.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/QMR7RuLMJZ8.jpg', 1454082845, 1, 'project_comment', 0),
(483, 1, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg.jpg', 1454083079, 164, 'project_comment', 0),
(484, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn86.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn86.jpg', 1454412236, 1, 'project_comment', 0),
(485, 1, 0, 'O3_UmxyM-Uw.jpg', 'image/jpeg', 64, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/O3_UmxyM-Uw.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/O3_UmxyM-Uw.jpg', 1454412236, 1, 'project_comment', 0),
(486, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn87.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn87.jpg', 1454413190, 167, 'project_comment', 0),
(487, 1, 0, 'Chrysanthemum.jpg', 'image/jpeg', 859, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Chrysanthemum1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Chrysanthemum1.jpg', 1454413997, 168, 'project_comment', 0),
(488, 1, 0, '4cHOMzmqOpE.jpg', 'image/jpeg', 78, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/4cHOMzmqOpE.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/4cHOMzmqOpE.jpg', 1454414065, 169, 'project_comment', 0),
(490, 1, 0, 'Chrysanthemum.jpg', 'image/jpeg', 859, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Chrysanthemum2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Chrysanthemum2.jpg', 1454414065, 169, 'project_comment', 0),
(492, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn88.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn88.jpg', 1454414065, 169, 'project_comment', 0),
(493, 1, 0, 'I7gijddoroA.jpg', 'image/jpeg', 39, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/I7gijddoroA.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/I7gijddoroA.jpg', 1454414065, 169, 'project_comment', 0),
(494, 1, 0, 'Jellyfish.jpg', 'image/jpeg', 758, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Jellyfish.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Jellyfish.jpg', 1454414065, 169, 'project_comment', 0),
(495, 1, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Koala.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Koala.jpg', 1454414065, 169, 'project_comment', 0),
(496, 1, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Lighthouse.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Lighthouse.jpg', 1454414065, 169, 'project_comment', 0),
(497, 1, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg1.jpg', 1454414065, 169, 'project_comment', 0),
(498, 1, 0, 'nLAgkm0moyM.jpg', 'image/jpeg', 64, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/nLAgkm0moyM.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/nLAgkm0moyM.jpg', 1454414065, 169, 'project_comment', 0),
(500, 1, 0, 'Penguins.jpg', 'image/jpeg', 17, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Penguins.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Penguins.jpg', 1454414065, 169, 'project_comment', 0),
(501, 1, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/QMR7RuLMJZ8.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/QMR7RuLMJZ8.jpg', 1454414065, 169, 'project_comment', 0),
(502, 1, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/qt8YdgnOvSI3.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/qt8YdgnOvSI3.jpg', 1454414065, 169, 'project_comment', 0),
(503, 1, 0, 'RglNWD-0mf0.jpg', 'image/jpeg', 49, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/RglNWD-0mf03.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/RglNWD-0mf03.jpg', 1454414065, 169, 'project_comment', 0),
(504, 1, 0, 'Wo_4ptX-ZZE.jpg', 'image/jpeg', 60, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Wo_4ptX-ZZE.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Wo_4ptX-ZZE.jpg', 1454414065, 169, 'project_comment', 0),
(506, 1, 0, 'Desert.jpg', 'image/jpeg', 826, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert2.jpg', 1454414142, 170, 'project_comment', 0),
(507, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn89.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn89.jpg', 1454414142, 170, 'project_comment', 0),
(508, 1, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Lighthouse1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Lighthouse1.jpg', 1454414142, 170, 'project_comment', 0),
(509, 1, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg2.jpg', 1454414142, 170, 'project_comment', 0),
(510, 1, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/QMR7RuLMJZ81.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/QMR7RuLMJZ81.jpg', 1454414142, 170, 'project_comment', 0),
(511, 1, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/qt8YdgnOvSI4.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/qt8YdgnOvSI4.jpg', 1454414142, 170, 'project_comment', 0),
(513, 4, 0, '', '', 0, 'assets/images/client-image.jpg', '', 0, NULL, NULL, 0),
(514, 5, 0, '', '', 0, 'assets/images/client-image.jpg', '', 0, NULL, NULL, 0),
(515, 1, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU4.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU4.jpg', 1454496197, 1, 'project_comment', 0),
(516, 1, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU5.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU5.jpg', 1454496511, 1, 'project_comment', 0),
(517, 2, 0, 'Desert.jpg', 'image/jpeg', 826, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Desert18.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Desert18.jpg', 1454496788, 2, 'project_comment', 0),
(518, 2, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/fBenut51Rn8.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/fBenut51Rn8.jpg', 1454496798, 185, 'project_comment', 0),
(519, 2, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/fBenut51Rn81.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/fBenut51Rn81.jpg', 1454496896, 2, 'project_comment', 0),
(520, 2, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/fBenut51Rn82.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/fBenut51Rn82.jpg', 1454496900, 2, 'project_comment', 0),
(521, 2, 0, 'Chrysanthemum.jpg', 'image/jpeg', 859, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum6.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum6.jpg', 1454496941, 2, 'project_comment', 0),
(522, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn810.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn810.jpg', 1454499674, 1, 'project_comment', 0),
(524, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn811.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn811.jpg', 1454500138, 1, 'project_comment', 0),
(532, 1, 0, '4cHOMzmqOpE.jpg', 'image/jpeg', 78, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/4cHOMzmqOpE1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/4cHOMzmqOpE1.jpg', 1454501124, 0, 'project_comment', 0),
(533, 1, 0, '12439253_1177561945605378_8423594419344677719_n.png', 'image/png', 425, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/12439253_1177561945605378_8423594419344677719_n.png', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/12439253_1177561945605378_8423594419344677719_n.png', 1454501124, 0, 'project_comment', 0),
(534, 1, 0, 'Chrysanthemum.jpg', 'image/jpeg', 859, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Chrysanthemum3.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Chrysanthemum3.jpg', 1454501124, 0, 'project_comment', 0),
(535, 1, 0, 'Desert.jpg', 'image/jpeg', 826, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert3.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert3.jpg', 1454501124, 0, 'project_comment', 0),
(536, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn812.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn812.jpg', 1454501124, 0, 'project_comment', 0),
(538, 1, 0, 'Jellyfish.jpg', 'image/jpeg', 758, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Jellyfish1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Jellyfish1.jpg', 1454501124, 0, 'project_comment', 0),
(539, 1, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Koala1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Koala1.jpg', 1454501124, 0, 'project_comment', 0),
(540, 1, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Lighthouse2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Lighthouse2.jpg', 1454501124, 0, 'project_comment', 0),
(541, 1, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg3.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg3.jpg', 1454501124, 0, 'project_comment', 0),
(543, 1, 0, 'O3_UmxyM-Uw.jpg', 'image/jpeg', 64, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/O3_UmxyM-Uw1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/O3_UmxyM-Uw1.jpg', 1454501124, 0, 'project_comment', 0),
(544, 1, 0, 'Penguins.jpg', 'image/jpeg', 17, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Penguins1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Penguins1.jpg', 1454501124, 0, 'project_comment', 0),
(545, 1, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/QMR7RuLMJZ82.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/QMR7RuLMJZ82.jpg', 1454501124, 0, 'project_comment', 0),
(546, 1, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/qt8YdgnOvSI5.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/qt8YdgnOvSI5.jpg', 1454501124, 0, 'project_comment', 0),
(547, 1, 0, 'RglNWD-0mf0.jpg', 'image/jpeg', 49, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/RglNWD-0mf04.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/RglNWD-0mf04.jpg', 1454501124, 0, 'project_comment', 0),
(548, 1, 0, 'Wo_4ptX-ZZE.jpg', 'image/jpeg', 60, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Wo_4ptX-ZZE1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Wo_4ptX-ZZE1.jpg', 1454501124, 0, 'project_comment', 0),
(549, 1, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU6.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU6.jpg', 1454501124, 0, 'project_comment', 0),
(550, 1, 0, 'Desert.jpg', 'image/jpeg', 826, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert4.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert4.jpg', 1454501143, 0, 'project_comment', 0),
(551, 1, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/QMR7RuLMJZ83.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/QMR7RuLMJZ83.jpg', 1454501157, 0, 'project_comment', 0),
(552, 1, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Koala2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Koala2.jpg', 1454501175, 0, 'project_comment', 0),
(553, 1, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Lighthouse3.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Lighthouse3.jpg', 1454501175, 0, 'project_comment', 0),
(554, 1, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg4.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg4.jpg', 1454501175, 0, 'project_comment', 0),
(555, 1, 0, 'Penguins.jpg', 'image/jpeg', 17, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Penguins2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Penguins2.jpg', 1454501175, 0, 'project_comment', 0),
(556, 1, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/QMR7RuLMJZ84.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/QMR7RuLMJZ84.jpg', 1454501175, 0, 'project_comment', 0),
(557, 1, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/qt8YdgnOvSI6.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/qt8YdgnOvSI6.jpg', 1454501175, 0, 'project_comment', 0),
(558, 1, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg5.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg5.jpg', 1454501715, 186, 'project_comment', 0),
(559, 1, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Koala3.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Koala3.jpg', 1454501724, 187, 'project_comment', 0),
(560, 1, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Lighthouse4.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Lighthouse4.jpg', 1454501724, 187, 'project_comment', 0),
(561, 1, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg6.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg6.jpg', 1454501724, 187, 'project_comment', 0),
(562, 1, 0, 'Penguins.jpg', 'image/jpeg', 17, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Penguins3.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Penguins3.jpg', 1454501724, 187, 'project_comment', 0),
(563, 1, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/QMR7RuLMJZ85.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/QMR7RuLMJZ85.jpg', 1454501724, 187, 'project_comment', 0),
(564, 1, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/qt8YdgnOvSI7.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/qt8YdgnOvSI7.jpg', 1454501724, 187, 'project_comment', 0),
(565, 1, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU7.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/zjZRL-RsvoU7.jpg', 1454501724, 187, 'project_comment', 0),
(566, 1, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Lighthouse5.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Lighthouse5.jpg', 1454501747, 0, 'project_comment', 0),
(567, 1, 0, 'Desert.jpg', 'image/jpeg', 826, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert5.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert5.jpg', 1454502910, 1, 'project_comment', 0),
(568, 1, 0, 'Desert.jpg', 'image/jpeg', 826, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert6.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert6.jpg', 1454502915, 1, 'project_comment', 0),
(569, 1, 0, 'Desert.jpg', 'image/jpeg', 826, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert7.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert7.jpg', 1454502976, 1, 'project_comment', 0),
(570, 1, 0, 'Desert.jpg', 'image/jpeg', 826, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert8.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert8.jpg', 1454502997, 1, 'project_comment', 0),
(571, 1, 0, 'Desert.jpg', 'image/jpeg', 826, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert9.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Desert9.jpg', 1454503000, 1, 'project_comment', 0),
(572, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn813.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn813.jpg', 1454503576, 1, 'project_comment', 0),
(573, 1, 0, 'Jellyfish.jpg', 'image/jpeg', 758, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/Jellyfish2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/Jellyfish2.jpg', 1454503582, 1, 'project_comment', 0),
(574, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn814.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn814.jpg', 1454503593, 1, 'project_comment', 0),
(575, 1, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn815.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/fBenut51Rn815.jpg', 1454504552, 7, 'project', 0),
(576, 1, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg7.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg7.jpg', 1454504575, 7, 'project', 0),
(577, 1, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg8.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg8.jpg', 1454504585, 7, 'project', 0),
(578, 6, 0, '', '', 0, 'assets/images/developer-image.jpg', '', 0, NULL, NULL, 0),
(579, 2, 0, 'logo.png', 'image/png', 2, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/logo.png', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/logo.png', 1454584781, 29, 'diary', 0),
(580, 7, 0, '', '', 0, 'assets/images/client-image.jpg', '', 0, NULL, NULL, 0),
(581, 2, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/fBenut51Rn83.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/fBenut51Rn83.jpg', 1454596052, 31, 'diary', 0),
(582, 1, 0, 'socket.js', 'text/html', 4, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/socket.js', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/socket.js', 1454684239, 188, 'project_comment', 0),
(583, 2, 0, '4cHOMzmqOpE.jpg', 'image/jpeg', 78, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/4cHOMzmqOpE.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/4cHOMzmqOpE.jpg', 1454934444, 36, 'diary', 0),
(584, 2, 0, '12439253_1177561945605378_8423594419344677719_n.png', 'image/png', 425, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/12439253_1177561945605378_8423594419344677719_n.png', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/12439253_1177561945605378_8423594419344677719_n.png', 1454934444, 36, 'diary', 0),
(585, 2, 0, 'Chrysanthemum.jpg', 'image/jpeg', 859, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum7.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum7.jpg', 1454934444, 36, 'diary', 0),
(586, 2, 0, 'Desert.jpg', 'image/jpeg', 826, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Desert19.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Desert19.jpg', 1454934444, 36, 'diary', 0),
(587, 2, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/fBenut51Rn84.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/fBenut51Rn84.jpg', 1454934444, 36, 'diary', 0),
(588, 2, 0, 'I7gijddoroA.jpg', 'image/jpeg', 39, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/I7gijddoroA.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/I7gijddoroA.jpg', 1454934444, 36, 'diary', 0),
(589, 2, 0, 'Jellyfish.jpg', 'image/jpeg', 758, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Jellyfish40.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Jellyfish40.jpg', 1454934444, 36, 'diary', 0),
(590, 2, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala72.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala72.jpg', 1454934444, 36, 'diary', 0),
(591, 2, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse49.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse49.jpg', 1454934444, 36, 'diary', 0),
(592, 2, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg.jpg', 1454934444, 36, 'diary', 0),
(593, 2, 0, 'nLAgkm0moyM.jpg', 'image/jpeg', 64, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/nLAgkm0moyM.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/nLAgkm0moyM.jpg', 1454934444, 36, 'diary', 0),
(594, 2, 0, 'O3_UmxyM-Uw.jpg', 'image/jpeg', 64, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw.jpg', 1454934444, 36, 'diary', 0),
(595, 2, 0, 'Penguins.jpg', 'image/jpeg', 17, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Penguins5.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Penguins5.jpg', 1454934444, 36, 'diary', 0),
(596, 2, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ8.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ8.jpg', 1454934444, 36, 'diary', 0),
(597, 2, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI.jpg', 1454934444, 36, 'diary', 0),
(598, 2, 0, 'RglNWD-0mf0.jpg', 'image/jpeg', 49, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/RglNWD-0mf0.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/RglNWD-0mf0.jpg', 1454934444, 36, 'diary', 0),
(599, 2, 0, 'Wo_4ptX-ZZE.jpg', 'image/jpeg', 60, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Wo_4ptX-ZZE.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Wo_4ptX-ZZE.jpg', 1454934444, 36, 'diary', 0),
(600, 2, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/zjZRL-RsvoU.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/zjZRL-RsvoU.jpg', 1454934444, 36, 'diary', 0),
(601, 2, 0, 'Penguins.jpg', 'image/jpeg', 17, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Penguins6.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Penguins6.jpg', 1454934733, 37, 'diary', 0),
(602, 2, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala73.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala73.jpg', 1454935665, 39, 'diary', 0),
(603, 2, 0, 'Penguins.jpg', 'image/jpeg', 17, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Penguins7.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Penguins7.jpg', 1454935702, 40, 'diary', 0),
(604, 2, 0, 'Chrysanthemum.jpg', 'image/jpeg', 859, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum8.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum8.jpg', 1455088576, 41, 'diary', 0),
(605, 2, 0, 'Desert.jpg', 'image/jpeg', 826, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Desert20.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Desert20.jpg', 1455088576, 41, 'diary', 0),
(606, 2, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/fBenut51Rn85.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/fBenut51Rn85.jpg', 1455088576, 41, 'diary', 0),
(607, 2, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala74.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala74.jpg', 1455088576, 41, 'diary', 0),
(608, 2, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse50.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse50.jpg', 1455088576, 41, 'diary', 0),
(609, 2, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg1.jpg', 1455088576, 41, 'diary', 0),
(610, 2, 0, 'Penguins.jpg', 'image/jpeg', 17, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Penguins8.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Penguins8.jpg', 1455088576, 41, 'diary', 0),
(611, 2, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ81.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ81.jpg', 1455088576, 41, 'diary', 0),
(612, 2, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI1.jpg', 1455088576, 41, 'diary', 0),
(613, 2, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/zjZRL-RsvoU1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/zjZRL-RsvoU1.jpg', 1455088576, 41, 'diary', 0),
(614, 2, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI2.jpg', 1455091590, 42, 'diary', 0),
(615, 2, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg2.jpg', 1455091788, 44, 'diary', 0),
(616, 2, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg3.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg3.jpg', 1455092782, 45, 'diary', 0),
(617, 2, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg4.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg4.jpg', 1455093370, 46, 'diary', 0),
(618, 2, 0, 'I7gijddoroA.jpg', 'image/jpeg', 39, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/I7gijddoroA1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/I7gijddoroA1.jpg', 1455097849, 49, 'diary', 0),
(619, 2, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg5.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg5.jpg', 1455098972, 50, 'diary', 0),
(620, 2, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/zjZRL-RsvoU2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/zjZRL-RsvoU2.jpg', 1455099013, 52, 'diary', 0),
(621, 2, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg6.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg6.jpg', 1455101536, 61, 'diary', 0),
(622, 2, 0, '4cHOMzmqOpE.jpg', 'image/jpeg', 78, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/4cHOMzmqOpE1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/4cHOMzmqOpE1.jpg', 1455108198, 66, 'diary', 0),
(623, 2, 0, '12439253_1177561945605378_8423594419344677719_n.png', 'image/png', 425, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/12439253_1177561945605378_8423594419344677719_n1.png', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/12439253_1177561945605378_8423594419344677719_n1.png', 1455108198, 66, 'diary', 0),
(624, 2, 0, 'Chrysanthemum.jpg', 'image/jpeg', 859, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum9.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum9.jpg', 1455108198, 66, 'diary', 0),
(625, 2, 0, 'Desert.jpg', 'image/jpeg', 826, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Desert21.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Desert21.jpg', 1455108198, 66, 'diary', 0),
(626, 2, 0, 'fBenut51Rn8.jpg', 'image/jpeg', 35, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/fBenut51Rn86.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/fBenut51Rn86.jpg', 1455108198, 66, 'diary', 0),
(627, 2, 0, 'I7gijddoroA.jpg', 'image/jpeg', 39, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/I7gijddoroA2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/I7gijddoroA2.jpg', 1455108198, 66, 'diary', 0),
(628, 2, 0, 'Jellyfish.jpg', 'image/jpeg', 758, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Jellyfish41.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Jellyfish41.jpg', 1455108198, 66, 'diary', 0),
(629, 2, 0, 'Koala.jpg', 'image/jpeg', 763, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala75.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Koala75.jpg', 1455108199, 66, 'diary', 0),
(630, 2, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse51.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse51.jpg', 1455108199, 66, 'diary', 0),
(631, 2, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg7.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg7.jpg', 1455108199, 66, 'diary', 0),
(632, 2, 0, 'nLAgkm0moyM.jpg', 'image/jpeg', 64, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/nLAgkm0moyM1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/nLAgkm0moyM1.jpg', 1455108199, 66, 'diary', 0),
(633, 2, 0, 'O3_UmxyM-Uw.jpg', 'image/jpeg', 64, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw1.jpg', 1455108199, 66, 'diary', 0),
(634, 2, 0, 'Penguins.jpg', 'image/jpeg', 17, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Penguins9.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Penguins9.jpg', 1455108199, 66, 'diary', 0),
(635, 2, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ82.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ82.jpg', 1455108199, 66, 'diary', 0),
(636, 2, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI3.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI3.jpg', 1455108199, 66, 'diary', 0),
(637, 2, 0, 'RglNWD-0mf0.jpg', 'image/jpeg', 49, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/RglNWD-0mf01.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/RglNWD-0mf01.jpg', 1455108199, 66, 'diary', 0),
(638, 2, 0, 'Wo_4ptX-ZZE.jpg', 'image/jpeg', 60, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Wo_4ptX-ZZE1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Wo_4ptX-ZZE1.jpg', 1455108199, 66, 'diary', 0),
(639, 2, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/zjZRL-RsvoU3.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/zjZRL-RsvoU3.jpg', 1455108199, 66, 'diary', 0),
(640, 2, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ83.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ83.jpg', 1455176386, 0, 'diary', 0),
(641, 2, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI5.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI5.jpg', 1455176386, 0, 'diary', 0),
(642, 1, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg9.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c4ca4238a0b923820dcc509a6f75849b/NICJiNWKlIg9.jpg', 1455176781, 13, '', 0),
(643, 2, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ84.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ84.jpg', 1455179271, 69, 'diary', 0),
(644, 2, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI6.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI6.jpg', 1455179271, 69, 'diary', 0),
(645, 2, 0, 'O3_UmxyM-Uw.jpg', 'image/jpeg', 64, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw2.jpg', 1455179425, 70, 'diary', 0),
(646, 2, 0, 'Wo_4ptX-ZZE.jpg', 'image/jpeg', 60, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Wo_4ptX-ZZE2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Wo_4ptX-ZZE2.jpg', 1455179438, 71, 'diary', 0),
(647, 2, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/zjZRL-RsvoU4.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/zjZRL-RsvoU4.jpg', 1455179438, 71, 'diary', 0),
(648, 2, 0, 'Chrysanthemum.jpg', 'image/jpeg', 859, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum10.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum10.jpg', 1455179471, 72, 'diary', 0),
(649, 2, 0, 'O3_UmxyM-Uw.jpg', 'image/jpeg', 64, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw3.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw3.jpg', 1455179471, 72, 'diary', 0),
(650, 2, 0, 'O3_UmxyM-Uw.jpg', 'image/jpeg', 64, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw4.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw4.jpg', 1455179505, 73, 'diary', 0),
(651, 2, 0, 'RglNWD-0mf0.jpg', 'image/jpeg', 49, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/RglNWD-0mf02.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/RglNWD-0mf02.jpg', 1455179505, 73, 'diary', 0),
(652, 2, 0, 'O3_UmxyM-Uw.jpg', 'image/jpeg', 64, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw5.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw5.jpg', 1455179505, 74, 'diary', 0),
(653, 2, 0, 'RglNWD-0mf0.jpg', 'image/jpeg', 49, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/RglNWD-0mf03.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/RglNWD-0mf03.jpg', 1455179505, 74, 'diary', 0),
(654, 2, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg8.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg8.jpg', 1455179874, 75, 'diary', 0),
(655, 2, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg9.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/NICJiNWKlIg9.jpg', 1455180743, 76, 'diary', 0),
(656, 2, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI7.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI7.jpg', 1455180743, 76, 'diary', 0),
(657, 2, 0, 'O3_UmxyM-Uw.jpg', 'image/jpeg', 64, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw6.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw6.jpg', 1455180786, 77, 'diary', 0),
(658, 2, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ85.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ85.jpg', 1455180786, 77, 'diary', 0);
INSERT INTO `files` (`id`, `user_id`, `deleted`, `name`, `type`, `size`, `path`, `full_path`, `date`, `attachment_id`, `class`, `status`) VALUES
(659, 2, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/zjZRL-RsvoU5.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/zjZRL-RsvoU5.jpg', 1455180786, 77, 'diary', 0),
(660, 2, 0, 'barcelonaguide.jpg', 'image/jpeg', 56, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/barcelonaguide.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/barcelonaguide.jpg', 1455181112, 78, 'diary', 0),
(661, 2, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ86.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ86.jpg', 1455186051, 80, 'diary', 0),
(662, 2, 0, 'Chrysanthemum.jpg', 'image/jpeg', 859, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum11.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum11.jpg', 1455186075, 81, 'diary', 0),
(663, 2, 0, 'barcelonaguide.jpg', 'image/jpeg', 56, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/barcelonaguide1.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/barcelonaguide1.jpg', 1455186110, 82, 'diary', 0),
(664, 2, 0, 'Chrysanthemum.jpg', 'image/jpeg', 859, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum12.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Chrysanthemum12.jpg', 1455186110, 82, 'diary', 0),
(665, 2, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ87.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ87.jpg', 1455191107, 83, 'diary', 0),
(666, 2, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse52.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse52.jpg', 1455191115, 84, 'diary', 0),
(667, 2, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ88.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/QMR7RuLMJZ88.jpg', 1455191115, 84, 'diary', 0),
(668, 8, 0, '', '', 0, 'assets/images/developer-image.jpg', '', 0, NULL, NULL, 0),
(669, 8, 0, 'barcelonaguide.jpg', 'image/jpeg', 56, 'upload/users/c9f0f895fb98ab9159f51fd0297e236d/barcelonaguide.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c9f0f895fb98ab9159f51fd0297e236d/barcelonaguide.jpg', 1455260241, 85, 'diary', 0),
(670, 8, 0, 'NICJiNWKlIg.jpg', 'image/jpeg', 63, 'upload/users/c9f0f895fb98ab9159f51fd0297e236d/NICJiNWKlIg.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c9f0f895fb98ab9159f51fd0297e236d/NICJiNWKlIg.jpg', 1455260241, 85, 'diary', 0),
(671, 8, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c9f0f895fb98ab9159f51fd0297e236d/QMR7RuLMJZ8.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c9f0f895fb98ab9159f51fd0297e236d/QMR7RuLMJZ8.jpg', 1455260241, 85, 'diary', 0),
(672, 8, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c9f0f895fb98ab9159f51fd0297e236d/qt8YdgnOvSI.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c9f0f895fb98ab9159f51fd0297e236d/qt8YdgnOvSI.jpg', 1455260241, 85, 'diary', 0),
(673, 8, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c9f0f895fb98ab9159f51fd0297e236d/zjZRL-RsvoU.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c9f0f895fb98ab9159f51fd0297e236d/zjZRL-RsvoU.jpg', 1455260241, 85, 'diary', 0),
(674, 8, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c9f0f895fb98ab9159f51fd0297e236d/Lighthouse.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c9f0f895fb98ab9159f51fd0297e236d/Lighthouse.jpg', 1455261063, 86, 'diary', 0),
(675, 8, 0, 'QMR7RuLMJZ8.jpg', 'image/jpeg', 144, 'upload/users/c9f0f895fb98ab9159f51fd0297e236d/QMR7RuLMJZ81.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c9f0f895fb98ab9159f51fd0297e236d/QMR7RuLMJZ81.jpg', 1455261063, 86, 'diary', 0),
(676, 2, 0, 'qt8YdgnOvSI.jpg', 'image/jpeg', 166, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI8.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/qt8YdgnOvSI8.jpg', 1455261112, 87, 'diary', 0),
(677, 2, 0, 'zjZRL-RsvoU.jpg', 'image/jpeg', 70, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/zjZRL-RsvoU6.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/zjZRL-RsvoU6.jpg', 1455261112, 87, 'diary', 0),
(678, 2, 0, 'barcelonaguide.jpg', 'image/jpeg', 56, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/barcelonaguide2.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/barcelonaguide2.jpg', 1455261489, 88, 'diary', 0),
(679, 2, 0, 'I7gijddoroA.jpg', 'image/jpeg', 39, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/I7gijddoroA3.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/I7gijddoroA3.jpg', 1455262064, 89, 'diary', 0),
(680, 2, 0, 'O3_UmxyM-Uw.jpg', 'image/jpeg', 64, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw7.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/O3_UmxyM-Uw7.jpg', 1455262064, 89, 'diary', 0),
(681, 2, 0, 'Lighthouse.jpg', 'image/jpeg', 548, 'upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse53.jpg', 'C:/xampp/htdocs/developer-escrow/upload/users/c81e728d9d4c2f636f067f89cc14862c/Lighthouse53.jpg', 1455262192, 90, 'diary', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `link` varchar(250) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `links`
--

INSERT INTO `links` (`project_id`, `user_id`, `name`, `link`, `date`, `id`) VALUES
(1, 1, '[po[po[', 'http://op[', '2016-01-25 21:00:00', 1),
(1, 1, '[po[po[[po[po[po[op', 'http://op[', '2016-01-25 21:00:00', 2),
(1, 1, '[po[po[[po[po[po[op[po[po[po[op[po[', 'http://op[', '2016-01-25 21:00:00', 3),
(1, 1, '[po[po[[po[po[po[op[po[po[po[op[po[[op[po[p', 'http://op[', '2016-01-25 21:00:00', 4),
(1, 1, 'kjhnkhkhjkjhk', 'http://', '2016-01-25 21:00:00', 5),
(1, 1, 'uuuuuuuuuuuuu', 'http://', '2016-01-25 21:00:00', 6),
(1, 1, 'ooooooooooooooooo', 'http://', '2016-01-25 21:00:00', 7);

-- --------------------------------------------------------

--
-- Структура таблицы `milestones`
--

CREATE TABLE IF NOT EXISTS `milestones` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `description` text,
  `project_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `milestones`
--

INSERT INTO `milestones` (`id`, `name`, `created_at`, `updated_at`, `price`, `description`, `project_id`, `status`) VALUES
(1, 'a', '2016-01-26 07:19:14', NULL, '3', '', 5, 0),
(2, 'b', '2016-01-26 07:19:14', NULL, '5', '', 5, 0),
(3, 'c', '2016-01-26 11:45:06', NULL, '1', 'l nfwenjowqdowinm  we fwefew ', 6, 0),
(4, 'd aewfewfrew ', '2016-01-26 12:39:59', NULL, '1', 'iu', 5, 0),
(5, 'g', '2016-01-29 12:29:37', NULL, '2', '.'';.''mreoem e tga ewt', 6, 0),
(6, '111', '2016-02-08 12:47:33', NULL, '60', 'dfsg rdsgv eryg verg', 11, 0),
(7, '222', '2016-02-08 12:47:33', NULL, '90', 'dfg', 11, 0),
(8, 'mil 3', '2016-02-10 07:14:17', NULL, '77', 'sadca d qwd qwdqwdq', 11, 0),
(9, 'gtgtg', '2016-02-10 07:14:26', NULL, '6', 'ftxh rthtr', 11, 0),
(10, '6', '2016-02-10 07:14:30', NULL, '6', 'h6y', 11, 0),
(11, 'fgnn', '2016-02-10 07:14:37', NULL, '77', 'tyhtyhty', 11, 0),
(12, 'tttt', '2016-02-10 07:14:49', NULL, '66', 'thrthrt', 11, 0),
(13, 'yhr', '2016-02-10 07:14:57', NULL, '654', 'rthgrthtr', 11, 0),
(14, 'time project', '2016-02-10 07:15:04', NULL, '66', 'tyhrthtrh', 11, 0),
(15, 'yh', '2016-02-10 07:15:10', NULL, '64', '6yh', 11, 0),
(16, 'vd', '2016-02-11 07:46:45', NULL, '6', 'sdvsv dsd', 13, 0),
(17, 'vcdvfds', '2016-02-11 07:46:45', NULL, '55', 'cv df', 13, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL,
  `message` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `notifications`
--

INSERT INTO `notifications` (`id`, `message`) VALUES
(1, '<''client''> sent you a disput for <''project''> project.\r\n'),
(2, '<''client''> added a comment in <''project''> project.\r\n'),
(3, '<''client''> added a comment for disput of <''project''> project.\r\n'),
(4, '<''client''> asked you to complate description for <''project''> project.\r\n'),
(5, '<''client''> complated description for <''project''> project.\r\n'),
(6, '<''client''> has invited you to the job <''project''>.');

-- --------------------------------------------------------

--
-- Структура таблицы `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `pay` float NOT NULL,
  `payer_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `paid_id` int(11) NOT NULL,
  `paid_type` enum('bank','paypal','card') NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `payments`
--

INSERT INTO `payments` (`id`, `project_id`, `milestone_id`, `pay`, `payer_id`, `receiver_id`, `paid_id`, `paid_type`, `date`) VALUES
(1, 10, 0, 1200, 2, 1, 2147483647, 'bank', '2016-02-12 14:19:44');

-- --------------------------------------------------------

--
-- Структура таблицы `paypals`
--

CREATE TABLE IF NOT EXISTS `paypals` (
  `id` int(6) NOT NULL,
  `user_id` int(255) NOT NULL,
  `txnid` varchar(20) NOT NULL,
  `paypal_account` varchar(100) NOT NULL,
  `payment_status` varchar(25) NOT NULL,
  `itemid` varchar(25) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `paypals`
--

INSERT INTO `paypals` (`id`, `user_id`, `txnid`, `paypal_account`, `payment_status`, `itemid`, `status`) VALUES
(2, 1, '', 'cs@mail.rt', '', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '0',
  `description` text,
  `fixed_price` int(11) DEFAULT NULL,
  `hourly_rate` int(11) NOT NULL,
  `hours_per_week` int(11) NOT NULL,
  `currency` int(1) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `fund` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`id`, `user_id`, `name`, `type`, `description`, `fixed_price`, `hourly_rate`, `hours_per_week`, `currency`, `status`, `created_at`, `updated_at`, `fund`) VALUES
(1, 1, 'project 1', 0, 'vtrvrtvr yrbrybybbryb', NULL, 2, 4, 1, 0, '2016-01-20 11:38:31', NULL, ''),
(2, 1, 'PROJECT 2 ', 1, 'LALAL', 8, 0, 0, 2, 0, '2016-01-20 12:11:31', NULL, '8'),
(3, 1, '333', 0, '', NULL, 50, 6, 2, 2, '2016-01-20 15:05:10', NULL, '300'),
(4, 1, 'lalala', 0, 'pr_create_date pr_create_date pr_create_date  pr_create_date pr_create_datepr_create_datepr_create_datepr_create_datepr_create_datepr_create_date pr_create_datepr_create_date Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.', 0, 0, 0, 1, 0, '2016-01-25 06:26:09', NULL, ''),
(5, 1, 'aaa', 1, '', 40, 0, 0, 1, 0, '2016-01-26 07:19:14', NULL, '40'),
(6, 1, 'lalala', 1, '', 13, 0, 0, 1, 0, '2016-01-26 11:45:06', NULL, '13'),
(7, 1, 'ewqewqeqw', 0, 'qweeeeeeeeeeeeeee', NULL, 22, 22, 1, 2, '2016-01-26 13:48:21', NULL, '484'),
(8, 7, '444', 1, 'lka ihLANS ', 4, 0, 0, 2, 0, '2016-02-04 14:15:51', NULL, '4'),
(9, 7, 'AAA', 0, '', NULL, 11, 55, 2, 0, '2016-02-04 14:16:17', NULL, '605'),
(10, 2, 'tfgxh', 0, '', 0, 0, 0, 1, 0, '2016-02-08 11:33:46', NULL, ''),
(11, 2, 'gedsg', 1, 'erdsagdrg', 9520, 0, 0, 1, 2, '2016-02-08 12:47:33', NULL, '9520'),
(12, 1, 'hourly work', 0, '', NULL, 122, 12, 2, 0, '2016-02-11 07:45:44', NULL, '1464'),
(13, 1, 'fixed', 1, 'dsfvsd k wadfl e kj dfkwefehff e fefef', 122, 0, 0, 1, 0, '2016-02-11 07:46:45', NULL, '122'),
(14, 1, 'aaa', 0, '', NULL, 3, 45, 2, 0, '2016-02-12 06:56:40', NULL, '135'),
(15, 2, 'dev project', 0, '', NULL, 55, 5, 1, 0, '2016-02-12 07:17:52', NULL, '275');

-- --------------------------------------------------------

--
-- Структура таблицы `project_files`
--

CREATE TABLE IF NOT EXISTS `project_files` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `file_description` varchar(250) NOT NULL,
  `file_uniqid` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `project_files`
--

INSERT INTO `project_files` (`id`, `project_id`, `file_id`, `file_description`, `file_uniqid`) VALUES
(1, 13, 642, '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `project_milestones`
--

CREATE TABLE IF NOT EXISTS `project_milestones` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `milestone_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблицы `tokens`
--

CREATE TABLE IF NOT EXISTS `tokens` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `api_key` varchar(255) NOT NULL,
  `secret_key` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tokens`
--

INSERT INTO `tokens` (`id`, `user_id`, `api_key`, `secret_key`) VALUES
(1, 1, 'PGXYOvtmJZ', 'RVlvBJPTEoVttZQ'),
(2, 2, 'komuSZmhgk', 'Lrt1gjX95JGjX28'),
(3, 3, 'WD9jxk4AJx', '9ZtP4Pfk4Z2BRNh'),
(4, 4, 'bTjOhs7wmN', 'JTV1lKUIHrOZ0Nz'),
(5, 5, 'KxwSmERRC5', 'ThurmssNsguKU13'),
(6, 6, 'GKY3qmL1m3', 'RY4xBpF2PhKgT3n'),
(7, 7, 'VcDmPrqNbI', 'qiP0cA8KRTVVZWy'),
(8, 8, 'PvaObpgq1T', '3sGB617qjxVLw0J');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `profile_image_id` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `deleted` int(1) NOT NULL,
  `user_type` int(1) NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `notifications` varchar(255) DEFAULT NULL,
  `available` float DEFAULT NULL,
  `earned_pay` varchar(255) DEFAULT NULL,
  `user_key` varchar(255) DEFAULT NULL,
  `stripe_username` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `profile_image_id`, `status`, `deleted`, `user_type`, `username`, `email`, `password`, `created_at`, `updated_at`, `phone_number`, `notifications`, `available`, `earned_pay`, `user_key`, `stripe_username`) VALUES
(1, 1, 0, 0, 1, 'Client', 'client@gmail.com', '202cb962ac59075b964b07152d234b70', '2016-01-20 11:36:33', NULL, NULL, 'immediate', 1653, NULL, NULL, ''),
(2, 2, 0, 0, 0, 'developer', 'developer@gmail.com', '202cb962ac59075b964b07152d234b70', '2016-01-20 11:37:09', NULL, NULL, 'immediate', 844, NULL, NULL, ''),
(3, 3, NULL, 0, 0, 'aaa', 'a@a.a', '202cb962ac59075b964b07152d234b70', '2016-01-20 13:01:18', NULL, NULL, NULL, NULL, NULL, NULL, ''),
(4, 513, NULL, 0, 1, 'Mari', 'mari.khangeldian@gmail.com', '5f1ef72bef10499ab3e64b4ce20b08a1', '2016-02-02 12:00:01', NULL, NULL, NULL, NULL, NULL, NULL, ''),
(5, 514, NULL, 0, 1, 'Ani', 'ani.ohanyan1111@gmail.com', 'd06a9778eb1b2cbddc149128513a96ea', '2016-02-02 12:04:36', NULL, NULL, NULL, NULL, NULL, NULL, ''),
(6, 578, NULL, 0, 0, 'ii', 'ohani@mail.ru', '725e92300ee814c297febad406d751fa', '2016-02-03 14:49:09', NULL, NULL, 'immediate', NULL, NULL, NULL, ''),
(7, 580, NULL, 0, 1, 'name', '4@4.com', '202cb962ac59075b964b07152d234b70', '2016-02-04 14:15:14', NULL, NULL, 'immediate', NULL, NULL, NULL, ''),
(8, 668, NULL, 0, 0, 'dev1', 'dev@gmail.com', '202cb962ac59075b964b07152d234b70', '2016-02-12 06:55:39', NULL, NULL, 'immediate', NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Структура таблицы `user_notifications`
--

CREATE TABLE IF NOT EXISTS `user_notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `is_read` tinyint(1) NOT NULL,
  `type` text NOT NULL,
  `type_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=266 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_notifications`
--

INSERT INTO `user_notifications` (`id`, `user_id`, `sender_id`, `notification_id`, `project_id`, `is_read`, `type`, `type_id`, `date`) VALUES
(213, 1, 2, 2, 7, 0, 'project', 7, '2016-01-28 16:11:30'),
(214, 1, 2, 2, 1, 1, 'project', 1, '2016-01-28 16:22:56'),
(215, 1, 2, 2, 7, 1, 'project', 7, '2016-01-29 11:43:28'),
(216, 1, 2, 2, 7, 1, 'project', 7, '2016-01-29 11:47:14'),
(217, 28, 2, 3, 4, 0, 'dispute', 28, '2016-01-29 11:50:51'),
(218, 28, 2, 3, 4, 0, 'dispute', 28, '2016-01-29 11:52:23'),
(219, 28, 2, 3, 4, 0, 'dispute', 28, '2016-01-29 11:55:22'),
(220, 28, 2, 3, 4, 0, 'dispute', 28, '2016-01-29 11:55:25'),
(221, 28, 2, 3, 4, 0, 'dispute', 28, '2016-01-29 11:56:57'),
(222, 2, 1, 2, 6, 0, 'project', 6, '2016-01-29 12:30:26'),
(223, 1, 2, 2, 7, 1, 'project', 7, '2016-01-29 13:50:58'),
(224, 1, 2, 2, 7, 1, 'project', 7, '2016-01-29 14:04:44'),
(225, 2, 1, 2, 6, 0, 'project', 6, '2016-01-29 14:35:12'),
(226, 2, 1, 2, 6, 0, 'project', 6, '2016-01-29 14:43:09'),
(227, 2, 1, 2, 6, 0, 'project', 6, '2016-01-29 14:53:33'),
(228, 2, 1, 2, 6, 0, 'project', 6, '2016-01-29 15:24:16'),
(229, 2, 1, 2, 6, 0, 'project', 6, '2016-01-29 15:24:30'),
(230, 2, 1, 2, 6, 0, 'project', 6, '2016-01-29 15:25:14'),
(231, 2, 1, 2, 6, 0, 'project', 6, '2016-01-29 15:58:01'),
(232, 2, 1, 2, 7, 0, 'project', 7, '2016-02-02 11:39:16'),
(233, 2, 1, 2, 7, 0, 'project', 7, '2016-02-02 11:39:23'),
(234, 2, 1, 2, 7, 0, 'project', 7, '2016-02-02 11:39:58'),
(235, 2, 1, 2, 7, 0, 'project', 7, '2016-02-02 11:53:25'),
(236, 2, 1, 2, 7, 0, 'project', 7, '2016-02-02 11:54:46'),
(237, 2, 1, 2, 7, 0, 'project', 7, '2016-02-02 11:55:48'),
(238, 2, 1, 2, 7, 0, 'project', 7, '2016-02-02 12:33:22'),
(239, 2, 1, 2, 7, 0, 'project', 7, '2016-02-02 13:02:16'),
(240, 2, 1, 2, 7, 0, 'project', 7, '2016-02-02 13:08:31'),
(241, 2, 1, 2, 7, 0, 'project', 7, '2016-02-02 13:09:43'),
(242, 2, 1, 2, 7, 0, 'project', 7, '2016-02-02 13:14:06'),
(243, 2, 1, 2, 7, 0, 'project', 7, '2016-02-03 07:06:05'),
(244, 2, 1, 2, 7, 0, 'project', 7, '2016-02-03 07:35:00'),
(245, 2, 1, 2, 7, 0, 'project', 7, '2016-02-03 07:35:10'),
(246, 2, 1, 2, 7, 0, 'project', 7, '2016-02-03 07:37:10'),
(247, 2, 1, 2, 7, 0, 'project', 7, '2016-02-03 09:02:28'),
(248, 2, 1, 2, 7, 0, 'project', 7, '2016-02-03 09:02:54'),
(249, 2, 1, 2, 7, 0, 'project', 7, '2016-02-03 10:34:39'),
(250, 1, 2, 2, 7, 1, 'project', 7, '2016-02-03 10:53:03'),
(251, 1, 2, 2, 7, 1, 'project', 7, '2016-02-03 10:53:22'),
(252, 2, 1, 2, 7, 0, 'project', 7, '2016-02-03 12:15:16'),
(253, 2, 1, 2, 7, 0, 'project', 7, '2016-02-03 12:15:28'),
(254, 2, 7, 6, 8, 1, 'project', 8, '2016-02-04 14:15:51'),
(255, 2, 7, 6, 9, 1, 'project', 9, '2016-02-04 14:16:17'),
(256, 2, 1, 4, 6, 0, 'project', 6, '2016-02-05 14:12:29'),
(257, 2, 1, 2, 7, 0, 'project', 7, '2016-02-05 14:57:21'),
(258, 1, 2, 6, 10, 0, 'project', 10, '2016-02-08 11:33:46'),
(259, 1, 2, 6, 11, 0, 'project', 11, '2016-02-08 12:47:33'),
(260, 2, 1, 6, 12, 0, 'project', 12, '2016-02-11 07:45:44'),
(261, 2, 1, 6, 13, 0, 'project', 13, '2016-02-11 07:46:45'),
(262, 2, 1, 4, 12, 0, 'project', 12, '2016-02-11 15:05:02'),
(263, 8, 1, 6, 14, 0, 'project', 14, '2016-02-12 06:56:40'),
(264, 1, 2, 6, 15, 0, 'project', 15, '2016-02-12 07:17:52'),
(265, 1, 1, 4, 15, 0, 'project', 15, '2016-02-15 10:20:59');

-- --------------------------------------------------------

--
-- Структура таблицы `user_projects`
--

CREATE TABLE IF NOT EXISTS `user_projects` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `user_projects`
--

INSERT INTO `user_projects` (`id`, `project_id`, `user_id`) VALUES
(1, 1, 2),
(2, 2, 2),
(3, 3, 2),
(4, 4, 2),
(5, 5, 2),
(6, 6, 2),
(7, 7, 2),
(8, 8, 2),
(9, 9, 2),
(10, 10, 1),
(11, 11, 1),
(12, 12, 2),
(13, 13, 2),
(14, 14, 8),
(15, 15, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Индексы таблицы `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `diaries`
--
ALTER TABLE `diaries`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `disputes`
--
ALTER TABLE `disputes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `dispute_comments`
--
ALTER TABLE `dispute_comments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `milestones`
--
ALTER TABLE `milestones`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `paypals`
--
ALTER TABLE `paypals`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `project_files`
--
ALTER TABLE `project_files`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `project_milestones`
--
ALTER TABLE `project_milestones`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_projects`
--
ALTER TABLE `user_projects`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `cards`
--
ALTER TABLE `cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=189;
--
-- AUTO_INCREMENT для таблицы `diaries`
--
ALTER TABLE `diaries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT для таблицы `disputes`
--
ALTER TABLE `disputes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT для таблицы `dispute_comments`
--
ALTER TABLE `dispute_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT для таблицы `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=682;
--
-- AUTO_INCREMENT для таблицы `links`
--
ALTER TABLE `links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `milestones`
--
ALTER TABLE `milestones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблицы `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `paypals`
--
ALTER TABLE `paypals`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `project_files`
--
ALTER TABLE `project_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `project_milestones`
--
ALTER TABLE `project_milestones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=266;
--
-- AUTO_INCREMENT для таблицы `user_projects`
--
ALTER TABLE `user_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
