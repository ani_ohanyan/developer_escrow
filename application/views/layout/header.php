<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <title><?=$title?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Developer Escrow"/>
    <meta name="description" content="Developer Escrow"/>
    <meta name="robots" content="index, follow" />
    <meta name="author" content="">
    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Favicons
    ================================================== -->
    <link rel="icon" href="<?= base_url('assets/images/favicon.ico') ?>" type="image/x-icon"/>
    <!-- CSS
    ================================================== -->
    <!-- Font awesome-->
    <link rel="stylesheet" href="<?= base_url('assets/css/fonts.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
    <!--jquery-ui-->
    <link rel="stylesheet" media="screen" type="text/css" href="<?= base_url() ?>assets/css/jquery-ui.css"/>
    <!-- Template styles-->
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
    <!-- Responsive styles-->
    <link rel="stylesheet" href="<?= base_url('assets/css/responsive.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/perfect-scrollbar.min.css') ?>"

    <!-- Styles from webstyle -->
    <link href="<?= base_url('assets/webstyle/style.css') ?>" rel='stylesheet' type='text/css'>
    <link href="<?= base_url('assets/webstyle/reset.css') ?>" rel='stylesheet' type='text/css'>
    <link href="<?= base_url('assets/webstyle/fonts.css') ?>" rel='stylesheet' type='text/css'>
    <link href="<?= base_url('assets/webstyle/ionicons.min.css') ?>" rel='stylesheet' type='text/css'>
    <link href="<?= base_url('assets/webstyle/select2.min.css') ?>" rel="stylesheet" />
    <script type="text/javascript" src="<?=base_url('assets/js/jquery.js')?>"></script>


    <script src="<?= base_url();?>assets/js/select2.min.js"></script>
    <script src="<?= base_url();?>assets/js/perfect-scrollbar.jquery.min.js"></script>

<!--    <link rel="stylesheet" href="--><?//= base_url('assets/css/bootstrap.min.css') ?><!--">-->
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap-select.css') ?>">

    <link href="<?= base_url();?>assets/webstyle/select2.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url();?>assets/css/tracker.css" rel="stylesheet" type="text/css"/>
    <!--<link href="<?/*= base_url() */?>assets/js/jquery.fileapi/jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>-->

    <script src="<?= base_url() ?>assets/js/bootstrap-select.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"> </script>
</head>
<body class="<?= ($this->uri->segment(1) !== null && $this->uri->segment(1) !== 'home') ? 'admin-template' : '' ?> <?= ($this->uri->segment(1) == null) ? 'landing-home' : '' ?> ">
<?php if($this->uri->segment(1) == NULL || $this->uri->segment(1) == 'home' || $this->uri->segment(1) == 'contacts' || $this->uri->segment(1) == 'privacy_policy' || $this->uri->segment(1)=='terms' || $this->uri->segment(1) == 'pricing' ) : ?>
<header class="group">
    <div class="top-menu group">
        <div class="wrapper group">
            <ul>
                <li><a href="javascript:void(0);" id="features">Features</a></li>
                <li><a href="javascript:void(0);" id="how-works">How it works</a></li>
                <li><a href="<?=base_url()?>"><img src="<?=base_url('/assets/images/developer-escrow-logo.png')?>" alt="Developer Escrow"></a></li>
                <li><a href="#">Support</a></li>
                <li><a href="<?=base_url().'contacts'?>">Contact</a></li>
            </ul>
        </div>
    </div>
    <div class="logo group">
        <a href="<?=base_url()?>"><img src="<?=base_url('assets/images/developer-escrow-logo.png')?>" alt="Developer Escrow"><span>Developer Escrow</span></a>
    </div>
    <div class="main-menu group">
        <div class="wrapper group">
            <nav class="nav">
                <ul class="nav-list">
                    <li class="nav-item mobile-only"><a href="#">Features</a></li>
                    <li class="nav-item mobile-only"><a href="#">How it works</a></li>
                    <li class="nav-item mobile-only"><a href="#">Support</a></li>
                    <li class="nav-item mobile-only"><a href="<?=base_url().'contacts'?>">Contact</a></li>
                    <?php if ($logged) : ?>
                        <li class="nav-item last"><a href="<?=base_url('profile')?>">My Profile</a></li>
                        <li class="nav-item last"><a href="<?=base_url('logout')?>">Log Out</a></li>
                    <?php else : ?>
                        <li class="nav-item"><a href="<?=base_url('login')?>" class="login_btn noborder">Log In</a></li>
                        <li class="nav-item last"><a href="<?=base_url('sign-up')?>" class="home_page_signup_btn">Sign Up</a></li>
                    <?php endif; ?>
                </ul>
            </nav>
        </div>
    </div>
    <?php if($this->uri->segment(1) == 'privacy_policy' || $this->uri->segment(1)=='terms' ||$this->uri->segment(1) == 'contacts'|| $this->uri->segment(1) == 'pricing' ) : ?>
</header>
    <?php else : ?>
    <div class="text group">
        <h1>Developer Escrow</h1>

        <p>&nbsp;</p>

        <p>We ensure developers and designers get paid, and clients get the assets they paid for.  No worries.</p>

        <p>&nbsp;</p>

        <p><a href="#" class="btn_learn_more button green">Learn More</a></p>
    </div>

    <a href="#" class="next-content"><i class="fa fa-angle-down"></i></a>
</header>
<?php endif; ?>
<?php else : ?>
<header class="group">
    <div class="wrapper group">
        <div class="logo-div group"><a href="<?=base_url()?>" ><img src="<?= base_url('assets/images/developer-escrow-logo.png') ?>" alt="Developer Escrow" ></a></div>
        <div class="menu-div group">
            <ul>
                <?php if($user_type && $user_type == 1) : ?>
                    <li <?= ($this->uri->segment(1) == 'projects') ? 'class="active"' : '' ?>><a href="<?= base_url('/projects') ?>">Providers</a></li>
                <?php else : ?>
                    <li <?= ($this->uri->segment(1) == 'projects') ? 'class="active"' : '' ?>><a href="<?= base_url('/projects') ?>">Projects</a></li>
                <?php endif; ?>
                <li <?= ($this->uri->segment(1) == 'payments') ? 'class="active"' : '' ?>><a href="<?= base_url('/payments') ?>">Payments</a></li>
                <li <?= ($this->uri->segment(1) == 'disputes') ? 'class="active"' : '' ?>><a href="<?=base_url('/disputes')?>">Disputes</a></li>
                <li <?= ($this->uri->segment(1) == 'diaries') ? 'class="active"' : '' ?>><a href="<?=base_url('/diaries')?>">Diaries</a></li>
            </ul>
        </div>
        <div class="right-div group">
            <ul>
                <li class="round user"><a href="javascript:void(0);"><img class="user_profile_img" src="<?=base_url().$this->session->userdata('avatarPath') ;?>" alt="#"></a>

                </li>
                <li class="round gray profile"><a href="#"><i class="ion-gear-a"></i></a>
                    <ul class="dropdown">
                        <li class="dropdown_view_profile"><a href="<?=base_url("profile")?>">View Profile</a></li>
                        <li class="dropdown_logout"><a href="<?=base_url('logout')?>">Log Out</a></li>
                    </ul>
                </li>
                <li class="round border">
                    <a href="javascript:void(0);">
                        <i class="ion-ios-bell"></i>
                        <?php if(isset($not_count) && !empty($not_count) && $not_count['not_count'] !=0):?>
                            <small id="noteround"><?=$not_count['not_count']?></small>
                        <?php endif ;?>
                    </a>
                    <ul class="dropdown notification_dropdown">
                        <?php  if(isset($notifications) && !empty($notifications)):?>
                            <?php foreach($notifications as $note): //echo '<pre>';print_r($note);die;?>
                                <?php if($note['is_read']):?>
                                    <li class="notif_is_read notif_item">
                                        <h4 class="job-closed notification_back" >
                                            <?php if($note['not_id'] == 1 || $note['not_id'] == 2 ){ ?>
                                                    <i class="fa fa-exclamation"></i>
                                            <?php } elseif($note['not_id'] == 6 ){ ?>
                                                    <i class="fa fa-file-o"></i>
                                            <?php } elseif($note['not_id'] == 4 ){ ?>
                                                    <i class="ion-checkmark-round"></i>
                                            <?php  } else { ?>
                                                    <i class="ion-checkmark-round"></i>
                                            <?php } ?>
                                        </h4>
                                        <?php echo $note['message']; ?>
                                        <a href="#"></a>
                                    </li>
                                <?php else:?>
                                    <li class="notif_item">
                                        <?php if($note['not_id'] == 1 || $note['not_id'] == 2 ){ ?>
                                            <h4 class="job-closed red_not">
                                                <i class="fa fa-exclamation"></i>
                                            </h4>
                                        <?php } elseif($note['not_id'] == 6 ){ ?>
                                            <h4 class="job-closed blue_not">
                                                <i class="fa fa-file-o"></i>
                                            </h4>
                                        <?php } elseif($note['not_id'] == 4 ){ ?>
                                            <h4 class="job-closed gray_not">
                                                <i class="ion-checkmark-round"></i>
                                            </h4>
                                        <?php  } else { ?>
                                            <h4 class="job-closed">
                                                <i class="ion-checkmark-round"></i>
                                            </h4>
                                        <?php } ?>
                                        <?php echo $note['message']; ?>
                                        <a href="#"></a>
                                    </li>
                                <?php endif ;?>
                            <?php endforeach ?>
                        <?php endif ?>
                        <li><a href="<?=base_url()?>notifications" class="view">View all notifications</a></li>
                    </ul>
                </li>
                <?php //if($user_type && $user_type == 1) : ?>
                <li class="round"><a href="<?= base_url('/new-project')?>" class="button bnt_new_project">New Project</a></li>
                <?php //endif; ?>
            </ul>
        </div>
    </div>
    <div class="mobile-menu group">
        <nav class="nav">
            <ul class="nav-list">

                <?php if($user_type && $user_type == 1) : ?>
                    <li class="nav-item"><a href="<?=base_url();?>projects">Providers</a></li>
                    <li class="nav-item"><a href="<?=base_url();?>new-project">New Project</a></li>
                <?php else : ?>
                    <li class="nav-item"><a href="<?=base_url();?>projects">Projects</a></li>
                <?php endif; ?>
                <li class="nav-item"><a href="<?=base_url();?>projects;?>payments">Payments</a></li>
                <li class="nav-item"><a href="<?=base_url();?>disputes">Disputes</a></li>
                <li class="nav-item"><a href="#">View Profile</a></li>
                <li class="nav-item"><a href="<?=base_url();?>logout">Log Out</a></li>
                <!--<li class="nav-item mobile-only"><a href="#"># <img src="images/dropdown-arrow.png" alt="#"></a>
                      <ul class="nav-submenu">
                       <li class="nav-submenu-item"><a href="#">#</a></li>
                        <li class="nav-submenu-item"><a href="#">#</a></li>
                       <li class="nav-submenu-item"><a href="#">#</a></li>
                        <li class="nav-submenu-item"><a href="#">#</a></li>
                   </ul>
                 </li>-->
            </ul>
        </nav>
    </div>
</header>
</body>
<?php endif; ?>
<section  class="<?=(empty($this->uri->segment(1))) ? 'no-padding' : ''?> main group">
    <div class="wrapper group <?=(empty($this->uri->segment(1))) ? 'homepage-wrapper' : ''?>">