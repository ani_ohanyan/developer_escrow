       <?php
            if($page != 'project_view/project' && $page!='home/home' && isset($this->session->all_userdata()['is_client_login']) && !empty($this->session->all_userdata()['is_client_login'])) { $this->load->view('layout/sidebar'); }?>
    </div>
</section>

<div class="overlay"></div>

<!-- Javascript Files
================================================== -->
<!-- Bootstrap jQuery -->
<script type="text/javascript" src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
<!-- Template custom -->
<!--<script type="text/javascript" src="--><?//=base_url('assets/js/custom.js')?><!--"></script>-->
<script type="text/javascript" src="<?=base_url('assets/js/script.js')?>"></script>

<script type="text/javascript" src="<?=base_url('assets/js/html5shiv.js') ?>"></script>
<script type="text/javascript" src="<?=base_url('assets/js/select2.min.js') ?>"></script>
<!--<script type="text/javascript" src="<?/*=base_url('assets/js/jquery.matchHeight.js') */?>"></script>-->
<script type="text/javascript" src="<?=base_url('assets/js/classie.js') ?>"></script>
<script type="text/javascript" src="<?=base_url('assets/js/flaunt.js') ?>"></script>
<script type="text/javascript" src="<?=base_url('assets/js/custom.js') ?>"></script>
<script>
    $(document).ready(function () {
        /*if ($(this).width() > 768) {
            $(function () {
                $('section.what-you-get figure').matchHeight();
            });
        }
        ;*/
    });
</script>

<footer class="group">
    <div class="wrapper group">
        <div class="table-div">
            <div class="table-div-cell">

                <div class="f-left text-left">
                    <ul>
                        <li><a href="<?=base_url().'terms'?>">Terms of Service</a></li>
                        <li><a href="<?=base_url().'privacy_policy'?>">Privacy Policy</a></li>
                        <li><a href="<?=base_url().'pricing'?>">Pricing</a></li>
                        <li><a href="<?=base_url().'contacts'?>">Contact</a></li>
                    </ul>
                </div>
                <div class="f-right last text-right">
                    <ul>
                        <li>Follow Us</li>
                        <li><a href="https://www.facebook.com/Developer-Escrow-1698780053676610/" target="_blank"><i class="ion-social-facebook"></i></a></li>
                        <li><a href="https://twitter.com/developerescrow" target="_blank"><i class="ion-social-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</footer>
</html>
