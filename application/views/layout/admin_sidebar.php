<?php if ($this->session->userdata('is_admin_login')) : ?>


<div class="admin_navbar">
<nav class="navbar navbar-default">
    <div class="container-fluid" style="padding: 0;">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <?php $page = $this->uri->segment(2);?>
        <div class="collapse navbar-collapse no-padding pull-left" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a class="logo" href="<?= base_url()?>admin/users">
                        <img src="<?= base_url() ?>assets/images/logo.png" alt="Applister" class="logo-img"/>
                    </a>
                </li>
                <li class="<?= ($page == 'users') ? "active" : ""?>"><a href="<?= base_url(); ?>admin/users">Users <span class="sr-only">(current)</span></a>
                </li>
                <li class="<?= ($page == 'projects') ? "active" : ""?>"><a href="<?= base_url(); ?>admin/projects">Projects</a></li>
                <li class="<?= ($page == 'disputes' || $page == 'add_disputes') ? "active" : ""?>"><a href="<?= base_url(); ?>admin/disputes">Disputes</a></li>
                <!--<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">One more separated link</a></li>
                  </ul>
                </li>-->
            </ul>
            <form class="navbar-form navbar-left" role="search">
<!--                <div class="form-group">-->
<!--                    <input type="text" class="form-control" placeholder="Search">-->
<!--                </div>-->
                <!--<button type="submit" class="btn btn-default">Submit</button>-->
            </form>
            <!--<ul class="nav navbar-nav navbar-right">
                <!--<li><a href="#">Link</a></li>-->
               <!-- <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">User name <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Log Out</a></li>

                    </ul>
                </li>
            </ul>-->
        </div>
        <!-- /.navbar-collapse -->
        <div id="admin_header">
            <div class="col-md-12 col-sm-12 col-xs-12 page-wrapper no-float  no-padding">

                <div class="pull-right header-control">
                    <ul class="header-menu">
                        <li class="pull-left">
                            Admin &nbsp;
                            <a href="<?= base_url() ?>admin/settings">
                                <i class="fa fa-cog"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url() ?>admin/logout">
                                <i class="fa fa-sign-out"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
</nav>
</div>
<?php endif; ?>