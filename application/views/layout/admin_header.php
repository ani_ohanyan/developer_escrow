<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Developer Escrow | The place to get apps built</title>
        <link rel="shortcut icon" type="image/ico" href="<?= base_url();?>assets/images/favicon.ico"/>

        <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css"/>
<!--        <link rel="stylesheet" media="screen" type="text/css" href="--><?//= base_url() ?><!--assets/css/datepicker.css"/>-->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/fonts.css"/>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/admin.css"/>
<!--        <link rel="stylesheet" href="--><?//= base_url() ?><!--assets/css/media.css"/>-->
<!--        <link href="--><?//= base_url() ?><!--assets/js/jquery.fileapi/statics/main.css" rel="stylesheet" type="text/css"/>-->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/tracker.css"/>

        <link rel="stylesheet" href="//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css"/>
<!--        <link rel="stylesheet" href="--><?//= base_url() ?><!--assets/css/bootstrap-select.min.css"/>-->
        <script src="<?=base_url()?>assets/js/jquery-1.10.1.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>



    </head>
    <body>
        <div class="body-inner">
            <?php $this->load->view('layout/admin_sidebar'); ?>