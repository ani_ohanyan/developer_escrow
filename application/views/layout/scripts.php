<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    var selDiv = "";
    var  selDivFile = "";
    document.addEventListener("DOMContentLoaded", init, false);

    function init() {
        if($('.filesListUpload1').length){
            document.querySelector('.filesListUpload1').addEventListener('change', handleFileSelect1, false);
//            selDiv = document.querySelector("#selectedFiles1");
        }
        if($('.filesListUpload2').length){
            document.querySelector('.filesListUpload2').addEventListener('change', handleFileSelect2, false);
//            selDivFile = document.querySelector("#selectedFile1");
        }
        if($('.filesListUpload3').length){
            document.querySelector('.filesListUpload3').addEventListener('change', handleFileSelect3, false);
            selDivFile = document.querySelector("#selectedFile3");
        }
    }
    function handleFileSelect1(e) {
        if(!e.target.files) return;
        var files = e.target.files;
        for (var i = 0; i < files.length; i++) {
            var f = files[i];
            $("#selectedFiles1").addClass('comment_upload_files ');
            $("#selectedFiles1").append("<span class='upload_files_name'>"+f.name + "</span>");

        }
    }

    function handleFileSelect2(e) {
        if(!e.target.files) return;
        $("#selectedFile1").html('');
//        selDivFile.innerHTML = "";
        var files = e.target.files;
        for (var i = 0; i < files.length; i++) {
            var f = files[i];
//            selDivFile.innerHTML += "<span>"+f.name + "</span>";
            $("#selectedFile1").addClass('comment_upload_files ');
            $("#selectedFile1").append("<span class='upload_files_name'>"+f.name + "</span>");
        }
    }

    function handleFileSelect3(e) {
        if(!e.target.files) return;
        $("#selectedFile3").html('');
//        selDivFile.innerHTML = "";
        var files = e.target.files;
        for (var i = 0; i < files.length; i++) {
            var f = files[i];
//            selDivFile.innerHTML += "<span>"+f.name + "</span>";
            $("#selectedFile3").addClass('comment_upload_files ');
            $("#selectedFile3").append("<span class='upload_files_name'>"+f.name + "</span>");
        }
    }
    var base_url = "<?=base_url();?>";
</script>

