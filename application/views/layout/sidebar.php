<?php if($this->uri->segment(1) == NULL || $this->uri->segment(1) == 'home' || $this->uri->segment(1) == 'contacts' || $this->uri->segment(1) == 'privacy_policy' || $this->uri->segment(1)=='terms' || $this->uri->segment(1) == 'pricing' ) { ?>
<div>  </div>
<?php } else { ?>
    <script>
        $(document).ready(function() {
            $('select.custom-home').select2({
                theme: "classic",
                minimumResultsForSearch: Infinity,

            });
        });
        $(document).ready(function() {
            $('select.custom-home').select2({
                theme: "classic",
                minimumResultsForSearch: Infinity,

            });

            $('select.custom-paymentmethod').select2({
                theme: "classic",
                minimumResultsForSearch: Infinity,

            });
        });
    </script>
<div class="sidebar group">
    <div class="main-title uppercase group">Statistics</div>
    <div class="stat-box group">
        <div class="half-div">
            <h3>Available</h3>
            <p class="available">$<?php if(isset($user->available)){echo $user->available; } else echo '0'; ?></p>
        </div>
        <div class="half-div text-right last">
            <?php if($user->user_type):?>
                <a data-value = "<?php if(isset($user->available_pays)){echo $user->available_pays; } else echo '0'; ?>"  href="#" class="button gray request" data-toggle="modal" data-target=".withdrawal_amount_modal">Add Funds</a>
            <?php else:?>
                <a data-value = "<?php if(isset($user->available_pays)){echo $user->available_pays; } else echo '0'; ?>"  href="#" class="button gray request" data-toggle="modal" data-target=".withdrawal_amount_modal" ">Get Paid</a>
            <?php endif?>
        </div>
    </div>

    <div class="stat-box group">
        <div class="half-div">
            <h3>Projects</h3>
            <p><?=$stat['pr_count'][0]['pr_count'];?></p>
        </div>
        <div class="half-div last">
            <h3><?=($this->session->userdata()['user_type'] == 1) ? 'Providers' : 'Clients'?></h3>
            <p><?=$stat['user_count'][0]['user_count'];?></p>
        </div>
    </div>
    <?php if(!$user->user_type):?>
        <div class="stat-box group">
            <h3>Time Logged</h3>
            <p>0h 0min</p>
        </div>
    <?php endif?>
    <?php if($user->user_type):?>
        <div class="stat-box group">
            <h3>PAID</h3>
            <p>$<?php if(isset($user->earned_pay)){echo $user->earned_pay; } else echo '0'; ?></p>
        </div>
    <?php else: ?>
        <div class="stat-box group">
            <h3>Money Earned</h3>
            <p>$<?php if(isset($user->earned_pay)){echo $user->earned_pay; } else echo '0'; ?></p>
        </div>
    <?php endif;?>
</div>
<?php } ?>


    <!--    Add Funds Popup     -->
<div class="modal fade withdrawal_amount_modal" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="loading_content"> <img src="<?=base_url()?>assets/images/load_freelancer.gif">   </div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Specify Withdrawal Amount</h4>
            </div>
            <div class="modal-body">
                <div class="middle-full group">
<!--                    <form method="post" action="--><?//=base_url('payments/found_get_paid')?><!--">-->
                        <input type="hidden" name="project" id = "modal_project" value="">
                        <input type="text" name="amount" class="wa-input" placeholder="Insert amount" value="">
                </div>
                <div class="funds_error"> </div>
            </div>
            <div class="modal-footer">
                <div class="middle group">
                    <label>Outpayment Method</label>
                    <div class="f-left select-custom-div-big">
                        <select class="custom-paymentmethod" name="#">
                            <option value="">Pre-paid Mastercard</option>
                            <option value="card">Pre-paid Visa</option>
                            <option value="paypal">PayPal</option>
                            <option value="bank">Bank transfer</option>
                        </select>
                    </div>
                    <div class="f-right"><button class="button green">Get Paid</button></div>
<!--                    </form>-->
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<!------------>

<!--<section class="popup small group withdrawal-ammount">-->
<!--    <div class="top group">-->
<!--        <h1>Specify Withdrawal Amount</h1>-->
<!--        <a href="javascript:void(0);" id="close-it"><i class="ion-close-round"></i></a>-->
<!--    </div>-->
<!--    <div class="middle-full group">-->
<!--        <form method="post" action="--><?//=base_url('payments/found_get_paid')?><!--">-->
<!--            <input type="hidden" name="project" id = "modal_project" value="">-->
<!--            <input type="text" name="amount" class="wa-input" placeholder="Insert amount" value="">-->
<!--    </div>-->
<!--    <div class="middle group">-->
<!--        <label>Outpayment Method</label>-->
<!--        <div class="f-left select-custom-div-big">-->
<!--            <select class="custom-paymentmethod" name="#">-->
<!--                <option value="#">Pre-paid Mastercard</option>-->
<!--                <option value="#">Pre-paid Visa</option>-->
<!--                <option value="#">PayPal</option>-->
<!--                <option value="#">Bank transfer</option>-->
<!--            </select>-->
<!--        </div>-->
<!--        <div class="f-right"><button class="button green">Get Paid</button></div>-->
<!--        </form>-->
<!--    </div>-->
<!---->
<!--</section>-->

<section class="popup small group withdrawal-thanks">
    <div class="top group">
        <h1>Withdrawal Request</h1>
        <a href="javascript:void(0);" id="close-it"><i class="ion-close-round"></i></a>
    </div>
    <div class="middle text-center group">
        <p>Thank you for your withdrawal request. We are processing the payment and it should complete in <strong>X days</strong>.</p>
        <p>&nbsp;</p>
        <a href="#" class="button gray">Get Paid</a>
    </div>

</section>


