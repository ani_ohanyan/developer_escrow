<section class="main group">
    <div class="wrapper group">
        <div class="content group">
            <div class="main-title uppercase group">Active Projects</div>
            <div class="project-box single-project group">
                <div class="details group">
                    <div class="user-details group">
                        <div class="avatar group"><img src="<?=base_url("assets/images/client-image.jpg")?>" alt="#"></div>
                        <a href="view-project-provider.html" class="simple">MediaTask</a>
                        <div class="clear"></div>
                        <small>$20/hour, 40h per week</small>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute </p>
                        <a href="view-project-provider.html" class="button gray">View Project</a>
                    </div>
                </div>
                <div class="details-numbers group">
                    <h3>Spent</h3>
                    <p>6 hrs, $120</p>
                    <h3 class="margin">This week</h3>
                    <div class="status-bar group"><div class="progress" style="width:50%;"></div></div>
                </div>
            </div>

            <div class="project-box single-project group">
                <div class="details group">
                    <div class="user-details group">
                        <div class="avatar group"><img src="<?=base_url("assets/images/client-image.jpg")?>" alt="#"></div>
                        <a href="view-project-provider.html" class="simple">HTML coding</a>
                        <div class="clear"></div>
                        <small class="milestone">$150 fix price, <span class="text-underline">3 milestones</span></small>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute </p>
                        <a href="view-project-provider.html" class="button gray">View Project</a>
                    </div>
                </div>
                <div class="details-numbers group">
                    <h3>Spent</h3>
                    <p>1 milestone, $50</p>
                    <h3 class="margin">Milestones</h3>
                    <ul class="milestone-dots group">
                        <li><span class="active"></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                    </ul>
                </div>
            </div>

            <div class="project-box single-project last group">
                <div class="details group">
                    <div class="user-details group">
                        <div class="avatar group"><img src="<?=base_url("assets/images/client-image.jpg")?>" alt="#"></div>
                        <a href="view-project-provider.html" class="simple">GoJeo</a>
                        <div class="clear"></div>
                        <small>$20/hour, 40h per week</small>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute </p>
                        <a href="view-project-provider.html" class="button gray">View Project</a>
                    </div>
                </div>
                <div class="details-numbers group">
                    <h3>Spent</h3>
                    <p>10 hrs, $200</p>
                    <h3 class="margin">This week</h3>
                    <div class="status-bar group"><div class="progress" style="width:30%;"></div></div>
                </div>
            </div>

            <div class="main-title uppercase group">Past Projects</div>

            <div class="project-box single-project group">
                <div class="details group">
                    <div class="user-details group">
                        <div class="avatar group"><img src="<?=base_url("assets/images/client-image.jpg")?>" alt="#"></div>
                        <a href="view-project-provider.html" class="simple completed">GoJeo <i class="ion-checkmark-round"></i></a>
                        <div class="clear"></div>
                        <small>$20/hour, 40h per week</small>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute </p>
                        <a href="view-project-provider.html" class="button gray">View Project</a>
                    </div>
                </div>
                <div class="details-numbers group">
                    <h3>Spent</h3>
                    <p>20 hrs, $400</p>
                    <h3 class="margin">Completed</h3>
                    <div class="complete-date group">Nov 02, 2015</div>
                </div>
            </div>


        </div>
        <div class="sidebar group">
            <div class="main-title uppercase group">Statistics</div>
            <div class="stat-box group">
                <div class="half-div">
                    <h3>Available</h3>
                    <p>$3500</p>
                </div>
                <div class="half-div text-right last"><a href="#" class="button gray">Add Funds</a></div>
            </div>

            <div class="stat-box group">
                <div class="half-div">
                    <h3>Projects</h3>
                    <p>4</p>
                </div>
                <div class="half-div last">
                    <h3>Providers</h3>
                    <p>1</p>
                </div>
            </div>

            <div class="stat-box group">
                <h3>Paid</h3>
                <p>$25.700</p>
            </div>

        </div>
    </div>
</section>