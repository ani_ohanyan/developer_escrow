<script type="text/javascript">

    $(".contact_send_form").on('submit',function(){
        event.preventDefault();
        var __this = $(this);
        var action = $(this).attr('action');
        var contact_name = __this.find('input[name="contact_name"]').val();
        var contact_email = __this.find('input[name="contact_email"]').val();
        var contact_message = __this.find('textarea[name="contact_message"]').val();
        if(contact_name == ''){
            $('.name_field_required').show();
            return;
        }else{
            $('.name_field_required').hide();
        }
        if(contact_email == ''){
            $('.email_field_required').show();
        }else{
            $('.email_field_required').hide();
        }
        if(contact_message == ''){
            $('.message_field_required').show();
        }else{
            $('.message_field_required').hide();
        }

        $.ajax ({
            url: action,
            type: 'post',
            data: __this.serialize(),
            dataType: 'text',
            success: function(data){
                $('.contact_name').val('');
                $('.contact_email').val('');
                $('.textarea').val('');
                $('.email_send_successfull').show();
                setTimeout(function(){
                    $('.email_send_successfull').hide();
                }, 4000);
            },
            error: function(){

            }
        });
        return false;
    });

    $(".view_map").on('click',function(){
        scrollToBottom('map_grey_block');
    });

    function scrollToBottom(id){
        div_height = $("#"+id).height();
        div_offset = $("#"+id).offset().top;
        window_height = $(window).height();
        $('html,body').animate({
            scrollTop: div_offset-window_height+div_height
        },'slow');
    }
    var page = '<?php echo $this->uri->segment(1);?>';
    if(page == 'contacts'){
        $('.wrapper').addClass('contact_wrapper');
    }

</script>