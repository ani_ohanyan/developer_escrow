<div ><?=$map_one['js'];?><?=$map_two['js'];?></div>
<div class="">
    <section id="contact_page_section" >
        <div class="page_title">
            <h1 class="page_title">CONTACT US</h1>
            <img src="<?=base_url()?>assets/images/map_icon.png"/>
            <p><a href="#" class="view_map">View Map</a></p>

        </div>
        <div class="contact_page_section_left">
            <ul>
                <li>USA</li>
                <li>NOVP Inc</li>
                <li>2609 Crooks Road</li>
                <li>Troy</li>
                <li>MI, 48084</li>
                <li>Tel. +1 313 329-3885</li>
            </ul>
        </div>
        <div class="contact_page_section_right">
            <ul>
                <li>Europe</li>
                <li>NOVP Limited</li>
                <li>6-9 Trinity Street</li>
                <li>Dublin</li>
                <li>Dublin 2, Ireland</li>
                <li>Tel. +44 7852 506268</li>
            </ul>
        </div><br>

        <div class="clear_both"></div><br>
        <div class="contact_form_block">
            <div class="contact_form_section">
                <div class="contact_form_section_right">
                    <h2 class="page_title" style="margin-bottom: 20px; color: #fff;">GET IN TOUCH</h2>
                    <form action="<?=base_url()?>Contacts/send_email" method="post" class="contact_send_form">
                        <span class="contact_error_message name_field_required"> The Name field is required</span>
                        <input class="contact_name" type="text" name="contact_name" placeholder="Enter Your Name"  />
                        <span class="contact_error_message email_field_required"> The Email field is required</span>
                        <input class="contact_email" type="email" name="contact_email" placeholder="Enter Your Email Address"  />
                        <div class="textarea">
                            <span class="contact_error_message message_field_required"> The Message field is required</span>
                            <textarea name="contact_message" class="textarea" rows="8" placeholder="Enter Your Message"></textarea>
                        </div>
                        <span class="email_send_successfull"> Success! Message received</span>
                        <input value="Send" class="contact_send" type="submit" name="contact_send" />
                    </form>
                </div>
            </div>
        </div>
        <br><br><br><br><br><br>
        <div id="map_grey_block">
            <div class="map_block">
                <div class="map_content"><?php echo $map_one['html']; ?></div>
                <div class="map_content" style="margin-right: 0;"><?php echo $map_two['html']; ?></div>
            </div>
        </div>

    </section>


</div>

