<script type="text/javascript">
    $('#changeUserImg').on('change', function(e) {
        $('.loading').show();
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('img_file', file_data);
        $.ajax({
            url: base_url+'admin/updateAdminAvatar',
            type: 'post',
            data: form_data,
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                var data = $.parseJSON(data);
                if(data.status){
                    $('.image .user-picture').css('background-image','url('+base_url+data.path+')');
                    $('img.user_profile_img').attr('src',base_url+data.path);
                    $('.loading').hide();
                }
            }
        });

    });
</script>
