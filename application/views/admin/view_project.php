<?php
if($project['currency'] == 1) {
    $project['currency'] = "$";
} else if($project['currency'] == 2) {
    $project['currency'] = "&#8364;";
} else if($project['currency'] == 3) {
    $project['currency'] = "&#163;";
}
?>
<div class="content group">
    <div class="main-title group"><?=$project['name'];?></div>
    <div class="project-box group">
        <div class="details group">
            <h3><?=$project['user_type']?'Client':'Developer';?></h3>
            <div class="user-details group">
                <div class="avatar group"><img src="<?=base_url('assets/images/client-image.jpg')?>" alt="#"></div>
                <p><?=$project['username']?></p>
                <?php if($project['type'] == 0) : ?>
                    <small><?=$project['currency'] . " " . $project['hourly_rate'] ?>/hour, <?=$project['hours_per_week']?>h per week</small>
                <?php else : ?>
                    <small class="milestone"><?=$project['currency']  . " " . $project['fixed_price'];?> fix price , <?=$project['mil_count'];?> milestone</small>
                <?php endif; ?>
                <p><a href="<?=base_url();?>user/<?=md5($project['user_id']);?>" class="v-profile">View Profile</a></p>
            </div>
        </div>
        <div class="details-numbers group">
            <h3>Earnings</h3>
            <p><?=($project['type'] == 1) ? "0 milestone" : "6 hrs, $120"?></p>
            <h3 class="margin">This week</h3>
            <div class="status-bar group"><div class="progress" style="width:50%;"></div></div>
        </div>
        <div class="clear"></div>
        <div class="main-text group">
            <?php if(empty($project['description'])){?>
                <button type="button" class="button icon gray ion-plus add-description" data-toggle="modal" data-target="#popup_add_description">Add description</button>
            <?php } else { ?>
                <p><?=$project['description']?></p>
            <?php } ?>
        </div>
        <?php if($project['st'] != 2){?>
        <div>
            <?php if ($user->user_type){ ?>
                <div class="f-left">
                    <a href="#" class="button icon gray ion-plus">Add Funds</a>
                    <a href="#" class="button icon gray ion-pause">Pause Project</a>
                </div>
            <?php  } else { ?>
                <div class="f-left">
                    <button class="button icon gray ion-android-stopwatch" data-toggle="modal" data-target="#trackerModal"> Open Time Tracker</button>
                </div>
            <?php }  ?>
            <div class="f-right"><a href="#" data-id="<?=$project['pr_id'];?>" class="button <?=((int)$project['st']==2)?'finishProject':'';?>" id="endProjectId">End Project</a></div>
        </div>
        <?php } ?>
        <?php if(!empty($project['milestones'])){ ?>
        <div class="full_width">
            <h3>Milestones</h3>
            <table class='milestone-list'>
               <?php $i=0; foreach($project['milestones'] as $milestone) { $i++ ?>
                <tr data-id="countMil">
                    <td class="number_milestone"><?=$i;?></td>
                    <td class="none_border"><h2 class="title_miles"><?=$milestone->name;?><span>-</span> $<?=$milestone->price?></h2><span><?=$milestone->description;?></span>
                    </td>
                    <td class="delete_el_miles"><a><i class="ion-ios-close remove-milestone"></i></a></td>
                </tr>
                <?php } ?>
            </table>
        </div>
        <?php } ?>
        <div class="clear"></div>
    </div>

    <div class="project_discussion">
        <div class="project-comments group">
            <h3>Discussion</h3>
            <?php if($project['st'] != 2){?>
            <div class="insert-comment group">
                <div class="input group">
                    <div class="triangle-bg group"></div>
                    <form action="#" method="post" id="addDisputeFormId">
                        <textarea name="message" placeholder="Add Comment..." required></textarea>
                        <input type="text" name="status" value="1" class="hidden" id="disputStatus" />
                        <input type="text" name="project_id" value="<?=$project['pr_id'];?>" class="hidden" />
                        <div class="clear"></div>
                        <!--<div class="col-md-12 no-padding">
                            <div class="uploadField choose_file">
                                <span>Upload Files </span>
                                <input type="file" id="files" name="job_attachment[]" class="filesListUpload" multiple>
                            </div>
                            <div id="selectedFiles"></div>
                        </div>-->
                        <ul>
                            <li><div class="checkbox checked " id="disputePublic">Public</div></li>
                            <li><div class="checkbox disputePrivate" id="disputePrivate">Private</div></li>
                            <li><button type="submit" class="button gray">Post</button></li>
                        </ul>
                    </form>
                </div>
            </div>
            <?php } if(isset($comments)) {
                foreach($comments as $comment) {
                    if ($comment['status']) {
                        $comment_class = "comment group";
                    } else {
                        $comment_class = "comment representative group";
                    }
                    ?>
                    <?php if($comment['status'] || ($this_user == $comment['user_id']) && !$comment['status'] ):?>
                        <div class="<?=$comment_class?>">
                            <div class="input group">
                                <p><?=$comment['message'];?></p>
                            </div>
                            <div class="user group">
                                <ul>
                                    <li>
                                        <img src="<?=base_url($comment['path'])?>" alt="#">
                                    </li>
                                    <li>
                                        <strong><?=$comment['username'];?></strong><?=$comment['date'];?>
                                    </li><!--5 hours ago-->
                                </ul>
                            </div>
                        </div>
                    <?php endif;?>
                <?php } } ?>
            </div>
        </div>
    </div>
    <div class="sidebar group">
        <div class="main-title group">
            <ul>
                <li><a href="javascript:void(0);" id="tab-link-1" class="active">Files</a></li>
                <li><a href="javascript:void(0);" id="tab-link-2" class="">Links (<?=$link['count'][0]['lc'];?>)</a></li>
            </ul>
        </div>
        <div class="files-links group">
            <div class="tab-content-1 group display">
                <div class="search-box group">
                    <form method="post" action="#" id="searchFileId">
                        <input type="text" class="hidden" value="file" name="type" />
                        <input type="text" name="search" placeholder="Search">
                        <button type="submit" name="submit"><i class="ion-search"></i></button>
                    </form>
                </div>
                <div class="link group"><a href="#" id="open-upload-files" class="button gray">Upload</a></div>
                <div class="clear"></div>
                <?php if(isset($files) && !empty($files)) { ?>
                    <?php $c = $files[0]['description'];
                    foreach($files as $k => $file) { ?>
                        <?php if($file['description'] != $c ||  $k == 0 ) {
                            $c = $file['description'];  ?>
                            <div class="gray-box group">
                            <ul>
                                <strong><?=$file['description'];?></strong>
                                <p><?=date("d M, Y",strtotime($file['date']));?></p>
                            <?php } ?>
                        <!-- <p>1 hour ago</p>-->
                                <li class="file_download">
                                    <a href="<?=base_url();?>files/download?file=<?=$file['path'];?>&name=<?=$file['name'];?>" class="button"><?=$file['name'];?></a>
                                    <input type="hidden" id="file_path" value="<?=md5($file['user_id'])?>">
                                </li>
                            <?php if($k+1 < count($files) && $files[$k+1]['description'] != $c ) {
                                $c = $file['description'];  ?>
                            </ul>
                        </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
        <div class="tab-content-2 group">
            <div class="search-box group">
                <form method="post" action="#" id="searchLinkId">
                    <input type="text" class="hidden" value="link" name="type" />
                    <input type="text" name="search" placeholder="Search">
                    <button type="submit" name="submit"><i class="ion-search"></i></button>
                </form>
            </div>
            <div class="link group"><a href="#" class="button gray" id="open-upload-link">Add</a></div>
            <div class="clear"></div>
            <?php if(isset($link['list']) && !empty($link['list'])) { ?>
                <?php foreach ($link['list'] as $link ) { ?>
                    <div class="gray-box group">
                        <strong><?=$link['name'];?></strong>
                        <p><?=date("d M,Y",strtotime($link['date']));?></p>
                        <ul class="short group">
                            <li><a href="<?=$link['link'];?>" target="_blank" class="simple"><?=$link['link'];?></a></li>
                        </ul>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>

<!-- Modal Add Upload  -->
<section class="popup group upload-files" id="uploadFilesId">
    <div class="top group">
        <h1>Upload Files</h1>
        <a href="javascript:void(0);" id="close-it"><i class="ion-close-round"></i></a>
    </div>
    <div class="middle group">
        <form id="fileupload" action="<?=base_url();?>projects/upload" method="POST" enctype="multipart/form-data">
            <div class="clear"></div>
            <input type="text" class="hidden" name="project_id" value="<?=$project['pr_id'];?>"/>
            <input type="text" name="description" placeholder="Box Name" required />
            <div class="clear"></div>
            <div class="drop-files group fileupload-buttonbar">
                <p>Drop multiple files here or</p>
                <p>&nbsp;</p>
                <div class="scrollUloadImg">
                    <!--<div class="files"></div>-->
                    <div id="selectedFiles"></div>
                </div>
                <div class="uploadField choose_file">
                    <span>Upload Files </span>
                    <input type="file" id="files" name="project_attachment[]" class="filesListUpload" multiple>
                </div>
                    <!--<div><span class="btn button gray uploadImg fileinput-button">
                        <span>Select Files</span>
                        <input type="file" name="files[]" multiple>
                    </span>
                </div>
                <span class="fileupload-process"></span>-->
            </div>
            <div class="clear"></div>
            <div class="f-left"><a href="#" class="button discard_section">Discard</a></div>
            <div class="buttons group fileupload-buttonbar">
                <div class="f-right">
                    <button type="submit" class="button green start">
                        <span>Upload Files</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>

<!-- Modal Add Link  -->
<section class="popup group upload-link" id="addLinkModalId">
    <div class="top group">
        <h1>Add Link</h1>
        <a href="javascript:void(0);" id="close-link"><i class="ion-close-round"></i></a>
    </div>
    <div class="middle group">
        <form method="post" action="#" id="addLinkFormId">
            <input type="text" name="name" placeholder="Name the link">
            <input type="text" name="link" value="http://">
            <input type="text" class="hidden" name="project_id" value="<?=$project['pr_id'];?>"/>
            <div class="clear"></div>
            <div class="f-left"><a href="#" class="button discard_section">Discard</a></div>
            <div class="f-right"><button type="submit" class="button green">Add Link</button></div>
        </form>
    </div>
</section>

<!-- Modal Add Project Description -->
<div class="modal fade" id="popup_add_description" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><a href="javascript:void(0);"><i class="ion-close-round"></i></a></button>
                <h4 class="modal-title"> Add description</h4>
            </div>
            <form method="post" action="<?=base_url('projects/add_description')?>">
                <div class="modal-body">
                    <input type="hidden" name="projects" value = "<?=$project['pr_id']?>" />
                    <textarea class="description" id="fixed-project-description" name="description"></textarea>

                    <div class="f-left"><button class="button icon gray " data-dismiss="modal">Discard</button></div>
                    <div class="f-right"> <button class="button icon gray ion-plus" type="submit"  >Add</button></div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="trackerModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="logo_section">
                    <a href="#"><img src="<?=base_url();?>assets/images/logo_time_tracker.png" /></a>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div id="time_tracker_page">
                    <div class="no-padding container wrapper">
                        <div class="content">
                            <form action="<?=base_url();?>projects/<?=md5($project['pr_id']);?>" method="post" enctype="multipart/form-data">
                                <div class="form_section">
                                    <div class="col-md-6 no-padding">
                                        <p class="select_title">Select Project</p>
                                        <select name="project_id" class="contractProject">
                                            <?php foreach($projects as $project) { ?>
                                                <option value="<?=$project['id'];?>"><?=$project['name'];?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class=" col-md-6 counter_hrs_min_section">
                                        <p class="select_title">Enter Time</p>
                                        <input id="input-number-mod" class="mod" name="hr" min="0" max="24" type="number" value="0" />
                                        <span>hrs</span>
                                        <input id="input-number-mod2" class="mod" name="min" min="0" max="59" type="number" value="0" />
                                        <span>min</span>
                                    </div>

                                    <div class="description form-group">
                                        <textarea  placeholder="Add Description" rows="4" id="description_time_tracker" name="description" class="form-control"></textarea>
                                    </div>
                                    <div class="col-md-12 no-padding">
                                        <div class="uploadField choose_file">
                                            <span>Upload Files </span>
                                            <input type="file" id="files" name="job_attachment[]" class="filesListUploads" multiple>
                                        </div>
                                        <div id="selectedFile"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="btn_work">
                                    <input type="submit" value="Submit Work" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>