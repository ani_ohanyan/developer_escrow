<script>

    $("#disputePublic , #disputePrivate").on("click",function(){
        if($(this).attr('id') == 'disputePublic') {
            el = $("#disputePrivate");
            $("#disputStatus").val(1);
        } else {
            el = $("#disputePublic");
            $("#disputStatus").val(0);
        }
        console.log(el);
        $(this).addClass("checked");
        el.removeClass("checked");


    })
    $("#addDisputeFormId").on('submit',function() {
        event.preventDefault();
        var __this = $(this);
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: 'post',
            data: __this.serialize(),
            dataType: 'text',
            success: function (data) {
                var data = $.parseJSON(data);
                console.log(data);
                if (data.status) {
                    var comment_class;
                    if(data.comment.status == "1"){
                        comment_class = "comment group";
                    }else{
                        comment_class = "comment representative group";
                    }
                    $('.comment_area').val('');
                    $(".project-comment-append").append('<div class="'+comment_class+'"><div class="input group"><div class="arrow-down"></div><p>'+data.comment["message"]+'</p></div><div class="user group"><ul><li><img id="myimg" src="'+base_url+data.comment["path"]+'" alt="#"></li><li><strong>'+data.comment["username"]+'</strong>'+data.comment["date"]+'</li></ul></div></div>');
                }

            },
            error: function () {
                /*$("#addJobAppId").prop("disabled", false);*/
            }
        });
        return false;
    });
//    $("#close_dispute").on("click", function () {
//        $.ajax ({
//            url: base_url+"disputes/close_dispute",
//            type: 'post',
//            data: $(this).serialize(),
//            success: function(data){
//
//                console.log(data);
//
//            },
//            error: function(){
//            }
//        });
//
//    });

</script>