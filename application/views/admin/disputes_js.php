<script type="text/javascript">
    $(document).ready(function(){
        $('#job_categories_info').DataTable();
    });

    /*
    * Delete categories
    * */
    $(document).on("click",".delete_categories", function () {

        var category_id = $(this).parents('tr').attr("data-id");
        var __this = $(this);
        $.ajax ({
            type:"POST",
            url: base_url+"admin/delete_category",
            data: {category_id : category_id},
            success: function(data){
                if(data){
                    location.reload();
                }
            },
            error: function(){
                alert("Failure");
            }
        });
    });

    /*
     * Edit categories
     * */

    $(document).on("click",".fa-pencil", function () {
        var category_name = $(this).parents('tr').children('.category_names').html();
        $(this).parents('tr').children('.category_names').html('<input type="text" name="category_names" value="'+category_name+'">');
        $(this).parents('tr').children('.category_types').html('<select name="category_types"><option value="mobile">Mobile</option><option value="desktop">Desktop</option></select>');
        $(this).removeClass('fa-pencil');
        $(this).addClass('fa-check-circle');
        $(this).addClass('green');
    });

    $(document).on("click",".fa-check-circle", function () {
        var __this = $(this);
        var category_id = $(this).parents('tr').attr("data-id");
        var category_name = $(this).parents('tr').find('.category_names input').val();
        var category_type = $(this).parents('tr').find('.category_types select').val();
        $.ajax ({
            type:"POST",
            url: base_url+"admin/edit_category",
            data: {
                category_id : category_id,
                category_name : category_name,
                category_type : category_type
            },
            success: function(data){
                if(data){
                    location.reload();
                }
            },
            error: function(){
                location.reload();
            }
        });
    })


</script>
