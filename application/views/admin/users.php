<div class="admin_wrapper">
    <table id="users_info" class="display dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
        <thead>
            <tr role="row">
                <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-label="Position: activate to sort column ascending" style="width: 100px;">Username
                </th>
                <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-label="Office: activate to sort column ascending" style="width: 102px;">Email
                </th>
                <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-label="Start date: activate to sort column ascending" style="width: 92px;">Phone Number
                </th>
                <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-label="Start date: activate to sort column ascending" style="width: 92px;">User Type
                </th>
                <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-label="Start date: activate to sort column ascending" style="width: 100px;"><!--Edit /--> Delete
                </th>
            </tr>
        </thead>
        <tbody
            <?php foreach ($user_data as $user) {?>
                <tr role="row" class="odd" data-id="<?=$user->id?>">
                    <td class="sorting_1"><?=$user->username;?></td>
                    <td><?=$user->email;?></td>
                    <td><?=$user->phone_number;?></td>
                    <td><?= ($user->user_type == 1) ? 'Client' : 'Developer'?></td>
                    <td align="center"><!--<i class="fa fa-pencil edit_user"></i>--> <i class="fa fa-trash delete_user"></i></td>
                </tr>
            <?php } ?>
            </tbody>
    </table>
</div>