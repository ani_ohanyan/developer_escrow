<div class="admin_wrapper">
    <table id="job_categories_info" class="display dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
        <thead>
            <tr role="row">
                <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 137px;">
                    Dispute Name
                </th>
                <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 137px;">
                    Invited Email
                </th>
                <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 137px;">
                    Phone Number
                </th>
                <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 137px; text-align: right">
                    View Dispute
                </th>
                <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 137px; text-align: right">
                    Status
                </th>
            </tr>
        </thead>
        <tbody
            <?php foreach ($disputes as $dispute) { ?>
                <tr role="row" class="odd" data-id="<?=$dispute->id?>">
                    <td class="category_names"><?=$dispute->description;?></td>
                    <td class="category_types"><?=$dispute->invited_email;?></td>
                    <td class="category_types"><?=$dispute->phone_number;?></td>
                    <td class="edit_delete"><a href="<?=base_url()?>admin/view_dispute/<?=md5($dispute->id)?>" class="button gray">View</a></td>
                    <td class="edit_delete"><?=($dispute->status == 0) ? 'Open' : 'Closed'?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>