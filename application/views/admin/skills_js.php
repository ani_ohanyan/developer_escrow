<script type="text/javascript">
    $(document).ready(function(){
        $('#skills_info').DataTable();
    });

    $(document).on("click",".delete_skills", function () {

        var skill_id = $(this).parents('tr').attr("data-id");
        var __this = $(this);
        $.ajax ({
            type:"POST",
            url: base_url+"admin/delete_skill",
            data: {skill_id : skill_id},
            success: function(data){
                if(data){
                    location.reload();
                }
            },
            error: function(){
                alert("Failure");
            }
        });
    });

    $(document).on("click",".fa-pencil", function () {
        var skill_name = $(this).parents('tr').children('.skill_names').html();
        $(this).parents('tr').children('.skill_names').html('<input type="text" name="skill_names" value="'+skill_name+'">');
        $(this).removeClass('fa-pencil');
        $(this).addClass('fa-check-circle');
        $(this).addClass('green');
    });

    $(document).on("click",".fa-check-circle", function () {
        var __this = $(this);
        var skill_id = $(this).parents('tr').attr("data-id");
        var skill_name = $(this).parents('tr').find('.skill_names input').val();
         $.ajax ({
             type:"POST",
             url: base_url+"admin/edit_skill",
             data: {
                 skill_id : skill_id,
                 skill_name : skill_name
             },
             success: function(data){
             if(data){
             location.reload();
             }
             },
                error: function(){
             alert("Failure");
             }
         });
    })

</script>
