<div class="admin_wrapper">
    <div class="project-box group">
        <div class="tab-content-1 hourly-div group display">
            <div class="row-settings group avatarImg">
                <p>Profile Photo</p>
                <div class="image">
                    <?php //print_r($this->session->userdata());die;?>
                    <div class="user-picture" style="background-image:url(<?= base_url() . $this->session->userdata('avatarPath'); ?>);">
                    </div>
                </div>
                <label class="editImg"><i class="fa fa-pencil"></i>
                    <input id="changeUserImg" name="img_file" type="file"/>
                </label>
            </div>
            <h3>Profile</h3>
            <form method="post" class="profile_settings" action="<?= base_url() ?>admin/settings" id="settingsFormId">
                <div class="row-settings group">
                    <input type="text" name="name" placeholder="Name" value="<?= $admin_data['username'] ?>" class="form-control"/>
                </div>
                <div class="row-settings group">
                    <input type="text" name="email" placeholder="Email" value="<?= $admin_data['email'] ?>" class="form-control" />
                </div>
                <h3>Password</h3>
                <div class="row-settings group">
                    <input type="password" class="old_password form-control" name="old_password" placeholder="Old Password" value="" autocomplete="off">
                </div>
                <div class="row-settings group">
                    <input type="password" class="new_password form-control" name="new_password" placeholder="New Password" value="">
                    <div class="successfull"> Your changes saved successfully</div>
                    <div class="failed"> Saving failed, Please input correct data</div>
                </div>


                <div class="clear"></div>
                <input type="submit" class="green_button" value="Save">
            </form>
        </div>
    </div>
</div>