<div class="admin_wrapper">
    <a href="<?=base_url()?>admin/add_skills" class="add-btn"><i class="fa fa-plus-circle"></i> Add Skills</a>
    <table id="skills_info" class="display dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
        <thead>
            <tr role="row">
                <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 87%!important;">
                    Skill Name
                </th>
                <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 13%">
                    Edit / Delete
                </th>
            </tr>
        </thead>
        <tbody
            <?php foreach ($skills as $skill) { ?>
                <tr role="row" class="odd" data-id="<?=$skill->id?>">
                    <td class="skill_names"><?=$skill->name;?></td>
                    <td class="edit_delete"><i class="fa fa-pencil edit_skills"></i> &nbsp; / &nbsp; <i class="fa fa-trash delete_skills"></i></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>