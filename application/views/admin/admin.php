<div class="body-inner">
    <div id="time_tracker_page">
        <div class="no-padding container wrapper">
            <div class="content">
                <div class="logo_section">
                    <a href="<?=base_url();?>jobs/"><img src="<?=base_url();?>assets/images/logo.png" /></a>
                </div>
                <form action="#" method="POST" enctype="multipart/form-data" id="adminLoginFormId">
                    <div class="form_section">
                        <div class="modal-body modal-body-login">
                            <p class="form-description"></p>
                            <p class="form-error password_or_email"></p>

                            <div class="form-group col-md-12 col-sm-12 col-xs-12 no-padding">
                                <p class="form-error email"></p>
                                <?= form_error("email", '<div class="form-error">', '</div>') ?>
                                <input class="form-control app-lister-input email required" placeholder="Your Email" name="email" type="text">
                            </div>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12 no-padding">
                                <p class="form-error password"></p>
                                <?= form_error("password", '<div class="form-error">', '</div>') ?>
                                <input class="form-control app-lister-input required password" placeholder="Password" name="password" type="password">
                            </div>
                        </div>

                    <div class="btn_work buttons group fileupload-buttonbar">
                        <button type="submit" class="button green start trackerBtn">
                            <span>Log In</span>
                        </button>
                        <span class="fileupload-process"></span>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div><!-- Body inner end -->