<div class="admin_wrapper">
    <table id="projects_info" class="display dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
        <thead>
            <tr role="row">
                <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 137px;">
                    Project Name
                </th>
                <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-label="Position: activate to sort column ascending" style="width: 215px;">Project Description
                </th>
                <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-label="Office: activate to sort column ascending" style="width: 102px;">Project Fund
                </th>
                <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
                    aria-label="Start date: activate to sort column ascending" style="width: 92px;">User Type
                </th>
            </tr>
        </thead>
        <tbody
            <?php foreach ($projects as $project) { ?>

                <tr role="row" class="odd">
                    <td class="sorting_1"><?=$project->name;?></td>
                    <td><?=$project->description;?></td>
                    <td><?=$project->fund;?></td>
                    <td><?= ($project->type == 1) ? 'Client' : 'Developer'?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>