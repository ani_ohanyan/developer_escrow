<div class="admin_wrapper">
    <h2>Add Skills</h2>
    <form action="<?=base_url()?>admin/add_skills" method="post">
        <div class="row">
            <label>Skill Name</label>
            <input type="text" name="name" class="form-control" />
        </div>
        <div class="row">
            <button type="submit" class="btn pull-right">Add Skill</button>
        </div>
    </form>
</div>

