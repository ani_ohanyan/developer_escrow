<script type="text/javascript">
    $(".contractProject").select2();

    $("#addLinkFormId").on('submit',function(){
        $.ajax ({
            url: base_url+"links/addLink",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'text',
            success: function(data){
                var data = $.parseJSON(data);
                if(data.status){

                    var html = "<div class='gray-box group'><strong>"+data.name+"</strong><p>"+data.date+"</p><ul class='short group'><li><a href='"+data.link+"' target='_blank' class='simple'>"+data.link+"</a></li></ul></div>";
                    $(".tab-content-2").append(html);

                    $('#addLinkModalId').removeClass('display');
                    $('.overlay').removeClass('display');
                }
            },
            error: function(){
            }
        });
        return false;
    });

    $("#endProjectId").on("click", function () {
        $.ajax ({
            url: base_url+"projects/changeProjectStatus",
            type: 'post',
            data: {id:$(this).attr('data-id')},
            dataType: 'text',
            success: function(data){
                console.log(data);
                if(data)
                    $("#endProjectId").addClass('finishProject');
                window.location.href=base_url+'projects';
            },
            error: function(){
            }
        });
        return false;
    });



    $("#disputePublic , #disputePrivate").on("click",function(){
        if($(this).attr('id') == 'disputePublic') {
            el = $("#disputePrivate");
            $("#disputStatus").val(1);
        } else {
            el = $("#disputePublic");
            $("#disputStatus").val(0);
        }
        console.log(el);
        $(this).addClass("checked");
        el.removeClass("checked");
    });

    $("#addDisputeFormId").on('submit',function() {
        $.ajax({
            url: base_url + "comments/addComment",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'text',
            success: function (data) {
                var data = $.parseJSON(data);
                if (data.status) {
                    var comment_class;
                    if(data.comment.status == "1"){
                        comment_class = "comment group";
                    }else{
                        comment_class = "comment representative group";
                    }
                    $(".project-comments").append('<div class="'+comment_class+'"><div class="input group"><p>'+data.comment['message']+'</p></div><div class="user group"><ul><li><img src="'+base_url+data.comment["path"]+' " alt="#"></li><li><strong>'+data.comment['username']+'</strong>'+data.comment['date']+'</li></ul></div></div>');
                    $('#addDisputeFormId textarea').val('');
                }

            },
            error: function () {
                /*$("#addJobAppId").prop("disabled", false);*/
            }
        });
        return false;
    });

    $("#searchFileId").on("submit", function () {
        $.ajax({
            type: "POST",
            url: base_url + "projects/searchByData",
            data: $(this).serialize(),
            success: function(msg) {
                var data = $.parseJSON(msg);
                if (data.status) {
                    $(".tab-content-1 .gray-box.group").css('display','none');
                        for(i=0; i < data.datas.length;i++){
                            var date = data.datas[i]['date'];
                            data.datas[i]['date'] = date.toString('dd-MM-yy');

                            var html = "<div class='gray-box search group'><strong>"+data.datas[i]['file_description']+"</strong><p>"+data.datas[i]['date']+"</p><ul class='short group'><li>"+data.datas[i]['name']+"</li></ul></div>";
                           $(".tab-content-1").append(html);
                        }
                        $(".tab-content-1 .gray-box.search.group").css('display','block');
                    }
                }
            });
        return false;
    });

    $("#searchLinkId").on("submit", function () {
        $.ajax({
            type: "POST",
            url: base_url + "projects/searchByData",
            data: $(this).serialize(),
            success: function(msg) {
                var data = $.parseJSON(msg);
                if (data.status) {
                    $(".tab-content-2 .gray-box").css('display','none');
                    for(i=0; i < data.datas.length;i++){
                        var date = data.datas[i]['date'];
                        data.datas[i]['date'] = date.toString('dd-MM-yy');
                        var html = "<div class='gray-box search group'><strong>"+data.datas[i]['name']+"</strong><p>"+data.datas[i]['date']+"</p><ul class='short group'><li><a href='"+data.datas[i]['link']+"' target='_blank' class='simple'>"+data.datas[i]['link']+"</a></li></ul></div>";
                        $(".tab-content-2").append(html);
                    }
                    $(".tab-content-2 .gray-box.search.group").css('display','block');
                }

            }
        });
        return false;
    });


    $(".file_download").on("click", function () {
        var file_name = $(this).children('a').html();
        var job_no = $(this).children('#file_path').val();
        window.location.href = "<?php echo site_url('Projects/file_download') ?>?file_name="+ file_name;
    });

    /*    function prd_download(ele)
        {
            var image_name = $('.file_download').html();
            //console.log(image_name);return;
            var job_no = $('#file_path').val();
            var file_name = new Array();
            var jobno = new Array();

            for(j = 0; j < job_no.length; j++)
            {
                jobno[j] = $(job_no[j]);
            }
            for(i = 0; i < image_name.length; i++)
            {
                file_name[i] = $(image_name[i]);
            }
            $.get('<?php //echo site_url('project_controller/file_download') ?>', {file_name : file_name, jobno : jobno});
        }
*/
</script>