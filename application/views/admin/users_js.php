<script type="text/javascript">
    $(document).ready(function(){
        $('#users_info').DataTable();
    });
    $(document).on("click",".delete_user", function () {
        var user_id = $(this).parents('tr').attr("data-id");
        var __this = $(this);
        $.ajax ({
            type:"POST",
            url: base_url+"admin/delete_user",
            data: {user_id : user_id},
            success: function(data){
                if(data){
                    location.reload();
                }
            },
            error: function(){
                alert("Failure");
            }
        });
    });
</script>
