<script>
    $(".msgIsRead .button").bind("click",function(e){
        note_id = $(this).attr('data-id');
        readNote(note_id);
//        console.log($("#noteId"+note_id+".not_icons"));
    });
    function readNote(note_id){
        $.ajax({
            url:  base_url+'notifications/readmsg',
            data: {'note_id': note_id},
            success: function (data) {
                $("#noteId"+note_id).removeClass('msgIsRead');

                data = JSON.parse(data);
                if(data.status) {
                    $("#noteId"+note_id).find('.not_icons').removeClass("red_not").addClass('notification_back');
                    $("#noteId"+note_id).removeClass('msgIsRead');
                    $("#noteId"+note_id).addClass('read_notif');
                    if(parseInt(data.count)) {
                        $("#noteround").html(data.count);
                    } else {
                        $("#noteround").addClass('hidden');
                    }
                }
            }
        });
    }
</script>