
<div class="content group">
    <div class="payment-div group notifications">
        <div class="main-title uppercase group main_title_block">Notifications</div>
        <?php if($notifications && !empty($notifications)):?>
            <?php foreach($notifications as $notes):?>
                <?php $class = '';
                if(!$notes['is_read']) {
                    $class = 'msgIsRead';
                } else {
                    $class = 'read_notif';
                }?>
                <div style="position: relative" class="<?=$class?> single-payment group" id="noteId<?=$notes['id']?>">
                    <?php if($notes['is_read']):?>
                        <li class="notif_is_read">
                            <h4 class="job-closed not_icons notification_back" >
                                <?php if($notes['not_id'] == 1 || $notes['not_id'] == 2 ){ ?>
                                    <i class="fa fa-exclamation"></i>
                                <?php } elseif($notes['not_id'] == 6 ){ ?>
                                    <i class="fa fa-file-o"></i>
                                <?php } elseif($notes['not_id'] == 4 ){ ?>
                                    <i class="ion-checkmark-round"></i>
                                <?php  } else { ?>
                                    <i class="ion-checkmark-round"></i>
                                <?php } ?>
                            </h4>
                        </li>
                    <?php else:?>
                        <li>
                            <?php if($notes['not_id'] == 1 || $notes['not_id'] == 2 ){ ?>
                                <h4 class="job-closed red_not not_icons">
                                    <i class="fa fa-exclamation"></i>
                                </h4>
                            <?php } elseif($notes['not_id'] == 6 ){ ?>
                                <h4 class="job-closed blue_not not_icons">
                                    <i class="fa fa-file-o"></i>
                                </h4>
                            <?php } elseif($notes['not_id'] == 4 ){ ?>
                                <h4 class="job-closed gray_not not_icons">
                                    <i class="ion-checkmark-round"></i>
                                </h4>
                            <?php  } else { ?>
                                <h4 class="job-closed">
                                    <i class="ion-checkmark-round"></i>
                                </h4>
                            <?php } ?>
                        </li>
                    <?php endif ;?>
                    <div style="margin-left: 40px;" class="text group padding_top_7">
                        <div style=><?=$notes['message']?></div>
                    </div>
                    <div class="price group">
                        <ul>
                            <li>Sent:<small><?=$notes['date']?></small></li>
                            <li>
                                <?php if($notes['type'] == 'dispute'):?>
                                    <a href="<?=base_url()?>disputes/view_dispute/<?=md5($notes['type_id'])?>" data-id="<?=$notes['id']?>" class="button gray">View</a>
                                <?php elseif($notes['type'] == 'project'):?>
                                    <a href="<?=base_url()?>projects/<?=md5($notes['type_id'])?>" data-id="<?=$notes['id']?>" class="button gray">View</a>
                                <?php endif?>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php endforeach?>
        <?php else:?>
            Notification list is empty!
        <?php endif;?>
    </div>
</div>
