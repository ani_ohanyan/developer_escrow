<script type="text/javascript">
    $(".contractProject").select2();
    $('#selectedFiles').perfectScrollbar();
    $('.comment_block').perfectScrollbar();

    $("#addLinkFormId").on('submit',function(){
        $.ajax ({
            url: base_url+"links/addLink",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'text',
            success: function(data){
                var data = $.parseJSON(data);
                if(data.status){
                    var html = "<div class='gray-box group'><strong>"+data.name+"</strong><p>"+data.date+"</p><ul class='short group'><li><a href='"+data.link+"' target='_blank' class='simple'>"+data.link+"</a></li></ul></div>";
                    $(".tab-content-2").append(html);
                    $('#addLinkModalId').removeClass('display');
                    $('.overlay').removeClass('display');
                }
            },
            error: function(){
            }
        });
        return false;
    });

    $("#endProjectId").on("click", function () {
        $.ajax ({
            url: base_url+"projects/changeProjectStatus",
            type: 'post',
            data: {id:$(this).attr('data-id')},
            dataType: 'text',
            success: function(data){
                console.log(data);
                if(data)
                    $("#endProjectId").addClass('finishProject');
                window.location.href=base_url+'projects';
            },
            error: function(){
            }
        });
        return false;
    });



    $("#disputePublic , #disputePrivate").on("click",function(){
        if($(this).attr('id') == 'disputePublic') {
            el = $("#disputePrivate");
            $("#disputStatus").val(1);
        } else {
            el = $("#disputePublic");
            $("#disputStatus").val(0);
        }
        $(this).addClass("checked");
        el.removeClass("checked");
    });

//    $("#fileupload").on('submit',function(e) {
//        e.preventDefault();
////        var form_data = new FormData();
//        var form_data = $(this).serialize();
//        for(i=0; i < $("#files2").prop('files').length; i++){
//            form_data.append("project_attachment[]", document.getElementById('files').files[i]);
//        }
//        $.ajax({
//            url: base_url + "projects/upload",
//            type: 'post',
//            dataType: 'text',
//            data: form_data,
//            success: function (data) {
//                console.log(data)
//            }
//        });
//    })


    $("#fileupload2").on('submit',function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('#loading_comment_upload').show();
        var form_data = new FormData();
        for(i=0; i < $("#files2").prop('files').length; i++){
            form_data.append("project_attachment[]", document.getElementById('files2').files[i]);
        }
        $.ajax({
            url: base_url + "comments/addCommentFile",
            type: 'post',
            data: form_data,
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                var data = $.parseJSON(data);
                $('#uploadFiles_modals').modal('hide');
                $('#uploadFiles_modals').find('#selectedFiles1').html('');
                $('.upload_files_grey_div').fadeIn();
                var i = 0;
                for(i=0; i<data.name.length ; i++){
                    $('#message_file').append('<div class="comment_upload_files">' +
                        '<a class="upload_files_name" href="javascript:void (0)">'+data.name[i]+'<span class="delete_uploaded_file" data_id ="'+data.id[i]+'" path ="'+data.path[i]+'"><i class="fa fa-times"></i>' +
                        '</span></a>' +
                        '<input type="hidden" name="message_file[]" value="'+data.id[i]+'" /><input type="hidden" name="message_file_name[]" value="'+data.name[i]+'" /><input type="hidden" name="message_file_path[]" value="'+data.path[i]+'" /></div>');
                }
                $('#files2').val('');
                $('#loading_comment_upload').hide();
            },
            error: function () {
            }
        });
    })

    $(document).on('click','.delete_uploaded_file', function(){
        var data = $(this).attr('data_id');
        var path = $(this).attr('path')
        var _this = $(this);
        $.ajax({
            url: base_url + "comments/delete_upload_file",
            type: 'post',
            dataType: 'text',
            data:{delete_file: data, delete_file_path:path},
            success: function (data) {
                if(data){
                    _this.parent().parent().remove();
                }
                if ($('#message_file').children().length == 0){
                    $('.upload_files_grey_div').hide();
                }
            }
        })
    })

    $("#addDisputeFormId").on('submit',function(e) {
        e.preventDefault();
        e.stopPropagation();
//         for(i=0; i < $("#files2").prop('files').length; i++){
//            form_data.append("project_attachment[]", document.getElementById('files2').files[i]);
//        }
//        form_data.append()
//        form_data.append('status', $('#disputStatus').val() );
//        form_data.append('message', $('#addDisputeFormId textarea').val() );
//        form_data.append('project_id', $('#project_id').val() );
        $.ajax({
            url: base_url + "comments/addComment",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'text',
            success: function (data) {
                $("#message_file").html('');
                $('.upload_files_grey_div').hide();
                var data = $.parseJSON(data);
                if (data.status) {
                    var comment_class;
                    if(data.comment.status == "1"){
                        comment_class = "comment group";
                    } else {
                        comment_class = "comment representative group";
                    }
                    $(".comment_block").prepend('<div class="'+comment_class+'"><div class="input group"><div class="upload_files_content" id="id'+data.comment['id']+'"><p>'+data.comment['message']+'</p></div></div><div class="user group"><ul><li><img src="'+base_url+data.comment["path"]+' " alt="#"></li><li><strong>'+data.comment['username']+'</strong>'+data.comment['date']+'</li></ul></div></div>');
                    if(data.file){
                        for(i=0; i < data.file.length; i++) {
                            var ext = data.file_name[i].split('.').pop().toLowerCase();
                            if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                                $("#id"+data.comment['id']).append('<div class="comment_upload_files"><a class="upload_files_name" href="'+base_url+'files/download?file='+data.file_path[i]+'&name='+data.file_name[i]+'">'+data.file_name[i]+'</a></div>');
                            } else {
                                $("#id"+data.comment['id']).prepend(generateDiv(data.file[i], data.file_name[i], data.file_path[i]));
                            }
                        }
                    }
                    $('#addDisputeFormId textarea').val('');
                    $('#comment_error').hide()
                } else {
                    $('#comment_error').show()
                }
            },
            error: function () {
                /*$("#addJobAppId").prop("disabled", false);*/
            }
        });
        return false;
    });

    function generateDiv(file, name, path){
        var content = $("<div>", {class: "comment_upload_files"});
        var link = $("<a>", {href: base_url+'files/download?file='+path+'&name='+name});
        var img = $("<img>", {src: base_url+path});
        link.appendTo(content);
        img.appendTo(link);
        return content;
    }

    $("#searchFileId").on("submit", function () {
        $.ajax({
            type: "POST",
            url: base_url + "projects/searchByData",
            data: $(this).serialize(),
            success: function(msg) {
                var data = $.parseJSON(msg);
                if (data.status) {
                    $(".tab-content-1 .gray-box.group").css('display','none');
                        for(i=0; i < data.datas.length;i++){
                            var date = data.datas[i]['date'];
                            data.datas[i]['date'] = date.toString('dd-MM-yy');

                            var html = "<div class='gray-box search group'><strong>"+data.datas[i]['file_description']+"</strong><p>"+data.datas[i]['date']+"</p><ul class='short group'><li>"+data.datas[i]['name']+"</li></ul></div>";
                           $(".tab-content-1").append(html);
                        }
                        $(".tab-content-1 .gray-box.search.group").css('display','block');
                    }
                }
            });
        return false;
    });

    $("#searchLinkId").on("submit", function () {
        $.ajax({
            type: "POST",
            url: base_url + "projects/searchByData",
            data: $(this).serialize(),
            success: function(msg) {
                var data = $.parseJSON(msg);
                if (data.status) {
                    $(".tab-content-2 .gray-box").css('display','none');
                    for(i=0; i < data.datas.length;i++){
                        var date = data.datas[i]['date'];
                        data.datas[i]['date'] = date.toString('dd-MM-yy');
                        var html = "<div class='gray-box search group'><strong>"+data.datas[i]['name']+"</strong><p>"+data.datas[i]['date']+"</p><ul class='short group'><li><a href='"+data.datas[i]['link']+"' target='_blank' class='simple'>"+data.datas[i]['link']+"</a></li></ul></div>";
                        $(".tab-content-2").append(html);
                    }
                    $(".tab-content-2 .gray-box.search.group").css('display','block');
                }

            }
        });
        return false;
    });


    $(".file_download").on("click", function () {
        var file_name = $(this).children('a').html();
        var job_no = $(this).children('#file_path').val();
        window.location.href = "<?php echo site_url('Projects/file_download') ?>?file_name="+ file_name;
    });

    /* add milestone */
    $(".form_add_milestone").on('submit',function(){
        var __this = $(this);
        $.ajax ({
            url: base_url+"milestones/addMilestone",
            type: 'post',
            data: __this.serialize(),
            dataType: 'text',
            success: function(data){
                location.reload();
            },
            error: function(){
            }
        });
        return false;
    });

    /* Delete milestone */
    $(document).on('click', '.remove-milestone', function(remove){
        var mile_id = $(this).parents('tr').attr('data-id');
        $.ajax ({
            url: base_url+"milestones/remove_milestone",
            type: 'post',
            data: {mile_id:mile_id},
            dataType: 'text',
            success: function(data){
                var __data = JSON.parse(data);
                if(__data.success){
                    location.reload();
                }
            },
            error: function(){
            }
        });
        return false;
    });

    $(document).on('click','.discard_section', function(){
        $('#selectedFile1').empty();
        $('#selectedFiles1').empty();
    })
</script>