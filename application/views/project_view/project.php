<?php
if($project['currency'] == 1) {
    $project['currency'] = "$";
} else if($project['currency'] == 2) {
    $project['currency'] = "&#8364;";
} else if($project['currency'] == 3) {
    $project['currency'] = "&#163;";
}
?>
<div class="content group">
    <div class="main-title group main_title_block"><a href="<?=$_SERVER['HTTP_REFERER']?>" class="dispute_go_back"><i class="fa fa-long-arrow-left"></i></a><?=$project['name'];?></div>
    <div class="project-box group">
        <div class="details group">
            <h3><?=$project['user_type']?'Client':'Developer';?></h3>
            <div class="user-details group">
                <div class="avatar group"><img src="<?=base_url('assets/images/client-image.jpg')?>" alt="#"></div>
                <p><?=$project['username']?></p>
                <?php if($project['type'] == 0) : ?>
                    <small><?=$project['currency'] . " " . $project['hourly_rate'] ?>/hour, <?=$project['hours_per_week']?>h per week</small>
                <?php else : ?>
                    <small class="milestone"><?=$project['currency']  . " " . number_format($project['fixed_price']);?> fix price, <span class="mile_underline"><?=$project['mil_count'];?> milestones</span></small>
                <?php endif; ?>
                <p><a href="<?=base_url();?>user/<?= ($this->session->userdata()['user_type'] == 0) ? md5($project['user_id']) : md5($project['invited_user']) ;?>" class="v-profile">View Profile</a></p>
            </div>
        </div>
        <div class="details-numbers group">
            <h3><?=($user->user_type) ? 'Spent' : 'Earnings'?></h3>
            <p><?=($project['type'] == 1) ? "0 milestone" : "6 hrs, $120"?></p>
            <h3 style="margin-bottom: 0;" class="margin">This week</h3>
            <div class="status-bar group">
                <div class="progress" style="width:0%;"></div>
            </div>
            <small style="color: #b8b8b8;" class="group ">Since <?=date('M d, Y',strtotime($project['pr_create_date']))?></small>
        </div>
        <div class="clear"></div>
        <div class="main-text group">
            <?php if(empty($project['description'])){?>
                <?php //if(empty($type)):?>
                    <button type="button" class="button icon gray ion-plus add-description" data-toggle="modal" data-target="#popup_add_description">Add description</button>
                <?php //endif; ?>
            <?php } else { ?>
                <p><?=$project['description']?></p>
            <?php } ?>
        </div>
        <?php if($project['st'] != 2){?>
        <div>
            <?php if ($user->user_type){ ?>
                <div class="f-left">
                    <a href="#" class="button icon gray ion-plus">Add Funds</a>
                    <a href="#" class="button icon gray ion-pause">Pause Project</a>
                </div>
            <?php  } else { ?>
                <div class="f-left">
                    <button class="button icon gray ion-android-stopwatch" data-toggle="modal" data-target="#trackerModal"> Open Time Tracker</button>
                </div>
            <?php }  ?>
            <div class="f-right"><a href="#" data-id="<?=$project['pr_id'];?>" class="button <?=((int)$project['st']==2)?'finishProject':'';?>" id="endProjectId">End Project</a></div>
        </div>
        <?php }  ?>
        <?php if(!empty($project['milestones'])){ ?>
        <div class="full_width">
            <h3>Milestones</h3>
            <table class='milestone-list'>
                <?php $i=0; foreach($project['milestones'] as $milestone) { $i++ ?>
                    <tr data-id="<?=$milestone->id?>" class="mile_row">
                        <td class="number_milestone"><?=$i;?></td>
                        <td class="none_border">
                            <h2 class="title_miles">
                                <?=$milestone->name;?>
                                <span>-</span>
                                $<?=$milestone->price?>
                                <input type="checkbox" class="check_checkbox" id="chek_item<?=$milestone->id;?>" name="chek_item" >
                                <label class="label_check_checkbox" for="chek_item<?=$milestone->id;?>"></label>
                            </h2>
                            <span><?=$milestone->description;?></span>
                        </td>
                        <td class="delete_el_miles"><a><!--<span class="remove-milestone"><i class="ion-ios-close"></i></span>--></a></td>
                    </tr>
                <?php } ?>
            </table>
            <div class="clear"></div>
            <div class="f-left add_milestone" style="margin-top: 15px;">
                <p class="icon ion-plus-round addMilestones" data-toggle="modal"
                   data-target="#milestoneModal"> Add Milestone</p>
            </div>

        </div>
        <?php } ?>
        <div class="clear"></div>
    </div>

    <div class="project_discussion">
        <div class="project-comments group">
            <h3>Discussion</h3>
            <?php if($project['st'] != 2){?>
            <div class="insert-comment group">
                <div class="input group">
                    <div class="triangle-bg group"></div>
                    <form action="#" method="post" id="addDisputeFormId">
                        <span id="comment_error" class="failed name_field_required" style="display: none">Please, enter the text.</span>
                        <textarea name="message" class="form-control" placeholder="Add Comment..." ></textarea>
                        <div class="comment upload_files_grey_div">
                            <div class="input group">
                                <div id="message_file">

                                </div>
                            </div>
                        </div>
                        <input type="text" name="status" value="1" class="hidden" id="disputStatus" />
                        <input type="text" name="project_id" id="project_id" value="<?=$project['pr_id'];?>" class="hidden" />
                        <div class="clear"></div>
                        <!--<div class="col-md-12 no-padding">
                            <div class="uploadField choose_file">
                                <span>Upload Files </span>
                                <input type="file" id="files" name="job_attachment[]" class="filesListUpload" multiple>
                            </div>
                            <div id="selectedFiles"></div>
                        </div>-->
                        <button type="button" class="upload_files_btn" data-toggle="modal" data-target="#uploadFiles_modals"> Upload Files</button>
                        <div id=""></div>
                        <ul>
                            <li><div class="checkbox checked " id="disputePublic">Public</div></li>
                            <li><div class="checkbox disputePrivate" id="disputePrivate">Private</div></li>
                            <li><button type="submit" class="button gray">Post</button></li>
                        </ul>
                    </form>
                </div>
            </div>

            <?php } if(isset($comments)) :?>
                <div class="comment_block comments_block_border">
                    <?php foreach($comments as $comment) {
                        if ($comment['status']) {
                            $comment_class = "comment group";
                        } else {
                            $comment_class = "comment representative group";
                        }
                        ?>
                        <?php if($comment['status'] || ($this_user == $comment['user_id']) && !$comment['status'] ):?>
                            <div class="<?=$comment_class?>">
                                <div class="input group">
                                    <div class="upload_files_content">
                                        <?php if($comment['files']):?>
                                            <?php foreach($comment['files'] as $f):?>
                                                <div class="comment_upload_files">
                                                    <?php if($f['type'] == 'image/jpeg' || $f['type'] == 'image/jpg' || $f['type'] == 'image/png' || $f['type'] == 'image/gif'):?>
                                                        <a href="<?=base_url();?>files/download?file=<?=$f['path'];?>&name=<?=$f['name'];?>">
                                                            <img src="<?=base_url();?><?=$f['path']?>" alt="<?=base_url();?><?=$f['name']?>" title="<?=$f['name']?>" />
                                                        </a>
                                                    <?php else: ?>
                                                        <a class="upload_files_name" href="<?=base_url();?>files/download?file=<?=$f['path'];?>&name=<?=$f['name'];?>"><?=$f['name']?></a>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                    <p><?=$comment['message'];?></p>
                                </div>
                                <div class="user group">
                                    <ul>
                                        <li>
                                            <img src="<?=base_url($comment['path'])?>" alt="#">
                                        </li>
                                        <li>
                                            <strong><?=$comment['username'];?></strong><?=$comment['date'];?>
                                        </li><!--5 hours ago-->
                                    </ul>
                                </div>
                            </div>
                        <?php endif;?>
                    <?php } ;?>
                </div>
            <?php endif;?>

        </div>
    </div>
    </div>
    <div class="sidebar group">
        <div class="main-title group">
            <ul>
                <li><a href="javascript:void(0);" id="tab-link-1" class="active">Files</a></li>
                <li><a href="javascript:void(0);" id="tab-link-2" class="">Links (<?=$link['count'][0]['lc'];?>)</a></li>
            </ul>
        </div>
        <div class="files-links group">
            <div class="tab-content-1 group display">
                <div class="search-box group">
                    <form method="post" action="#" id="searchFileId">
                        <input type="text" class="hidden" value="file" name="type" />
                        <input type="text" name="search" placeholder="Search">
                        <button type="submit" name="submit"><i class="ion-search"></i></button>
                    </form>
                </div>
                <div class="link group"><a href="#"   data-toggle="modal" data-target=".popup_upload_files"  class="button gray">Upload</a></div>
                <div class="clear"></div>
                <div class="right_search_block">
<!--                    <div class="right_search_header">-->
<!--                        <p>Today</p>-->
<!--                    </div>-->
                    <div class="right_block_content">
                        <?php if(isset($files) && !empty($files)):?>
                            <?php foreach($files as $k => $file) { ?>
                                <div class="right_block_row">
                                    <div class="right_block_cols upload_name_block">
                                        <?php if($file['status']):?>
                                                <a href="javascript:void (0)">
                                                <span><i class="fa fa-lock"></i></span>
                                            <?php else: ?>
                                                <a href="<?=base_url();?>files/download?file=<?=$file['path'];?>&name=<?=$file['name'];?>">
                                            <?php endif; ?>
                                            <?=$file['name'];?>
                                        </a>
                                        <input type="hidden" id="file_path" value="<?=md5($file['user_id'])?>">
                                    </div>
                                    <div class="right_block_cols mb_cols">
                                        <a href="javascript:void (0)"><?=round((int)$file['size']/125);?>mb</a>
                                    </div>
                                    <div class="right_block_cols date_cols">
                                        <a href="javascript:void (0)"><?=$file['date1']?></a>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php endif ;?>
                    </div>
                </div>
<!--                <div class="right_search_block">-->
<!--                    <div class="right_search_header">-->
<!--                        <p>This Week</p>-->
<!--                    </div>-->
<!--                    <div class="right_block_content">-->
<!--                        <div class="right_block_row">-->
<!--                            <div class="right_block_cols upload_name_block">-->
<!--                                <a href="javascript:void (0)">Homepage.psd</a>-->
<!--                            </div>-->
<!--                            <div class="right_block_cols mb_cols">-->
<!--                                <a href="javascript:void (0)">30mb</a>-->
<!--                            </div>-->
<!--                            <div class="right_block_cols date_cols">-->
<!--                                <a href="javascript:void (0)">20m ago</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="clear"></div>
            </div>
            <div class="tab-content-2 group">
                <div class="search-box group">
                    <form method="post" action="#" id="searchLinkId">
                        <input type="text" class="hidden" value="link" name="type" />
                        <input type="text" name="search" placeholder="Search">
                        <button type="submit" name="submit"><i class="ion-search"></i></button>
                    </form>
                </div>
                <div class="link group"><a href="#" class="button gray"  data-toggle="modal" data-target="#addLinkModalId">Add</a></div>
                <div class="clear"></div>
                <?php if(isset($link['list']) && !empty($link['list'])) { ?>
                    <?php foreach ($link['list'] as $link ) { ?>
                        <div class="gray-box group">
                            <strong><?=$link['name'];?></strong>
                            <p><?=date("d M,Y",strtotime($link['date']));?></p>
                            <ul class="short group">
                                <li><a href="<?=$link['link'];?>" target="_blank" class="simple"><?=$link['link'];?></a></li>
                            </ul>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>

    </div>




<!-- Modal Add Upload  -->
<div class="modal fade popup_upload_files" id="uploadFilesId" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><a href="javascript:void(0);"><i class="ion-close-round"></i></a></button>
                <h4 class="modal-title">Upload Files</h4>
            </div>
            <div class="modal-body">
                <div class="middle group">
                    <form id="fileupload" action="<?=base_url();?>projects/upload" method="POST" enctype="multipart/form-data">
                        <div class="fileupload_zone">
                            <div class="clear"></div>
                            <input type="text" class="hidden" name="project_id" value="<?=$project['pr_id'];?>"/>
                            <div class="clear"></div>
                            <div class="drop-files group fileupload-buttonbar">
                                <p class="drop_info">Drop multiple files here or</p>
                                <div class="uploadField choose_file">
                                    <span>Select Files</span>
                                    <input type="file" id="files" name="project_attachment[]" class="filesListUpload2" multiple>
                                </div>
                                <div class="scrollUloadImg">
                                    <div id="selectedFile1">
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding">
                                <div class="select_milestone_content">
                                    <label>Select Milestone</label>
                                    <div class="select_milestone_div">
                                        <select class="select_milestone" name="milestone">
                                             <option value="">Select Milestone</option>
                                             <?php foreach($project['milestones'] as $mil ):?>
                                                 <option value="<?=$mil->id?>"><?=$mil->name?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding">
                                <?php if(!$type):?>
                                    <div class="lock_checkbox_item">
                                        <input type="checkbox" class="lock_checkbox_select" name="check_is_locked" id="lock_file">
                                        <label for="lock_file">Lock files until payment released</label>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="clear"></div>
                           <div class="btns_section">
                            <div class="f-left"><a href="#" class="button discard_section">Discard</a></div>
                            <div class="buttons group fileupload-buttonbar">
                                <div class="f-right">
                                    <button type="submit" class="button green start">
                                        <span>Upload</span>
                                    </button>
                                </div>
                            </div>
                       </div>
                       <div class="clear"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal  Upload Files In Comments -->
<div class="modal fade" id="uploadFiles_modals" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><a href="javascript:void(0);"><i class="ion-close-round"></i></a></button>
                <h4 class="modal-title">Upload Files</h4>
            </div>
            <div class="modal-body">
                <div class="middle group">
                    <form  id="fileupload2" action="#" method="POST" enctype="multipart/form-data">
                        <div class="fileupload_zone">
                            <div class="clear"></div>
                            <input type="text" class="hidden" name="project_id" value="<?=$project['pr_id'];?>"/>
                            <div class="clear"></div>
                            <div class="drop-files group fileupload-buttonbar">
                                <p class="drop_info">Drop multiple files here or</p>
                                <div class="uploadField choose_file">
                                    <span>Select Files</span>
                                    <input type="file" id="files2" name="project_attachment[]" class="filesListUpload1" multiple>
                                </div>
                                <div id="loading_comment_upload" style="display: none"><img src="<?=base_url()?>assets/images/loading.gif"></div>
                                <div class="scrollUloadImg">
                                    <div id="selectedFiles1"></div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                        <div class="btns_section">
                            <div class="f-left"><a href="#" class="button discard_section">Discard</a></div>
                            <div class="buttons group fileupload-buttonbar">
                                <div class="f-right">
                                    <button type="submit" class="button green start">
                                        <i class="fa fa-plus"></i>
                                        <span>Add</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Add Link  -->
<div class="modal fade popup" id="addLinkModalId" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Link</h4>
            </div>
            <div class="modal-body">
                    <div class="middle group">
                        <form method="post" action="#" id="addLinkFormId">
                            <input type="text" name="name" placeholder="Name the link" autocomplete="off">
                            <input type="text" name="link" value="http://">
                            <input type="text" class="hidden" name="project_id" value="<?=$project['pr_id'];?>"/>
                            <div class="clear"></div>
                            <div class="f-left"><a href="#" class="button discard_section">Discard</a></div>
                            <div class="f-right"><button type="submit" class="button green">Add Link</button></div>
                        </form>
                    </div>
            </div>
        </div>
    </div>
</div>
<!--<section class="popup group upload-link" id="addLinkModalId">-->
<!--    <div class="top group">-->
<!--        <h1>Add Link</h1>-->
<!--        <a href="javascript:void(0);" id="close-link"><i class="ion-close-round"></i></a>-->
<!--    </div>-->
<!--</section>-->

<!-- Add Milestone Modal -->
<div class="modal fade" id="milestoneModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" class="form_add_milestone">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin-top: -2px;display: inline-block">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Milestone</h4>
                </div>
                <div class="modal-body">
                    <input type="text" name="name" class="milestone_title milestone_fields" placeholder="Title"/>
                    <input type="hidden" id="mile_project_id" name="project_id" value="<?=$project['pr_id']?>" />
                    <textarea class="milestone_description milestone_fields" name="description" placeholder="Description"></textarea>
                    <span id="milestone_error" class="red" style="display: none">The milestone's fee should not be more than the fixed price</span>
                    <input type="number" name="price" class="milestone_price milestone_fields" placeholder="Value"/>
                    <button type="button" class="btn btn-primary milestone_discard_btn" data-dismiss="modal" aria-label="Close">Discard</button>
                    <button type="submit" class="btn btn-primary add-milestone-button">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Add Project Description -->
<div class="modal fade" id="popup_add_description" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><a href="javascript:void(0);"><i class="ion-close-round"></i></a></button>
                <h4 class="modal-title"> Add description</h4>
            </div>
            <form method="post" action="<?=base_url('projects/add_description')?>">
                <div class="modal-body">
                    <input type="hidden" name="projects" value = "<?=$project['pr_id']?>" />
                    <textarea class="description" id="fixed-project-description" name="description"></textarea>

                    <div class="f-left"><button class="button icon gray " data-dismiss="modal">Discard</button></div>
                    <div class="f-right"> <button class="button icon gray ion-plus" type="submit"  >Add</button></div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Time Tracker Modal -->
<div class="modal fade" id="trackerModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="logo_section">
                    <a href="#"><img src="<?=base_url();?>assets/images/logo_time_tracker.png" /></a>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div id="time_tracker_page">
                    <div class="no-padding container wrapper">
                        <div class="content">
                            <form action="<?=base_url();?>projects/<?=md5($project['pr_id']);?>" method="post" enctype="multipart/form-data">
                                <div class="form_section">
                                    <div class="col-md-6 no-padding">
                                        <?php if(isset($milestones) && !empty($milestones)):?>
                                            <p class="select_title">Select a milestone</p>
                                            <input type="hidden" name="project_id" value="<?=$projects_id?>" />
                                            <select name="milestone_id" class="contractProject">
                                                <option value="NULL">Select a milestone</option>
                                                <?php foreach($milestones as $mil) { ?>
                                                    <option value="<?=$mil->id;?>"><?=$mil->name;?></option>
                                                <?php } ?>
                                            </select>
                                        <?php else:?>
                                            <p class="select_title">Project name</p>
                                            <input type="hidden" name="project_id" value="<?=$projects_id?>" />
                                            <button class="button gray" data-toggle="modal" data-target="#trackerModal"><?=$projects?></button>
                                        <?php endif; ?>
                                    </div>
                                    <div class=" col-md-6 counter_hrs_min_section">
                                        <p class="select_title">Enter Time</p>
                                        <input id="input-number-mod" class="mod" name="hr" min="0" max="24" type="number" placeholder="00" />
                                        <span>hrs</span>
                                        <input id="input-number-mod2" class="mod" name="min" min="0" max="59" type="number" placeholder="00" />
                                        <span>min</span>
                                    </div>
                                    <div class="description form-group">
                                        <textarea  placeholder="Add Description" rows="4" id="description_time_tracker" name="description" class="form-control"></textarea>
                                    </div>
                                    <div class="col-md-12 no-padding">
                                        <div class="uploadField choose_file">
                                            <span>Upload </span>
                                            <input type="file" id="files" name="job_attachment[]" class="filesListUpload3" multiple>
                                        </div>
                                        <div id="selectedFile3"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="btn_work">
                                    <input type="submit" value="Submit Work" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".select_milestone").select2();
</script>
