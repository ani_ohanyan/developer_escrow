<div class="content group">
    <div class="main-title group main_title_block">New Project</div>
    <div class="project-box group">
        <form id="fileupload" action="<?= base_url() ?>projects/create_project" method="post" enctype="multipart/form-data">
            <h3>Project Type</h3>
            <input id="project_type" type="hidden" name="type" value=""/>
            <ul class="project-type group">
                <li><a data-value="1" class="checkbox project_type active" id="hourly_rate">Hourly Work</a></li><!--href="javascript:void(0);" -->
                <li><a data-value="2" class="checkbox project_type" id="fixed_price">Fixed-Price Project</a></li><!--href="javascript:void(0);" -->
                <li><span class="question" data-toggle="tooltip" data-placement="right" data-html="true" title="Choose Hourly Work to set the fee that will be paid for each hour of work performed. <br> or <br>
                    Choose fixed-Price Project to set the fee that will be paid for a defined scope of work regardless of the cost or effort to deliver it.">(?)</span></li>
            </ul>
            <div class="hourly-div group display tab-content-1">
                <h3>Project Name</h3>
                <span class="failed name_field_required"> The Name field is required</span>
                <input class = "name" id="hourly-project-name" type="text" name="name" value="<?= set_value('name') ?>">
                <div class="left-box group ">
                    <h3>Project Description</h3>
                    <ul class="group hourly_rate_section">
                        <li><a href="javascript:void(0);" id="link-desc-client" class="checkbox active"><?=($user_type) ? 'Provider To Provide' : 'Client To Provide'?></a></li>
                        <li><a href="javascript:void(0);" id="link-desc-me" class="checkbox">I'll Add</a></li>
                    </ul>
                </div>
                <div class="desc-me group">
                    <textarea class = "description" id = "hourly-project-description" name="description"><?= set_value('description') ?></textarea>
                    <div class="fileupload-buttonbar">
                        <div class="">
                            <!--<a href="#" id="open-upload-files" > Upload files </a>-->
                                <!--<div class="uploadField choose_file no-padding">
                                    <a class="button gray">Upload Files</a>
                                    <input type="file" id="projectFileIds" name="project_attachment[]" class="filesListUpload" multiple>
                                </div>
                                <div id="selectedFiles"></div>-->
                            <button type="button" class="upload_files_btn" data-toggle="modal" data-target="#uploadFiles_modals"> Upload Files</button>
                            <div id="new_project_file"></div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <?php  if( $user_type == 1) { ?>
                    <h3>Invite Freelancer</h3>
                <?php }else { ?>
                    <h3>Invite Client</h3>
                <?php } ?>
                <ul class="invite-freelancer group">
                    <span class="failed freelancer_field_required"> The Freelancer field is required</span>
                    <li><input type="text" placeholder="Enter email address" name="freelancer"> <img class="load_freelancer" src="<?=base_url()?>assets/images/load_freelancer.gif"></li>
                    <span class="invalid_email"></span>
                </ul>
                <div class="clear"></div>
                <div class="hourly_rate_section">
                    <div class="left-box group">
                        <h3>Hourly Rate</h3>
                        <input type="text" name="hourly_rate" value="">
                        <span class="failed h_rate_field_required"> The Hourly Rate field is required</span>
                    </div>
                    <div class="left-box group">
                        <h3>Hours Per Week</h3>
                        <input type="text" name="hours_per_week" value="">
                        <span class="failed h_week_field_required"> The Hours Per Week field is required</span>
                    </div>
                    <div class="left-box group">
                        <h3>Currency</h3>
                        <input type="hidden" id="currency" name="currency" value="1"/>
                        <ul class="group">
                            <li><a  data-value="1" href="#" class="checkbox currency_type active">USD</a></li>
                            <li><a  data-value="2" href="#" class="checkbox currency_type">EUR</a></li>
<!--                            <li><a  data-value="3" href="#" class="checkbox currency_type">GBP</a></li>-->
                        </ul>
                    </div>
                </div>
                <div class="milestones group fixed_price_section">
                    <div class="left-box group">
                        <h3>Fixed Price</h3>
                        <span class="failed fixed_price_field_required"> The Fixed Price field is required</span>
                        <input type="number" name="fixed_price" placeholder="0"  id="fixed_price_input" >
                    </div>
                    <div class="left-box group">
                        <h3>Currency</h3>
                        <ul class="group">
                            <li><a  data-value="1" href="#" class="checkbox currency_type active">USD</a></li>
                            <li><a  data-value="2" href="#" class="checkbox currency_type">EUR</a></li>
<!--                            <li><a  data-value="3" href="#" class="checkbox currency_type">GBP</a></li>-->
                        </ul>
                    </div>
                    <div class="fixed_price_error clear" style="display: none">The fixed price should not be less than the milestone price</div>
                    <div class="clear"></div>
                    <h3>Milestones</h3>

                    <table class='milestone-list'></table>
                    <div class="left-box group">
                        <input type="hidden" name="milestones" id="milestones-id-hidden">
                        <p class="icon ion-plus-round addMilestones" data-toggle="modal"
                           data-target="#milestoneModal"> Add Milestone</p>
                    </div>
                    <div class="clear"></div>

                </div>
                <div class="clear"></div>
                <p>&nbsp;</p>
                <button type="submit" class="button green big">Start Project</button>
            </form>
        </div>
        <p>&nbsp;</p>
    </div>
</div>

<!-- Modal  Upload Files In Comments -->
<div class="modal fade" id="uploadFiles_modals" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><a href="javascript:void(0);"><i class="ion-close-round"></i></a></button>
                <h4 class="modal-title">Upload Files</h4>
            </div>
            <div class="modal-body">
                <div class="middle group">
                    <form  id="projectFileupload" action="#" method="POST" enctype="multipart/form-data">
                        <div class="fileupload_zone">
                            <div class="clear"></div>
                            <input type="text" class="hidden" name="project_id" value=""/>
                            <div class="clear"></div>
                            <div class="drop-files group fileupload-buttonbar">
                                <p class="drop_info">Drop multiple files here or</p>
                                <div class="uploadField choose_file">
                                    <span>Select Files</span>
                                    <input type="file" id="files2" name="project_attachment[]" class="filesListUpload1" multiple>
                                </div>
                                <div id="loading_comment_upload" style="display: none"><img src="<?=base_url()?>assets/images/loading.gif"></div>
                                <div class="scrollUloadImg">
                                    <div id="selectedFiles1"></div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                        <div class="btns_section">
                            <div class="f-left"><a href="#" class="button discard_section">Discard</a></div>
                            <div class="buttons group fileupload-buttonbar">
                                <div class="f-right">
                                    <button type="submit" class="button green start">
                                        <i class="fa fa-plus"></i>
                                        <span>Add</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="milestoneModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" class="form_add_milestone">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin-top: -2px;display: inline-block">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Milestone</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="milestone_title milestone_fields" placeholder="Title"/>
                    <textarea class="milestone_description milestone_fields" placeholder="Description"></textarea>
                    <span id="milestone_error" class="red" style="display: none">The milestone's fee should not be more than the fixed price</span>
                    <input type="number" class="milestone_price milestone_fields" placeholder="Value"/>
                    <button type="button" class="btn btn-primary milestone_discard_btn" data-dismiss="modal" aria-label="Close">Discard</button>
                    <button type="button" class="btn btn-primary add-milestone-button">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>