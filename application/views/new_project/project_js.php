<script type="text/javascript">
    $(document).ready(function() {
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
     /*if ( $(this).width() > 768 ) {
            $(function() {
                //$('section.what-you-get figure').matchHeight();
            });
        };*/
    });
    $("#hourly_rate").on("click",function(){
        $(".hourly_rate_section").css('display','block');
        $(".fixed_price_section").css('display','none');
        $(".desc-me").css('display','none');
        $("#project_type").val(0);
    });
    $("#fixed_price").on("click",function(){
        $(".fixed_price_section").css('display','block');
        $(".hourly_rate_section").css('display','none');
        $(".desc-me").css('display','block');
        $("#project_type").val(1);
    });

    $('.project_type').on('click',function(e){
        e.preventDefault();
        $('.project_type' ).removeClass( 'active' );
        $(this).addClass('active');
    });

    $('.currency_type').on('click',function(e){
        e.preventDefault();
        $('.currency_type' ).removeClass('active');
        $(this).addClass('active');
        var data = $(this).data('value');
        $('#currency').val(data);
        var currency = data;
        if(currency == 1) {
            currency = "$";
        } else if(currency == 2) {
            currency = "&#8364;";
        } else if(currency == 3) {
            currency = "&#163;";
        }
        $(".milListTr" ).each(function() {
                $(this).find('.mile_currency_type').html(currency);

        });
    });


    $('a#link-desc-client').click(function(e) {
        e.preventDefault();
        $(this).addClass('active');
        $('a#link-desc-me').removeClass('active');
        //$('.desc-me').removeClass('display');
        $(".desc-me").css('display','none');
    });

    $('a#link-desc-me').click(function(e) {
        e.preventDefault();
        $(this).addClass('active');
        $('a#link-desc-client').removeClass('active');
        $(".desc-me").css('display','block');
    });
        /*$('#fileupload').change(function (e) {
            var _this = $(this);
            var filename = e.target.files;
            $.each(filename, function(key,value) {
                var name = value.name + "<br>";
                _this.next().append(name);
            });
        });*/
        /*$('#fileupload').fileupload({
            xhrFields: {withCredentials: true},
            url: base_url+'Projects/file_upload',
            done: function (e, data) {
                if(data.result != '') {
                    $('#file_id_valie').val(data.result.file_id);
                    $('#file_uniq_id').val(data.result.file_uniqid);
                }

            }
        });
        */
    $(document).on("click",".milestones_popup .milestone-list li", function() {
        $(".milestones_popup .milestone-list li").removeClass("active");
        $(this).addClass("active");
    });

    $(document).on("click",".remove-milestone", function() {
        $(this).parents("li").fadeOut("fast",function() {
            $(this).remove();
        });
    });

    $("#fileupload").on('submit',function(){
        event.preventDefault();
        var __this = $(this);
        var action = $(this).attr('action');
        var name = __this.find('input[name="name"]').val();
        var description = __this.find('textarea[name="description"]').val();
        var freelancer = __this.find('input[name="freelancer"]').val();
        var currency = $('#currency').val();

        var form_data = new FormData();
//        for(i=0; i < $(".project_files_input").prop('project_file').length; i++){
//            form_data.append("project_attachment[]", document.getElementById('projectFileIds').files[i]);
//        }

        form_data.append('type',$("#project_type").attr('value') );
        form_data.append('name',name );
        form_data.append('description',description );
        form_data.append('freelancer',freelancer );
        form_data.append('currency',currency );
        files = new Array();
        $('.project_upload_files').each(function() {
            file = new Object();
            file.file =  $(this).find('.project_files').attr("value");
            file.name = $(this).find('.project_files_name').attr("value");
            file.path = $(this).find('.project_files_path').attr("value");
            files.push(file);
        });
        form_data.append("files",JSON.stringify(files));
        console.log(form_data);

        milestones = new Array();
        $( ".milList" ).each(function() {
            mile = new Object();
            mile.name =  $(this).find('.name_milestone').attr("value");
            mile.description = $(this).find('.description_milestone').attr("value");
            mile.price = $(this).find('.price_milestone').attr("value");
            milestones.push(mile);
        });
        form_data.append("milestones",JSON.stringify(milestones));
        if($("#project_type").attr("value")){
            var fixed_price = __this.find('input[name="fixed_price"]').val();
            form_data.append('fixed_price',fixed_price );
            /*if(fixed_price == ''){
             $('.fixed_price_field_required').show();
             $('.fixed_price_field_required').css('display','block');
             return;
             }else{
             $('.freelancer_field_required').hide();
             }*/
        } else {
            var hourly_rate = __this.find('input[name="hourly_rate"]').val();
            var hours_per_week = __this.find('input[name="hours_per_week"]').val();
            form_data.append('hourly_rate',hourly_rate );
            form_data.append('hours_per_week',hours_per_week );
            /* if(hourly_rate == ''){
             $('.h_rate_field_required').show();
             $('.h_rate_field_required').css('display','block');
             return;
             }else{
             $('.h_rate_field_required').hide();
             }
             if(hours_per_week == ''){
             $('.h_week_field_required').show();
             $('.h_week_field_required').css('display','block');
             return;
             }else{
             $('.h_week_field_required').hide();
             }*/
        }
        if(name == ''){
            $('.name_field_required').show();
            $('.name_field_required').css('display','block');
            return;
        } else {
            $('.name_field_required').hide();
        }
        if(freelancer == ''){
            $('.freelancer_field_required').show();
            $('.freelancer_field_required').css('display','block');
            return;
        } else{
            $('.freelancer_field_required').hide();
        }
            $('.load_freelancer').show();
            $.ajax ({
                url: action,
                type: 'post',
                data: form_data,
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                success: function(data){
                    $('.load_freelancer').hide();
                    var __data = JSON.parse(data);
                    if(__data.success){
                        __this.find('input').val('');
                        __this.find('textarea').val('');
                        $('.invalid_email').css('display','none');
                        window.location.href = base_url+'projects';
                    }else{
                        $('.invalid_email').css('display','block');
                        $('.invalid_email').html(__data.message);
                        setTimeout(function(){ $('.invalid_email').css('display','none'); }, 3500);

                    }
                },
                error: function(){

                }
            });
            return false;
    });

    var countMil = 0;
    var milestome_sum = 0;

    $(document).on("click", ".add-milestone-button", function() {
        var milestone_name = $(".milestone_title").val();
        var milestone_description = $(".milestone_description").val();
        var milestone_price = $(".milestone_price").val();
        var currency = $('#currency').val();
        if(currency == 1) {
            currency = "$";
        } else if(currency == 2) {
            currency = "&#8364;";
        } else if(currency == 3) {
            currency = "&#163;";
        }
        countMil++;
        var html_li = '<tr class="milListTr" data-id="'+countMil+'"><td class="number_milestone">'+countMil+'</td><td class="none_border"><input id="m'+countMil+'" type="hidden" value="'+milestone_price+'"><h2 class="title_miles"><span class="html_title_miles">'+milestone_name+' </span><span>-</span><span class="mile_currency_type">'+currency+'</span><span class="html_price_miles">'+milestone_price+'</span></h2><span class="html_descr_miles">'+milestone_description+'</span></td><td class="delete_el_miles"><a><i class="fa fa-pencil edit_milestone" data-toggle="modal" data-target="#milestoneModal" data_id="'+countMil+'"></i><i class="ion-ios-close remove-milestone" data_id="'+countMil+'"></i></a></td></tr>';
        var data_html = '<div class="milList" data_id="'+countMil+'"><input type="hidden" class="name_milestone" name="milestones['+countMil+'][name]" value="'+milestone_name+'">\n\
        <input type="hidden" name="milestones['+countMil+'][description]" class="description_milestone" value="'+milestone_description+'">\n\
        <input type="hidden" name="milestones['+countMil+'][price]" class="price_milestone" value="'+milestone_price+'"></div>';
        $(html_li).appendTo(".milestones .milestone-list");
        $(data_html).appendTo(".milestones .milestone-list");
        $("#milestoneModal").modal('hide');
        milestome_sum += +$('.milestone_price').val();

        setTimeout(function(){
            $("#search-milestone").val('');
            $(".milestones_popup .milestone-list").empty();
            $(".milestones_popup .milestone-list li").removeClass("active");
        },500);
    });

    $(document).on("click", ".edit_mile_btn", function() {
        var milestone_name = $(".milestone_title").val();
        var milestone_description = $(".milestone_description").val();
        var milestone_price = $(".milestone_price").val();
        var currency = $('#currency').val();
        if(currency == 1) {
            currency = "$";
        } else if(currency == 2) {
            currency = "&#8364;";
        } else if(currency == 3) {
            currency = "&#163;";
        }
        var data_id = $(this).attr('data_id');
        $(".milList" ).each(function() {
            if($(this).attr('data_id') == data_id){
                $(this).find('.name_milestone').attr('value', milestone_name);
                $(this).find('.description_milestone').attr('value', milestone_description);
                $(this).find('.price_milestone').attr('value', milestone_price);
            }
        });
        $(".milListTr" ).each(function() {
            if($(this).attr('data-id') == data_id){
                $(this).find('.html_title_miles').html(milestone_name);
                $(this).find('.html_descr_miles').html(milestone_description);
                $(this).find('.html_price_miles').html(milestone_price);
            }
        });

        $("#milestoneModal").modal('hide');
    });

    $(document).on("click", ".edit_milestone", function() {
        var milestone_title = $(this).parents('tr').find('.html_title_miles').html();
        var milestone_price = $(this).parents('tr').find('.html_price_miles').html();
        var milestone_descr = $(this).parents('tr').find('.html_descr_miles').html();
        var data_id = $(this).attr('data_id');
        $('.add-milestone-button').html('Save');
        $('.add-milestone-button').addClass('edit_mile_btn');
        $('.add-milestone-button').removeClass('add-milestone-button');
        $('.edit_mile_btn').attr('data_id', data_id);
        $('.milestone_title').val(milestone_title);
        $('.milestone_description').val(milestone_descr);
        $('.milestone_price').val(milestone_price);
        $('#milestoneModal').show();

    });

    $('#milestoneModal').on('hidden.bs.modal', function () {
        $('.milestone_price').removeClass('border_red');
        $('#milestone_error').hide();
    })
//    checking milestone values
    $('.add-milestone-button').attr("disabled", true);
    $('.milestone_price').on('keyup  change', function(){
        if($('.milestone_price ').val() == '' || $('.milestone_price ').val() == '0'){
            $('.add-milestone-button').attr("disabled", true);
            $('.milestone_price').addClass('border_red');
        } else {
            $('.add-milestone-button').attr("disabled", false);
            $('.milestone_price').removeClass('border_red');
            $('#milestone_error').hide();
        }
        if($('#fixed_price_input').val() != ''){
            var milestone = $(this).val();
            var price = $('#fixed_price_input').val();
            if(+milestone+milestome_sum > +price){
                $('#milestone_error').show();
                $('.add-milestone-button').attr("disabled", true);
                $('.milestone_price').addClass('border_red');
            } else {
                $('#milestone_error').hide();
                $('.add-milestone-button').attr("disabled", false);
                $('.milestone_price').removeClass('border_red');
            }
        }
    })
    $('#fixed_price_input').on('keyup  change', function(){
        var price = $(this).val();
        if((+price < milestome_sum || +price == 0) && price != '' ){
            $('#fixed_price_input').addClass('border_red');
            $('.fixed_price_error').show();
            $('#fileupload .button').attr("disabled", true);
        } else {
            $('#fixed_price_input').removeClass('border_red')
            $('#fileupload .button').attr("disabled", false);
            $('.fixed_price_error').hide();
        }
    })


var i =0;

    /* Delete milestone */
    $(document).on('click', '.remove-milestone', function(remove){
        var _this = $(this);
        var id = $(this).attr('data_id');
        var m = +$('#m'+id).val();
        milestome_sum -= m;
        _this.parents('tr').remove();
        $('.milestone-list tr').each(function(){
            i++
            $(this).find('.number_milestone').text(i);
        })
        i = 0;
        countMil--
    });

    /* Empty modal fields */
    $(document).on('click', '.addMilestones', function(remove){
        $('.milestone_fields').val('');
        $('.edit_mile_btn').html('Add');
        $('.edit_mile_btn').addClass('add-milestone-button');
        $('.edit_mile_btn').removeClass('edit_mile_btn');

    });


    $("#projectFileupload").on('submit',function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('#loading_comment_upload').show();
        var form_data = new FormData();
        for(i=0; i < $("#files2").prop('files').length; i++){
            form_data.append("project_attachment[]", document.getElementById('files2').files[i]);
        }
        $.ajax({
            url: base_url + "projects/addProjectFile",
            type: 'post',
            data: form_data,
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                var data = $.parseJSON(data);
                $('#uploadFiles_modals').modal('hide');
                $('#uploadFiles_modals').find('#selectedFiles1').html('');
                $('.upload_files_grey_div').fadeIn();
                var i = 0;
                for(i=0; i<data.name.length ; i++){
                    $('#new_project_file').append('<div class="project_upload_files">' +
                        '<a class="upload_files_name" href="javascript:void (0)">'+data.name[i]+'<span class="delete_uploaded_file" data_id ="'+data.id[i]+'" path ="'+data.path[i]+'"><i class="fa fa-times"></i>' +
                        '</span></a>' +
                        '<input type="hidden" name="project_file[]" class="project_files" value="'+data.id[i]+'" /><input type="hidden" name="project_file_name[]" class="project_files_name" value="'+data.name[i]+'" /><input type="hidden" name="project_file_path[]" class="project_files_path" value="'+data.path[i]+'" /></div>');
                }
                $('#files2').val('');
                $('#loading_comment_upload').hide();
            },
            error: function () {
            }
        });
    })

    $(document).on('click','.delete_uploaded_file', function(){
        var data = $(this).attr('data_id');
        var path = $(this).attr('path')
        var _this = $(this);
        $.ajax({
            url: base_url + "comments/delete_upload_file",
            type: 'post',
            dataType: 'text',
            data:{delete_file: data, delete_file_path:path},
            success: function (data) {
                if(data){
                    _this.parent().parent().remove();
                }
                if ($('#message_file').children().length == 0){
                    $('.upload_files_grey_div').hide();
                }
            }
        })
    })

    $(document).on('click','.discard_section', function(){
        $('#selectedFiles1').empty();
    })


</script>