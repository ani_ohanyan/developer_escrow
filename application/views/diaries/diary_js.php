<script type="text/javascript">
    $(document).ready(function() {
        $( ".datepicker_calendar" ).datepicker();
    });

    $('#diary_provider_select').on('change', function(){
        $('#diary_project_select').html('');
        var provider_id = $("#diary_provider_select").val();
        $.ajax ({
            url: base_url+"diaries/getProjectByProject",
            type: 'post',
            dataType: 'text',
            data: {provider_id:provider_id},
            success: function(data){
                var data = $.parseJSON(data);
                if(data.status) {
                    $('#diary_project_select').append('<option value="0">No Contract</option>');
                    for(i=0; i < data.projects.length; i++ ){
                        $('#diary_project_select').append('<option value="'+data.projects[i].id+'">'+data.projects[i].name+'</option>');
                    }
                }
            }
        });
        return false;
    })

    $('#search_diaries').on('click', function(){
        var project_id = $("#diary_project_select").val();
        var provider_id = $("#diary_provider_select").val();
        var end_date = $("#search_end_date").val();
        var start_date = $("#search_start_date").val();
        $.ajax ({
            url: base_url+"diaries/diarySearch",
            type: 'post',
            dataType: 'text',
            data: {project_id:project_id, provider_id:provider_id, end_date:end_date, start_date:start_date},
            success: function(data){
                var data = $.parseJSON(data);
                if(data.status) {
                    $('.diaries_page_content').html('');
                    for(i=0; i < data.result.length; i++) {
                        $('.diaries_page_content').append(generateDiaryDiv(data.result[i]).fadeIn(500));
                    }
                } else {
                    $('.diaries_page_content').html('');
                    $('.diaries_page_content').append("<span>This project has no diaries.</span>");
                }
            }
        });
        return false;
    })

    function generateDiaryDiv(data){
        var content = $("<div>", {class: "diaries_sections"});
        content.append('<a class="diaries_section_title" href="'+base_url+'projects/'+data.project_id+'">'+data.name+'</a>').fadeIn(500);
        if(data.milestone){
            content.append('<p class="diaries_section_date" >'+data.date+' <i class="fa fa-clock-o"></i> '+data.time+'</a><span class="milestone" style="font-size: 14px;"> For milestone:</span> <span style="font-size: 14px;" class="mile_underline"> '+ data.milestone+'</span></p>').fadeIn(500);
        } else {
            content.append('<p class="diaries_section_date" >'+data.date+' <i class="fa fa-clock-o"></i> '+data.time+'</a></p>').fadeIn(500);
        }
        content.append('<span class="" >'+data.description+'</span>');
        var design_images_content = $("<div>", {class: "design_images_content"});
        design_images_content.appendTo(content).fadeIn(500);
        for(f=0; f<data.files.length; f++){
            design_images_content.append('<div class="design_images" time="'+data.date+'"><span data_id="'+data.files[f].id+'" data-toggle="modal" data-target="#view_snapshot_modal"><img src="'+base_url+data.files[f].path+'"></span></div>').fadeIn(500)
        }
        return content;
    }

    $('#cr_dispute').on('submit', function(e) {
        e.preventDefault();
        var el = $(this);
        var input = el.find("input");
        var data = $("#cr_dispute").serialize();
        if ($('.other_email').val() == '') {
            $(".error_hide_party").css("display", "block");
            console.log("error")
        } else if ($('#description').val() == '') {
            $(".error_hide_description").css("display", "block");
            console.log("error")
        }
        else if ($('.custom-project').val() == null) {
            $(".error_hide_project").css("display", "block");
            console.log("error")
        }
        else {
            $.ajax({
                type: "POST",
                url: base_url + "disputes/create_dispute",
                data: data,
                success: function (msg) {
                    $.ajax({
                        url: base_url + "notification_sender/send_notification",
                        type: 'post',
                        data: data,
                        success: function (data) {
                            var data = $.parseJSON(data);
                        },
                        error: function () {
                            /*$("#addJobAppId").prop("disabled", false);*/
                        }
                    });
                    var msg = JSON.parse(msg);
                    window.location.href = base_url + "disputes";
                }

            });
        }
    });

    $(".diaries_select").select2();

    /* diary popup */
    $(document).on("click",".design_images", function () {
        $('#view_snapshot_modal').show();
        var src = $(this).find('img').attr('src');
        var title = $(this).parents('.diaries_sections').find('a').text();
        var date = $(this).attr('time');
        $('#view_snapshot_modal .image_block img').attr('src', src);
        $('.diaries_section_title').text(title);
        $('#view_snapshot_modal .diaries_section_date').text(date);
    });


</script>