<div class="content group">
    <div class="payment-div group">
        <div class="main-title uppercase group main_title_block">Diaries</div>
        <div class="diaries_page_blocks">
            <div class="diaries_page_header ">
                <div class="select_provider_div">
                    <p class="select_title">Provider</p>
                    <select class="diaries_select" id="diary_provider_select">
                        <option value="0">Select a provider</option>
                        <?php if(isset($providers) && !empty($providers)):?>
                            <?php foreach($providers as $prov):?>
                                <option value="<?=$prov['id']?>"><?=$prov['username']?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="select_project_div">
                    <p class="select_title">Project</p>
                    <select class="diaries_select" id="diary_project_select">
                        <option>Select a project</option>
<!--                        --><?php //if(isset($projects) && !empty($projects)):?>
<!--                            --><?php //foreach($projects as $proj):?>
<!--                                <option value="--><?//=$proj['id']?><!--">--><?//=$proj['name']?><!--</option>-->
<!--                            --><?php //endforeach; ?>
<!--                        --><?php //endif; ?>
                    </select>
                </div>
                <div class="select_timespan_div">
                    <p class="select_title">Timespan</p>
                    <input type="text" id="search_start_date" class="form-control datepicker_calendar"/>
<!--                    <select class="diaries_select">-->
<!--                        <option> Thu, Jan 28, 2016 - Wed Feb 3, 2016  </option>-->
<!--                        <option> Thu, Jan 28, 2016 - Wed Feb 3, 2016  </option>-->
<!--                        <option> Thu, Jan 28, 2016 - Wed Feb 3, 2016  </option>-->
<!--                    </select>-->
                </div>
                <div class="select_timespan_div select_timespan_div2">
                    <input type="text" id="search_end_date" class="form-control datepicker_calendar"/>
                </div>
                <div class="select_timespan_button">
                    <button type="text" id="search_diaries" class="button gray">Search</button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="diaries_page_content">
<!--                <div class="diaries_sections">-->
<!--                    <a href="#" class="diaries_section_title">Time tracker desktop app design</a>-->
<!--                    <p class="diaries_section_date"> Thu Jan 28 2016. 02:00pm - 03:20pm  <i class="fa fa-clock-o"></i> 1h 20min</p>-->
<!--                    <div class="design_images_content">-->
<!--                        <div class="design_images">-->
<!--                            <a href="javascript:void(0)"  data-toggle="modal" data-target="#view_snapshot_modal">-->
<!--                                 <img src="--><?//=base_url();?><!--assets/images/diaries_design.png" />-->
<!--                            </a>-->
<!--                        </div>-->
<!--                        <div class="design_images">-->
<!--                            <a href="javascript:void(0)"  data-toggle="modal" data-target="#view_snapshot_modal">-->
<!--                                <img src="--><?//=base_url();?><!--assets/images/diaries_design.png" />-->
<!--                            </a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="diaries_sections">-->
<!--                    <a href="javascript:void (0)" class="diaries_section_title">Website design</a>-->
<!--                    <p class="diaries_section_date"> Thu Jan 28 2016. 03:20pm - 04:30pm  <i class="fa fa-clock-o"></i> 1h 10min</p>-->
<!--                    <div class="design_images_content">-->
<!--                        <div class="design_images">-->
<!--                            <a href="javascript:void(0)"  data-toggle="modal" data-target="#view_snapshot_modal">-->
<!--                                <img src="--><?//=base_url();?><!--assets/images/diaries_design.png" />-->
<!--                            </a>-->
<!--                        </div>-->
<!--                        <div class="design_images">-->
<!--                            <a href="javascript:void(0)"  data-toggle="modal" data-target="#view_snapshot_modal">-->
<!--                                <img src="--><?//=base_url();?><!--assets/images/diaries_design.png" />-->
<!--                            </a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="payment-div group">
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="view_snapshot_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h4 class="modal-title">View Snapshot</h4>
            </div>
            <div class="modal-body">
                <p class="diaries_section_title">Time tracker desktop app design 02:50pm</p>
                <p class="diaries_section_date">02:50pm</p>
                <div class="image_block">
                    <img src="<?=base_url();?>assets/images/snapshot_img.png" />
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery-ui.js"></script>
<!--<script type="text/javascript" src="--><?//=base_url()?><!--js/classie.js"></script>-->
<!--<script type="text/javascript" src="--><?//=base_url()?><!--js/flaunt.js"></script>-->
<!--<script type="text/javascript" src="--><?//=base_url()?><!--js/custom.js"></script>-->