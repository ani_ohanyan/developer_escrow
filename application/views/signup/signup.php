<!DOCTYPE html><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Developer Escrow</title>
    <meta name="keywords" content="Developer Escrow"/>
    <meta name="description" content="Developer Escrow"/>
    <meta name="robots" content="index, follow" />
    <link href='assets/webstyle/style.css' rel='stylesheet' type='text/css'>
    <link href='assets/webstyle/reset.css' rel='stylesheet' type='text/css'>
    <link href='assets/webstyle/fonts.css' rel='stylesheet' type='text/css'>
    <link href='assets/webstyle/ionicons.min.css' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon" />
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- get jQuery from the google apis -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link href="assets/webstyle/select2.min.css" rel="stylesheet" />
    <script src="assets/js/select2.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('select.custom-home').select2({
                theme: "classic",
                minimumResultsForSearch: Infinity,

            });
        });
    </script>

</head>
<body class="landing-home gray">

<section class="signup group">
    <div class="internal group">
        <p class="logo"><a href="<?=base_url()?>"><img src="assets/images/developer-escrow-logo.png" alt="Developer Escrow" title="Developer Escrow"></a></p>
        <div class="form group">
            <form method="post" action="<?=base_url("sign-up")?>" id="sign_up_form">
                <select class="custom-home" name="user_type">
                    <option value="0">I'm a Developer</option>
                    <option value="1">I'm a Client</option>
                </select>
                <input type="text" name="username" placeholder="My name is">
                <?= form_error("username", '<div class="form-error">', '</div>') ?>
                <p class="error name"></p>
                <input type="text" name="email" placeholder="My email">
                <p class="error email"></p>
                <input type="password" name="password" placeholder="Password">
                <p class="error password"></p>
                <input type="password" name="r_password" placeholder="Repeat Password">
                <p class="error r_password"></p>

                <p class="error incorrect_credentials"></p>
                <button class="button green" id="signupSubmit">Sign Up  <i class="sinup_icon ion-arrow-right-c"></i></button>
            </form>
        </div>
    </div>
</section>
<section class="have-acc group"><a href="/login">Already have an account?</a></section>

<!--<script type="text/javascript" src="assets/js/custom.js"></script>-->

</body>
</html>

