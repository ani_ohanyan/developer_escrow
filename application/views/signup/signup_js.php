<script type="text/javascript">
    var base_url = "<?=base_url()?>";

    $(document).ready(function() {
        $("#sign_up_form").unbind("submit").on("submit", function (e) {
            e.preventDefault();
            var el = $(this);
            var input = el.find("input");
            var data = el.serialize();
            $.ajax({
                type: "POST",
                url: base_url + "sign-up",
                data: data,
                success: function(msg) {
                    var msg = JSON.parse(msg);
                    el.find("input").css({"border": "1px solid transparent"});
                    el.find(".error").html("");
                    if (msg.validation) {
                        $.each(msg.validation, function (key, value) {
                            console.log(value);
                            el.find("input[name='" + key + "']").css("cssText","border-bottom: 2px solid rgb(255,128,128) !important;");
                            el.find(".error." + key).html(value);
                        });
                    }
                    if (msg.status == "error") {
                        el.find(".incorrect_credentials").html('Incorrect Credentials. Please try again.');
                    } else if (msg.status == "success") {
                        window.location = base_url + "projects";
                    }
                }
            });
            return false;
        });
    });
</script>