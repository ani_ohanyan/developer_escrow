<script type="text/javascript">
    $(document).ready(function() {
        $('select.custom-home').select2({
            theme: "classic",
            minimumResultsForSearch: Infinity,

        });
    });
</script>
<section class=" what-you-get group">
    <div class="group wrapper">
        <h3 class="text-center">What you Get</h3>

        <div class="inside group">
            <figure class="group">
                <div class="icon blue group"><i class="ion-ios-folder-outline"></i></div>
                <strong>Security of Code & Creative Assets</strong>

                <p class="icons_info_text">We make sure that code and creative assets are kept secure for both parties, ensuring there is always
                    a copy of the work.</p>
            </figure>

            <figure class="last group">
                <div class="icon green1 group"><i class="ion-ios-checkmark-outline"></i></div>
                <strong>Review of Code & Creative</strong>

                <p class="icons_info_text">We review code and creative each week, for each project ensuring that, there is a correlation between
                    billings and work.</p>
            </figure>

            <div class="clear"></div>

            <figure class="group">
                <div class="icon red group"><i class="ion-ios-reverse-camera-outline"></i></div>
                <strong>Work Insight & Snapshots</strong>

                <p class="icons_info_text">Our expert team can give feedback on your projects, and our desktop app allows work snapshots, if
                    required.</p>
            </figure>

            <figure class="last group">
                <div class="icon violet group"><i class="ion-ios-locked-outline"></i></div>
                <strong>Security of Funds </strong>

                <p class="icons_info_text">Clients lodge funds with us, and these are disbursed, through the preferred method, to developers
                    weekly, upon review.</p>
            </figure>

            <div class="clear"></div>

            <figure class="group">
                <div class="icon green group"><i class="ion-ios-list-outline"></i></div>
                <strong>Flexible Outpayment Options</strong>

                <p class="icons_info_text">DeveloperEscrow can outpay through PayPal, directly into bank accounts, or we supply and top up
                    pre-paid cards.</p>
            </figure>

            <figure class="last group">
                <div class="icon orange group"><i class="ion-ios-sunny-outline"></i></div>
                <strong>Transparent Pricing</strong>

                <p class="icons_info_text">5%* covers code review, asset protection and peace of mind
                    <small>* currency conversion is at cost and typically 1-2% extra.</small>
                </p>
            </figure>

            <div class="clear"></div>
        </div>
    </div>
</section>

<section class="how-it-works group">
    <div class="wrapper text-center group">
        <h3 class="text-center">How it Works</h3>

        <p>&nbsp;</p>

        <div class="half-div group"><img src="<?=base_url('assets/images/client.jpg')?>" alt="#" class="image-auto"></div>
        <div class="half-div last group desktop-only"><img src="<?=base_url('assets/images/developer.jpg')?>" alt="#" class="image-auto"></div>
        <div class="clear"></div>
        <div class="line group mobile-only"></div>
        <div class="half-div group mobile-only"><img src="<?=base_url('assets/images/client-below.jpg')?>" alt="#" class="image-auto"></div>
    </div>
    <div class="line desktop-only group"></div>
    <div class="wrapper text-center group">
        <p class="mobile-only">&nbsp;</p>

        <div class="half-div last group mobile-only"><img src="<?=base_url('assets/images/developer.jpg')?>" alt="#" class="image-auto"></div>
        <div class="clear"></div>
        <div class="line group mobile-only"></div>
        <div class="half-div group desktop-only"><img src="<?=base_url('assets/images/client-below.jpg')?>" alt="#" class="image-auto"></div>
        <div class="half-div last group"><img src="<?=base_url('assets/images/developer-below.jpg')?>" alt="#" class="image-auto"></div>
    </div>
</section>
<?php if (!$logged) { ?>

<section class="no-padding-bottom  try-it-now group">
    <div class="text-center group">
        <h3 class="text-center">Try it Now</h3>

        <div class="form group">
            <form method="post" action="<?=base_url("sign-up")?>" id="sign_up_form">
                <select class="custom-home" name="user_type">
                    <option value="0">I'm a Developer</option>
                    <option value="1">I'm a Client</option>
                </select>
                <input type="text" name="username" placeholder="My name is">
                <?= form_error("username", '<div class="form-error">', '</div>') ?>
                <p class="error name"></p>
                <input type="text" name="email" placeholder="My email">
                <p class="error email"></p>
                <input type="password" name="password" placeholder="Password">
                <p class="error password"></p>
                <input type="password" name="r_password" placeholder="Repeat Password">
                <p class="error r_password"></p>

                <p class="error incorrect_credentials"></p>
                <button class="button" id="signupSubmit">Sign Up  <i class="sinup_icon ion-arrow-right-c"></i></button>
            </form>
        </div>
    </div>
    <a href="#" class="scroll-on-top"><img src="<?=base_url('assets/images/icon-scroll-on-top.png')?>" alt="#"></a>
</section>
<?php } ?>
