<link href="/assets/skins/flat/grey.css" rel="stylesheet" />

<section class="main group">
    <div class="wrapper group">
        <div class="content group">
            <div class="main-title group">
                <ul>
                    <li><a href="javascript:void(0);" id="tab-link-1" class="active">Credit or Debit Card	</a></li>
                    <li><a href="javascript:void(0);" id="tab-link-2" class="">Paypal</a></li>
                    <li><a href="javascript:void(0);" id="tab-link-3" class="">Bank Transfer</a></li>
                </ul>
            </div>

            <div class="tab-content-1 found-client group display">

                <?php if(isset($cards) && !empty($cards)){$i = 0;  foreach($cards as $card){?>


                    <div id ="saved_card" class="cc-row group">
                        <form method="post" id="saved_pay" action="<?= base_url('payments/stripe_saved_pay') ?>">
                            <div class="f-left group">
                                <div class="table-div">
                                    <div class="table-div-cell">
                                        <input type="radio" checked name="radio" id="payment-1"><label for="payment-1">Card ending <?php if(isset($end_number)){echo $end_number[$i]; $i++;} ?></label></div>
                                </div>
                            </div>

                            <div class="f-right ccv group display">
                                <div class="table-div">
                                    <div class="table-div-cell">
                                        <input type="text" name="cvc" placeholder="Enter your CVV/CVC">
                                        <span class="failed vcv_error"></span>
                                    </div>
                                </div>
                            </div>

                    </div>


                <?php }} else{ ?>

                    <div id = "new_card" class="cc-row new-card group">
                        <div class="f-left group">
                            <div class="table-div">
                                <div class="table-div-cell"><input type="radio" name="radio" id="payment-3"><label for="payment-3">Add New Card</label></div>
                            </div>
                        </div>
                        <div class="clear"></div>


                        <div class="insert-card group">
                            <form method="post" id="payment" action="<?= base_url('payments/get_paid') ?>">
                                <div class="box group">
                                    <div class="form-row">
                                        <label>
                                            <span class="text">Card Number</span>
                                            <input class = "stripe_input" name="card_number" type="text" size="20" data-stripe="number" autocomplete="off"/>
                                        </label>
                                    </div>
                                    <div class="form-row">
                                        <label>
                                            <span class="text">CVC</span>
                                            <input class = "stripe_input" name="cvc" type="text" size="4" data-stripe="cvc" autocomplete="off"/>
                                        </label>
                                    </div>
                                    <div class="form-row">
                                        <label>
                                            <span class="text">Expiration (MM/YYYY)</span>
                                            <input id = "month" class = "stripe_input" type="text" name="expire_month" size="2" data-stripe="exp-month" autocomplete="off"/>
                                            <span> / <input  id = "year"class = "stripe_input" type="text" name="expire_year" size="4" data-stripe="exp-year" autocomplete="off"/> </span>
                                        </label>
                                    </div>
                                    <span class="payment-errors"> </span>
                                    <!---->
                                    <!--<!--                            <label>Name On Card</label>--
                                    <!--<!--                            <input type="text" name="cc-name" >-->
                                    <!--<!--                            <p>&nbsp;</p>-->
                                    <!--<!--                            <label>Expiry Date</label>-->
                                    <!--                           <div class="clear"></div>-->
                                    <!--                          <div class="half-div select-custom-div">-->
                                    <!--<!--                                <select class="custom-select" name="cc-month">-->
                                    <!--<!--                                    <option value="01">January (01)</option>-->
                                    <!--<!--                                    <option value="02">February (02)</option>-->
                                    <!--<!--                                    <option value="03">March (03)</option>-->
                                    <!--<!--                                    <option value="04">April (04)</option>-->
                                    <!--<!--                                    <option value="05">May (05)</option>-->
                                    <!--<!--                                    <option value="06">June (06)</option>--
                                    <!--<!--                                    <option value="07">July (07)</option>-->
                                    <!--<!--                                    <option value="08">August (08)</option>-->
                                    <!--<!--                                    <option value="09">September (09)</option>-->
                                    <!--<!--                                    <option value="10">October (10)</option>-->
                                    <!--<!--                                    <option value="11">November (11)</option>-->
                                    <!--<!--                                    <option value="12">December (12)</option>-->
                                    <!--<!--                                </select>-->
                                    <!--                        </div>-->
                                    <!--<!--                            <input class = "stripe_input" type="text" size="4" data-stripe="exp-year" autocomplete="off"/>--></div>
                                <!--<!---->
                                <!--                  <div class="box last group">-->
                                <!--<!--                            <label>Card Number</label>-->
                                <!--<!--                            <input class = "stripe_input"  type="text" size="20" data-stripe="number" autocomplete="off"/>-->
                                <!--<!--                            <p>&nbsp;</p>-->
                                <!--                            <div class="half-div">-->
                                <!--                             <label>CVV/CVC</label>-->
                                <!--                               <input class = "stripe_input"  type="text" size="4" data-stripe="cvc" autocomplete="off"/>-->
                                <!--                         </div>-->
                                <!--                       </div>-->
                                <!--                  </div>-->
                        </div>
                    </div>
                <?php } ?>
                <input type="hidden" value="<?=$amount?>" name="amount">

                <button type="submit" class="button green big"><strong>Pay $<?=$amount?></strong></button>
                </form>
            </div>

            <div class="tab-content-2 found-client group">
                <form class="paypal" action="<?=base_url()?>paypal/buy" method="post" id="paypal_form" target="_blank">

                    <input type="hidden" name="currency_code" value="USD" />
                    <input type="hidden" name="username" value="<?=$user->username?>"  />
                    <div class="paypal-box group">
                        <label>Paypal EMail</label>
                        <input type="email" name="payer_email" placeholder="<?= $email ?>" value="<?= $email ?>"  />
                    </div>

                    <input type="hidden" name="value" value="<?=$amount?>" />
                    <button type="submit" class="button green big"><strong>Pay $<?=$amount?></strong></button>
                </form>

            </div>

            <div class="tab-content-3 found-client group">

                <div class="bank-box group">
                    <p>Connect to your online banking or call your bank and transfer exactly:<strong> $<?=$amount?></strong></p>
                    <p>&nbsp;</p>
                    <p>To DeveloperEscrow�s account below:</p>
                    <p>&nbsp;</p>
                    <div class="details group">
                        <div class="box group">
                            <p>To<strong>DeveloperEscrow</strong></p>
                            <p>&nbsp;</p>
                            <p>UK Sort Code<strong>20-95-61</strong></p>
                        </div>

                        <div class="box last group">
                            <p>Use this reference<strong>P329482</strong></p>
                            <p>&nbsp;</p>
                            <p>Account Number<strong>348398327</strong></p>
                        </div>
                    </div>
                    <p>You must manually send the money from an account in your name.</p>
                </div>
                <p>&nbsp;</p>
                <button type="submit" class="button green big"><strong>I've sent the money</strong></button>
            </div>

            <div class="found-thanks group">
                <div class="inside group">
                    <p><strong>Thank you for your payment</strong></p>
                    <p>&nbsp;</p>
                    <p>The funds will be processed in the next 24 hours.</p>
                </div>
                <a href="view-project-client.html" class="button gray big">Go to Projects</a>
            </div>


        </div>
        <div class="overlay"></div>
