<script type="text/javascript">
    $(document).ready(function() {
        $('select.custom-home').select2({
            theme: "classic",
            minimumResultsForSearch: Infinity,

        });
    });
</script>





<section class="main group">
    <div class="wrapper group">
        <div class="content group">
            <div class="main-title uppercase group">Funding</div>
            <div class="found-div group">
                <div class="top group">
                    <div class="f-left">
                        <div class="table-div">
                            <div class="table-div-cell"><strong>MediaTask</strong></div>
                        </div>
                    </div>

                    <div class="f-right">
                        <div class="table-div">
                            <div class="table-div-cell"><span>Requested <span> $50</span><small>(fees paid by provider)</small></div>
                        </div>
                    </div>

                </div>

                <div class="payment noborder total group">
                    <div class="f-right">
                        <div class="table-div">
                            <div class="table-div-cell">TOTAL<strong>$50</strong></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="payment-method group">
                <h3>Select Payment method</h3>
                <figure class="group">
                    <p>Pay By<strong>Credit or Debit Card</strong></p>
                    <p>&nbsp;</p>
                    <p><img src="images/icon-cc.png" alt="#" class="image-auto"></p>
                    <p>&nbsp;</p>
                    <p><a href="found-client-pay.html" class="button green big"><strong>Pay $4052.5</strong></a></p>
                    <small>$400 + fees</small>
                </figure>

                <figure class="group">
                    <p>Pay By<strong>PayPal</strong></p>
                    <p>&nbsp;</p>
                    <p><img src="images/icon-paypal.png" alt="#" class="image-auto"></p>
                    <p>&nbsp;</p>
                    <p><a href="found-client-pay.html" class="button green big"><strong>Pay $53</strong></a></p>
                    <small>$400 + fees</small>
                </figure>

                <figure class="last group">
                    <p>Pay By<strong>Bank Transfer</strong></p>
                    <p>&nbsp;</p>
                    <p><img src="images/icon-bank-transfer.png" alt="#" class="image-auto"></p>
                    <p>&nbsp;</p>
                    <p><a href="found-client-pay.html" class="button green big"><strong>Pay $50</strong></a></p>
                    <small>&nbsp;</small>
                </figure>
            </div>


        </div>
        </section>
