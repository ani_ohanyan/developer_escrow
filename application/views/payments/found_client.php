<link href="/assets/skins/flat/grey.css" rel="stylesheet" />
<?php // echo '<pre>';
//        print_r($data); die;?>

<section class="main group">
    <div class="wrapper group">
        <div class="content group">
            <div class="main-title uppercase group main_title_block">Funding</div>
            <div class="found-div group">
                <div class="top group">
                    <div class="f-left">
                        <div class="table-div">
                            <div class="table-div-cell">
                                <strong><?php if(isset($data)){echo $data['currency']; echo $data['pay'];}?></strong>
                            </div>
                        </div>
                    </div>
                    <div class="f-right">
                        <div class="table-div ">
                            <div class="table-div-cell">
                                <span>Requested <span><?php if(isset($data)){echo $data['currency'];echo $data['pay'];}?></span>
                                <small>(fees paid by provider)</small></span>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if (isset($miles) && !empty($miles)){  foreach($miles as $mile){ ?>
                <div class="payment group">
                    <div class="f-left">
                        <div class="table-div milestone-section">
                            <div class="table-div-cell"><input data-value = "<?=$fund/count($miles) ?>" type="checkbox"  name="#" value = "" class="payment"><label for="payment-1"><?=$mile?></label></div>
                        </div>
                    </div>
                    <div class="f-right">
                        <div class="table-div">
                            <div class="table-div-cell"><span><?=$currency?> <?=$fund/count($miles) ?></span></div>
                        </div>
                    </div>
                </div>
                <?php }}?>

                <div class="payment total group">
                    <div class="f-right">
                        <div class="table-div">
                            <div class="table-div-cell" >TOTAL<strong ><span data-value = "<?php if(isset($data)){echo $data['pay'];}?>" id="fund"> </span></strong></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="payment-method group">
<!--                <form method="post" action="--><?//= base_url('payments/found_client_pay') ?><!--">-->
                    <h3>Select Payment method</h3>
<!--                    <input type="hidden" name="project" value="--><?//=$project_id?><!--">-->
                <figure class="group">
                    <p>Pay By<strong>Credit or Debit Card</strong></p>
                    <p>&nbsp;</p>
                    <p><img src="/assets/images/icon-cc.png" alt="#" class="image-auto"></p>
                    <p>&nbsp;</p>
                    <p><a href="<?=base_url()?>payments/found_client_pay/<?=$type?>/<?=md5($data['link'])?>" class="button green big"><strong>Pay <?=$data['currency']?><?=$data['pay']?> <span class="debit_card"></span></strong></a></p>
                    <small><?=$data['currency']?> <?= $data['pay']?> + fees</small>
                </figure>

                <figure class="group">
                    <p>Pay By<strong>PayPal</strong></p>
                    <p>&nbsp;</p>
                    <p><img src="/assets/images/icon-paypal.png" alt="#" class="image-auto"></p>
                    <p>&nbsp;</p>
                    <p><a href="<?=base_url()?>payments/found_client_pay/<?=$type?>/<?=md5($data['link'])?>" class="button green big"><strong>Pay <?=$data['currency']?><?=$data['pay']?> <span class="paypal_card"></span></strong></a></p>
                    <small><?=$data['currency']?> <?=$data['pay'] ?> + fees</small>
                </figure>

                <figure class="last group">
                    <p>Pay By<strong>Bank Transfer</strong></p>
                    <p>&nbsp;</p>
                    <p><img src="/assets/images/icon-bank-transfer.png" alt="#" class="image-auto"></p>
                    <p>&nbsp;</p>
                    <p><a href="<?=base_url()?>payments/found_client_pay/<?=$type?>/<?=md5($data['link'])?>" class="button green big"><strong>Pay <?=$data['currency']?><?=$data['pay']?> <span class="bank_transfer"></span> </strong></a></p>
                    <small>&nbsp;</small>
                </figure>
<!--            </form>-->
            </div>
        </div>
        <div class="overlay"></div>

