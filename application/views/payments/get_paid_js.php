<!--<script type="text/javascript" src="https://js.stripe.com/v2/"> </script>-->
<script type="text/javascript" src="/assets/js/icheck.min.js"></script>
<script>

    $(document).ready(function() {
        $('select.custom-select').select2({
            theme: "classic",
            minimumResultsForSearch: Infinity,

        });

        $("#saved_pay").on("submit", function (){
            event.preventDefault();
            var __this = $(this);
            var cvc = __this.find('input[name="cvc"]').val();
            var action = $(this).attr('action');
            if(cvc ==''){
                $('.vcv_error').css('display','block');
                $('.vcv_error').css('margin-top','5px');
                $('.vcv_error').html('The CVV/CVC field is required');
                return;
            }else{
                $('.vcv_error').css('display','none');
            }

            $.ajax ({
                url: action,
                type: 'post',
                data: __this.serialize(),
                dataType: 'text',
                success: function(data){
                    var __data = JSON.parse(data);
                    if(__data.success == false){
                        $('.vcv_error').css('display','block');
                        $('.vcv_error').css('margin-top','5px');
                        $('.vcv_error').css('margin-left','10px');
                        $('.vcv_error').html(__data.message.message);
                    }else{
                        $('.vcv_error').css('display','none');

                        window.location.href = base_url+'payments';
                    }
                }
            });
            return false;
        });

        $("#payment").on("submit", function (){
            event.preventDefault();
            var __this = $(this);
            var cvc  = __this.find('input[name="cvc"]').val();
            var action = $(this).attr('action');

            $.ajax ({
                url: action,
                type: 'post',
                data: __this.serialize(),
                dataType: 'text',
                success: function(data){
                    //var __data = JSON.parse(data);
                   // console.log(data.success);return;
                    if(data.success == false){

                    }else{
                        $('.vcv_error').css('display','none');

                        window.location.href = base_url+'payments';
                    }
                }
            });
            return false;
        });



        Stripe.setPublishableKey('pk_test_eqsukeBAJlSnSn3XiVws1UyQ');

        jQuery(function($) {
            $('#payment').submit(function(event) {
                var $form = $(this);

                // Disable the submit button to prevent repeated clicks
                $form.find('button').prop('disabled', true);

                Stripe.card.createToken($form, stripeResponseHandler);

                // Prevent the form from submitting with the default action
                return false;
            });
        });
        function stripeResponseHandler(status, response) {
            var $form = $('#payment');
           // console.log($form);

            if (response.error) {
                // Show the errors on the form
                $form.find('.payment-errors').text(response.error.message);
                $form.find('button').prop('disabled', false);
            } else {
                // response contains id and card, which contains additional card details
                var token = response.id;
                // Insert the token into the form so it gets submitted to the server
                $form.append($('<input type="hidden" name="stripeToken" />').val(token));
                // and submit
                $form.get(0).submit();
            }
        }




    });
    </script>