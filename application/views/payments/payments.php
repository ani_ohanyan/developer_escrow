<div class="content group">
<!--    --><?php //echo "<pre>";
//    print_r($projects); ?>
<?php if ($user_type == '0'){ ?>
    <!-- developer payments  !-->
        <div class="main-title uppercase group main_title_block"> Work in Progress</div>
        <div class="payment-div group">
            <?php if (isset($projects) && !empty($projects)): ?>
                <?php foreach ($projects as $project){ ?>
                    <div class="single-payment group">
                        <div class="text group">
                            <strong><?= $project['name'] ?></strong>
    <!--                            --><?php //if (isset($user_name) && $user_name[$j] != '') {
    //                                echo $user_name[$j];
    //                            } ?>
                        </div>
                        <div class="price group">
                            <ul>
                                <li><strong><?php echo $project['currency']; ?> <?= $project['pay'] ?></strong>- - -</li>
                                <li class="release-required group">
                                    <a data-value="<?= $project['id'] ?>" href="#" class="button gray request" data-toggle="modal" data-target=".withdrawal_amount_modal" id="">Request Release</a>
                                </li>
                                <li class="release-required-remind group">Release Requested<span><a href="#">Remind</a></span></li>
                                <li>
                                    <a href="#" class="quick-pay" data-toggle="modal" data-target=".quick_payments_modal" id=""><img src="assets/images/icon-wallet.png"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <?php if(isset($project['mil']) && !empty($project['mil'])):?>
                        <?php foreach ($project['mil'] as $proj):?>
                            <div class="single-payment group">
                                <div class="text group">
                                    <strong>Milestone - <?= $proj['name'] ?></strong>
                                    <!--                            --><?php //if (isset($user_name) && $user_name[$j] != '') {
                                    //                                echo $user_name[$j];
                                    //                            } ?>
                                </div>
                                <div class="price group">
                                    <ul>
                                        <li><strong><?php echo $proj['currency']; ?> <?= $proj['pay'] ?></strong>- - -</li>
                                        <li class="release-required group">
                                            <a data-value="<?=$proj['id'] ?>" href="#" class="button gray request" data-toggle="modal" data-target=".withdrawal_amount_modal" id="">Request Release</a>
                                        </li>
                                        <li class="release-required-remind group">Release Requested<span><a href="#">Remind</a></span></li>
                                        <li>
                                            <a href="#" class="quick-pay" data-toggle="modal" data-target=".quick_payments_modal" id=""><img src="assets/images/icon-wallet.png"></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif;?>
                <?php } ?>
            <?php endif; ?>
        </div>

    <!-------------pending----------->
        <div class="payment-div group">
            <div class="main-title uppercase group">Pending</div>
<!--           <?php /*//if(isset($projects)){foreach($projects as $project) {if($project->pay != 0 && $project->fund > $project->pay){ */?>
            <div class="single-payment group">
                <div class="text group">
                    <strong><?/*//=$project->name*/?></strong><?php /*//if(isset($user_name) && $user_name[$j] != ''){ echo $user_name[$j] ; $j++;} */?>
                </div>
                <div class="price group">
                    <ul>
                        <li><strong><?php /*//echo $project->currency; */?><?/*//=$project['pay']*/?></strong><?/*//= $project->updated_at*/?></li>
                        <li class="release-required group"><a  data-value = "<?/*//=$project->id*/?>" href="#" class="button gray request" data-toggle="modal" data-target=".withdrawal_amount_modal" id="">Request Release</a></li>
                        <li class="release-required-remind group">Release Requested<span><a href="#">Remind</a></span></li>
                        <li><a href="#" class="quick-pay" data-toggle="modal" data-target=".quick_payments_modal" id=""><img src="assets/images/icon-wallet.png"></a></li>
                    </ul>
                </div>
            </div>
            --><?php /*//}} }  */?>
        </div>
    <!-------------Paid----------->
            <div class="payment-div group">
                <div class="main-title uppercase group">Paid</div>
<!--                <?php /*if(isset($projects)){ foreach($projects as $project) {  if ($project->pay == 0){*/?>
                    <div class="single-payment group">
                        <div class="text group">
                            <strong><?/*=$project->name*/?></strong><?php /*if(isset($user_name) && $user_name[$j] != ''){ echo $user_name[$j]; $j++;;} */?>
                        </div>
                        <div class="price group">
                            <ul>
                                <li><strong><?php /*if(isset($project->pay))*/?><?/*= $project->updated_at*/?></strong></li>

                                <li><span class="paid">Paid</span></li>
                                <li><a href="#" class="quick-pay" data-toggle="modal" data-target=".quick_payments_modal" id=""><img src="assets/images/icon-wallet.png" alt="#"></a></li>
                            </ul>
                        </div>
                    </div>
                --><?php /*}}}*/?>
            </div>
        </div>
    <?php } else {?>


<!-- Client payments  !-->

        <div class="main-title uppercase group main_title_block"> Work in Progress </div>
        <div class="payment-div group">
            <?php if (isset($projects) && !empty($projects)): ?>
                <?php foreach ($projects as $project){ ?>
                    <div class="single-payment group">
                        <div class="text group">
                            <strong><?=$project['name']?></strong>
                        </div>
                        <div class="price group">
                            <ul>
                                <li><strong><?php echo $project['currency']; ?> <?=$project['pay']?></strong></li>
                                <li>
                                   <a href="<?=base_url()?>payments/found_client/project/<?=md5($project['project_id'])?>" class="button gray "><i class="fa fa-plus"></i> Fund</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <?php if(isset($project['mil']) && !empty($project['mil'])):?>
                        <?php foreach ($project['mil'] as $proj['mil']):?>
                            <div class="single-payment group">
                                <div class="text group">
                                    <strong>Milestone - <?=$proj['mil']['name']?></strong>
                                </div>
                                <div class="price group">
                                    <ul>
                                        <li><strong><?php echo $proj['mil']['currency']; ?> <?=$proj['mil']['pay']?></strong></li>
                                        <li>
                                           <a href="<?=base_url()?>payments/found_client/milestone/<?=md5($proj['mil']['milestone_id'])?>" class="button gray "><i class="fa fa-plus"></i> Fund</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif;?>
                <?php } ?>
            <?php endif ?>
        </div>

        <div class="payment-div group">
            <div class="main-title uppercase group">Pending</div>
            <?php /*if(isset($projects)){$j = 0; foreach($projects as $project) {if($project->pay != 0 && $project->fund > $project->pay){ */?><!--
                <div class="single-payment group">
                    <div class="text group">
                        <strong><?/*=$project->name*/?></strong><?php /*if(isset($user_name) && $user_name[$j] != ''){ echo $user_name[$j] ; $j++;} */?>
                    </div>
                    <div class="price group">
                        <ul>
                            <li><strong><?php /*echo $project->currency; */?><?/*=$project->pay*/?></strong><?/*= $project->updated_at*/?></li>
                              <li><form method="post" action="<?/*= base_url('payments/found_client') */?>">
                                   <input type="hidden" name="project" value="<?/*= $project->id */?>">
                               <button class="button gray icon ion-plus-round">Release</button>
                            </form>
                            </li>
                        </ul>
                    </div>
                </div>
            --><?php /*}} }  */?>
        </div>

        <div class="payment-div group">
            <div class="main-title uppercase group">Paid</div>
            <?php /*if(isset($projects)){ $j = 0;foreach($projects as $project) {  if ($project->pay == 0){*/?><!--
                <div class="single-payment group">
                    <div class="text group">
                        <strong><?/*=$project->name*/?></strong><?php /*if(isset($user_name) && $user_name[$j] != ''){ echo $user_name[$j]; $j++;;} */?>
                    </div>
                    <div class="price group">
                        <ul>
                            <li><strong><?php /*if(isset($project->pay))*/?></strong><?/*= $project->updated_at*/?></li>
                            <li><span class="paid">Paid</span></li>
                            <li><a href="#" class="quick-pay"  id="" data-toggle="modal" data-target=".quick_payments_modal"><img src="assets/images/icon-wallet.png" alt="#"></a></li>
                        </ul>
                    </div>
                </div>
            --><?php /*}}}*/?>
        </div>
        </div>
    <?php }?>

<!--------------------------->

<section class="popup small group withdrawal-ammount">
    <div class="top group">
        <h1>Specify Withdrawal Amount</h1>
        <a href="javascript:void(0);" id="close-it"><i class="ion-close-round"></i></a>
    </div>
    <div class="middle-full group">
        <form method="post" action="<?=base_url('payments/found_client_pay')?>">
            <input type="hidden" name="project" id = "modal_project" value="">
            <input type="text" name="amount" class="wa-input" placeholder="Insert amount" value="">
    </div>
    <div class="middle group">
        <label>Outpayment Method</label>
        <div class="f-left select-custom-div-big">
            <select class="custom-paymentmethod" name="#">
                <option value="#">Pre-paid Mastercard</option>
                <option value="#">Pre-paid Visa</option>
                <option value="#">PayPal</option>
                <option value="#">Bank transfer</option>
            </select>
        </div>
        <div class="f-right"><button class="button green">Get Paid</button></div>
        </form>
    </div>
</section>

<section class="popup small group withdrawal-thanks">
    <div class="top group">
        <h1>Withdrawal Request</h1>
        <a href="javascript:void(0);" id="close-it"><i class="ion-close-round"></i></a>
    </div>
    <div class="middle text-center group">
        <p>Thank you for your withdrawal request. We are processing the payment and it should complete in <strong>X days</strong>.</p>
        <p>&nbsp;</p>
        <a href="#" class="button gray">Get Paid</a>
    </div>
</section>

<section class="popup small group request-funding-short">
    <div class="top group">
        <h1>Request Funding</h1>
        <a href="javascript:void(0);" id="close-it"><i class="ion-close-round"></i></a>
    </div>

    <div class="middle text-center group">
        <div class="milestone-row radio group">
            <div class="f-left group">
                <div class="table-div">
                    <div class="table-div-cell"><input type="radio" checked name="who-will-pay-short" id="radio-1"><label for="radio-1">Client to Pay Developer Escrow Fees<small>(You earn $600)</small></label></div>
                </div>
            </div>
        </div>

        <div class="milestone-row radio group">
            <div class="f-left group">
                <div class="table-div">
                    <div class="table-div-cell"><input type="radio" checked name="who-will-pay-short" id="radio-2"><label for="radio-2">Provider to Pay Developer Escrow Fees<small>(You earn $540)</small></label></div>
                </div>
            </div>
        </div>
        <p>&nbsp;</p>
        <a href="#" class="button green big" id="open-request-funding-thanks">Submit Request</a>
    </div>
</section>

<section class="popup small group request-funding-thanks">
    <div class="top group">
        <h1>Request Funding</h1>
        <a href="javascript:void(0);" id="close-it"><i class="ion-close-round"></i></a>
    </div>
    <div class="middle text-center group">
        <p>Your Request for Funding has been <strong>Sent!</strong></p>
        <p>&nbsp;</p>
        <a href="#" class="button gray big">Thanks</a>
    </div>
</section>

<section class="popup small group request-release-thanks">
    <div class="top group">
        <h1>Request Release</h1>
        <a href="javascript:void(0);" id="close-it"><i class="ion-close-round"></i></a>
    </div>
    <div class="middle text-center group">
        <p>A release has been requested.</p>
        <p>&nbsp;</p>
        <a href="#" class="button gray big">Thanks</a>
    </div>
</section>

<!--<section class="popup small group quick-payment-request">-->
<!--    <div class="top group">-->
<!--        <h1>Quick Payment Request</h1>-->
<!--        <a href="javascript:void(0);" id="close-it"><i class="ion-close-round"></i></a>-->
<!--    </div>-->
<!--    <div class="middle info group">-->
<!--        <p>Quick Payments are for one-off payments and not subject to normal Developer Escrow oversight.  They cost a <strong>2.5%</strong> Developer Escrow handling fee, plus any currency conversion costs.</p>-->
<!--    </div>-->
<!--    <div class="middle-full group">-->
<!--        <form method="post" action="#">-->
<!--            <input type="text" name="withdrawal-ammount" class="wa-input" placeholder="$ Amount">-->
<!--    </div>-->
<!--    <div class="middle text-center group">-->
<!--        <div class="milestone-row radio group">-->
<!--            <div class="f-left group">-->
<!--                <div class="table-div">-->
<!--                    <div class="table-div-cell"><input type="radio" checked name="who-will-pay-quick" id="radio-1"><label for="radio-1">Client to Pay Developer Escrow Fees<small>(You earn $600)</small></label></div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--        <div class="milestone-row radio group">-->
<!--            <div class="f-left group">-->
<!--                <div class="table-div">-->
<!--                    <div class="table-div-cell"><input type="radio" checked name="who-will-pay-quick" id="radio-2"><label for="radio-2">Provider to Pay Developer Escrow Fees<small>(You earn $540)</small></label></div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <p>&nbsp;</p>-->
<!--        <button class="button green big">Submit Request</button>-->
<!--        </form>-->
<!--    </div>-->
<!---->
<!--</section>-->

<!-- Modal -->
<div class="modal fade quick_payments_modal"  tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Quick Payment Request</h4>
            </div>
            <div class="modal-body">
                <div class="middle info group">
                    <p>Quick Payments are for one-off payments and not subject to normal Developer Escrow oversight.  They cost a <strong>2.5%</strong> Developer Escrow handling fee, plus any currency conversion costs.</p>
                </div>
                <div class="middle-full group">
                    <form method="post" action="#">
                        <input type="text" name="withdrawal-ammount" class="wa-input" placeholder="$ Amount">
                </div>
                <div class="middle text-center group">
                    <div class="milestone-row radio group">
                        <div class="f-left group">
                            <div class="table-div">
                                <div class=" payments_radio_item">
                                    <input type="radio"  name="who-will-pay-quick" id="radio1" class="payments_radio" value="1">
                                    <label for="radio1">Client to Pay Developer Escrow Fees  <span>(You earn $600)</span></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="milestone-row radio group">
                        <div class="f-left group">
                            <div class="table-div">
                                <div class=" payments_radio_item">
                                    <input type="radio"  checked name="who-will-pay-quick" id="radio2" class="payments_radio" value="2">
                                    <label for="radio2">Provider to Pay Developer Escrow Fees  <span>(You earn $540)</span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p>&nbsp;</p>
                    <button class="button green big">Submit Request</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>