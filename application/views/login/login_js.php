<script type="text/javascript">


    var base_url = "<?=base_url()?>";

    $(document).ready(function () {

        /* forgot_password */
        var forgot_section = $('.forgot_section');
        var signup_section = $('.signup_section');
        var link_to_login  = $('.link_to_login');
        var link_to_signup = $('.link_to_signup');
        $('.forgot_password_link').on( "click", function() {
            signup_section.hide();
            link_to_signup.hide();
            link_to_login.show();
            forgot_section.show();
            if (forgot_section.is(':visible')) {
                link_to_login.click(function(){
                    signup_section.show();
                    link_to_signup.show();
                    forgot_section.hide();
                    link_to_login.hide();
                })
            }
        });
        /* forgot_password */

        $("#login_form").unbind("submit").on("submit", function (e) {
            e.preventDefault();
            var el = $(this);
            var input = el.find("input");
            var data = el.serialize();
            $.ajax({
                type: "POST",
                url: base_url + "login",
                data: data,
                success: function (msg) {
                    var msg = JSON.parse(msg);
                    el.find("input").css({"border": "1px solid transparent"});
                    el.find(".error").html("");
                    if (msg.validation) {
                        $.each(msg.validation, function (key, value) {
                            el.find("input[name='" + key + "']").css("cssText", "border-bottom: 2px solid rgb(255, 0, 0) !important;");
                            el.find(".error." + key).html(value);
                        });
                    }
                    if (msg.status == "error") {
                        el.find(".password_or_email").html(msg.value);
                    } else if (msg.status == "success") {
                        window.location = base_url + msg.url;
                    }
                }
            });
            return false;
        });

        $("#forgot_pass_form").unbind("submit").on("submit", function (e) {
            e.preventDefault();
            var data = $(this).serialize();
            $.ajax({
                type: "POST",
                url: base_url + "forgot_pass",
                data: data,
                success: function (data) {
                    data = JSON.parse(data);
                    if(data.status){
                        $('#forgot_pass_form .info_text').html(data.sendEmail);
                        $('#forgot_pass_form .info_text').addClass('success_msg');
                        $('#forgot_pass_form button').hide();
                        $('#forgot_pass_form .email_error').hide();
                        $('.link_to_login ').hide();
                        setTimeout(function(){window.location.href =  base_url +'login'; }, 8000);
                    } else {
                        $('#forgot_pass_form .email_error').html(data.sendEmail);
                        $('#forgot_pass_form .email_forgot_pass').css('border-bottom', '2px solid #E27164');
                    }
                }
            });
        })

    });
</script>