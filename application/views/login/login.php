<!DOCTYPE html><head>
    <title>Developer Escrow | Login</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Developer Escrow"/>
    <meta name="description" content="Developer Escrow"/>
    <meta name="robots" content="index, follow" />
    <link href='<?=base_url('assets/webstyle/style.css')?>' rel='stylesheet' type='text/css'>
    <link href='<?=base_url('assets/webstyle/reset.css')?>' rel='stylesheet' type='text/css'>
    <link href='<?=base_url('assets/webstyle/fonts.css')?>' rel='stylesheet' type='text/css'>
    <link href='<?=base_url('assets/webstyle/ionicons.min.css')?>' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <link rel="shortcut icon" href="<?=base_url('assets/images/favicon.ico')?>" type="image/x-icon" />
    <!--[if lt IE 9]>
    <script src="<?=base_url('assets/js/html5shiv.js')?>"></script>
    <![endif]-->

    <!-- get jQuery from the google apis -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link href="<?=base_url('assets/webstyle/select2.min.css')?>" rel="stylesheet" />
    <script src="<?=base_url('assets/js/select2.min.js')?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('select.custom-home').select2({
                theme: "classic",
                minimumResultsForSearch: Infinity
            });
        });
    </script>

</head>
<body class="landing-home gray">

<section class="signup group">
    <div class="internal group">
        <p class="logo"><a href="<?=base_url()?>"><img src="<?=base_url('assets/images/developer-escrow-logo.png')?>" alt="Developer Escrow" title="Developer Escrow"></a></p>
        <div class="form group signup_section">
            <form method="post" action="<?=base_url('login')?>" id="login_form">
                <p class="error password_or_email"></p>
                <input type="text" name="email" placeholder="My email">
                <p class="error email"></p>
                <input type="password" name="password" placeholder="Password">
                <a href="#" class="forgot_password_link">Forgot Password</a>
                <a  class="error password"></a>
<!--                <button class="button green"> Sign In <i class="ion-arrow-right-c"></i></button>-->
                <button class="button green sign_in_button"> Sign In <i class="ion-arrow-right-c"></i></button>
            </form>
        </div>
        <div class="form group forgot_section">
            <form method="post" action="<?=base_url('forgot_pass')?>" id="forgot_pass_form">
                <p class="info_text">Enter your email below to receive your password reset instructions</p>
                <p class="error password_or_email"></p>
                <input type="text" name="email_forgot_pass" class="email_forgot_pass" placeholder="Your Email">
                <p class="email_error"></p>
                <button type="submit" class="button green">Send Instrunctions <i class="ion-arrow-right-c"></i></button>
            </form>
        </div>
    </div>
</section>
<section  class="link_to_signup have-acc group"><a href="<?=base_url('sign-up')?>">Sign Up</a></section>
<section class="link_to_login have-acc group" style="display: none"><a href="#">Changed your mind?</a></section>
<!--<script type="text/javascript" src="assets/js/custom.js"></script>-->

</body>
</html>