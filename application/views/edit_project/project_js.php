<script type="text/javascript">
    /*$(document).ready(function() {
        if ( $(this).width() > 768 ) {
            $(function() {
                //$('section.what-you-get figure').matchHeight();
            });
        };
    });*/
    $("#hourly_rate").on("click",function(){
        $(".hourly_rate_section").css('display','block');
        $(".fixed_price_section").css('display','none');
        $(".desc-me").css('display','none');
        $("#project_type").val(0);
    });
    $("#fixed_price").on("click",function(){
        $(".fixed_price_section").css('display','block');
        $(".hourly_rate_section").css('display','none');
        $(".desc-me").css('display','block');
        $("#project_type").val(1);
    });

    $('.project_type').on('click',function(e){
        e.preventDefault();
        $('.project_type' ).removeClass( 'active' );
        $(this).addClass('active');
    });

    $('.currency_type').on('click',function(e){
        e.preventDefault();
        $('.currency_type' ).removeClass('active');
        $(this).addClass('active');
        var data = $(this).data('value');
        $('#currency').val(data);
        var currency = data;

        if(currency == 1) {
            currency = "$";
        } else if(currency == 2) {
            currency = "&#8364;";
        } else if(currency == 3) {
            currency = "&#163;";
        }
        $(".milListTr" ).each(function() {
            $(this).find('.mile_currency_type').html(currency);

        });
    });


    $('a#link-desc-client').click(function(e) {
        e.preventDefault();
        $(this).addClass('active');
        $('a#link-desc-me').removeClass('active');
        //$('.desc-me').removeClass('display');
        $(".desc-me").css('display','none');
    });

    $('a#link-desc-me').click(function(e) {
        e.preventDefault();
        $(this).addClass('active');
        $('a#link-desc-client').removeClass('active');
        $(".desc-me").css('display','block');
    });
    $(document).on("click",".milestones_popup .milestone-list li", function() {
        $(".milestones_popup .milestone-list li").removeClass("active");
        $(this).addClass("active");
    });

    $(document).on("click",".remove-milestone", function() {
        $(this).parents("li").fadeOut("fast",function() {
            $(this).remove();
        });
    });

    $("#project_edit").on('submit',function(){
        event.preventDefault();
        var __this = $(this);
        var action = $(this).attr('action');
        console.log(action);
        var id = __this.find('input[name="id"]').val();
        var name = __this.find('input[name="name"]').val();
        var description = __this.find('textarea[name="description"]').val();
        var freelancer = __this.find('input[name="freelancer"]').val();
        var currency = $('#currency').val();

        var form_data = new FormData();
        for(i=0; i < $("#projectFileIds").prop('files').length; i++){
            form_data.append("project_attachment[]", document.getElementById('projectFileIds').files[i]);
        }
        form_data.append('type',$("#project_type").attr('value') );
        form_data.append('id',id );
        form_data.append('name',name );
        form_data.append('description',description );
        form_data.append('freelancer',freelancer );
        form_data.append('currency',currency );
        milestones = new Array();
        $( ".milList" ).each(function() {
            mile = new Object();
            mile.id =  $(this).attr("mile-id");
            mile.name =  $(this).find('.name_milestone').attr("value");
            mile.description = $(this).find('.description_milestone').attr("value");
            mile.price = $(this).find('.price_milestone').attr("value");
            milestones.push(mile);
        });
        form_data.append("milestones",JSON.stringify(milestones));
        //if($("#project_type").attr("value")){
            var fixed_price = __this.find('input[name="fixed_price"]').val();
            form_data.append('fixed_price',fixed_price );
        //} else {
            var hourly_rate = __this.find('input[name="hourly_rate"]').val();
            var hours_per_week = __this.find('input[name="hours_per_week"]').val();
            form_data.append('hourly_rate',hourly_rate );
            form_data.append('hours_per_week',hours_per_week );
        //}
        if(name == ''){
            $('.name_field_required').show();
            $('.name_field_required').css('display','block');
            return;
        } else {
            $('.name_field_required').hide();
        }
        if(freelancer == ''){
            $('.freelancer_field_required').show();
            $('.freelancer_field_required').css('display','block');
            return;
        } else{
            $('.freelancer_field_required').hide();
        }
        console.log(form_data);
            $.ajax ({
                url: action,
                type: 'post',
                data: form_data,
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                success: function(data){
                     window.location.href = base_url+'projects';
                },
                error: function(){

                }
            });
            return false;
    });

    var countMil = 0;
    var milestome_sum = 0;

    $(document).on("click", ".add-milestone-button", function() {
        var milestone_name = $(".milestone_title").val();
        var milestone_description = $(".milestone_description").val();
        var milestone_price = $(".milestone_price").val();
        var currency = $('#currency').val();
        if(currency == 1) {
            currency = "$";
        } else if(currency == 2) {
            currency = "&#8364;";
        } else if(currency == 3) {
            currency = "&#163;";
        }
        var miles = $('.milList');
        if(miles){
            miles.each(function() {
                countMil = $(this).attr('data_id');
                countMil++;
            });
        }
        countMil++;
        var html_li = '<tr class="milListTr" data_id="'+countMil+'"><td class="number_milestone">'+countMil+'</td><td class="none_border"><input id="m'+countMil+'" type="hidden" value="'+milestone_price+'"><h2 class="title_miles"><span class="html_title_miles">'+milestone_name+' </span><span>-</span><span class="mile_currency_type">'+currency+'</span><span class="html_price_miles">'+milestone_price+'</span></h2><span class="html_descr_miles">'+milestone_description+'</span></td><td class="delete_el_miles"><a><i class="fa fa-pencil edit_milestone" data-toggle="modal" data-target="#milestoneModal" data_id="'+countMil+'"></i><i class="ion-ios-close remove-milestone" data_id="'+countMil+'"></i></a></td></tr>';
        var data_html = '<div class="milList" data_id="'+countMil+'"><input type="hidden" class="name_milestone" name="milestones['+countMil+'][name]" value="'+milestone_name+'">\n\
        <input type="hidden" name="milestones['+countMil+'][description]" class="description_milestone" value="'+milestone_description+'">\n\
        <input type="hidden" name="milestones['+countMil+'][price]" class="price_milestone" value="'+milestone_price+'"></div>';
        $(html_li).appendTo(".milestones .milestone-list");
        $(data_html).appendTo(".milestones .milestone-list");
        $("#milestoneModal").modal('hide');
        milestome_sum += +$('.milestone_price').val();

        setTimeout(function(){
            $("#search-milestone").val('');
            $(".milestones_popup .milestone-list").empty();
            $(".milestones_popup .milestone-list li").removeClass("active");
        },500);
    });

    $(document).on("click", ".edit_mile_btn", function() {
        var milestone_name = $(".milestone_title").val();
        var milestone_description = $(".milestone_description").val();
        var milestone_price = $(".milestone_price").val();
        var currency = $('#currency').val();
        if(currency == 1) {
            currency = "$";
        } else if(currency == 2) {
            currency = "&#8364;";
        } else if(currency == 3) {
            currency = "&#163;";
        }
        var data_id = $(this).attr('data_id');
        $(".milList" ).each(function() {
            if($(this).attr('data_id') == data_id){
                $(this).find('.name_milestone').attr('value', milestone_name);
                $(this).find('.description_milestone').attr('value', milestone_description);
                $(this).find('.price_milestone').attr('value', milestone_price);
            }
        });
        $(".milListTr" ).each(function() {
            if($(this).attr('data_id') == data_id){
                $(this).find('.html_title_miles').html(milestone_name);
                $(this).find('.html_descr_miles').html(milestone_description);
                $(this).find('.html_price_miles').html(milestone_price);
            }
        });

        $("#milestoneModal").modal('hide');
    });

    $(document).on("click", ".edit_milestone", function() {
        var milestone_title = $(this).parents('tr').find('.html_title_miles').html();
        var milestone_price = $(this).parents('tr').find('.html_price_miles').html();
        var milestone_descr = $(this).parents('tr').find('.html_descr_miles').html();
        var data_id = $(this).attr('data_id');
        $('.add-milestone-button').html('Save');
        $('.add-milestone-button').addClass('edit_mile_btn');
        $('.add-milestone-button').removeClass('add-milestone-button');
        $('.edit_mile_btn').attr('data_id', data_id);
        $('.milestone_title').val(milestone_title);
        $('.milestone_description').val(milestone_descr);
        $('.milestone_price').val(milestone_price);
        $('#milestoneModal').show();

    });

    $('#milestoneModal').on('hidden.bs.modal', function () {
        $('.milestone_price').removeClass('border_red');
        $('#milestone_error').hide();
    })
//    checking milestone values
    $('.milestone_price').on('keyup  change', function(){
        if($('.milestone_price ').val() == '' || $('.milestone_price ').val() == '0'){
            $('.milestone_price').addClass('border_red');
        } else {
            $('.milestone_price').removeClass('border_red');
            $('#milestone_error').hide();
        }
        if($('#fixed_price_input').val() != ''){
            var milestone = $(this).val();
            var price = $('#fixed_price_input').val();
            if(+milestone+milestome_sum > +price){
                $('#milestone_error').show();
                $('.milestone_price').addClass('border_red');
            } else {
                $('#milestone_error').hide();
                $('.milestone_price').removeClass('border_red');
            }
        }
    })
    $('#fixed_price_input').on('keyup  change', function(){
        var price = $(this).val();
        if((+price < milestome_sum || +price == 0) && price != '' ){
            $('#fixed_price_input').addClass('border_red');
            $('.fixed_price_error').show();
            //$('#project_edit .button').attr("disabled", true);
        } else {
            $('#fixed_price_input').removeClass('border_red')
          //  $('#project_edit .button').attr("disabled", false);
            $('.fixed_price_error').hide();
        }
    })


var i =0;

    /* Delete milestone */
    $(document).on('click', '.remove-milestone', function(remove){
        var _this = $(this);
        var id = $(this).attr('data_id');
        var m = +$('#m'+id).val();
        milestome_sum -= m;
        _this.parents('tr').remove();
        $('.milestone-list tr').each(function(){
            i++
            $(this).find('.number_milestone').text(i);
        })
        i = 0;
        countMil--
    });

    /* Empty modal fields */
    $(document).on('click', '.addMilestones', function(remove){
        $('.milestone_fields').val('');
        $('.edit_mile_btn').html('Add');
        $('.edit_mile_btn').addClass('add-milestone-button');
        $('.edit_mile_btn').removeClass('edit_mile_btn');

    });

    /* Delete milestone */
    $(document).on('click', '.remove-milestone', function(remove){
        var mile_id = $(this).parents('tr').attr('data-id');
        $.ajax ({
            url: base_url+"milestones/remove_milestone",
            type: 'post',
            data: {mile_id:mile_id},
            dataType: 'text',
            success: function(data){
                var __data = JSON.parse(data);
                if(__data.success){
                    location.reload();
                }
            },
            error: function(){
            }
        });
        return false;
    });


</script>