<?php
    $currency = $project['currency'];
    if($currency == 1) {
        $currency = "$";
    } else if($currency == 2) {
        $currency = "&#8364;";
    } else if($currency == 3) {
        $currency = "&#163;";
    }
?>
<div class="content group">
    <div class="main-title group main_title_block">Edit Project</div>
    <div class="project-box group">
        <form id="project_edit" action="<?= base_url() ?>projects/editProject" method="post" enctype="multipart/form-data">
            <h3>Project Type</h3>
            <input id="project_type" type="hidden" name="type" value="<?=($project['type'])?>"/>
            <input type="hidden" name="id" value="<?=$project['pr_id']?>"/>
            <ul class="project-type group">
                <li><a data-value="1" class="checkbox project_type <?=($project['type'] == 0) ? 'active' : ''?>" id="hourly_rate">Hourly Work</a></li><!--href="javascript:void(0);" -->
                <li><a data-value="2" class="checkbox project_type <?=($project['type'] == 1) ? 'active' : ''?>" id="fixed_price">Fixed-Price Project</a></li><!--href="javascript:void(0);" -->
                <li><span class="question">(?)</span></li>
            </ul>
            <div class="hourly-div group display tab-content-1">
                <h3>Project Name</h3>
                <span class="failed name_field_required"> The Name field is required</span>
                <input class = "name" id="hourly-project-name" type="text" name="name" value="<?= $project['name'] ?>">
                <div class="left-box group ">
                    <h3>Project Description</h3>
                </div>
                    <textarea class = "description" id = "hourly-project-description" name="description"><?= $project['description'] ?></textarea>
                    <div class="fileupload-buttonbar">
                        <div class="">
                            <!--<a href="#" id="open-upload-files" > Upload files </a>-->
                                <div class="uploadField choose_file no-padding" style="border: 0;">
                                    <a class="button gray">Upload Files</a>
                                    <input type="file" id="projectFileIds" name="project_attachment[]" class="filesListUpload" multiple>
                                </div>
                                <div id="selectedFiles"></div>
                        </div>
                    </div>
                <div class="clear"></div>
                <?php  if( $user_type == 1) { ?>
                    <h3>Invite Freelancer</h3>
                <?php }else { ?>
                    <h3>Invite Client</h3>
                <?php } ?>
                <ul class="invite-freelancer group">
                    <span class="failed freelancer_field_required"> The Freelancer field is required</span>
                    <li><input type="text" placeholder="Enter email address" name="freelancer" value="<?=$project['invited_user']?>" readonly="readonly"></li>
                    <span class="invalid_email"></span>
                </ul>
                <div class="clear"></div>
                <div class="hourly_rate_section" style="<?=($project['type']==0) ? 'display:block' : 'display:none'?>">
                    <div class="left-box group">
                        <h3>Hourly Rate</h3>
                        <input type="text" name="hourly_rate" value="<?=$project['hourly_rate']?>">
                        <span class="failed h_rate_field_required"> The Hourly Rate field is required</span>
                    </div>
                    <div class="left-box group">
                        <h3>Hours Per Week</h3>
                        <input type="text" name="hours_per_week" value="<?=$project['hours_per_week']?>">
                        <span class="failed h_week_field_required"> The Hours Per Week field is required</span>
                    </div>
                    <div class="left-box group">
                        <h3>Currency</h3>
                        <input type="hidden" id="currency" name="currency" value="1"/>
                        <ul class="group">
                            <li><a  data-value="1" href="#" class="checkbox currency_type <?=($project['currency'] == 1) ? 'active' : ''?>">USD</a></li>
                            <li><a  data-value="2" href="#" class="checkbox currency_type <?=($project['currency'] == 2) ? 'active' : ''?>">EUR</a></li>
<!--                            <li><a  data-value="3" href="#" class="checkbox currency_type">GBP</a></li>-->
                        </ul>
                    </div>
                </div>
                <div class="milestones group fixed_price_section" style="<?=($project['type']==1) ? 'display:block' : 'display:none'?>">
                    <div class="left-box group">
                        <h3>Fixed Price</h3>
                        <span class="failed fixed_price_field_required"> The Fixed Price field is required</span>
                        <input type="number" name="fixed_price" placeholder="0"  id="fixed_price_input" value="<?=$project['fixed_price']?>" >
                    </div>
                    <div class="left-box group">
                        <h3>Currency</h3>
                        <ul class="group">
                            <li><a  data-value="1" href="#" class="checkbox currency_type <?=($project['currency'] == 1) ? 'active' : ''?>">USD</a></li>
                            <li><a  data-value="2" href="#" class="checkbox currency_type <?=($project['currency'] == 2) ? 'active' : ''?>">EUR</a></li>
<!--                            <li><a  data-value="3" href="#" class="checkbox currency_type">GBP</a></li>-->
                        </ul>
                    </div>
                    <div class="fixed_price_error clear" style="display: none">The fixed price should not be less than the milestone price</div>
                    <div class="clear"></div>
                    <h3>Milestones</h3>
                    <table class='milestone-list'>
                        <?php  $i=0; foreach($project['milestones'] as $milestone) {  $i++; ?>
                            <tr data-id="<?=$milestone->id?>" data_id="<?=$i;?>" class="milListTr">
                                <td class="number_milestone"><?=$i;?></td>
                                <td class="none_border">
                                    <h2 class="title_miles">
                                        <span class="html_title_miles"><?=$milestone->name;?></span>
                                        <span>-</span>
                                        <span class="mile_currency_type"><?=$currency;?></span><span class="html_price_miles"><?=$milestone->price?></span>
                                        <input type="checkbox" class="check_checkbox" id="chek_item<?=$milestone->id;?>" name="chek_item" >
                                        <label class="label_check_checkbox" for="chek_item<?=$milestone->id;?>"></label>
                                    </h2>
                                    <span class="html_descr_miles"><?=$milestone->description;?></span>
                                </td>
                                <td class="delete_el_miles"><a><span><i class="fa fa-pencil edit_milestone" data-toggle="modal" data-target="#milestoneModal" data_id="<?=$i;?>"></i></span><span class="remove-milestone"><i class="ion-ios-close"></i></span></a></td>
                            </tr>
                            <div class="milList" data_id="<?=$i?>" mile-id ="<?=$milestone->id;?>">
                                <input type="hidden" name="milestones[<?=$i;?>][name]" class="name_milestone" value="<?=$milestone->name;?>" />
                                <input type="hidden" name="milestones[<?=$i;?>][description]" class="description_milestone" value="<?=$milestone->description;?>"/>
                                <input type="hidden" name="milestones[<?=$i;?>][price]" class="price_milestone" value="<?=$milestone->price;?>"/>
                            </div>
                        <?php } ?>
                    </table>

                    <div class="left-box group">
                        <input type="hidden" name="milestones" id="milestones-id-hidden">
                        <p class="icon ion-plus-round addMilestones" data-toggle="modal"
                           data-target="#milestoneModal"> Add Milestone</p>
                    </div>
                    <div class="clear"></div>

                </div>
                <div class="clear"></div>
                <p>&nbsp;</p>
                <button type="submit" class="button green big">Save Project</button>
            </form>
        </div>
        <p>&nbsp;</p>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="milestoneModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" class="form_add_milestone">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin-top: -2px;display: inline-block">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Milestone</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="milestone_title milestone_fields" placeholder="Title"/>
                    <textarea class="milestone_description milestone_fields" placeholder="Description"></textarea>
                    <span id="milestone_error" class="red" style="display: none">The milestone's fee should not be more than the fixed price</span>
                    <input type="number" class="milestone_price milestone_fields" placeholder="Value"/>
                    <button type="button" class="btn btn-primary milestone_discard_btn" data-dismiss="modal" aria-label="Close">Discard</button>
                    <button type="button" class="btn btn-primary add-milestone-button">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>