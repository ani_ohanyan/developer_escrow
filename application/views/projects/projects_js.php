<script type="text/javascript">

    $(document).ready(function() {
//        $('select.custom-home').select2({
//            theme: "classic",
//            minimumResultsForSearch: Infinity,
//
//        });

        $("#login_form").unbind("submit").on("submit", function (e) {
            e.preventDefault();
            var el = $(this);
            var input = el.find("input");
            var data = el.serialize();
            $.ajax({
                type: "POST",
                url: base_url + "login",
                data: data,
                success: function(msg) {
                    var msg = JSON.parse(msg);
                    el.find("input").css({"border": "1px solid transparent"});
                    el.find(".error").html("");
                    if (msg.validation) {
                        $.each(msg.validation, function (key, value) {
                            el.find("input[name='" + key + "']").css({"border": "1px solid #f00"});
                            el.find(".error." + key).html(value);
                        });
                    }
                    if (msg.status == "error") {
                        el.find(".password_or_email").html(msg.value);
                    } else if (msg.status == "success") {
                        window.location = base_url;
                    }
                }
            });
            return false;
        });

        $(".sendNotification").on("click", function () {
            var data = $(this).attr('data-id');
            $(".projId").val(data);
        });
        $('.sendRemindForDescriprion').on('click',function(){
            var data = $('#popup_notification').find($('.projId')).val();
            console.log(data);
            $.ajax({
                type: "POST",
                url: base_url + "projects/send_reminder",
                data: {projId:data},
                success: function(result) {
                    if(result){
                        $('#popup_notification').modal('hide');
                        $('#proj'+data).html("<span style='color:#7DB440'>You have already sent reminder. </span><span class='sendNotification' data-toggle='modal' data-target='#popup_notification' data-id="+data+" >Click here</span> to send it again.");
                    }
                }
            });
        })
    });
</script>