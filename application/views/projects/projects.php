<div class="content group">
    <?php if(isset($projects) && !empty($projects)) :?>
        <?php  $project_current_status = array(true,true,true); ?>
        <?php foreach($projects as $project) : ?>
            <?php
                if($project['currency'] == 1) {
                    $project['currency'] = "$";
                } else if($project['currency'] == 2) {
                    $project['currency'] = "&#8364;";
                } else if($project['currency'] == 3) {
                    $project['currency'] = "&#163;";
                }
            if($project['st'] == 1 && $project_current_status[0]) { $project_current_status[0]  = false; ?>
                <div class="main-title uppercase group"> Active Projects</div>
            <?php  } elseif($project['st'] == 2 && $project_current_status[1] ) { $project_current_status[1]  = false;?>
                <div class="main-title uppercase group">Past Projects</div>
            <?php } elseif($project['st'] == 0 && $project_current_status[2]) { $project_current_status[2]  = false;?>
                <div class="main-title uppercase group main_title_block">Current Projects</div>
            <?php } ?>
            <div class="project-box single-project group">
                <div class="details group">
                    <div class="user-details group">
                        <div class="avatar group"><img src="<?=$project['file_path'] ?>" alt="#"></div>
                        <a href="<?=base_url();?>projects/<?=md5($project['project_id']);?>" class="simple <?=($project['st'] == 2)?'':'activeProject';?>"><?=$project['name']?><?php if($project['st'] == 2): ?> <i class="fa fa-check"></i><?php endif; ?>
                        </a>
                        <div class="clear"></div>
                        <?php if($project['type'] == 0) : ?>
                            <small><?=$project['currency'] . " " . $project['hourly_rate'] ?>/hour, <?=$project['hours_per_week'];?>h per week</small>
                        <?php else : ?>
                            <small class="milestone"><?=$project['currency']  . " " . number_format($project['fixed_price']);?> fix price, <span class="mile_underline"><?=$project['milCount'];?> milestones</span></small>
                        <?php endif; ?>
                        <p style="    max-height: 55px; overflow: hidden;">
                            <?php if($project['description']):?>
                                    <?=$project['description'];?>
                            <?php else: ?>
                                <p class="developer">No description added.</p>
                                    <p class="developer" id="proj<?=$project["project_id"].'"';?> >
                                        <?php if($project['not_id'] == 4):?>
                                            <span style='color:#7DB440'>You have already sent reminder.</span>
                                            <span class="sendNotification" data-toggle="modal" data-target="#popup_notification" data-id=<?=$project["project_id"];?> >Click here</span> to send it again.
                                        <?php else: ?>
                                            <span class="sendNotification" data-toggle="modal" data-target="#popup_notification" data-id=<?=$project["project_id"];?> >Click here</span> to send reminder to the <?=($this->session->userdata['user_type']==0)? 'client' : 'provider'?>.
                                        <?php endif?>
                                    </p>
                                <?php //endif ?>
                            <?php endif?>
                        </p>
                        <a href="<?=base_url();?>projects/<?=md5($project['project_id']);?>" class="button gray">View Project</a>
                        <a href="<?=base_url();?>projects/edit/<?=md5($project['project_id']);?>" class="button gray">Edit Project</a>
                    </div>
                </div>
                <div class="details-numbers group">
                    <h3>
                        <?=$user_type?"Spent":"Earnings";?>
                    </h3>
                    <?php if($project['st'] == 0) :
                        if($project['type'] == 1){ ?>
                            <p><?=($project['milCountFin']." milestone, ".$project['currency'].number_format($project['milPriceFin']))?></p>
                            <h3 class="margin milestones_title">Milestones</h3>
                            <ul class="milestone-dots group">
                                <li><span class=""></span></li>
                                <li><span></span></li>
                                <li><span></span></li>
                            </ul>
                            <?php }else{?>
                                <p>0 hrs, <?=$project['currency']?>0</p>
                                <h3 style="margin-bottom: 0" class="margin">This week</h3>
                                <div class="status-bar group">
                                    <div class="progress" style="width:0;"></div>
                                </div>
                            <?php }?>
                        <div class="create_date group ">Since <?=date('M d, Y',strtotime($project['created_at']))?></div>
                    <?php else:
                        if($project['type'] == 1){ ?>
                        <p><?=($project['milCountFin']." milestone, ".$project['currency'].number_format($project['milPriceFin']))?></p>
                        <?php }else{?>
                            <p>0 hrs, <?=$project['currency']?>0</p>
                        <?php }?>
                        <h3> completed</h3>
                        <div class="complete-date group"><?=date("M d, Y",strtotime($project['updated_at']));?></div>
                    <?php endif ?>
                </div>
            </div>
        <?php endforeach ;?>
    <?php  else : ?>
        <div class="main-title uppercase group main_title_block"> Active Projects </div>
        <div class="welcome_message_block">
            <img src="<?= base_url('assets/images/message_img.png') ?>">
            <?php if($this->session->userdata()['user_type'] == 1): ?>
                <p class="welcome_message">Thanks for joining Developer Escrow!  We'll be the oil on the wheels of your commerce.</p>
            <?php else: ?>
                <p class="welcome_message">Welcome to Developer Escrow.  Thanks so much for signing up. We're here to make sure you get paid for all your hard work.</p>
            <?php endif;?>
            <a href="<?=base_url()?>new-project" class="start_project_btn">Start your first project now</a>
        </div>
    <?php endif ?>
</div>

<!--modal-->
<div class="modal fade" id="popup_notification" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><a href="javascript:void(0);"><i class="ion-close-round"></i></a></button>
                <h4 class="modal-title"> Send notification</h4>
            </div>
            <div class="modal-body">
                <p class="descriprion_txt">Send a remind to the <?=($this->session->userdata['user_type']==0)? 'client' : 'developer'?> to complate descriprion.</p>
                <input type="hidden" value="" name="projId" class="projId" />
                <div class="text-center">
                    <button class="sendRemindForDescriprion button icon green">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

