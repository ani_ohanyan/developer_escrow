<script type="text/javascript">

        $(document).ready(function() {
            $('select.custom-project').select2({
                theme: "classic",
                placeholder: "Select a state",
            });
        $('#open-new-dispute').on('click', function () {
            $('.error_message').hide();
        });
       $('#description').on('focusout', function(){
        if($('#description').val() == ''){
            $(".error_hide_description").css("display", "block");
        }
        else{
            $(".error_hide_description").css("display", "none");
        }
    });
        $('.other_email').on('focusout', function(){
            if($('.other_email').val() == ''){
                $(".error_hide_party").css("display", "block");
            }
            else{
                $(".error_hide_party").css("display", "none");
            }
        })
    });



    $('#cr_dispute').on('submit', function(e) {
        e.preventDefault();
        //$('#submit_dispute').attr("disabled", true);
        var el = $(this);
        var input = el.find("input");
        var data = $("#cr_dispute").serialize();
        if ($('.other_email').val() == '') {
            $(".error_hide_party").css("display", "block");
        } else if ($('#description').val() == '') {
            $(".error_hide_description").css("display", "block");
        } else if ($('.custom-project').val() == null) {
            $(".error_hide_project").css("display", "block");
        } else {
            $.ajax({
                type: "POST",
                url: base_url + "disputes/create_dispute",
                data: data,
                success: function (response) {
                    if(response.success == false){
                        $('.error_hide_party').css('display','block');
                        $('.error_hide_party .font_error_message').html(response.message);
                    }else{
                        window.location.href = base_url + "disputes";
                        //$('#submit_dispute').attr("disabled", false);
                    }
                }

            });
        }
    });

//    $(".button.green").on("click", function (e) {
//                e.preventDefault();
//                var el = $(this);
//                var input = el.find("input");
//                var data = el.serialize();
//                $.ajax({
//                    type: "POST",
//                    url: base_url + "disputes/create_dispute",
//                    data: data,
//
//                    success: function(msg) {
//                        var msg = JSON.parse(msg);
//                        el.find("input").css({"border": "1px solid transparent"});
//                        el.find(".error").html("");
//                        if (msg.validation) {
//                            $.each(msg.validation, function (key, value) {
//                                el.find("input[name='" + key + "']").css({"border": "1px solid #f00"});
//                                el.find(".error." + key).html(value);
//                            });
//                        }
//                        if (msg.status == "error") {
//                            el.find(".password_or_email").html(msg.value);
//                        } else if (msg.status == "success") {
//                            window.location = base_url;
//                        }
//                    }
//                });
//                return false;
//            });
//
//
//

//        $('select.custom-home').select2({
//            theme: "classic",
//            minimumResultsForSearch: Infinity,
//
//        });

//        $("#login_form").unbind("submit").on("submit", function (e) {
//            e.preventDefault();
//            var el = $(this);
//            var input = el.find("input");
//            var data = el.serialize();
//            $.ajax({
//                type: "POST",
//                url: base_url + "login",
//                data: data,
//                success: function(msg) {
//                    var msg = JSON.parse(msg);
//                    el.find("input").css({"border": "1px solid transparent"});
//                    el.find(".error").html("");
//                    if (msg.validation) {
//                        $.each(msg.validation, function (key, value) {
//                            el.find("input[name='" + key + "']").css({"border": "1px solid #f00"});
//                            el.find(".error." + key).html(value);
//                        });
//                    }
//                    if (msg.status == "error") {
//                        el.find(".password_or_email").html(msg.value);
//                    } else if (msg.status == "success") {
//                        window.location = base_url;
//                    }
//                }
//            });
//            return false;
//        });
</script>