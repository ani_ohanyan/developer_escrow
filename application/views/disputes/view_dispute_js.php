<script>

    $("#disputePublic , #disputePrivate").on("click",function(){
        if($(this).attr('id') == 'disputePublic') {
            el = $("#disputePrivate");
            $("#disputStatus").val(1);
        } else {
            el = $("#disputePublic");
            $("#disputStatus").val(0);
        }
        console.log(el);
        $(this).addClass("checked");
        el.removeClass("checked");


    })
    $("#addDisputeFormId").on('submit',function() {
        event.preventDefault();
        var __this = $(this);
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: 'post',
            data: __this.serialize(),
            dataType: 'text',
            success: function (data) {
                var data = $.parseJSON(data);
                console.log(data);
                if (data.status) {
                    var comment_class;
                    if(data.comment.status == "1"){
                        comment_class = "comment group";
                    }else{
                        comment_class = "comment representative group";
                    }
                    $('.comment_area').val('');
                    $(".dispute_group .comment_block").prepend('<div class="'+comment_class+'"><div class="input group"><p>'+data.comment["message"]+'</p></div><div class="user group"><ul><li><img id="myimg" src="'+base_url+data.comment["path"]+'" alt="#"></li><li><strong>'+data.comment["username"]+'</strong>'+data.comment["date"]+'</li></ul></div></div>');
                }

            },
            error: function () {
                /*$("#addJobAppId").prop("disabled", false);*/
            }
        });
        return false;
    });
//    $("#close_dispute").on("click", function () {
//        $.ajax ({
//            url: base_url+"disputes/close_dispute",
//            type: 'post',
//            data: $(this).serialize(),
//            success: function(data){
//            },
//            error: function(){
//            }
//        });
//
//    });
    $(".disp_view_project").on("click",function(){
        var disp_id = $('.disp_id').val();
        $.ajax({
            url: base_url + "projects/viewProject",
            type: 'post',
            data: {disp_id:disp_id},
            dataType: 'text',
            success: function (data) {
               // var data = $.parseJSON(data);
            },
            error: function () {
            }
        });
    })
    $('.comment_block').perfectScrollbar();
</script>