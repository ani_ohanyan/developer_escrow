<div class="content group">
    <div class="payment-div group">
        <div class="main-title uppercase group main_title_block"> Open Disputes </div>
        <?php if (isset($disputes) && !empty($disputes)):?>

            <?php foreach($disputes as $object):?>
                <?php if($object['status'] != "1"):?>
                   <div class="single-payment group">
                        <div class="text group padding_top_7">
                        <input type="hidden" name="dispute_description" value="<?= $object['description'] ?>">
                        <input type="hidden" name="project_id" value="<?=$object ['project_id'] ?>">
                        <div style="word-break: break-all;"><?= $object['description'] ?></div>
                        </div>
                        <div class="price group">
                            <ul>
                                <?php if($object['date']):?>
                                <li>Last Comment:<small><?=$object['date'] ?></small></li> <!--2 hours ago-->
                                <?php endif ?>
                                <li><a href="<?=base_url()?>disputes/view_dispute/<?=md5($object['id'] )?>" class="button gray">View</a></li>
                            </ul>
                        </div>
                   </div>
                <?php endif;?>
             <?php endforeach ?>
        <?php endif?>
        <div class="clear"></div>
        <p>&nbsp;</p>
        <a href="#" class="button gray icon ion-plus-round" data-toggle="modal" data-target="#popup_new_dispute">New Dispute</a>
    </div>
<!--closed disputes-->
    <div class="payment-div group">
        <div class="main-title uppercase group">Closed Disputes</div>
        <?php if (isset($disputes) && !empty($disputes)):?>
            <?php $i = 0;?>
            <?php foreach($disputes as $object):?>
                <?php $i+=$object['status'] ;?>
                <?php if($object['status']):?>
                    <div class="single-payment group">
                        <div class="text group">
                            <input type="hidden" name="dispute_description" value="<?= $object['description'] ?>">
                            <input type="hidden" name="project_id" value="<?= $object['project_id'] ?>">
                            <div style="word-break: break-all;"><?= $object['description'] ?></div>
                        </div>
                        <div class="price group">
                            <ul>
                                <li>Last Comment:<small><?=$object['date'] ?></small></li>
                                <li><a href="<?=base_url()?>disputes/view_dispute/<?=md5($object['id'])?>" class="button gray">View</a></li>
                            </ul>
                        </div>
                    </div>
                <?php endif;?>
            <?php endforeach ?>

            <?php if($i == 0):?>
                <p class="no_close_disputes"> You don't have closed disputes. </p>
            <?php endif ?>
        <?php else : ?>
            <p class="no_close_disputes"> You don't have closed disputes. </p>
        <?php endif?>
    </div>
</div>


<!-- Popups new dispute -->
<div class="modal fade" id="popup_new_dispute" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><a href="javascript:void(0);" ><i class="ion-close-round"></i></a></button>
                <h4 class="modal-title" id="myModalLabel">Open Dispute</h4>
            </div>
            <div class="modal-body">
                <div class="middle group">
                    <form id="cr_dispute" method="post" action="">
                        <div class="select-custom-div group">
                            <div style="display:none;" class="error_hide_project"><font class="font_error_message"> State is not selected</font></div>
                            <select id="#select_project" class="custom-project" name="project_id">
                                <?php
                                if (isset($projects)) :?>
                                    <option>
                                        Select a project
                                    </option>
                                    <?php if(empty($projects)){ ?>
                                        <option>You Don't have any project</option>
                                    <?php } foreach ($projects as $project) {?>
                                        <option value="<?= $project['project_id'] ?> ">
                                            <?php echo $project['name']; ?>
                                        </option>
                                    <?php  }?>
                                <?php endif ?>
                                <font color="red"> other party information required</font>
                            </select>
                        </div>
                        <div class="clear"></div>
                        <div style="display:none;" class="error_message error_hide_description"><font class="font_error_message"> The description field is required</font></div>
                        <textarea name="description" id="description" placeholder="Describe the problem..."></textarea>
                        <label>Your Contact Information</label>
                        <input type="text"  placeholder="Email Address" value="<?=$user_profile->email?>" disabled>
                        <input type="text"  placeholder="Phone Number" value="<?=(!empty($user_profile->phone_number) ? $user_profile->phone_number : "" );?>" disabled>
                        <p>&nbsp;</p>
                        <label>Other Party Contact Information</label>
                        <div style="display:none;" class="error_message error_hide_party"><font class="font_error_message"> The email field is required</font></div>
                        <input class="other_email" type="text" name="email" placeholder="Email Address" >
                        <input type="text" name="phone_number" placeholder="Phone Number">
                        <p>&nbsp;</p>
                        <div class="clear"></div>
                        <div class="buttons group">
                            <div class="f-left discardbtn"><a href="<?= base_url('disputes')?>" class="button discard_btn">Discard</a></div>
                            <div id = "" class="f-right"><button  id = "submit_dispute" type="submit" class="button green">Submit Dispute</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?=base_url()?>assets/js/classie.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/flaunt.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/custom.js"></script>
