<div class="content group">
    <ul class="top-pages-nav group">
        <li><a href="<?= base_url() ?>disputes">Disputes</a></li>
        <li><?= $dispute['proj_name'] ?></li>
    </ul>
    <div class="project-comments group dispute_group">
        <h3>Description</h3>
        <div class="clear"></div>
        <div class="dispute-desc group">
            <p> <?= $dispute['disp_description'] ?></p>
<!--            <p>&nbsp;</p>-->
        </div>
        <div class="bottom-buttons group">
            <input type="hidden" name="project_id" value="<?= $dispute['proj_id'] ?>">
            <div class="f-left">
                <input type="hidden" value="<?=$dispute['disp_id']?>" class="disp_id">
                <a href="<?=base_url()?>projects/<?=md5($dispute['proj_id'])?>" class="button gray disp_view_project">View project</a>
            </div>
            <form method="post" action="<?= base_url() ?>disputes/close_dispute">
                <input type="hidden" name="dispute_status" value="2">
                <input type="hidden" name="dispute_id" value="<?= $dispute['disp_id'] ?>">
                <?php if (!$dispute['disp_status']) : ?>
                    <div id="close_dispute" class="f-right">
                        <button type="submit" class="button gray">Close dispute</button>
                    </div>
                <?php endif?>
            </form>
        </div>
        <div class="clear"></div>
        <h3>People</h3>
        <div class="dispute-people group">
            <div class="user group">
                <ul>
                    <li>
                        <img src="<?=base_url($dispute['sender_img']) ?>" alt="sender_img">
                    </li>
                    <li>
                        <strong>
                            <?= $dispute['sender_name'] ?>
                        </strong>
                        <?= ($dispute['sender_type'] == 0) ? 'Developer' : 'Client' ?>
                    </li>
                </ul>
            </div>
            <div class="user group">
                <ul>
                    <li>
                        <img src="<?=base_url($dispute['receiver_img']) ?>" alt="receiver_img">
                    </li>
                    <li>
                        <strong>
                            <?= $dispute['receiver_name'] ?>
                        </strong>
                        <?= ($dispute['receiver_type'] == 0) ? 'Developer' : 'Client' ?>
                    </li>
                </ul>
            </div>
            <div class="user group">
                <ul>
                    <li><img src="<?= base_url().$admin['admin_image']; ?>" alt="#"></li>
                    <li><strong><?=$admin['username']?></strong>DE Representative</li>
                </ul>
            </div>
        </div>
        <div class="project-comments project-comment-append group">
            <h3>Discussion</h3>
            <?php if (!$dispute['disp_status']): ?>
            <div class="insert-comment group">
                <div class="input group">
                    <div class="triangle-bg group"></div>
                    <form action="<?= base_url('comments/addComment/dispute_') ?>" method="post"
                          id="addDisputeFormId">
                        <textarea name="message" class="comment_area" placeholder="Add Comment..."
                                  required></textarea>
                        <input type="text" name="status" value="1" class="hidden" id="disputStatus"/>
                        <input type="text" name="dispute_id" value="<?= $dispute['disp_id'] ?>" class="hidden"/>

                        <div class="clear"></div>
                        <ul>
                            <li>
                                <div class="checkbox checked " id="disputePublic">Public</div>
                            </li>
                            <li>
                                <div class="checkbox disputePrivate" id="disputePrivate">Private</div>
                            </li>
                            <li>
                                <button type="submit" class="button gray">Post</button>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
            <?php endif ?>
             <div class="comment_block" >
            <?php if (isset($comments) && !empty($comments)):?>
                <?php foreach ($comments as $comment):?>
                    <?php if ($comment['status']) {
                        $comment_class = "comment group";
                    } else {
                        $comment_class = "comment representative group";
                    }
                    ?>
                    <?php if(!$comment['status']): ?>
                        <?php if($comment['user_id'] == $this_id || ($comment['user_id'] == 0 && $dispute['sender_id'] == $this_id ) ) :?>
                            <div class="<?= $comment_class ?>">
                                <div class="input group">
                                    <p><?= $comment['message']; ?></p>
                                </div>
                                <div class="user group">
                                    <ul>
                                        <li>
                                            <img src="<?=base_url().$comment['comment_image'];?>" alt="#">
                                        </li>
                                        <li>
                                            <strong><?= $comment['username']; ?></strong><?=$comment['date']; ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php endif ?>
                    <?php else:?>
                        <div class="<?= $comment_class ?>">
                            <div class="input group">
                                <p><?= $comment['message']; ?></p>
                            </div>
                            <div class="user group">
                                <ul>
                                    <li>
                                        <img src="<?=base_url().$comment['comment_image'];?>" alt="#">
                                    </li>
                                    <li>
                                        <strong><?= $comment['username']; ?></strong><?=$comment['date']; ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>
            <?php endif ?>
                    <div class="comment group">
                        <div class="input group">
                            <p>Welcome to your project. If you need help please read the <span class="mile_underline">How to use Developer Escrow.</span></p>
                        </div>
                        <div class="user group">
                            <ul>
                                <li>
                                    <img src="<?=base_url().$admin['admin_image'];?>" alt="#">
                                </li>
                                <li>
                                    <strong><?= $admin['username']; ?></strong><?=$dispute['admin_comment_date'];?>
                                </li>
                            </ul>
                        </div>
                    </div>
             </div>
        </div>
    </div>
</div>