<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Email </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<table align="center"  cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
 <tr>
 	<td>
	 	<table style="width:100%;background-color:#F6F6F6;">
	 		<tr style="background-color:#F6F6F6;">
	 			<td style="background-color:#F6F6F6;padding-left:30px;padding-top: 6px;padding-bottom: 6px;">
	 				<img  width="44" height="44" style="display: block;" alt="logo" src="http://40.76.205.167/mail_them/logo.png"/>
	 			</td>
	 			<td style="text-align:right;background-color:#F6F6F6;padding-left:20px;padding-top: 5px;padding-bottom: 6px;">
	 				<a style="color:#B8B8B8;font-size: 12px;font-family: Arial;margin-right: 30px;"  href="#">View in your browser</a>
	 			</td>
	 		</tr>
	 	</table>
 	</td>
 </tr>
  <tr>
 	<td>
	 	<table style="width:100%;">
	 		<tr>
	 			<td style="padding-left: 65px;padding-top: 35px;padding-bottom:5px;">
	 				<h1 style="margin-bottom: 5px;font-size:36px;color:#7DB440;font-family: sans-serif;">Hi <?=$username?>,</h1>
	 			</td>
      </tr>
	 	</table>
	 	<table style="width:100%;">
	 		 <tr>
				<td style="padding-left: 65px;">
					<p style="font-size: 18px;font-family: sans-serif;color: #5A5250;width: 500px;line-height: 25px;margin-top: 0px;">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vehicula turpis sed tempor
            ullamcorper. Vestibulum lobortis velit purus, id pellentesque ex ornare in.
          </p>
				</td>
	 		 </tr>
       <tr>
				<td style="padding-left: 65px;">
					<p style="font-size: 18px;font-family: sans-serif;color: #5A5250;width: 500px;line-height: 25px;margin-top: 0px;">
            <a style="color:#4B89DC;text-decoration:none;" href="http://developerescrow.com/">http://www.developerescrow.com</a>
          </p>
				</td>
	 		 </tr>
       <tr>
       <td style="padding-left: 65px;">
         <p style="font-size: 18px;font-family: sans-serif;color: #5A5250;width: 500px;line-height: 25px;margin-top: 0px;">
            Praesent condimentum, elit iaculis condimentum auctor,
            eros mauris ultricies odio, nec molestie lectus leo eget velit. Etiam
          </p>
       </td>
      </tr>
      <tr>
      <td style="padding-left: 65px;">
         <h3 style="color:#5A5250;text-transform:uppercase;font-family: sans-serif;"><span style=" display: inline-block;width: 16px;height: 16px;border-radius: 16px;-moz-border-radius: 16px;-webkit-border-radius: 16px;background-color: #7db440;color: #FFF;font-size: 14px;line-height: 18px;text-align: center;  margin-right: 10px;">1</span>Section WIth Photo</h3>
         <p style="font-size: 18px;font-family: sans-serif;color: #5A5250;width: 500px;line-height: 25px;margin-top: 25px;">
           Lorem ipsum dolor sit amet, consectetur adipiscing elit.risus.
            Sed fermentum eros nunc, in faucibus tellus fringilla at.
         </p>
      </td>
     </tr>
     <tr>
       <td style="padding-left: 65px;">
          <img  width="580" height="361" style="display: block;" alt="images" src="http://40.76.205.167/mail_them/image.png">
       </td>
     </tr>
     <tr>
     <td style="padding-top:30px;padding-left: 65px;">
        <h3 style="color:#5A5250;text-transform:uppercase;font-family: sans-serif;"><span style=" display: inline-block;width: 16px;height: 16px;border-radius: 16px;-moz-border-radius: 16px;-webkit-border-radius: 16px;background-color: #7db440;color: #FFF;font-size: 14px;line-height: 18px;text-align: center;  margin-right: 10px;">2</span>Section With list</h3>
        <p style="margin-bottom:0px;font-size: 18px;font-family: sans-serif;color: #5A5250;width: 500px;line-height: 25px;margin-top: 25px;">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit.risus.
           Sed fermentum eros nunc, in faucibus tellus fringilla at.
        </p>
     </td>
    </tr>
	 	</table>
    <table style="width:100%;">
      <tr>
      <td style="padding-left: 65px;padding-bottom:5px;">
        <p style="font-size:18px;font-family: sans-serif;color:#5A5250;margin-bottom:15px;">» Lorem ipsum dolor</p>
        <p style="font-size:18px;font-family: sans-serif;color:#5A5250;margin-bottom:15px;">» Consectetur adipiscing</p>
        <p style="font-size:18px;font-family: sans-serif;color:#5A5250;margin-bottom:15px;">» Nulla vel eros sit amet</p>
        <p style="font-size:18px;font-family: sans-serif;color:#5A5250;margin-bottom:15px;">» Porttitor et id urna</p>
        <p style="font-size:18px;font-family: sans-serif;color:#5A5250;margin-bottom:15px;">» Fusce tincidunt est nec</p>

      </td>
      <td style="padding-left:0px;padding-bottom:5px;">
        <p style="font-size:18px;font-family: sans-serif;color:#5A5250;margin-bottom:15px;">» Lorem ipsum dolor</p>
        <p style="font-size:18px;font-family: sans-serif;color:#5A5250;margin-bottom:15px;">» Consectetur adipiscing</p>
        <p style="font-size:18px;font-family: sans-serif;color:#5A5250;margin-bottom:15px;">» Nulla vel eros sit amet</p>
        <p style="font-size:18px;font-family: sans-serif;color:#5A5250;margin-bottom:15px;">» Porttitor et id urna</p>
        <p style="font-size:18px;font-family: sans-serif;color:#5A5250;margin-bottom:15px;">» Fusce tincidunt est nec</p>
      </td>
     </tr>
  </table>
    <table style="width:100%;">
      <tr>
      <td style="padding-left: 65px;">
         <h3 style="color:#5A5250;text-transform:uppercase;font-family: sans-serif;"><span style=" display: inline-block;width: 16px;height: 16px;border-radius: 16px;-moz-border-radius: 16px;-webkit-border-radius: 16px;background-color: #7db440;color: #FFF;font-size: 14px;line-height: 18px;text-align: center;  margin-right: 10px;">3</span>Section with buttons</h3>
         <p style="margin-bottom:0px;font-size: 18px;font-family: sans-serif;color: #5A5250;width: 500px;line-height: 25px;margin-top: 25px;">
           Lorem ipsum dolor sit amet, consectetur adipiscing elit.risus.
            Sed fermentum eros nunc, in faucibus tellus fringilla at.
         </p>
      </td>
     </tr>
    </table>
    <table style="width:100%; margin-top: 40px; margin-bottom: 40px;">
      <tr>
      <td style="padding-left: 65px;padding-bottom:5px;">
        <a href="#" style="text-decoration: none"><span style="cursor:pointer;outline: 0;border: 0px; background: #7DB440;color: #fff;padding: 18px 60px 18px 60px;border-radius: 30px;font-family: sans-serif;font-size: 18px;width: 95%;">
          Call to Action
        </span></a>

      </td>
      <td style="padding-left:0px;padding-bottom:5px;">
          <a href="#" style="text-decoration: none"><span style="cursor:pointer;margin-right:10px;padding: 7px 30px 7px 30px;outline: 0;border: 0px;font-size: 14px;color: #fff;background-color: #4B89DC;border-radius: 3px;text-transform: uppercase;font-family: sans-serif;">
            button
        </span></a>
        <a href="#" style="text-decoration: none"><span style="cursor:pointer;padding: 7px 30px 7px 30px;outline: 0;border: 0px;font-size: 14px;color: #5A5250;background-color: #B8B8B8;border-radius: 3px;text-transform: uppercase;font-family: sans-serif;">
              button
          </span></a>
      </td>
     </tr>
  </table>
 	</td>
 </tr>
 <tr style="border:0px;">
  <td style="margin-left:65px;border:0px;padding-top:15px;padding-bottom:40px; font-family: sans-serif;font-size:14px;">
    <table style="width: 82%;margin-left: 65px;background-color: #F6F6F6;">
      <tr>
      <td style="padding: 15px 10px 15px 60px;">
        <a style="margin-right:40px;" href="#"><img alt="soc1" src="http://40.76.205.167/mail_them/icon-facebook.png" /></a>
        <a style="margin-right:40px;" href="#"><img alt="soc2" src="http://40.76.205.167/mail_them/icon-twitter.png" /></a>
        <a style="margin-right:40px;" href="#"><img alt="soc3" src="http://40.76.205.167/mail_them/icon-google.png" /></a>
        <a style="margin-right:40px;" href="#"><img alt="soc4" src="http://40.76.205.167/mail_them/icon-linkedin.png" /></a>
        <a style="margin-right:40px;" href="#"><img alt="soc5" src="http://40.76.205.167/mail_them/icon-pinest.png" /></a>
        <a style="margin-right:40px;" href="#"><img alt="soc6" src="http://40.76.205.167/mail_them/icon-youtube.png" /></a>
        <a style="margin-right:40px;" href="#"><img alt="soc7" src="http://40.76.205.167/mail_them/icon-skype.png" /></a>
      </td>

     </tr>
  </table>
  </td>
 </tr>
 <tr>
 	<td>
	 	<table style="width:100%;background-color:#5A5250;">
	 		<tr>
	 			<td style="padding-left:20px;padding-top:20px;padding-bottom:20px;font-family: sans-serif;color:#fff;font-size:14px;">
	 				© Copyright 2015, DeveloperEscrow.com
	 			</td>
	 			<td style="text-align:right;padding-left:20px;padding-top:20px;padding-bottom:20px;">
	 				<a style="color: #B8B8B8;font-size: 14px;font-family: sans-serif;margin-right: 30px;"  href="#">
	 				Unsubscribe
	 				</a>
	 			</td>
	 		</tr>
	 	</table>
 	</td>
 </tr>
</table>
</body>
</html>
