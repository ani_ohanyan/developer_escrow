<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Email </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <style>
        .reset_password_btn span:hover {
            background: transparent!important;
            border: 2px solid  #7DB440!important;
            color: #7DB440!important;
        }
    </style>
</head>
<body style="margin: 0; padding: 0;">
<table align="center"  cellpadding="0" cellspacing="0" width="650" style="border-collapse: collapse;">
    <tr>
        <td>
            <table style="width:100%;background-color:#F6F6F6;">
                <tr style="background-color:#F6F6F6;">
                    <td style="background-color:#F6F6F6;padding-left:30px;padding-top: 6px;padding-bottom: 6px;">
                        <img  width="44" height="44" style="display: block;" alt="logo" src="<?=base_url()?>assets/images/mail_them/logo.png"/>
                    </td>
                    <td style="text-align:right;background-color:#F6F6F6;padding-left:20px;padding-top: 5px;padding-bottom: 6px;">
                        <a style="color:#B8B8B8;font-size: 12px;font-family: Arial;margin-right: 30px;"  href="<?=base_url()?>">View in your browser</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table style="width:100%;">
                <tr>
                    <td style="padding:0 65px;padding-top: 35px;padding-bottom:5px;">
                        <h1 style="margin-bottom: 5px;font-size:36px;color:#7DB440;font-family: sans-serif;">Hi <?=$user['username']?>,</h1>
                    </td>
                </tr>
            </table>
            <table style="width:100%;">
                <tr>
                    <td style="padding:0 65px;">
                        <p style="font-size: 25px;font-family: sans-serif;color: #5A5250;width: 100%;line-height: 40px;margin-top: 0;margin-bottom: 10px">
                           Your new password in DeveloperEscrow is
                        </p>
                        <input value="<?=$password?>" type="text" style="display: block;text-align: center;color: #4B89DC;margin: 0 auto;background-color: #F6F6F6;border: 0;height: 40px;font-size: 16px;margin-bottom: 10px">
                    </td>
                </tr>
                <tr>
                    <td style="padding:0 65px;">
                        <p style="color: #B8B8B8;text-align: center;font-size: 18px;font-family: sans-serif;">
                           Repeat Your new password and click on to
                        </p>

                    </td>
                </tr>
            </table>

            <table style="width:100%; margin-bottom: 40px;">
                <tr>
                    <td style="padding: 0 65px;padding-bottom:5px;">
                        <a href="<?=base_url()?>login" class="reset_password_btn" style="text-decoration: none">
                            <span style="cursor:pointer;
                            outline: 0;
                                border: 2px solid #7DB440;
                             background: #7DB440;color: #fff;
                             padding: 10px;
                             width: 40%;
                             border-radius: 30px;
                             font-family: sans-serif;
                             font-size: 18px;
                             text-transform: uppercase;
                             margin: 0 auto;
                             text-align: center;
                             display: table;
                             ">
                        Log in
                        </span></a>

                    </td>
                </tr>
            </table>
            <table style="width:100%; margin-top: 40px; margin-bottom: 40px;">
                <tr>
                    <td style="padding: 0 65px">
                        <p style="text-align: center;font-size: 18px;font-family: sans-serif;color: #5A5250;width: 100%;line-height: 25px;">If you ignore this message, your password won't be changed.
                            If you didn't request a password reset, please <a href="<?=base_url()?>" style="color:#4B89DC;text-decoration: none"> let us know</a></p>
                    </td>
                </tr>
            </table>
            <table style="width: 82%;margin-left: 65px;background-color: #F6F6F6;margin-bottom: 30px">
                <tr>
                    <td style="padding: 15px 10px 15px 60px;">
                        <a style="margin-right:40px;" href="#"><img alt="soc1" src="<?=base_url()?>assets/images/mail_them/icon-facebook.png" /></a>
                        <a style="margin-right:40px;" href="#"><img alt="soc2" src="<?=base_url()?>assets/images/mail_them/icon-twitter.png" /></a>
                        <a style="margin-right:40px;" href="#"><img alt="soc3" src="<?=base_url()?>assets/images/mail_them/icon-google.png" /></a>
                        <a style="margin-right:40px;" href="#"><img alt="soc4" src="<?=base_url()?>assets/images/mail_them/icon-linkedin.png" /></a>
                        <a style="margin-right:40px;" href="#"><img alt="soc5" src="<?=base_url()?>assets/images/mail_them/icon-pinest.png" /></a>
                        <a style="margin-right:40px;" href="#"><img alt="soc6" src="<?=base_url()?>assets/images/mail_them/icon-youtube.png" /></a>
                        <a style="margin-right:40px;" href="#"><img alt="soc7" src="<?=base_url()?>assets/images/mail_them/icon-skype.png" /></a>
                    </td>

                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table style="width:100%;background-color:#5A5250;">
                <tr>
                    <td style="padding-left:20px;padding-top:20px;padding-bottom:20px;font-family: sans-serif;color:#fff;font-size:14px;">
                        &copy; Copyright 2015, DeveloperEscrow.com
                    </td>
                    <td style="text-align:right;padding-left:20px;padding-top:20px;padding-bottom:20px;">
                        <a style="color: #B8B8B8;font-size: 14px;font-family: sans-serif;margin-right: 30px;"  href="<?=base_url()?>">
                            Unsubscribe
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
