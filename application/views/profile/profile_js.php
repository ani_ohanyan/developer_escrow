<!--<script type="text/javascript" src="https://js.stripe.com/v2/"></script>-->
<script type="text/javascript">
    Stripe.setPublishableKey('pk_test_eqsukeBAJlSnSn3XiVws1UyQ');
    $('.checkbox.notif').on('click',function(e){
        e.preventDefault();
        $( ".checkbox.notif.active" ).removeClass( "active" );
        $(this).addClass("active");
        var data = $(this).data("value");
        $('#hidden').val(data);

    })
    $('.checkbox.val').on('click',function(e){
        e.preventDefault();
        $( ".checkbox.val.active" ).removeClass( "active" );
        $(this).addClass("active");
        var data = $(this).data("value");
        $('#hidden').val(data);

    })
    $('.checkbox.cur').on('click',function(e){
        e.preventDefault();
        $( ".checkbox.cur.active" ).removeClass( "active" );
        $(this).addClass("active");
        var data = $(this).data("value");
        $('#hidden').val(data);

    })
    $('.checkbox.pre-paid').on('click',function(e){
        e.preventDefault();
        $( ".checkbox.pre-paid.active" ).removeClass( "active" );
        $(this).addClass("active");
        var data = $(this).data("value");
        $('#hidden').val(data);

    })
    $('.checkbox.pay').on('click',function(e){
        e.preventDefault();
        $( ".checkbox.pay.active" ).removeClass( "active" );
        $(this).addClass("active");
        var data = $(this).data("value");
        $('#hidden').val(data);

    })
    $(document).ready(function default_notification() {
        var act =  $('#hidden').val();
        if(act == ''){
            $('#default').addClass("active");
            var data = $('#default').data("value");
            $('#hidden').val(data);
        }
    });
    $('#settingsFormId').on('submit', function(e){
        e.preventDefault();
        var el = $(this);
        var input = el.find("input");
        var data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: base_url + "users/save_changes",
            data: data,
            success: function (msg){
                $('.old_password').val('');
                $('.new_password').val('');
                if(msg == true){
                    $('.successfull').css('display', 'block');
                    setTimeout(function(){ $('.successfull').css('display', 'none'); }, 4000);
                }else{
                    $('.failed').css('display', 'block');
                    setTimeout(function(){ $('.failed').css('display', 'none'); }, 4000);
                }
            }

        })
        return false;
    });


//    $('.checkbox').on('click',function(e){
//        e.preventDefault();
//        $( ".checkbox.active" ).removeClass( "active" );
//        $(this).addClass("active");
//        var data = $(this).data("value");
//        $('#hidden').val(data);
//
//    })
    jQuery(function($) {
//        $('#payment-form').submit(function(event) {
//            var $form = $(this);
//
//            // Disable the submit button to prevent repeated clicks
//            $form.find('button').prop('disabled', true);
//
//            Stripe.card.createToken($form, stripeResponseHandler);
//
//            // Prevent the form from submitting with the default action
//            return false;
//        });
    });

    function stripeResponseHandler(status, response) {
        var $form = $('#payment-form');
        if (response.error) {
            // Show the errors on the form
            $form.find('.payment-errors').text(response.error.message);
            $form.find('button').prop('disabled', false);
        } else {
            // response contains id and card, which contains additional card details
            var token = response.id;
            // Insert the token into the form so it gets submitted to the server
            $form.append($('<input type="hidden" name="stripeToken" />').val(token));
//            // and submit
//            $form.get(0).submit();
            add_cards();
        }
    }

    /*******************/

    //    $('#addcardbutton').on('click', function(){
    $('#payment-form').submit(function(event) {
        event.preventDefault();
        var $form = $(this);
        // Disable the submit button to prevent repeated clicks
        $form.find('button').prop('disabled', true);
        Stripe.card.createToken($form, stripeResponseHandler);
        // Prevent the form from submitting with the default action
//            return false;
    });

    function add_cards(){
        var card_number = $('#add-card').find('input[name=card_number]').val();
        var cvc = $('#add-card').find('input[name=cvc]').val();
        var expire_month = $('#add-card').find('input[name=expire_month]').val();
        var expire_year = $('#add-card').find('input[name=expire_year]').val();
        $.ajax({
            url: base_url + "users/add_card",
            type: 'post',
            data: {card_number:card_number, cvc:cvc, expire_month:expire_month, expire_year:expire_year},
            dataType: 'text',
            success: function (data){
                $('#add-card').modal('hide');
                $('#add-card').find('input[name=card_number]').val('');
                $('#add-card').find('input[name=cvc]').val('');
                $('#add-card').find('input[name=expire_month]').val('');
                $('#add-card').find('input[name=expire_year]').val('');
                $('#add-card').find('.payment-errors').val('');
            }
        });
        return false;
    }


    $('#generate').on('click', function(){
        $.ajax({
            url: base_url + "users/generate_token",
            success: function (msg){
                var data =   $.map(msg, function(value, key) {
                    return [key, value];
                });
                $('div').find('#api_key').html(data['1']);
                $('div').find('#secret_key').html(data['3']);
            },
            error: function(msg){
            }

        });
        return false;
    });


    $('#changeUserImg').on('change', function(e) {
        $('.loading').show();
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('img_file', file_data);
        $.ajax({
            url: base_url+'users/updateUserAvatar',
            type: 'post',
            data: form_data,
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                var data = $.parseJSON(data);
                if(data.status){
                    $('.image .user-picture').css('background-image','url('+base_url+data.path+')');
                    $('img.user_profile_img').attr('src',base_url+data.path);
                    $('.loading').hide();
                }
            }
        });
    });

//    {
//            $.ajax({
//                url: base_url + "notification_sender/send_notification",
//                type: 'post',
//                data: data,
//                success: function (data) {
//                    var data = $.parseJSON(data);
//                    //console.log(data);
//                },
//                error: function () {
//                    /*$("#addJobAppId").prop("disabled", false);*/
//                }
//            });
//            var msg = JSON.parse(msg);
//            window.location.href = base_url + "disputes";
//        }




    /*   enable   */
    $('.enable').on('click', function(){
        var type = $(this).attr('data');
        $.ajax({
            url: base_url + "users/enable",
            type: 'post',
            data: {type:type},
            dataType: 'text',
            success: function (data){
                $('#add-card').modal('hide');
            }
        });
        return false;
    })

    /*   disable   */
    $('.disable').on('click', function(){
        var type = $(this).attr('data');
        $.ajax({
            url: base_url + "users/disable",
            type: 'post',
            data: {type:type},
            dataType: 'text',
            success: function (data){
                $('#add-card').modal('hide');
            }
        });
        return false;

    })


    // save information in settings part
    $('.tab-content-2').find('.green').on('click', function(){
        var bank_name = $('.tab-content-2').find('input[name=bank_name]').val();
        var account_number = $('.tab-content-2').find('input[name=account_number]').val();
        var routing_number = $('.tab-content-2').find('input[name=routing_number]').val();
        var payment_account = $('.tab-content-2').find('input[name=paypal]').val();
        $.ajax({
            url: base_url + "users/add_payment_information",
            type: 'post',
            data: {bank_name:bank_name, account_number:account_number, routing_number:routing_number, payment_account:payment_account},
            dataType: 'text',
            success: function (data){
            }
        });
        return false;
    })

</script>