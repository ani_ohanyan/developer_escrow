<div class="content group">
    <div class="main-title group main_title_block">
        <ul>
            <li><a href="javascript:void(0);" id="tab-link-1" class="active">General Settings</a></li>
            <li><a href="javascript:void(0);" id="tab-link-2" class="">Payment Options</a></li>
            <li><a href="javascript:void(0);" id="tab-link-3" class="">People & Projects</a></li>
        </ul>
    </div>
    <div class="project-box group">
        <div class="tab-content-1 hourly-div group display">
            <div class="row-settings group avatarImg">
                <h3>Profile Photo</h3>
                <div class="image">
                    <div class="user-picture" style="background-image:url(<?= base_url() . $this->session->userdata('avatarPath'); ?>);">
                    </div>
                </div>
                <label class="editImg"><i class="fa fa-pencil"></i>
                    <input id="changeUserImg" name="img_file" type="file"/>
                </label>
            </div>
            <div class="row-settings group">
                <h3>Api Key</h3>
                <div id="api_key" class="row-settings group keys">
                    <?= $api_key ?>
                </div>
                <h3>Secret Key</h3>
                <div id="secret_key" class="row-settings group keys">
                    <?= $secret_key ?>
                </div>
                <!--<button  class="button green ">Generate</button>-->
                <div class="clear"></div>
                <input id="generate" type="submit" class="green_button" value="Generate">
            </div>
            <p>&nbsp;</p>
            <h3>Profile</h3>
            <form method="post" class="profile_settings" action="<?= base_url() ?>users/save_changes" id="settingsFormId">
                <div class="row-settings group">
                    <input type="text" name="name" placeholder="<?= $user_profile->username ?>" value="<?= $user_profile->username ?>">
                </div>
                <div class="row-settings group">
                    <input type="text" name="email" placeholder="<?= $user_profile->username ?>" value="<?= $user_profile->email ?>">
                </div>
                <div class="row-settings group">
                    <input type="text" name="phone_number" placeholder="Phone Number" value="<?=(!empty($user_profile->phone_number) ? "$user_profile->phone_number" : ""); ?>">
                </div>
                <h3 class="block_titles2">Password</h3>
                <div class="row-settings group">
                    <input type="password" class="old_password" name="old_password" placeholder="Old Password" value="" autocomplete="off">
                </div>
                <div class="row-settings group">
                    <input type="password" class="new_password" name="new_password" placeholder="New Password" value="">
                </div>
                <h3 class="block_titles">Notifications</h3>
                <div class="left-box group" style="margin-top: 0">
                    <input id="hidden" type="hidden" name="notification" value="<?= !empty($user_profile->notifications) ? $user_profile->notifications : 'immediate' ?>">
                    <ul class="group">
                        <li>
                            <a href="#" data-value="immediate" class="checkbox notif <?= ($user_profile->notifications == 'immediate' || $user_profile->notifications == '' ) ? 'active' : '' ?>">Immediate</a>
                        </li>
                        <li>
                            <a href="#" data-value="hourly" class="checkbox notif <?= ($user_profile->notifications == 'hourly') ? 'active' : '' ?>">Hourly</a>
                        </li>
                        <li>
                            <a href="#" data-value="daily" class="checkbox notif <?= ($user_profile->notifications == 'daily') ? 'active' : '' ?>">Daily Summary</a></li>
                    </ul>
                </div>
                <div class="row-settings group">
                    <div class="successfull">Your changes have been successfully saved</div>
                    <div class="failed"> Saving failed, Please input correct data</div>
                </div>
                <div class="clear"></div>
                <input type="submit" class="green_button" value="Save">
            </form>
        </div>

        <div class="tab-content-2 hourly-div group">
            <h3>Pre-Paid ATM card</h3>
            <div class="pre-paid-card group">
                <img src="<?= base_url('assets/images/icon-credit-card.png') ?>" class="image-auto mobile-inline-only">
                <div class="clear"></div>
                <span data-toggle="modal" data-target="#add-card" class="btn apply-here">  Apply here</span>
                <div class="clear"></div>
                <p>1.5% additional charge applies + $20 one-off charge to print and send the card.</p>

                <div class="left-box right group">
                    <ul class="group">
                        <li><a href="#" data="card" class="checkbox pre-paid active enable">Enabled</a></li>
                        <li><a href="#" data="bank" class="checkbox pre-paid disable">Disabled</a></li>
                    </ul>
                </div>
            </div>

            <div class="separator-line group"></div>
            <div class="padding225 group">
                <h3>Add Bank Information</h3>
<!--                <form method="post" action="--><?//= base_url("Users/add_bank_information") ?><!--">-->
                    <div class="row-settings client group">
                        <input type="text" name="bank_name" placeholder="Bank Name" value="">
                    </div>
                    <div class="row-settings client group">
                        <input type="text" name="account_number" placeholder="Account Number" value="">
                    </div>
                    <div class="row-settings client group">
                        <input type="text" name="routing_number" placeholder="Routing Number" value="">
                    </div>
                    <h3 class="block_titles">Currency</h3>
                    <div class="left-box group  no-margin">
                        <ul class="group">
                            <li><a href="#" class="checkbox val active">USD</a></li>
                            <li><a href="#" class="checkbox val">EUR</a></li>
<!--                            <li><a href="#" class="checkbox val">GBP</a></li>-->
                        </ul>
                    </div>
                    <div class="left-box right group">
                        <ul class="group">
                            <li><a href="#" data="bank" class="checkbox cur active enable">Enabled</a></li>
                            <li><a href="#" data="bank" class="checkbox cur disable">Disabled</a></li>
                        </ul>
                    </div>
            </div>
            <div class="separator-line group"></div>
            <div class="padding225 group">
                <h3>Add Paypal</h3>
                <div class="row-settings nomarginbottom client group">
                    <input type="text" name="paypal" placeholder="ronald_smith@gmail.com" value="">
                </div>
                <div class="left-box right group">
                    <ul class="group">
                        <li><a href="#" data="paypal"  class="checkbox pay active enable">Enabled</a></li>
                        <li><a href="#" data="paypal" class="checkbox pay disable">Disabled</a></li>
                    </ul>
                </div>
            </div>
            <p>&nbsp;</p>
            <button class="button green big w140">Save</button>
<!--            </form>-->
        </div>

        <div class="tab-content-3 hourly-div group">
            <h3>Current Projects</h3>
            <div class="table four-col group">
                <?php if (isset($people) && !empty($people)) : ?>
                    <?php foreach($people as $p):?>
                        <?php if($p['status'] == 0):?>
                            <div class="row group">
                                <div class="col group">
                                    <div class="table-div">
                                        <div class="table-div-cell">
                                            <img src="<?= base_url('assets/images/client-image.jpg') ?>" alt="#" class="round">
                                            <span><strong><?=$p['username'] ?></strong></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col text-center group">
                                    <div class="table-div">
                                        <div class="table-div-cell">
                                            <span>Projects: <strong><?=$p['proj_count']?></strong></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col text-center group">
                                    <div class="table-div">
                                        <div class="table-div-cell">
                                            <span>Last Project: <strong><?= $p['created_at'] ?></strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif  ?>
                    <?php endforeach ?>
                <?php endif  ?>
            </div>
            <h3>Past Projects</h3>
            <div class="table four-col group">
                <?php if (isset($people) && !empty($people)) : ?>
                    <?php foreach($people as $p):?>
                        <?php if($p['status'] == 2):?>
                            <div class="row group">
                                <div class="col group">
                                    <div class="table-div">
                                        <div class="table-div-cell">
                                            <img src="<?= base_url('assets/images/client-image.jpg') ?>" alt="#" class="round">
                                            <span><strong><?=$p['username'] ?></strong></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col text-center group">
                                    <div class="table-div">
                                        <div class="table-div-cell">
                                            <span>Projects: <strong><?=$p['proj_count']?></strong></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col text-center group">
                                    <div class="table-div">
                                        <div class="table-div-cell">
                                            <span>Last Project: <strong><?= $p['created_at'] ?></strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif  ?>
                    <?php endforeach ?>
                <?php endif  ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-card" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content stripe">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style=" margin-top: 15px; ">
                    <span><i class="ion-close-round"></i></span>
                </button>
                <h1> Add Card </h1>
            </div>
            <form action="#" method="POST" id="payment-form">
                <div class="modal-body">
                        <span class="payment-errors">
                        </span>
                        <div class="form-row">
                            <label>
                                <span class="text">Card Number</span>
                                <input class="stripe_input" name="card_number" type="text" size="20" data-stripe="number" autocomplete="off"/>
                            </label>
                        </div>
                        <div class="form-row">
                            <label>
                                <span class="text">CVC</span>
                                <input class="stripe_input" name="cvc" type="text" size="4" data-stripe="cvc" autocomplete="off"/>
                            </label>
                        </div>
                        <div class="form-row">
                            <span class="text">Expiration (MM/YYYY)</span>
                            <input id="month" class="stripe_input" type="text" name="expire_month" size="2" data-stripe="exp-month" autocomplete="off"/>
                            <span> /
                            <input id="year" class="stripe_input" type="text" name="expire_year" size="4" data-stripe="exp-year" autocomplete="off"/> </span>
                        </div>
                </div>
                <div class="modal-footer">
                    <div class="buttons group">
                        <div class="modal_payment f-left">
                            <button class="button" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                        <div class="f-right">
                            <button id="addcardbutton" type="submit" class="button green">Add card</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="overlay"></div>