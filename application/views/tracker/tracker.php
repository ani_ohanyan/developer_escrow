    <div class="body-inner">
        <span class="sendNotification" data-toggle="modal" data-target="#TrackerModal" >Click here</span>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="TrackerModal" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div id="time_tracker_page">
                        <div class="no-padding container wrapper">
                            <div class="content">
                                <div class="logo_section">
                                    <a href="#"><img src="<?=base_url();?>assets/images/logo_time_tracker.png" /></a>
                                </div>
                                <form action="" method="post" enctype="multipart/form-data">
                                    <div class="form_section">
                                        <div class="col-md-6 no-padding">
                                            <p class="select_title">Select Project</p>
                                            <select name="project_id" class="contractProject">
                                                <?php foreach($projects as $project) { ?>
                                                    <option value="<?=$project['id'];?>"><?=$project['name'];?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class=" col-md-6 counter_hrs_min_section">
                                            <p class="select_title">Enter Time</p>
                                            <input id="input-number-mod" class="mod" name="hr" min="0" max="24" type="number" value="0" />
                                            <span>hrs</span>
                                            <input id="input-number-mod2" class="mod" name="min" min="0" max="59" type="number" value="0" />
                                            <span>min</span>
                                        </div>

                                        <div class="description form-group">
                                            <textarea  placeholder="Add Description" rows="4" id="description_time_tracker" name="description" class="form-control"></textarea>
                                        </div>
                                        <div class="col-md-12 no-padding">
                                            <div class="uploadField choose_file">
                                                <span>Upload Files </span>
                                                <input type="file" id="files" name="job_attachment[]" class="filesListUpload" multiple>
                                            </div>
                                            <div id="selectedFiles"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="btn_work">
                                        <input type="submit" value="Submit Work" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

