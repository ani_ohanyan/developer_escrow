<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Diary_model extends MY_Model
{
    /**
     * Set validation rules
     *
     * @var array
     */
    protected $validate = array(
        array(
            'field' => 'hr',
            'label' => 'Hour',
            'rules' => 'required'
        ),
        array(
            'field' => 'min',
            'label' => 'Minute',
            'rules' => 'required'
        ),
        array(
            'field' => 'description',
            'label' => 'Tracker Description',
            'rules' => 'required|min_length[10]|trim'
        ),
        array(
            'field' => 'job_attachment[]',
            'label' => 'Files',
            'rules' => 'min_length[5]|max_length[255]|valid_email|trim'
        )
    );

    public function __construct() {
        parent::__construct();
    }

    public function get_all_projects($user_id, $provider_id) {
        $sql = "SELECT p.id, p.name FROM projects AS p
                LEFT JOIN user_projects AS up on(up.project_id = p.id)
                WHERE p.user_id = ".$user_id."
                AND up.user_id = ".$provider_id."
                UNION
                SELECT p.id, p.name FROM projects AS p
                LEFT JOIN user_projects AS up ON (up.project_id = p.id)
                WHERE up.user_id = ".$user_id."
                AND p.user_id = ".$provider_id;
        return $this->custom_get($sql);
    }

    public function get_all_providers($user_id) {
        $sql =  "SELECT DISTINCT u.id, u.username FROM users AS u
                LEFT JOIN projects as p ON(p.user_id = u.id)
                LEFT JOIN user_projects as up ON (up.project_id = p.id)
                WHERE p.user_id != ".$user_id." AND up.user_id = ".$user_id."
                UNION
                SELECT DISTINCT u.id, u.username FROM users AS u
                LEFT JOIN user_projects AS up ON (up.user_id = u.id)
                LEFT JOIN projects as p ON(p.id = up.project_id)
                WHERE up.user_id !=".$user_id." and p.user_id = ".$user_id;
        //." AND p.id =".$proj_id
        return $this->custom_get($sql);
    }

    public function get_diaries($this_user, $project= null, $provider, $to_date = null, $from_date = null){
        $query = '';
        $query1 = '';
        $query2 = '';
        $query3 = '';
        $query4 = '';
        if($provider){
            $query1 = " AND p.user_id = ".$provider ;
            $query = " AND up.user_id = ".$provider;
        }
        if($to_date){
            $query2 = " AND d.date >= '".$to_date."'";
        }
        if($from_date){
            $query3 = " AND d.date <= '".$from_date."'" ;
        }
        if($project){
            $query4 = "AND d.project_id = ".$project;
        }
        $sql = "SELECT * FROM
                    (SELECT DISTINCT
                            d.*, p.name,
                            m.name AS milestone
                        FROM
                            diaries AS d
                        LEFT JOIN projects AS p ON (d.project_id = p.id)
                        LEFT JOIN milestones AS m ON (m.id = d.milestone_id)
                        WHERE
                            p.user_id != ".$this_user." ".$query1." ".$query2." ".$query3." ".$query4." AND d.user_id = ".$this_user."
                    UNION
                        SELECT DISTINCT
                            d.*, p.name,
                            m.name AS milestone
                        FROM
                            diaries AS d
                        LEFT JOIN projects AS p ON (d.project_id = p.id)
                        LEFT JOIN milestones AS m ON (m.id = d.milestone_id)
                        LEFT JOIN user_projects AS up ON (up.project_id = p.id)
                        WHERE
                            up.user_id != ".$this_user." ".$query." ".$query2." ".$query3." ".$query4 ."
                    ) AS result
                ORDER BY
                    result.date DESC";
        return $this->custom_get($sql);

    }

    public function getDiaryFiles($ids){
        $sql = "SELECT attachment_id as diary_id, path, name, type
                FROM files
                WHERE attachment_id IN ($ids)
                AND class = 'diary'";
        return $this->custom_get($sql);
    }

}