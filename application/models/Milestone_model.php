<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Milestone_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getMilestones($id){
        $sql = "SELECT * FROM milestones  WHERE project_id =".$id." AND status = 0";
        $sql1 = "SELECT * FROM milestones  WHERE project_id =".$id." AND status = 1";
        $data['mil'] = $this->custom_get($sql);
        $data['milFin'] = $this->custom_get($sql1);
        return $data;
    }
}