<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dispute_comment_model extends MY_Model
{

    public function __construct() {
        parent::__construct();
    }

    public function getComment($id) {
        $sql = "SELECT d.*,u.username, f.path
                FROM dispute_comments AS d
                LEFT JOIN users AS u ON (u.id = d.user_id)
                LEFT JOIN files AS f ON (f.user_id = u.id)
                WHERE d.id = ".$id;
        return $this->custom_get($sql);
    }

    public function lastComment($did){
        $sql = "SELECT max(date) FROM dispute_comments WHERE dispute_id = ".$did;
        return $this->custom_get($sql);
    }
}