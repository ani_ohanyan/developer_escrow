<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Project_File_model extends MY_Model
{


    public function __construct()
    {
        parent::__construct();
    }


    public function findFileByData($user_id, $search ,$type) {
        if($type){
            $sql = "SELECT * FROM project_files pf LEFT JOIN projects p ON (p.id = pf.project_id) LEFT JOIN files f ON (f.id = pf.file_id) WHERE p.user_id = ".$user_id." AND file_description LIKE '%".$search."%' ORDER by f.date DESC";
        } else {
            $sql = "SELECT * FROM project_files pf LEFT JOIN user_projects p ON (p.id = pf.project_id) LEFT JOIN files f ON (f.id = pf.file_id) WHERE p.user_id = ".$user_id." AND file_description LIKE '%".$search."%' ORDER by f.date DESC";
        }

        return $this->custom_get($sql);
    }

}