<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends MY_Model
{

    /**
     * Set validation rules
     *
     * @var array
     */
    protected $validate = array(
        array(
            'field' => 'user_type',
            'label' => 'User Type',
            'rules' => 'required'
        ),
        array(
            'field' => 'username',
            'label' => 'Name',
            'rules' => 'required|min_length[1]|max_length[255]|trim'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|min_length[5]|max_length[255]|valid_email|trim|is_unique[users.email]'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|min_length[1]|max_length[255]|trim|matches[r_password]'
        ),
        array(
            'field' => 'r_password',
            'label' => 'Repeat Password',
            'rules' => 'required|min_length[1]|max_length[255]|trim'
        )
    );

    /**
     * The various callbacks available to the model. Each are
     * simple lists of method names (methods will be run on $this).
     */
    protected $before_create = array("removeData");


    public function __construct()
    {
        parent::__construct();
    }

    public function removeData($data)
    {
        if(!empty($data["signup"])) {
            unset($data["signup"]);
        }
        if(!empty($data["password"]) && !empty($data["r_password"])) {
            $data["password"] = md5($data["password"]);
            unset($data["r_password"]);
        }
        return $data;
    }
    public function update_data_by($arg)
    {
        if(!empty($arg)){
            $result = $this->update_by($arg);
            return $result;
        }

    }
    public function  get_data_by($arg)
    {
        if(!empty($arg)){
            $result = $this->get_by($arg);
            return $result;
        }
    }

//    public function getUserProfileDetails($user_id) {
//        $user_profile = $this->db->query(
//            "SELECT u.profile_image_id,u.user_type,u.email,u"
//        );
//
//        return $user_profile;
//    }

}