<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Link_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getProjectLinks($project_id){
        $sql = "SELECT * FROM links WHERE project_id = ".$project_id." ORDER by date DESC";
        $data['list'] = $this->custom_get($sql);
        $sql = "SELECT IF(count(*),count(*),0) as lc FROM links WHERE project_id = ".$project_id;
        $data['count'] = $this->custom_get($sql);
        return $data;
    }

    public function findLinksByData($user_id, $search) {
        $sql = "SELECT * FROM links WHERE user_id = ".$user_id." AND (name LIKE '%".$search."%' AND link LIKE'%".$search."%') ORDER by date DESC";
        return $this->custom_get($sql);
    }
}