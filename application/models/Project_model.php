<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Project_model extends MY_Model
{

    /**
     * Set validation rules
     *
     * @var array
     */
    protected $validate = array(
        array(
            'field' => 'name',
            'label' => 'name',
            'rules' => 'required'
        ),
        array(
            'field' => 'description',
            'label' => 'Project Description',
            'rules' => 'required|min_length[40]|trim'
        ),
        array(
            'field' => 'freelancer',
            'label' => 'Invite Freelancer',
            'rules' => 'min_length[5]|max_length[255]|valid_email|trim'
        )
    );

    /**
     * The various callbacks available to the model. Each are
     * simple lists of method names (methods will be run on $this).
     */

    protected $after_get = array("modifyData");

    public function __construct()
    {
        parent::__construct();
    }

    public function getProjects($user_id) {
        //if($type) {getProjects
        $sql = "SELECT p.*, u.username ,p.id as project_id ,p.status as st, un.notification_id as not_id
                    FROM
                        projects p
                    LEFT JOIN users u ON (u.id = p.user_id)
                    LEFT JOIN user_projects up ON (up.project_id = p.id)
                    LEFT JOIN (SELECT * FROM user_notifications WHERE notification_id = 4)AS un ON (p.id = un.project_id)
                    WHERE p.user_id = ".$user_id." OR up.user_id  = ".$user_id."
                    GROUP BY p.id
                    ORDER BY st,p.created_at DESC";
//        } else {
//            $sql = "SELECT up.*,p.*, u.username ,p.id as project_id ,p.status as st , un.notification_id as not_id
//                    FROM
//                        user_projects up
//                    LEFT  JOIN
//                        projects p ON (p.id = up.project_id)
//                    LEFT JOIN users u ON (u.id = p.user_id)
//                    LEFT JOIN (SELECT * FROM user_notifications WHERE notification_id = 4)AS un ON (p.id = un.project_id)
//                    WHERE up.user_id = ".$user_id."
//                    GROUP BY p.id
//                    ORDER BY st,p.created_at  DESC";
//        }
        //Where p.status = '1'
        return $this->custom_get($sql);
    }

    public function getProjectById($id){
        $sql = "SELECT
                    * ,p.created_at as pr_create_date,p.id as pr_id,p.status as st ,u.id as user_id, up.user_id as inv_user_id
                FROM
                    projects p
                LEFT JOIN users u ON (u.id = p.user_id)
                LEFT JOIN user_projects up ON (up.project_id = p.id)
                Where MD5(p.id) = '".$id."'";
        return $this->custom_get($sql);
    }

    public function modifyData($data)
    {
        if(!empty($data)) {
            $data->id = md5($data->id);
            if($data->currency == 1) {
                $data->currency = "$";
            } else if($data->currency == 2) {
                $data->currency = "&#8364;";
            } else if($data->currency == 3) {
                $data->currency = "&#163;";
            }
        }
        return $data;
    }

    public function getStatistic($user_id , $type){
        if($type) {
            $sql = "SELECT count(*) as pr_count FROM projects where user_id = ".$user_id;
            $sql1 = "SELECT count(*) as user_count FROM user_projects up LEFT JOIN projects p ON(p.id = up.project_id) where p.user_id = ".$user_id;
        } else {
            $sql = "SELECT count(*) as pr_count FROM user_projects up LEFT JOIN projects p ON (p.id = up.project_id)  where up.user_id = ".$user_id;
            $sql1 = "SELECT count(*) as user_count FROM projects p LEFT JOIN user_projects up ON(p.id = up.project_id) where up.user_id = ".$user_id;
        }
        $data['pr_count'] = $this->custom_get($sql);
        $data['user_count'] = $this->custom_get($sql1);
        return $data;
    }
    public function get_projects_by_id($id){
        $sql = "SELECT
                    * ,p.id as pr_id
                FROM
                    projects p
                    Where p.id = '".$id."'";

        return $this->custom_get($sql);
    }

    public function getPeopleAndProjects($user){
        $sql = "SELECT DISTINCT
                  COUNT(p.id) as proj_count,
	              p.*, u.username, u.id AS u_id
                FROM
                    projects as p
                LEFT JOIN user_projects up ON (p.id = up.project_id)
                LEFT JOIN users u ON (u.id = up.user_id)
                WHERE
                    p.user_id = '".$user."'
                GROUP BY p.`status`, u_id
                UNION
                SELECT DISTINCT
                    COUNT(p.id) as proj_count,
                    p.*, u.username, u.id AS u_id
                FROM
                    projects as p
                LEFT JOIN user_projects up ON (p.id = up.project_id)
                LEFT JOIN users u ON (u.id = p.user_id)
                WHERE
                    up.user_id = '".$user."'
                GROUP BY p.`status`, u_id";
        return $this->custom_get($sql);
    }

    public function get_users_project_count($user_id){
        $sql = "SELECT count(*)
                from projects  p
                JOIN user_projects up ON (p.id = up.project_id)
                where up.user_id = '".$user_id."'";
        return $this->custom_get($sql);
    }

    public function get_client_project_count($id) {
        $sql = "select count(*) as cost_count
                from projects  p
                where p . user_id = '".$id."'";
        return $this->custom_get($sql);
    }

//    public function get_users_by_project($project_id, $status){
//        $sql = "SELECT DISTINCT
//                p. NAME,
//                p.created_at,
//                p.user_id,
//                u1.id,
//                u1.username AS name2,
//                u.username AS name1,
//                p.`status`
//            FROM
//                projects p
//            LEFT JOIN user_projects up ON (p.id = up.project_id)
//            LEFT JOIN users u ON (u.id = p.user_id)
//            LEFT JOIN users u1 ON (u1.id = up.user_id)
//            WHERE
//                MD5(up.project_id) = '".$project_id."'
//            AND p.`status` =  '".$status."'";
//
//        return $this->custom_get($sql);
//    }

}