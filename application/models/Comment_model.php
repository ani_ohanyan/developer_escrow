<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Comment_model extends MY_Model
{
    /**
     * The various callbacks available to the model. Each are
     * simple lists of method names (methods will be run on $this).
     */
    public function __construct()
    {
        parent::__construct();
    }

    protected $validate = array(
        array(
            'field' => 'message',
            'label' => 'Add Comment',
            'rules' => 'required|max_length[255]|trim'
        )
    );

    public function getCommentsByProject($id , $type = 0){
        $sql = "SELECT d.*,u.username, f.path, d.id  as com_id
                FROM comments d
                LEFT JOIN users u ON (u.id = user_id)
                LEFT JOIN files as f ON (f.id = u.profile_image_id)
                WHERE d.project_id = ".$id." AND d.type = '".$type."'
                ORDER BY d.date DESC";
        return $this->custom_get($sql);
    }

    public function getComment($id){
        $sql = "SELECT c.*,u.username, f.path
                FROM comments c
                LEFT JOIN users u ON (u.id = c.user_id)
                LEFT JOIN files AS f ON (f.id = u.profile_image_id)
                WHERE c.id = ".$id;
        return $this->custom_get($sql);
    }

    public function getCommentFiles($ids){
        $sql = "SELECT attachment_id as com_id, path, name, type
                FROM files WHERE attachment_id IN ($ids) AND class = 'project_comment'";
        return $this->custom_get($sql);
    }
}