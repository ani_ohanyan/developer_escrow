<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getPaymentProjects($this_user_id, $id = null) {
        if($id){
            $sql1 = " AND md5(d.project_id) = '".$id."'";
        } else {
            $sql1 = "";
        }
        $sql = //first for client, second for developer
                "SELECT d.*, SUM(d.time) AS diary_time, p.name, p.type, p.currency , p.hourly_rate, p.hours_per_week, SUM(m.price) AS mil_price, pay.pay, pym.pay AS mil_pay
                FROM diaries AS d
                LEFT JOIN projects AS p ON (p.id = d.project_id)
                LEFT JOIN payments as pay ON (p.id = pay.project_id)
                LEFT JOIN milestones AS m ON (m.id = d.milestone_id)
                LEFT JOIN payments as pym ON (m.id = pym.milestone_id)
                LEFT JOIN users as u ON(p.user_id = u.id)
                WHERE p.user_id = ".$this_user_id."
                AND u.user_type = 1 ".$sql1."
                GROUP BY d.project_id
                UNION
                SELECT d.*, SUM(d.time) AS diary_time, p.name, p.type, p.currency, p.hourly_rate, p.hours_per_week, SUM(m.price) AS mil_price, pay.pay, pym.pay AS mil_pay
                FROM diaries AS d
                LEFT JOIN projects AS p ON (p.id = d.project_id)
                LEFT JOIN payments as pay ON (p.id = pay.project_id)
                LEFT JOIN milestones AS m ON (m.id = d.milestone_id)
                LEFT JOIN payments as pym ON (m.id = pym.milestone_id)
                LEFT JOIN user_projects as up ON(up.project_id = p.id)
                WHERE up.user_id = ".$this_user_id." ".$sql1."
                GROUP BY d.project_id";
        $result = $this->custom_get($sql);
        foreach( $result as &$res){
            if($res['type'] == 0){    // hourly work
                $pay = (round($res['diary_time']/60).",".($res['diary_time']%60))*$res['hours_per_week']*$res['hourly_rate'];
                $res['pay'] = $pay;
            } else {                  // fixed work
                $res['pay'] = $res['mil_price'];
            }
            if($res['currency'] == 1) {
                $res['currency'] = "$";
            } else if($res['currency'] == 2) {
                $res['currency'] = "&#8364;";
            } else if($res['currency'] == 3) {
                $res['currency'] = "&#163;";
            }
            $res['link'] = $res['project_id'];
        }
        return $result;
    }

    public function getProjectMilestones($this_user_id, $id = null) {
        if($id){
            $sql1 = " AND md5(d.milestone_id) ='".$id."'";
        } else {
            $sql1 = "";
        }
        $sql = "SELECT d.*, m.name, p.type, p.currency , p.hourly_rate, p.hours_per_week, SUM(m.price) AS mil_price, pay.pay, pym.pay AS mil_pay
                FROM diaries AS d
                LEFT JOIN projects AS p ON (p.id = d.project_id)
                LEFT JOIN payments as pay ON (p.id = pay.project_id)
                LEFT JOIN milestones AS m ON (m.id = d.milestone_id)
                LEFT JOIN users as u ON(p.user_id = u.id)
                LEFT JOIN payments as pym ON (m.id = pym.milestone_id)
                WHERE p.user_id = ".$this_user_id."
                AND u.user_type = 1 ".$sql1."
                GROUP BY d.milestone_id
                UNION
                SELECT d.*, m.name, p.type, p.currency, p.hourly_rate, p.hours_per_week, SUM(m.price) AS mil_price, pay.pay, pym.pay AS mil_pay
                FROM diaries AS d
                LEFT JOIN projects AS p ON (p.id = d.project_id)
                LEFT JOIN payments as pay ON (p.id = pay.project_id)
                LEFT JOIN milestones AS m ON (m.id = d.milestone_id)
                LEFT JOIN payments as pym ON (m.id = pym.milestone_id)
                LEFT JOIN user_projects as up ON(up.project_id = p.id)
                WHERE d.user_id = ".$this_user_id." ".$sql1."
                GROUP BY d.milestone_id";
        $result = $this->custom_get($sql);
        foreach( $result as &$res){
            if($res['currency'] == 1) {
                $res['currency'] = "$";
            } else if($res['currency'] == 2) {
                $res['currency'] = "&#8364;";
            } else if($res['currency'] == 3) {
                $res['currency'] = "&#163;";
            }
            if($res['type'] == 0){    // hourly work
                $pay = (round($res['time']/60).",".($res['time']%60))*$res['hours_per_week']*$res['hourly_rate'];
                $res['pay'] = $pay;
            } else {                  // fixed work
                $res['pay'] = $res['mil_price'];
            }
            $res['link'] = $res['milestone_id'];
        }
        return $result;
    }


}