<?php defined('BASEPATH') OR exit('No direct script access allowed');

class File_model extends MY_Model {

    protected $before_create = array("pushUserId");

    public function __construct() {
        parent::__construct();
    }

    /**
     * @param array $data
     * @return array
     */
    public function pushUserId(ARRAY $data) {
        if(empty($data['user_id'])){
            $data['user_id'] = $this->user_id;
        }
        return $data;
    }

    public function insertProjectFiles($data) {
        $this->_database->insert('project_files', $data);
        $insert_id = $this->_database->insert_id();
        return $insert_id;

    }

    public function getFiles($project_id) {
        $sql = "SELECT * FROM files f WHERE f.attachment_id = ".$project_id." ORDER by f.date DESC";
        return $this->custom_get($sql);
    }
}