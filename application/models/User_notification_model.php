<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_notification_model extends MY_Model
{
    /**
     * The various callbacks available to the model. Each are
     * simple lists of method names (methods will be run on $this).
     */
    public function __construct() {
        parent::__construct();
    }

    public function getUserNotificationById($id){

        $sql = "SELECT un.date, un.type_id, un.notification_id AS not_id, u.username, n.message, p.name AS proj_name, un.type, un.project_id, un.is_read, un.id
                FROM user_notifications AS un
                LEFT JOIN users AS u ON (un.sender_id = u.id)
                LEFT JOIN projects AS p ON (p.id = un.project_id)
                LEFT JOIN notifications AS n ON (un.notification_id = n.id)
                WHERE  md5(un.user_id) ='". $id ."'
                ORDER BY un.date DESC";
        $notes = $this->custom_get($sql);
        for( $i=0; $i < count($notes); $i++ ){
            $find = array("<'client'>", "<'project'>");
            $replace = array("<b>".$notes[$i]['username']."</b>", "<a href='".base_url('projects/'.md5($notes[$i]['project_id']))."'>".'"'.$notes[$i]['proj_name'].'"'."</a>");
            $notes[$i]['message'] = str_ireplace($find, $replace, $notes[$i]['message']);
        }
        return $notes ;
    }

    public function getlastNotes($id , $lmt ){
        $note = array();
        $noteR = array();
        $sql = "SELECT un.date, un.type_id, un.notification_id AS not_id, u.username, n.message, p.name AS proj_name, un.type, un.project_id, un.is_read, un.id
                FROM user_notifications AS un
                LEFT JOIN users AS u ON (un.sender_id = u.id)
                LEFT JOIN projects AS p ON (p.id = un.project_id)
                LEFT JOIN notifications AS n ON (un.notification_id = n.id)
                WHERE  md5(un.user_id) ='". $id ."'  AND un.is_read = 0
                ORDER BY un.date DESC
                 LIMIT 0,".$lmt ;
        $notes = $this->custom_get($sql);
        if(count($notes) < $lmt) {
            $l = $lmt - count($notes);
            $sql2 = "SELECT un.date, un.type_id, un.notification_id AS not_id, u.username, n.message, p.name AS proj_name, un.type, un.project_id, un.is_read, un.id
                FROM user_notifications AS un
                LEFT JOIN users AS u ON (un.sender_id = u.id)
                LEFT JOIN projects AS p ON (p.id = un.project_id)
                LEFT JOIN notifications AS n ON (un.notification_id = n.id)
                WHERE  md5(un.user_id) ='". $id ."'  AND un.is_read = 1
                ORDER BY un.date DESC
                 LIMIT 0,".$l;
            $noteR = $this->custom_get($sql2);
            $notes = array_merge($notes, $noteR);
        }
        for( $i=0; $i < count($notes); $i++ ){
            $find = array("<'client'>", "<'project'>");
            $replace = array("<b>".$notes[$i]['username']."</b>", "<a href='".base_url('projects/'.md5($notes[$i]['project_id']))."'>".'"'.$notes[$i]['proj_name'].'"'."</a>");
            $notes[$i]['message'] = str_ireplace($find, $replace, $notes[$i]['message']);
        }
        return $notes ;
    }

    public function getNoteCount($id){
        $sql = "SELECT count(*) as not_count
                FROM user_notifications as n
                WHERE n.user_id = ".$id."  AND  n.is_read = 0";
        $data = $this->custom_get($sql);
        $data = reset($data);
        return $data;
    }
}