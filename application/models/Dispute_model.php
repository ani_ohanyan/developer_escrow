<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dispute_model extends MY_Model
{

    public function __construct()    {
        parent::__construct();
    }

    public function getAllDisputesById($uId){
        $sql = "SELECT d.*, dc.date
                FROM disputes AS d
                LEFT JOIN ( SELECT * FROM (SELECT * FROM dispute_comments as a ORDER BY date DESC )  AS b GROUP BY b.dispute_id)
                AS dc ON (dc.dispute_id = d.id)
                WHERE ".$uId." = d.user_id OR ".$uId." = d.receiver_id
                ORDER BY d.id DESC ";
        return $this->custom_get($sql);
    }

    public function getDisputesById($dispute_id){
        $sql = "SELECT p.name AS proj_name, p.id AS proj_id, d.description AS disp_description, d.id AS disp_id, d.status AS disp_status, d.date AS disp_date,
                         u_receiver.username AS receiver_name, u_receiver.user_type AS receiver_type, u_receiver.id as receiver_id,
                         u_sender.username AS sender_name, u_sender.user_type AS sender_type, u_sender.id AS sender_id,
                         reseiver_img.path AS receiver_img, sender_img.path AS sender_img
                FROM disputes AS d
                LEFT JOIN users AS u_sender ON (d.user_id = u_sender.id)
                LEFT JOIN users AS u_receiver ON (d.receiver_id = u_receiver.id)
                LEFT JOIN projects AS p ON (d.project_id = p.id)
                LEFT JOIN files AS reseiver_img ON (u_receiver.id = reseiver_img.user_id)
                LEFT JOIN files AS sender_img ON (u_sender.id = sender_img.user_id)
                WHERE md5(d.id) = '".$dispute_id."'";
        $result = $this->custom_get($sql);
        $result = reset($result);
        return $result;
    }

    public function getDisputeComments($dispute_id){
        $sql = "SELECT comment_user.user_type, c.date, c.status AS status , c.message, comment_user.username, c.user_id as user_id
                FROM dispute_comments AS c
                LEFT JOIN disputes AS d ON (c.dispute_id = d.id)
                LEFT JOIN projects AS p ON (d.project_id = p.id)
                LEFT JOIN users AS comment_user ON (c.user_id = comment_user.id)
                WHERE md5(c.dispute_id) = '".$dispute_id."'
                ORDER BY c.date DESC" ;
        return $this->custom_get($sql);
    }
//    public function  getProjectsByUser($user_id)
//    {
//        $sql = "SELECT * FROM Projects where user_id =".$user_id;
//    }

}