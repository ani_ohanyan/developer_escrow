<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    /* --------------------------------------------------------------
     * VARIABLES
     * ------------------------------------------------------------ */
    /**
     * Logged user id
     */
    protected $user_id;
    /**
     * Checking is user logged status
     */
    protected $check_status = FALSE;
    /**
     * Checking is user logged
     */
    protected $check_is_login = FALSE;
    /**
     * Checking is admin logged
     */
    protected $check_is_admin_login = FALSE;
    /**
     * View variable
     */
    protected $title = '';
    protected $header_data = array();
    protected $header = "layout/header";
    protected $page;
    protected $footer = "layout/footer";
    protected $data_template = array();
    protected $scripts = "layout/scripts";
    /**
     * A list of helpers to be autoloaded
     */
    protected $validation_rules = array();
    /**
     * A list of models to be autoloaded
     */
    protected $models = array();
    /**
     * A formatting string for the model autoloading feature.
     * The percent symbol (%) will be replaced with the model name.
     */
    protected $model_string = '%_model';
    /**
     * A list of helpers to be autoloaded
     */
    protected $helpers = array();
    /**
     * A list of libraries to be autoloaded
     */
    protected $libraries = array();
    /**
     * side of Project
     */
    protected $side = 'project';
    /**
     * Checking is user logged type is client
     */
    protected $check_is_user_developer = FALSE;
    /* --------------------------------------------------------------
     * GENERIC METHODS
     * ------------------------------------------------------------ */
    /**
     * Initialise the controller, tie into the CodeIgniter superobject
     * and try to autoload the models and helpers
     */
    public function __construct() {
        parent::__construct();
        $this->_load_models();
        $this->_load_libraries();
        $this->_load_helpers();
        if ($this->check_is_login) {
            $this->is_login();
        }
        if ($this->check_is_admin_login) {
            $this->is_admin();
        }
        if ($this->check_status) {
            $this->status();
        }
        if ($this->session->userdata('is_client_login')) {
            $this->user_id = $this->session->userdata('is_client_login');
        }
    }

    /* --------------------------------------------------------------
     * VIEW LOADING
     * ------------------------------------------------------------ */
    protected function render()
    {
        if($this->user_id){
//            $header_data['notifications'] = $this->user_notification_model->getUserNotificationById(md5($this->user_id));
            $header_data['notifications'] = $this->user_notification_model->getlastNotes(MD5($this->user_id) , 5);
//             echo ' <pre>' ; print_r($page_data['notifications'] ); die;
            $header_data['not_count'] = $this->user_notification_model->getNoteCount($this->user_id);
        }
        $header_data["title"] =$this->title;
        $header_data["header_data"] = $this->header_data;

        $scripts = $this->scripts;
        $side = $this->side;

        $header = $this->header;
        $footer = $this->footer;
        if($side == 'admin'){
            $header = "layout/admin_header";
            $footer = "layout/admin_footer";
            $scripts = $this->scripts;
        } elseif ($this->session->userdata('is_client_login') ) {
            $header_data["logged"] = TRUE;
            $header_data["user_type"] = $this->session->userdata('user_type');

            if ($this->check_is_login){
                $user = $this->user_model->get_by('id', $this->user_id);
            }
            $statistic = $this->project_model->getStatistic($this->user_id,$header_data["user_type"]);
        }  elseif($this->session->userdata('is_client_login') && $side == "none") {
            $header = "layout/tracker_header";
            $footer = "layout/tracker_footer";
        } else {
            $header_data["logged"] = FALSE;
            $header_data["user_type"] = FALSE;
        }

        $page = $this->page;
        $page_js = $page . "_js";

        $page_data = $this->data_template;
        $page_data['page'] = $page;
        if(!empty($user)) {
            $page_data['user'] = $user;
        }
        if(!empty($statistic)) {
            $page_data['stat'] = $statistic;
        }
        if(!empty($header)) {
            $this->load->view($header, $header_data);
        }
        $this->load->view($page, $page_data);
        $this->load->view($page_js);
        $this->load->view($scripts);
        if(!empty($footer)) {
            $this->load->view($footer);
        }
    }
    /* --------------------------------------------------------------
     * MODEL LOADING
     * ------------------------------------------------------------ */
    /**
     * Load models based on the $this->models array
     */
    protected function _load_models()
    {
        foreach ($this->models as $model) {
            $this->load->model($this->_model_name($model));
        }
    }

    /**
     * Returns the loadable model name based on
     * the model formatting string
     */
    private function _model_name($model)
    {
        return str_replace('%', $model, $this->model_string);
    }

    /* --------------------------------------------------------------
     * HELPER LOADING
     * ------------------------------------------------------------ */
    /**
     * Load helpers based on the $this->helpers array
     */
    protected function _load_helpers()
    {
        foreach ($this->helpers as $helper) {
            $this->load->helper($helper);
        }
    }

    /* --------------------------------------------------------------
     * HELPER LOADING
     * ------------------------------------------------------------ */
    /**
     * Load libraries based on the $this->libraries array
     */
    protected function _load_libraries()
    {
        foreach ($this->libraries as $library) {
            $this->load->library($library);
        }
    }

    /* --------------------------------------------------------------
     * CHECKING LOGGED USER
     * ------------------------------------------------------------ */
    protected function is_login()
    {
        if (!$this->session->userdata('is_client_login')) {
            redirect(base_url("login"));
        }
    }
    /* --------------------------------------------------------------
     * CHECKING LOGGED ADMIN
     * ------------------------------------------------------------ */
    protected function is_admin()
    {
        if (!$this->session->userdata('is_admin_login') && $this->uri->segment(1) != "admin" ) {
            redirect(base_url("home"));
        }
    }

    /* --------------------------------------------------------------
     * CHECKING LOGGED USER STATUS
     * ------------------------------------------------------------ */
    protected function status()
    {
        if ($this->session->userdata('is_client_login') && $this->uri->segment(1) !== "logout") {
            if($this->session->userdata('user_type') == 1) {
                if($this->uri->segment(1) !== "providers" && $this->uri->segment(1) !== "new-project") {
                    redirect(base_url("/providers"));
                }
            } else {
                if($this->uri->segment(1) !== "projects" && $this->uri->segment(1) !== "project") {
                    redirect(base_url("/projects"));
                }
            }
        }
    }

    /* --------------------------------------------------------------
     * Forms validation
     * ------------------------------------------------------------ */

    protected function validate($type)
    {
        $rules = $this->validation_rules[$type];
        $this->form_validation->set_rules($rules);
        $errors = array();
        if (!$this->form_validation->run()) {
            foreach ($rules as $value) {
                $errors[$value["field"]] = form_error($value["field"]);
            }
            $errors = array_filter($errors);

            return $errors;
        }
        return true;
    }

    /* --------------------------------------------------------------
     * Uploading file
     * ------------------------------------------------------------ */

    protected function upload_file($file, $type = 'gif|jpg|png')
    {
        $user_folder = md5($this->user_id);
        $path = 'upload/users/' . $user_folder;
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $config['upload_path'] = $path;
        if (!empty($type))
            $config['allowed_types'] = $type;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($file)) {
            $data = array('error' => $this->upload->display_errors());
        } else {
            $data = $this->upload->data();
            $file_path = $path . "/" . $data["orig_name"];
            return array(
                "name" => $data["orig_name"],
                "type" => $data["file_type"],
                "size" => $data["file_size"],
                "path" => $file_path,
                "full_path" => $data["full_path"],
                "date" => time()
            );
        }
        return $data;
    }

    protected function upload_files($files /*,$limit = 5*/ )
    {
        // $type = $this->dwnTypes;
        $user_folder = md5($this->user_id);
        $path = 'upload/users/' . $user_folder;
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $config['upload_path'] = $path;
        $config['allowed_types'] = '*';

        $this->load->library('upload', $config);

        $images = array();
        $datas = array();
        foreach ($files['name'] as $key => $image) {
            /*if($limit--  > 0   ) {*/
                $_FILES['images[]']['name'] = $files['name'][$key];
                $_FILES['images[]']['type'] = $files['type'][$key];
                $_FILES['images[]']['tmp_name'] = $files['tmp_name'][$key];
                $_FILES['images[]']['error'] = $files['error'][$key];
                $_FILES['images[]']['size'] = $files['size'][$key];

                if ($this->upload->do_upload('images[]')) {
                    $data = $this->upload->data();
                    $file_path = $path . "/" . $data["file_name"];
                    array_push($datas, array(
                            "name" => $data["orig_name"],
                            "type" => $data["file_type"],
                            "size" => $data["file_size"],
                            "path" => $file_path,
                            "full_path" => $data["full_path"],
                            "date" => time())
                    );

                /*}*/
            }
        }

        return $datas;
    }

    /* --------------------------------------------------------------
     * Sending email
     * ------------------------------------------------------------ */

    protected function sendmail($to, $from, $subject, $message = array())
    {
        $this->load->library('email');
        $config = [
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.sendgrid.net',
            'smtp_port' => 587,
            'smtp_user' => 'novp_ap',
            'smtp_pass' => '3rone!mE',
            'charset' => "utf-8",
            'mailtype' => "html",
            'newline' => "\r\n"
        ];
        $this->email->initialize($config);
        $this->email->set_mailtype("html");


        if (is_array($from)) {
            $this->email->from($from['name'], $from['address']);
        } else {
            $this->email->from($from);
        }


        $this->email->subject($subject);
        if ($message) {
            $this->email->message(html_entity_decode($message));
        }
        $sendLog = [];


        if (is_array($from)) {
            $this->email->from($from['name'], $from['address']);
        } else {
            $this->email->from($from);
        }

        if (is_array($to) && count($to) > 1) {
            foreach ($to as $email) {
                $this->email->to($email);

                if ($this->email->send()) {
                    $statusCode = 'sent';
                } else {
                    $statusCode = 'not sent';
                }

                $sendLog[] = [
                    'statusCode' => $statusCode,
                    'email' => $email,
                ];
            }
        } else {
            $this->email->to($to);
            if ($this->email->send()) {
                $statusCode = 'sent';
            } else {
                $statusCode = 'not sent';
            }

            $sendLog[] = [
                'statusCode' => $statusCode,
                'email' => $to,
            ];
        }

        return $sendLog;
    }
    function timeAgo($time_ago)  {

        if(empty($time_ago)) {
            return "";
        }
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );

        // Seconds
        if($seconds <= 60){
            return "just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "one min ago";
            }
            else{
                return "$minutes mins ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour ago";
            }else{
                return "$hours hrs ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "one year ago";
            }else{
                return "$years years ago";
            }
        }
    }

    protected function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function noteCount(){
        $CI =& get_instance();
        $ud = $CI->session->all_userdata();
        $data = $CI->user_notification_model->getNoteCount($ud['is_client_login']);
        return $data['not_count'];
    }
}