<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Links extends MY_Controller
{
    /**
     * Checking is user logged
     */
    protected $check_is_login = TRUE;
    protected $models = array('link');

    public function __construct()
    {
        parent::__construct();
    }

    public function addLink(){
        $data = array();
        $data['status'] = false;
        $pr = $_POST;
        $pr['user_id'] =  $this->user_id;
        $pr['date'] =date("Y-m-d", time());

        $link_id = $this->link_model->insert($pr,FALSE);
        if($link_id) {
            $data = $_POST;
            $data['date'] = date("d M, Y",strtotime($pr['date']));
            $data['status'] = true;
        }

        echo json_encode($data);exit;
    }


}