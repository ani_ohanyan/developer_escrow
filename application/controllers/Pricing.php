<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pricing extends MY_Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->title = "Developer Escrow | Pricing";
        $this->page = "pricing/pricing";
        $this->render();
    }

}