<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Files extends MY_Controller
{

    /**
     * @var array
     */
    protected $models = array('file','project_file');

    /**
     * Checking is user logged status
     */
   // protected $check_status = TRUE;

    public function __construct()
    {
        parent::__construct();
    }

    public function download() {
        $file = $_GET['file'];
        $name = $_GET['name'];

        $this->load->helper('download');
        $data = file_get_contents(base_url().$file);
        force_download($name,$data);
    }
}