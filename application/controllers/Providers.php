<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Providers extends MY_Controller
{

    /**
     * @var array
     */
    protected $models = array('project','file', 'milestone', 'project_milestone');
    /**
     * Checking is user logged status
     */
    protected $check_status = TRUE;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
    /*    $data['projects'] = $this->project_model->getProjects();
        $this->title = "Developer Escrow | Providers";
        $this->page = "projects/projects";
        $this->data_template = $data;
        $this->render();
    */
        redirect(base_url('projects'));
    }

    public function viewProject($id)
    {
        $projects = $this->project_model->getProjectById($id);
        $data['project'] = $projects[0];
        /*if(empty($project)) {
           return show_404();
        }*/

        //$client = $this->user_model->get_by("id", $project->user_id);
        $this->title = "Developer Escrow | " . $projects[0]['name'];
        $this->page = "project_view/project";
        $this->data_template = $data;
        /*$this->data_template = array(
            "project" => $project,
            "client" => $client
        );*/
        $this->render();
    }


}