<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tokens extends MY_Controller
{
    /**
     * Checking is user logged
     */
    protected $check_is_login = TRUE;

    protected $models = array('token');

    public function __construct()
    {
        parent::__construct();
    }
}