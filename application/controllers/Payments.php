<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends MY_Controller
{
    private $stripeConfig = [
        'stripe_key_test_public' => 'pk_test_u6UyouBGDzUEWfVliFKP9zGk',
        'stripe_key_test_secret' => 'sk_test_8ZurwXcpCo7r2Sg9iWKbTgo5',
        'stripe_key_live_public' => 'pk_live_dSchXN2WjWt400PHbFx0HCLw',
        'stripe_key_live_secret' => 'sk_live_pX9QiNjLBN1kpoIT32jTkCGI',
        'stripe_test_mode'       => TRUE,
        'stripe_verify_ssl'      => FALSE,
    ];
    /**
     * Checking is user logged
     */
    protected $check_is_login = TRUE;
    protected $models = array("user", "project", "user_project", "card", "paypal", "bank", "project_milestone", "milestone", "payment");

    public function __construct() {
        parent::__construct();
        $this->load->library('Stripe', $this->stripeConfig);
        require_once(APPPATH.'libraries/Stripe/init.php');

    }

    public function index() {
//        $i = 0;
//        $projects = array();
//        $users = array();
//        $user_name = array();
//        $user_profile = $this->user_model->get_by("id", $this->user_id);
//        if ($user_profile->user_type) {
//            $projects = (array)$this->project_model->get_many_by('user_id', $this->user_id);
//            foreach ($projects as $k => $project) {
//                $users[$i] = (array)$this->project_model->get_users_by_project($project->id, $status = 1);
//                if (isset($users[$i][0])) {
//                    $user_name[$i] = $users[$i] [0] ['name2'];
//                    $i++;
//                } else {
//                    $user_name[$i] = '';
//                    $i++;
//                }
//            }
//        } else {
//            $query = $this->user_project_model->get_many_by('user_id', $this->user_id);
//            foreach ($query as $value) {
//                $projects = $this->project_model->get_many_by('id', $value->project_id);
//                foreach ($projects as $project) {
//                    $users[$i] = (array)$this->user_model->get_by('id', $project->user_id);
//                    $user_name[$i] = $users[$i]['username'];
//                    $i++;
//                }
//            }
//        }
//        $this->title = "Developer Escrow | Payments";
//        $this->page = "payments/payments";
//        $this->data_template = array(
//            "projects"  => $projects,
//            "users"     => $users,
//            'user_name' => $user_name
//        );
        $data['projects'] = $this->payment_model->getPaymentProjects($this->user_id);
        $milestones = $this->payment_model->getProjectMilestones($this->user_id);
        foreach($data['projects'] as &$res){
            $res['mil'] = [];
            foreach($milestones as $mil){
                if($mil['project_id'] == $res['project_id'] && isset($mil['milestone_id'])){
                    array_push($res['mil'], $mil);
                }
            }
        }
        $this->title = "Developer Escrow | Payments";
        $this->data_template = $data;
        $this->page = "payments/payments";
        $this->render();
    }

    public function pay() {
        $amount = $this->input->post('withdrawal-ammount');
        $project_id = $this->input->post('modal_project');
        $project = $this->project_model->get_by('md5(id)', $project_id);
        if ($project->currency == '$') {
            $currency = 'usd';
        } elseif ($project->currency == '&#8364;') {
            $currency = 'eur';
        } else {
            $currency = 'gbp';
        }
        $developer_id = $this->user_project_model->get_by('md5(project_id)', $project_id);
        $developer = $this->user_model->get_by('id', $developer_id->user_id);
        $pay = $project->pay;
        $user_card = $this->card_model->get_by("user_id", $this->user_id);
        $customer_id = $user_card->customer_id;
        $desc = $project->name;
        $response = $this->stripe->charge_customer($amount, $customer_id, $desc, $currency);
        $charge = json_decode($response, true);

        if (!empty($charge['error'])) {
            var_dump($charge['error']);
        } else {
            $data['pay'] = (string)($pay - $amount);
            $developer_data['available_pays'] = (string)($developer->available_pays + $amount);
            $query1 = $this->project_model->update_by('md5(id)', $project->id, $data);
            $query2 = $this->user_model->update_by('id', $developer->id, $developer_data);
            if ($query1 && $query2) {
                redirect(base_url('payments'), 'refresh');
            }
        }
    }

    public function found_client($type, $id) {
        if($type == 'project'){
            $data['data'] = (array)$this->payment_model->getPaymentProjects($this->user_id, $id)[0];
        } elseif($type == 'milestone'){
            $data['data'] = (array)$this->payment_model->getProjectMilestones($this->user_id, $id)[0];
        }
        $data['type'] = $type;
        $this->title = "Developer Escrow | Found";
        $this->page = "payments/found_client";
        $this->data_template = $data;
        $this->render();
//        echo '<pre>';
//        print_r($data['data']); die;
//        $miles = array();
//        $project = $this->project_model->get_by('id', $id);
//        $milestones = $this->project_milestone_model->get_many_by('project_id', $id);
//        $i = 0;
//        if(!empty($milestones) && isset($milestones)) {
//            foreach ($milestones as $milestone) {
//               $milestone_array = $this->milestone_model->get_by('id', $milestone->milestone_id);
//               $miles[$i] = $milestone_array->name;
//               $i++;
//            }
//        }
//            array(
//            'miles'         => $miles,
//            'currency'      => $project->currency,
//            'project_id'    => $project->id,
//            'project_name'  => $project->name,
//            'fund'          => $project->fund,
//        );
    }

    public function found_client_pay($type, $id) {
//        $end_num = '';
//        $cards = $this->card_model->get_many_by('user_id', $this->user_id);
//        $i = 0;
//        if (isset($cards)) {
//            foreach ($cards as $card) {
//                $card_num = str_split($card->card_number);
//                $index = count($card_num);
//                $end_num[$i] = $card_num[$index-4] . $card_num[$index-3] . $card_num[$index-2] . $card_num[$index-1];
//                $i++;
//            }
//        }
//        $user = $this->user_model->get_by('id', $this->user_id);
//        $project = $this->project_model->get_by('md5(id)', $id);
//        $this->title = "Developer Escrow | Found Pay";
//        $this->page = "payments/found_client_pay";
//        $this->data_template = array(
//            'user'       => $user,
//            'project'    => $project,
//            'cards'      => $cards,
//            'email'      => $user->email,
//            'end_number' => $end_num,
//            'project_id' => $id
//        );
        $data = [];
        if($this->card_model->get_by('user_id',$this->user_id)){
            $data['card']  = (array)$this->card_model->get_by('user_id',$this->user_id);
        }
        if($this->bank_model->get_by('user_id',$this->user_id)){
            $data['bank']  = (array)$this->bank_model->get_by('user_id',$this->user_id);
        }
        if($this->paypal_model->get_by('user_id',$this->user_id)){
            $data['paypal']  = (array)$this->paypal_model->get_by('user_id',$this->user_id);
        }
        if($type == 'project'){
            $data['payment'] = (array)$this->payment_model->getPaymentProjects($this->user_id, $id)[0];
        } elseif($type == 'milestone'){
            $data['payment'] = (array)$this->payment_model->getProjectMilestones($this->user_id, $id)[0];
        }
//        echo '<pre>'; print_r($data); die;
        $this->title = "Developer Escrow | Found Pay";
        $this->page = "payments/found_client_pay";
        $this->data_template = $data;
        $this->render();
    }

    public function stripe_saved_pay() {
        $data['status'] = false;
        $cvc = $this->input->post('cvc');
        $card = (array)$this->card_model->get_by('user_id', $this->user_id);
        if($card['cvc'] == $cvc){

        } else {
            echo 'Not correct cvc.';
        }

        $amount = $this->input->post('amount');
        $project_id = $this->input->post('project');
        $project = $this->project_model->get_by('md5(id)', $project_id);
        $cvc = $this->input->post('cvc');
        if (!empty($cvc)) {
            $card = $this->card_model->get_by('cvc', $cvc);
            if (isset($card) && !empty($card) && $card->user_id == $this->user_id) {
                if ($project->currency == '$') {
                    $currency = 'usd';
                } elseif ($project->currency == '&#8364;') {
                    $currency = 'eur';
                } else {
                    $currency = 'gbp';
                }
                $developer_id = $this->user_project_model->get_by('md5(project_id)', $project_id);
                $developer = $this->user_model->get_by('id', $developer_id->user_id);
                $pay = $project->pay;
                $user_card = $this->card_model->get_by("user_id", $this->user_id);
                $customer_id = $user_card->customer_id;
                $desc = $project->name;
                $response = $this->stripe->charge_customer($amount, $customer_id, $desc, $currency);
                $charge = json_decode($response, true);
                if (!empty($charge['error'])) {
                    $message = [
                        'success' => false,
                        'message' => $charge['error']
                    ];
                    echo json_encode($message);die;
                } else {
                    $data['pay'] = (string)($pay - $amount);
                    $developer_data['available_pays'] = (string)($developer->available_pays + $amount);
                    $query1 = $this->project_model->update_by('md5(id)', $project->id, $data);
                    $query2 = $this->user_model->update_by('id', $developer->id, $developer_data);
                    if ($query1 && $query2) {
                        $message = [
                            'success' => true,
                            'message' => [
                                'message' =>'Successfully payed'
                            ]
                        ];
                        echo json_encode($message);die;
                    }
                }
            }else {
                $message = [
                    'success' => false,
                    'message' => [
                     'message' =>'Card Problem'
                    ]
                ];
               // echo json_encode($message);die;
            }
            echo json_encode($message);die;
        }
    }

    public function stripe_once_pay(){
        $amount = $this->input->post('amount');
        print_r($this->input->post());die;


        $amount = $this->input->post('amount');
        $project_id = $this->input->post('project');
        $project = $this->project_model->get_by('md5(id)', $project_id);
        $user_profile = $this->user_model->get_by("id", $this->user_id);
        $data['user_id'] = $this->user_id;
        $card = $this->input->post('stripeToken');
        $desc = $project->name;
            if ($project->currency == '$') {
                $currency = 'usd';
            } elseif ($project->currency == '&#8364;') {
                $currency = 'eur';
            } else {
                $currency = 'gbp';
            }
        $charge = json_decode($this->stripe->charge_card($amount, $card, $desc, $currency), true);
        $developer_id = $this->user_project_model->get_by('md5(project_id)', $project_id);
        $developer = $this->user_model->get_by('id', $developer_id->user_id);
        $pay = $project->pay;
        if (!empty($charge['error'])) {
              var_dump($charge['error']);
        } else {
             $data['pay'] = (string)($pay - $amount);
             $developer_data['available_pays'] = (string)($developer->available_pays + $amount);
             $query1 = $this->project_model->update_by('md5(id)', $project->id, $data);
             $query2 = $this->user_model->update_by('id', $developer->id, $developer_data);
             if ($query1 && $query2) {
                redirect(base_url('payments'), 'refresh');
             }
        }
    }

    public function found_get_paid() {  ///   not used    ///
        $end_num = '';
        $cards = $this->card_model->get_many_by('user_id', $this->user_id);
        $i = 0;
        if (isset($cards)) {
            foreach ($cards as $card) {
                $card_num = str_split($card->card_number);
                $index = count($card_num);
                $end_num[$i] = $card_num[$index-4] . $card_num[$index-3] . $card_num[$index-2] . $card_num[$index-1];
                $i++;
            }
        }
        $user = $this->user_model->get_by('id', $this->user_id);
        $project_id = $this->input->post('project');
        $project = $this->project_model->get_by('md5(id)', $project_id);
        $amount = $user->available_pays;
        if(isset($amount) && $amount>0){

        }
        $this->title = "Developer Escrow | Get Paid";
        $this->page = "payments/get_paid";
        $this->data_template = array(
            'user'       => $user,
            'project'    => $project,
            'cards'      => $cards,
            'email'      => $user->email,
            'end_number' => $end_num,
            'project_id' => $project_id,
            'amount'     => (isset($amount) && $amount>0) ? $amount : ''
        );
        $this->render();
    }///   not used    ///


    public function get_paid(){
        \Stripe\Stripe::setApiKey("sk_test_8ZurwXcpCo7r2Sg9iWKbTgo5");
        $token_id = $this->input->post('stripeToken');
        $user_profile = $this->user_model->get_by("id", $this->user_id);
        $info =json_decode( $this->stripe->card_token_info( $token_id), true);
        $card_type = $info['card']['funding'];
        if($card_type != 'debit'){
            $message = [
                'success' => false,
                'message' => 'Card funding should be Debit'
            ];
            echo json_encode($message);die;
        }
        $amount = $this->input->post('amount');
        $recipient = \Stripe\Recipient::create(array(
                "name" => 'aaaa aa',
                "type" => "individual",
                "card" => $token_id,
                "email" => $user_profile->email)
        );
        $recipient_id = $recipient->id;
        $statement_descriptor = 'Developer Escrow';
        $transfer = \Stripe\Transfer::create(array(
                "amount" => 10, // amount in cents
                "currency" => "usd",
                "recipient" => $recipient_id,
                "statement_descriptor" => $statement_descriptor)
        );
        if($transfer){
            $data['available_pays'] = (string)($user_profile->available_pays - $amount);
            $data['earned_pay']     = (string)($user_profile->earned_pay + $amount);
            $query = $this->user_model->update_by('id', $user_profile->id, $data);
            if($query){
               // redirect(base_url('profile'), 'refresh');
            }
            else{
                die ('something goes wrong');
            }
        }
        else{
            die('somethings goes wrong with transfer');
        }
    }

    public function add_available_by_card(){
        header('Content-Type: application/json');
        $amount = $this->input->post('amount');
        $card = (array)$this->card_model->get_by('user_id', $this->user_id);
        unset($card['id']);
        unset($card['user_id']);
        unset($card['status']);
        $card['expiration'] = $card['expire_month']."/".$card['expire_year'];
        unset($card['expire_month']);
        unset($card['expire_year']);

        $token = $_POST["stripeToken"];
        $costumer_name = (array)$this->user_model->get_by('id', $this->user_id);
        \Stripe\Stripe::setApiKey("sk_test_8ZurwXcpCo7r2Sg9iWKbTgo5");

        if($this->session->userdata('user_type') == 1) {  // for client
            //        $customer = \Stripe\Customer::create(array(  // Create a Customer
//                "source" => $token,
//                "description" => $costumer_name['username'])
//        );
//        print_r($customer['id']);die;
//        \Stripe\Charge::create(array(
//            "amount"   => $amount*100, // $15.00 this time
//            "currency" => "usd",
//            "customer" => $customer['id'] // Previously stored, then retrieved
//        ));

//        $recipient = \Stripe\Recipient::create(array(
//                "name" => "Developer Escrow",
//                "type" => "corporation",
//                "bank_account" => $token,
//                "email" => $costumer_name['email'])
//        );
            // Create a transfer to the specified recipient
//        $transfer = \Stripe\Transfer::create(array(
//                "amount" => 1000, // amount in cents
//                "currency" => "usd",
//                "recipient" => $recipient['id'],
//                "bank_account" => $recipient['bank_account'],
//                "statement_descriptor" => "Developer Escrow payments")
//        );
            \Stripe\Stripe::setApiKey('sk_test_8ZurwXcpCo7r2Sg9iWKbTgo5');
            \Stripe\Transfer::create(array(
                'amount' => $amount*100,
                'currency' => "usd",
                'destination' => 'acct_26uqNDpQHd71fx'
            ));
            $result = $this->add_available($amount);
            echo json_encode($result);
            exit;

        } else {
            $customer = [];
//            var_dump($costumer_name['stripe_username']); die;
            if($costumer_name['stripe_username']){
                $customer['id'] = $costumer_name['stripe_username'];
            } else {
                $customer = \Stripe\Customer::create(array(  // Create a Customer
                        "source" => $token,
                        "description" => $costumer_name['username'])
                );
                $update['stripe_username'] = $customer['id'];
                $this->user_model->update_by('id', $this->user_id, $update);
            }

            \Stripe\Charge::create(array(
                "amount"   => $amount*100, // $15.00 this time
                "currency" => "usd",
                "customer" => $customer['id'] // Previously stored, then retrieved
            ));
            $result = $this->decrease_available($amount);
            echo json_encode($result);
            exit;
        }

    }

    public function create_stripe_token(){
        header('Content-Type: application/json');
        $card['status_code'] = false;
        if($this->card_model->get_by('user_id', $this->user_id)){
            $card = (array)$this->card_model->get_by('user_id', $this->user_id);
            if($card['status'] == 0){
                $card['status_code'] = true;
            }
        }
        echo json_encode($card);
        exit;
    }

    public function decrease_available($amount){
        header('Content-Type: application/json');
        $data['status'] = false;
        if($amount){
            $current_amount = (array)$this->user_model->get_by('id', $this->user_id);
            if($current_amount['available'] >= $amount){
                $data['available'] = ((float)$current_amount['available']) - ((float)$amount) ;
                $update = $this->user_model->update_by('id', $this->user_id, $data);
                if($update){
                    $data['status'] = true;
                }
            } else {
                $data['error'] ='Your amount is not enough.';
            }
        }
        echo json_encode($data);
        exit;
    }

    public function add_available($amount){
        header('Content-Type: application/json');
        $data['status'] = false;
        if($amount){
            $current_amount = (array)$this->user_model->get_by('id', $this->user_id);
            $data['available'] = ((float)$amount) + ((float)$current_amount['available']);
            $update = $this->user_model->update_by('id', $this->user_id, $data);
            if($update){
                $data['status'] = true;
            }
        }
        echo json_encode($data);
        exit;
    }

}