<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Milestones extends MY_Controller
{
    /**
     * @var array
     */
    protected $models = array('milestone');

    public function __construct()
    {
        parent::__construct();
    }

    public function searchMilestone() {
        $keyword = $this->input->post("query");
        $where = array(
            "name" => $keyword
        );
        $milestones = $this->milestone_model->get_by_like($where);

        if(!empty($milestones)) {
            echo json_encode($milestones);
        }
    }

    public function addMilestone() {
        $data = $this->input->post();
        //echo '<pre>';print_r($data);die;
        $milestone_id = $this->milestone_model->insert($data);
        echo $milestone_id;
    }

    public function remove_milestone(){
        $milestone_id = $this->input->post()['mile_id'];
        //print_r($milestone_id);die;
        $result = $this->milestone_model->delete($milestone_id);
        if($result == 1){
            $message = [
                'success' => true,
                'message' => 'Milestone deleted'
            ];
            echo json_encode($message);die;
        }
    }

}