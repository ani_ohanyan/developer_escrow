<?php
if (!class_exists('Api')) require_once APPPATH."controllers/app/modules/Descrow.php";

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class API extends MY_Controller{
    protected $models = array("user");
    public function index(){
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');

        if($method = $this->input->post("method")){

            if($token = $this->input->post("token")){
                $user = $this->getUserByToken($token);
                if($user){
                    if(method_exists($this, $method))
                        $this->$method($user);
                    else{
                        $this->generateError("Uknown method", 400);
                        return false;
                    }
                } else{
                    $this->generateError("Uncorrect token. Access Denied", 400, array('invalidAccessToken'=>1));
                    return false;
                }
            }

        }
        elseif($method = $this->input->get("method")){
            if($token = $this->input->get("token")){
                $user = $this->getUserByToken($token);

                if($user){
                    if(method_exists($this, $method))
                        $this->$method($user ,$token);
                    else{
                        $this->generateError("Uknown method", 400);
                        return false;
                    }
                } else{
                    $response =$this->generateError("Uncorrect token. Access Denied", 400, array('invalidAccessToken'=>1));
                    return $response;
                }
            } else{
                $this->generateError("Required token. Access Denied", 400, array('invalidAccessToken'=>1));
                return false;
            }
        } else {
            $this->generateError("Access Denied", 400, array('invalidAccessToken'=>1));
            return false;
        }
    }

    private function get_user($uid){
        $user = (array)$this->user_model->get_by('id',$uid);
        if(empty($user)){
            $user['status'] = 0;
            $user['description'] = "user does not exist";
        }

        echo json_encode(array('user'=>$user));

    }

    private function get_projects($uid,$token){

        if($this->input->post()){
            $this->generateError("Only GET request available", 400);
            return false;
        }
        $projects = $this->project_model->get_many_by("user_id",$uid);

        echo json_encode(array('projects'=>$projects));
    }


    protected function generateError($desc, $error_code=400, $params=false){
        $response = array(
            "status" => 0,
            "description" => $desc
        );
        if($params)
            $response = array_merge($response, $params);

        $classname = get_class($this);
        if($classname != "API")
            $global_params = $this->controller->input->post("params");
        else
            $global_params = $this->input->post("params");

        $error = urlencode($desc);
        if(isset($global_params['return'])){
            if(strpos($global_params['return'], "?") !== false)
                $global_params['return'] .= "&";
            else
                $global_params['return'] .= "?";
            header("Location: " . $global_params['return'] . "error=" . $error);
            exit;
        }
        //header('HTTP/ '.$error_code.' '.$desc);
        echo json_encode($response);
    }

    private function getUserByToken($t){

        $where = array(
            "api_key" => $t["api_key"],
            "secret_key" => $t["secret_key"]
        );
        $query = $this->db->get_where('tokens', $where);
        if($query->num_rows()){
            return $query->result()[0]->id;
        }else{
            return false;

        }
    }


 }