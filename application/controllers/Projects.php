<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends MY_Controller {
    /**
     * @var array
     */
    protected $models = array('file', 'milestone', 'user','user_project', 'comment', 'link', 'project_file', 'diary');

    /**
     * Checking is user logged status
     */
    protected $check_is_login = TRUE;

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->helper('download');
    }

    public function index() {
        $this->title = "Developer Escrow | Projects";
        $this->page = "projects/projects";
        $projects = $this->project_model->getProjects($this->user_id);
        if(isset($projects[0]['id']) && !empty($projects[0]['id'])){
            foreach($projects as $key => &$project) {
                $milestones = $this->milestone_model->getMilestones($project['project_id']);

                $project['milPrice'] = 0;
                $project['milPriceFin'] = 0;
                foreach($milestones['mil'] as $mile) {
                        $project['milPrice']  += $mile['price'];
                }
                foreach($milestones['milFin'] as $mile) {
                    $project['milPriceFin']  += $mile['price'];
                }
                $project['milCount'] = count($milestones['mil']);
                $project['milCountFin'] = count($milestones['milFin']);
                $project['milestones_count'] = count($milestones);
                $profile_image_id = $this->user_model->get_by('id', $project['user_id'])->profile_image_id;
                $file_path = $this->file_model->get_by('id', $profile_image_id)->path;
                $project['file_path'] = $file_path;
            }

            $this->data_template = array(
                'projects'    =>  $projects,
                'type'        =>  $this->session->userdata('user_type')
            );
        }
        $this->render();
    }

    public function viewProject($id){
        /*** tracker ***/
        $data = $this->input->post();
        if($data) {
            $data['time'] = $data['hr']*60+$data['min'];
            unset($data['hr']);
            unset($data['min']);
            $data['user_id'] = $this->user_id;
            $diary_id = $this->diary_model->insert($data, true);
             if(!empty($_FILES)){
                $uploads = $this->upload_files($_FILES['job_attachment']);
                if(!isset($uploads["error"])) {
                    foreach($uploads as $up) {
                        $up['class'] = 'diary';
                        $up['attachment_id'] = $diary_id;
                        $this->file_model->insert($up);
                    }
                }
            }
        }
//        $data['projects'] = $this->project_model->getProjects($this->user_id,$this->session->userdata('user_type'));
        /** * * **/

        $projects = $this->project_model->getProjectById($id);
        foreach($projects as $key => &$project) {
            $milestones = (array)$this->milestone_model->get_many_by('project_id', $project['pr_id']);
            $invited_user = $this->user_project_model->get_by('project_id', $project['pr_id'])->user_id;
            $project['invited_user'] = $invited_user;
            $mil_count = count($milestones);
            $project['milestones']  = $milestones;
            $project['mil_count']  = $mil_count;
        }
        $data['user'] = $this->user_model->get_by('id', $this->user_id);
        if(isset($projects) && !empty($projects)){
            $data['project'] = $projects[0];
        }
        $data['type'] = $this->session->userdata('user_type');
        $data['comments'] = $this->comment_model->getCommentsByProject($projects[0]['pr_id']);
        $attach_ids = [];
        if(isset($data['comments']) && !empty($data['comments'])){
            for($i=0; $i<count($data['comments']); $i++){
                $data['comments'][$i]['date'] = $this->timeAgo(strtotime($data['comments'][$i]['date']));
                array_push($attach_ids, $data['comments'][$i]['id']);
            }
            $attach_ids = implode(",", $attach_ids);
            $com_file = $this->comment_model->getCommentFiles($attach_ids);
            foreach($data['comments'] as &$com){
                $com['files'] = array();
                foreach($com_file as $comfile){
                    if($com['id'] == $comfile['com_id']){
                        array_push($com['files'],$comfile);
                    }
                }
            }
        }
        $data['link'] = $this->link_model->getProjectLinks((int)$projects[0]['pr_id']);
        $data['files'] = $this->file_model->getFiles((int)$projects[0]['pr_id']);
        for($i=0; $i<count($data['files']); $i++){
            $data['files'][$i]['date1'] = $this->timeAgo(($data['files'][$i]['date']));
        }
        $data['this_user'] = $this->user_id;
        if(isset($data['project']['milestones']) && !empty($data['project']['milestones'])){
            $data['milestones'] = $data['project']['milestones'];
            $data['projects_id'] = (int)$data['project']['pr_id'];
        } else {
            $data['projects'] = $data['project']['name'];
            $data['projects_id'] = (int)$data['project']['pr_id'];
        }
        $this->title = "Developer Escrow | " . $projects[0]['name'];
        $this->page = "project_view/project";
        $this->data_template = $data;
        $this->render();
    }

    public function edit($id) {

        $data['projects'] = $this->project_model->getProjects($this->user_id,$this->session->userdata('user_type'));
        /** * * **/

        $projects = $this->project_model->getProjectById($id);

        foreach($projects as $key => &$project) {
            $milestones = (array)$this->milestone_model->get_many_by('project_id', $project['pr_id']);
            $invited_user = $this->user_model->get_by('id', $project['inv_user_id'])->email;
            $project['invited_user'] = $invited_user;
            $mil_count = count($milestones);
            $project['milestones']  = $milestones;
            $project['mil_count']  = $mil_count;
        }
        $data['user'] = $this->user_model->get_by('id', $this->user_id);
        if(isset($projects) && !empty($projects)){
            $data['project'] = $projects[0];
        }
        $data['type'] = $this->session->userdata('user_type');
        $data['comments'] = $this->comment_model->getCommentsByProject($projects[0]['pr_id']);
        $attach_ids = [];
        if(isset($data['comments']) && !empty($data['comments'])){
            for($i=0; $i<count($data['comments']); $i++){
                $data['comments'][$i]['date'] = $this->timeAgo(strtotime($data['comments'][$i]['date']));
                array_push($attach_ids, $data['comments'][$i]['id']);
            }
            $attach_ids = implode(",", $attach_ids);
            $com_file = $this->comment_model->getCommentFiles($attach_ids);
            foreach($data['comments'] as &$com){
                $com['files'] = array();
                foreach($com_file as $comfile){
                    if($com['id'] == $comfile['com_id']){
                        array_push($com['files'],$comfile);
                    }
                }
            }
        }

        $data['link'] = $this->link_model->getProjectLinks((int)$projects[0]['pr_id']);
        $data['files'] = $this->file_model->getFiles((int)$projects[0]['pr_id']);
        for($i=0; $i<count($data['files']); $i++){
            $data['files'][$i]['date1'] = $this->timeAgo(($data['files'][$i]['date']));
        }
        $data['this_user'] = $this->user_id;
        $this->title = "Developer Escrow | Add project";
        $this->page = "edit_project/project";
        $this->data_template = $data;
        $this->render();
    }

    public function editProject(){
        $data = $this->input->post();
        if(isset($data['milestones']) && !empty($data['milestones'])){
            $milestones = json_decode($data['milestones']);

            foreach ($milestones as $milestone) {
                if(!isset($milestone->id)){
                    $data_miles = [
                        "name" => $milestone->name,
                        "price" => $milestone->price,
                        "description" => $milestone->description,
                        "project_id" => $data['id']
                    ];
                    $miles_id = $this->milestone_model->insert($data_miles);
                    echo json_encode($data_miles);
                }else{
                    $mile_id = $milestone->id;
                    $mile = array(
                        "name" => $milestone->name,
                        "price" => $milestone->price,
                        "description" => $milestone->description
                    );
                    $this->milestone_model->update_by("id", $mile_id, $mile);
                    echo json_encode($mile);
                }
            }
        }

        unset($data['freelancer']);
        unset($data['milestones']);
        $pr_upd = $this->project_model->update_by("id", $data['id'], $data);
        // notification
        if($pr_upd){
            $notification['sender_id'] = $this->user_id;
            $notification['type'] = "project";
            $notification['project_id'] = $data['id'];
            $notification['notification_id'] = 7;
            $notification['type_id'] = $data['id'];
            $receiver = (array)$this->user_project_model->get_by('project_id', $data['id']);
            $notification['user_id'] =  $receiver['user_id'];
            $this->user_notification_model->insert($notification);
        }
        if($pr_upd){
            $notification['sender_id'] = $this->user_id;
            $notification['type'] = "project";
            $notification['project_id'] = $data['id'];
            $notification['notification_id'] = 7;
            $notification['type_id'] = $data['id'];
            $receiver = (array)$this->user_project_model->get_by('project_id', $data['id']);
            $project_user = $this->project_model->get_by('id', $data['id'])->user_id;
            $notification['user_id'] =  $project_user;
            $this->user_notification_model->insert($notification);
        }
        redirect(base_url('projects'), 'refresh');
    }

    public function addProject() {
        $this->title = "Developer Escrow | Add project";
        $this->page = "new_project/project";
        $this->render();
    }

    public function addProjectProcess() {
        $this->check_is_user_client = TRUE;
        $data = $this->input->post();

        if (!empty($data['milestones'])) {
            $milestones = $data['milestones'];
        } else {
            $milestones = '';
        }
        if (!empty($data)) {
            if (isset($_FILES['file']['tmp_name'])) {
                $number_of_files = sizeof($_FILES['file']['tmp_name']);
                if ($number_of_files > 0) {
                    $files = $_FILES['file'];
                    $errors = array();
                    for ($i = 0; $i < $number_of_files; $i++) {
                        if ($_FILES['file']['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file ' . $_FILES['file']['name'][$i];
                    }
                    if (sizeof($errors) == 0) {
                        $config['upload_path'] = FCPATH . 'upload/';
                        $config['allowed_types'] = 'gif|jpg|png|rar|zip|txt';
                        for ($i = 0; $i < $number_of_files; $i++) {
                            $_FILES['file']['name'] = $files['name'][$i];
                            $_FILES['file']['type'] = $files['type'][$i];
                            $_FILES['file']['tmp_name'] = $files['tmp_name'][$i];
                            $_FILES['file']['error'] = $files['error'][$i];
                            $_FILES['file']['size'] = $files['size'][$i];
                            $upload = $this->upload_file("file", 'gif|jpg|png|rar|zip|txt');
                            if (!isset($upload["error"])) {
                                $fileId = $this->file_model->insert($upload);
                                $files['file_id'][] = $fileId;
                            } else {
                                $files['upload_errors'][$i] = $this->upload->display_errors();
                            }
                        }
                    }
                }
            }
            $data['user_id'] = $this->user_id;
            $freelancer = $data['freelancer'];
            unset($data['freelancer']);
            unset($data['milestones']);
            $project_id = $this->project_model->insert($data);
            if ($project_id) {
                if (!empty($milestones)) {
                    $milestones = explode(",", $milestones);
                    foreach ($milestones as $milestone_id) {
                        $data = array(
                            "project_id" => $project_id,
                            "milestone_id" => $milestone_id
                        );
                        $this->project_milestone_model->insert($data);
                    }
                }
                redirect(base_url('projects'), 'refresh');
            } else {
                $this->addProject();
            }
        }
    }

    public function create_project() {
        $this->check_is_user_client = TRUE;
        $data = $this->input->post();
        if(!empty($data['fixed_price'])) {
            $data['fund'] = $data['fixed_price'];
        } else {
            $data ['fund'] = '';
        }
        if (!empty($data['milestones'])) {
            $milestones = json_decode($data['milestones']);
        } else {
            $milestones = '';
        }
        if (!empty($data['files'])) {
            $files = json_decode($data['files']);
        } else {
            $files = '';
        }
        if(!empty($data['freelancer'])){
                 $data['status'] = 0;
        }
        if(!empty( $data['hourly_rate']) && !empty( $data['hours_per_week']) ){
            $data['fund'] = $data['hourly_rate'] * $data['hours_per_week'];
        }
        $dataF = array();
        $data['user_id'] = $this->user_id;
        $developer_email = $data['freelancer'];
        unset($data['freelancer']);
        unset($data['milestones']);
        unset($data['files']);

        if(!empty($developer_email)){
            $freelancer = $this->user_model->get_by('email',  $developer_email);

            if(empty($freelancer)){
                $message = [
                    'success'=> false,
                    'message' => "Invalid email address"
                ];
                echo json_encode($message);die;
            } else {
                $project_id = $this->project_model->insert($data,true);
                $dataF['user_id'] = $freelancer->id;
                $dataF['project_id'] = $project_id;
                $user_project_id = $this->user_project_model->insert($dataF);
                // notification
                if($user_project_id){
                    $notification['sender_id'] = $this->user_id;
                    $notification['type'] = "project";
                    $notification['project_id'] = $project_id;
                    $notification['notification_id'] = 6;
                    $notification['type_id'] = $project_id;
                    $receiver = (array)$this->user_project_model->get_by('project_id', $project_id);
                    $notification['user_id'] =  $receiver['user_id'];
                    $this->user_notification_model->insert($notification);
                }
            }
        }
        //  $mail = $this->sendmail($freelancer , $from, $subject, $message = array());
        if ($project_id) {
            if (!empty($milestones)) {
                foreach ($milestones as $milestone) {
                    $milestone->project_id = $project_id;
                    $this->milestone_model->insert((array)$milestone);
                }
            }
            if (!empty($files)) {
                foreach ($files as $file) {
                    $file_id = $file->file;
                    $file_data = [
                        'project_id' => $project_id,
                        'file_id' => $file_id
                    ];
                    $this->project_file_model->insert($file_data);
                    $data_f = ['attachment_id' => $project_id];
                    $this->file_model->update_by("id",$file_id,$data_f);
                }
            }
            $message = [
                'success'=> true,
                'message' => "Project added"
            ];
            echo json_encode($message);
        } else {
            $this->addProject();
        }
    }


    public function upload(){
        $dt = $this->input->post();
        if(!empty($_FILES)){
            $uploads = $this->upload_files($_FILES['project_attachment']);
            if(!isset($uploads["error"])) {
                foreach($uploads as $up) {
                    $up['user_id'] = $this->user_id;
                    if(isset($dt['check_is_locked'])){
                        $up['status'] = 1;
                    }
//                    if(isset($dt['message'])){
//                        $up['class'] = 'proj_comment';
//                    } else {
                        if(isset($dt['milestone']) && !empty($dt['milestone'])){
                            $up['class'] = 'milestone';
                            $up['attachment_id'] = $dt['milestone'];
                        } else {
                            $up['class'] = 'project';
                            $up['attachment_id'] = $dt['project_id'];
                        }
//                    }
                    $fileId = $this->file_model->insert($up);
                }
            }
        };

        redirect(base_url('projects/'.md5($dt['project_id'])), 'refresh');
    }

    public function changeProjectStatus() {
        $success = false;
        if(!empty($_POST)){
            $data = array();
            $data['status'] = 2;
            $d = $this->project_model->update_by("id",$_POST['id'],$data);
            if($d) {
                $success = true;
            }
        }
        echo json_encode($success);exit;
    }

    public function searchByData() {
        $data['success'] = false;
        if(!empty($_POST['search'])){
            if($_POST['type'] == 'link') {
                $datas = $this->link_model->findLinksByData($this->user_id,$_POST['search']);
            } else {
                $datas = $this->project_file_model->findFileByData($this->user_id,$_POST['search'],$this->session->userdata('user_type'));
            }
            $data['status'] = true;
            $data['datas'] = $datas;
        }
        echo json_encode($data);exit;

    }

    public function add_description(){
        $project_id =$this->input->post('projects');
        $data['description'] = $this->input->post('description');
        $query = $this->project_model->update_by('id', $project_id, $data);
        if($query){
            $notification['sender_id'] = $this->user_id;
            $notification['type'] = "project";
            $notification['project_id'] = $project_id;
            $notification['notification_id'] = 5;
            $notification['type_id'] = $project_id;
            $receiver = (array)$this->project_model->get_by('id', $project_id);
            $notification['user_id'] =  $receiver['user_id'];
            $res = $this->user_notification_model->insert($notification);
        }
        redirect(base_url('projects'), 'refresh');
    }

    public function send_reminder(){
        $success = false;
        $notification['sender_id'] = $this->user_id;
        $notification['type'] = "project";
        $notification['project_id'] = $this->input->post('projId');
        $notification['notification_id'] = 4;
        $notification['type_id'] = $this->input->post('projId');
        $receiver = (array)$this->user_project_model->get_by('project_id', $this->input->post('projId'));
        $notification['user_id'] =  $receiver['user_id'];
        $res = $this->user_notification_model->insert($notification);
        if($res){
            $success = true;
        }
        echo json_encode($success);exit;
    }

    public function addProjectFile(){
        $data['id'] = [];
        $data['name'] = [];
        $data['path'] = [];
        if(!empty($_FILES)){
            $uploads = $this->upload_files($_FILES['project_attachment']);
            if(!isset($uploads["error"])) {

                foreach($uploads as $up) {
                    $up['user_id'] = $this->user_id;
                    $up['class'] = 'new_project';
                    $up['attachment_id'] = $this->user_id;
                    $fileId = $this->file_model->insert($up);
                    array_push($data['id'], $fileId);
                    array_push($data['name'], $up['name']);
                    array_push($data['path'], $up['path']);
                }
                echo json_encode($data);die;
            }
        }
    }


}