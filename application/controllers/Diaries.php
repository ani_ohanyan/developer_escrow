<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Diaries extends MY_Controller
{
    /**
     * Checking is user logged
     */
    protected $models = array("diary", "user", "project", "comment", "diary");
    protected $check_is_login = TRUE;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->title = "Developer Escrow | Diaries";
        $this->page = "diaries/diary";
        $data = [];
//        $data['projects'] = $this->diary_model->get_all_projects($this->user_id);
        $data['providers'] = $this->diary_model->get_all_providers($this->user_id, (int)$this->input->post('project_id'));
//        echo '<pre>';
//        print_r($data);
        $this->data_template = $data;
        $this->render();
    }

    public function getProjectByProject(){
        header('Content-Type: application/json');
        $data['status'] = false;
        if($this->input->post('provider_id')){
//            $data['providers'] = $this->diary_model->get_all_providers($this->user_id, (int)$this->input->post('project_id'));
            $data['projects'] = $this->diary_model->get_all_projects($this->user_id, $this->input->post('provider_id'));
            $data['status'] = true;
        }
        echo json_encode($data);
        exit;
    }

    public function diarySearch(){
        header('Content-Type: application/json');
        $data['status'] = false;
        $project_id = '';
        $provider_id = '';
        $start_date = '';
        $end_date = '';
        if($this->input->post('provider_id') && !empty($this->input->post('provider_id'))){
            $provider_id = $this->input->post('provider_id');
        }
        if($this->input->post('project_id') && !empty($this->input->post('project_id'))){
            $project_id = $this->input->post('project_id');
        }
        if($this->input->post('start_date') && !empty($this->input->post('start_date'))){
            $start_date = $this->input->post('start_date');
        }
        if($this->input->post('end_date') && !empty($this->input->post('end_date'))){
            $end_date = $this->input->post('end_date');
        }
        if($this->input->post('provider_id') && !empty($this->input->post('provider_id'))){
            $data['result'] = $this->diary_model->get_diaries($this->user_id, $project_id, $provider_id, $start_date, $end_date);
        }
        $attach_ids = [];
        if(isset($data['result']) && !empty($data['result'])){
            for($i=0; $i<count($data['result']); $i++){
                array_push($attach_ids, $data['result'][$i]['id']);
                $data['result'][$i]['project_id'] = md5($data['result'][$i]['project_id']);
                $data['result'][$i]['date'] = date('M d Y. h:i a', strtotime($data['result'][$i]['date']));//Thu Jan 28 2016. 02:00pm
                $data['result'][$i]['time'] = round($data['result'][$i]['time']/60)."h ".($data['result'][$i]['time']%60)."min ";
            }
            $attach_ids = implode(",", $attach_ids);
            $files = $this->diary_model->getDiaryFiles($attach_ids);
            foreach($data['result'] as &$res){
                $res['files'] = array();
                foreach($files as $f){
                    if($res['id'] == $f['diary_id']){
                        array_push($res['files'], $f);
                    }
                }
            }
            if($data){
                $data['status'] = true;
            }
        }
        echo json_encode($data);
    }


}