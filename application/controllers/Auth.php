<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {
    /**
     * Set validation rules
     *
     * @var array
     */
    protected $validation_rules = array(
        'login_rules' => array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|max_length[255]|valid_email|trim'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|max_length[255]|trim'
            )
        ),
        'sign_up_rules' => array(
            array(
                'field' => 'user_type',
                'label' => 'User Type',
                'rules' => 'required'
            ),
            array(
                'field' => 'username',
                'label' => 'Name',
                'rules' => 'required|min_length[1]|max_length[255]|trim'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|min_length[5]|max_length[255]|valid_email|trim|is_unique[users.email]'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|min_length[1]|max_length[255]|trim|matches[r_password]'
            ),
            array(
                'field' => 'r_password',
                'label' => 'Repeat Password',
                'rules' => 'required|min_length[1]|max_length[255]|trim'
            )
        )
    );
    /**
     * Checking is user logged
     */
    protected $check_status = FALSE;
    /**
     * Set autoloaded libraries
     *
     * @var array
     */
    protected $libraries = array("form_validation");
    /**
     * Set autoloaded models
     *
     * @var array
     */
    protected $models = array("user","token","file");


    public function __construct() {
        parent::__construct();
    }

    public function login() {
        $this->page = "login/login";
        $this->header = '';
        $this->footer = '';
        $this->render();
    }

    public function loginProcess() {
        $validate = $this->validate("login_rules");

        if (!is_array($validate)) {
            $email = $this->input->post("email");
            $password = md5($this->input->post("password"));
            $data = array(
                "email" => $email,
                "password" => $password
            );
            $user_data = $this->user_model->get_by($data);
            if ($user_data) {
                if($user_data->user_type == 0) {
                    $url = "projects";
                } else if($user_data->user_type == 1) {
                    $url = "providers";
                }
                $file = $this->file_model->get_by("id",$user_data->profile_image_id);

                $user_data = $this->user_model->get_by($data);
                $session_data = array(
                    'is_client_login' => $user_data->id,
                    'user_type' => $user_data->user_type,
                    'avatarPath' => $file->path
                );

                $this->session->set_userdata($session_data);
                echo json_encode([
                    "status" => "success",
                    "url" => $url
                ]);
            } else {
                echo json_encode([
                    "status" => "error",
                    "value" => "Incorrect email or password"
                ]);
            }
        } else {
            echo json_encode(["validation" => $validate]);
        }
    }

    public function signUp() {
        $this->page = "signup/signup";
        $this->header = '';
        $this->footer = '';
        $this->render();
    }

    public function signUpProcess() {
        $validate = $this->validate("sign_up_rules");

        if (!is_array($validate)) {
            $data = $this->input->post();
            $data['notifications'] = 'immediate';
            $user_id = $this->user_model->insert($data);
            if ($user_id) {
                $token['user_id'] = $user_id;
                $token['api_key'] = $this->generateRandomString($lenght = 10);
                $token['secret_key'] = $this->generateRandomString($lenght = 15);
                $token['secret_key'] = $this->generateRandomString($lenght = 15);
                $query = $this->token_model->insert($token);
                $file['path'] = "assets/images/developer-image.jpg";
                if($data["user_type"]){
                    $file['path'] = "assets/images/client-image.jpg";
                }
                $file['user_id'] = $user_id;
                $file_id = $this->file_model->insert($file,FALSE);
                $dt['profile_image_id'] = $file_id;
                $this->user_model->update_by("id",$user_id,$dt);

                $session_data = array(
                    'is_client_login' => $user_id,
                    'user_type' => $data["user_type"],
                    'avatarPath' => $file['path']
                );

               $template = $this->load->view('send_messages/message',$data,true);
               $mail = $this->sendmail($data['email'], 'support@developerescrow.com', 'welcome', $message = $template);
               $this->session->set_userdata($session_data);
               echo json_encode(['status' => "success"]);
            } else {
                echo json_encode(['status' => 'error']);
            }
        } else {
            echo json_encode(["validation" => $validate]);
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect("/");
    }

    public function forgot_pass(){
        $data = array();
        $data['sendEmail'] = "Something went wrong.<br/> Please, try again.";
        $data['status'] = false;
        $post = $this->input->post();
        $this->load->library('email');
        if(!empty($post)){
            $mail_template = [];
            $mail_template['user'] = (array)$this->user_model->get_by('email', $post);
            if(!empty($mail_template['user']['id'])) {
                $this->email->from('support@developerescrow.com','Developer Escrow - Forgot Password');
                $mail_template['password'] = $this->randomPassword();

                $template = $this->load->view('send_messages/forgot_password_email', $mail_template, true);
                $mail = $this->sendmail($post['email_forgot_pass'], 'support@developerescrow.com', 'Forgot Password', $message = $template);
                $user_data = array();
                if($mail) {
                    $user_data['password']= md5($mail_template['password']);
                    $user = $this->user_model->update_by('id',(int)$mail_template['user']['id'], $user_data);
                    if($user) {
                        $str = "New password has been sent to your email.<br/> Please, check your inbox";
                        $data['sendEmail'] = $str;
                        $data['status'] = true;
                    }
                }
            }
        }
        echo json_encode($data);exit;
    }

    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 10; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

}