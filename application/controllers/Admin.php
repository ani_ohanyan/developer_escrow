<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller
{
    /**
     * @var array
     */
    protected $models = array('user', 'file', 'project', 'dispute', 'admin', 'dispute_comment');
    protected $libraries = array("form_validation");

    /**
     * Set validation rules
    */
    protected $validation_rules = array(
        'login_rules' => array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|max_length[255]|valid_email|trim'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|max_length[255]|trim'
            )
        )
    );

    /**
     * Checking is admin logged
     */
    protected $check_is_admin_login = TRUE;

    public function __construct(){
        parent::__construct();
        $this->check_is_admin_login = TRUE;
    }

    public function index(){
        $validate = $this->validate("login_rules");
        if(!empty($_POST)){
            if (!is_array($validate)) {
                $username = $this->input->post("email");
                $password = md5($this->input->post("password"));
                $data = array(
                    "email" => $username,
                    "password" => $password
                );
                $admin_data = $this->admin_model->get_by($data);
                $adminFile = $this->file_model->get_by("user_id",0)->path;
                if ($admin_data) {
                    $session_data = array(
                        'type' => $admin_data['type'],
                        'is_admin_login' => 1,
                        'username' => $admin_data['username'],
                        'avatarPath' => $adminFile
                    );
                    $this->session->set_userdata($session_data);
                    if($admin_data['email'] == $username && $admin_data['password'] == $password ){
                         redirect(base_url("/admin/users"));
                    }else{
                        /*'Incorrect email or password'*/
                    }
                } else {
                    /*'Incorrect email or password'*/
                }
            } else {
                //echo json_encode(["validation" => ]);
            }
        }
        $this->page = "admin/admin";
        $this->side = "admin";
        $this->render();
    }

    public function settings(){
        if ($this->session->userdata('is_admin_login')) {
            $data = $this->admin_model->get_all();
            $adminData['admin_data'] = reset($data);
            if (isset($_POST) && !empty($_POST)) {
                $admin_id = 0;
                $admin['username'] = $this->input->post("name");
                $admin['email'] = $this->input->post("email");

                if (!empty($this->input->post("old_password")) && !empty($this->input->post("new_password"))) {
                    if (md5($this->input->post("old_password")) == $adminData['admin_data']['password']) {
                        $admin['password'] = md5($this->input->post("new_password"));
                    }
                } else {
                    echo 'Incorrect Password';
                }
                $this->admin_model->update_by("id", $admin_id, $admin);
            }
            $this->page = "admin/settings";
            $this->data_template = $adminData;
            $this->side = "admin";
            $this->render();
        }else{
            redirect(base_url('admin'), 'refresh');
        }
    }
    public function users(){
        if ($this->session->userdata('is_admin_login')){
        $data['user_data'] = $this->user_model->get_all();
        $this->page = "admin/users";
        $this->data_template = $data;
        $this->side = "admin";
        $this->render();
        }else{
            redirect(base_url('admin'), 'refresh');
        }
    }

    public function projects(){
        if ($this->session->userdata('is_admin_login')) {
            $data['projects'] = $this->project_model->get_all();
            $this->page = "admin/projects";
            $this->data_template = $data;
            $this->side = "admin";
            $this->render();
        }else{
            redirect(base_url('admin'), 'refresh');
        }
    }

    public function disputes(){
        if ($this->session->userdata('is_admin_login')) {
        $disputes = $this->dispute_model->get_all();//get_by
        foreach($disputes as &$dispute){
            $invited_email = $this->user_model->get_by('id', $dispute->receiver_id)->email;
            $dispute->invited_email = $invited_email;
            $comments_last_date = $this->dispute_comment_model->lastComment('dispute_id', $dispute->id);
            $comments_last_date = reset($comments_last_date);
            $lastComment = $this->dispute_comment_model->get_by('date', $comments_last_date['max(date)'])->message;
            $dispute->last_comment = $lastComment;
        }
        $data['disputes'] = $disputes;
        $this->page = "admin/disputes";
        $this->data_template = $data;
        $this->side = "admin";
        $this->render();
        }else{
            redirect(base_url('admin'), 'refresh');
        }
    }

    public function view_dispute($disput_id) {
        if ($this->session->userdata('is_admin_login')) {
        $data['dispute'] = $this->dispute_model->getDisputesById($disput_id);
        $data['comments'] = $this->dispute_model->getDisputeComments($disput_id);
        foreach ($data['comments'] as &$comment){
            $comment_image = $this->file_model->get_by('user_id',$comment['user_id'])->path;
            $admin_name = $this->admin_model->get_by('id',$comment['user_id'])['username'];
            $comment['comment_image'] = $comment_image;
            if($comment['username'] == ''){
                $comment['username'] = $admin_name;
            }
        }

        $data['this_id'] = 0;
        $data['admin'] = $this->admin_model->get_by('id',0);

        $admin_image = $this->file_model->get_by('user_id',0)->path;
        $data['admin']['admin_image'] = $admin_image;

        $this->page = "admin/view_dispute";
        for($i=0; $i<count($data['comments']); $i++){
            $data['comments'][$i]['date'] = $this->timeAgo(strtotime($data['comments'][$i]['date']));
        }
        $this->data_template = $data;
        $this->side = "admin";
        $this->render();
        }else{
            redirect(base_url('admin'), 'refresh');
        }
    }

    /* Delete Users From Admin */
    public function delete_user(){
        if(!empty($this->input->post("user_id"))){
            $user_id = $this->input->post("user_id");
            $deleted_user = $this->user_model->delete_by('id', $user_id);
            echo $deleted_user;die;
        }

    }


    /*
    *  change user avatar photo ajax
   */
    public function updateAdminAvatar(){
        header('Content-Type: application/json');

        if (!empty($_FILES)) {

            $file = $this->upload_file("img_file");
            $admin = $this->admin_model->get_by("id",0);
            $adminFile = $this->file_model->get_by("user_id",$admin['id']);


            if (!isset($file["error"])) {

                if(!empty($adminFile) && $adminFile->id){
                    $res  = $this->file_model->update_by("user_id",$adminFile->user_id, $file);
                    $this->session->set_userdata('avatarPath', $file['path']);
                    $file_id = $adminFile->id;
                } else {
                    $file_id = $this->file_model->insert($file, FALSE);
                }

                if($file_id){
                    $data_admin["profile_image_id"] = $file_id;
                    $this->admin_model->update_by("id", $this->user_id, $data_admin);

                    $file = (array)$this->file_model->get_by("id",$file_id);
                    $this->session->userdata['avatarPath'] = $file['path'];

                    echo json_encode(array('status' => true,"path"=> $file['path']));exit;
                }
                 redirect(base_url('admin/settings'), 'refresh');

            }
        }
        echo json_encode(array('status' => false));exit;

    }

    public function logout() {
        $this->session->sess_destroy();
        redirect("/");
    }


}