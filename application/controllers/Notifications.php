<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends MY_Controller
{
    /**
     * Checking is user logged
     */
    protected $check_is_login = TRUE;

    protected $models = array('notification', 'user');

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->title = "Developer Escrow | Notifications";
        $this->page = "notifications/notifications";
        $data['notifications'] = $this->user_notification_model->getUserNotificationById(md5($this->user_id));
        for($i=0; $i<count($data['notifications']); $i++){
            $data['notifications'][$i]['date'] = $this->timeAgo(strtotime($data['notifications'][$i]['date']));
        }
        $this->data_template = $data;
        $this->render();
    }

    public function readmsg(){
        if(!empty($this->input->post()) && empty($this->input->post('note'))) {
            $data['notes'] = $this->notification_model->getlastNotes(MD5($this->user_id) , 3);
        }
        if(!empty($_GET)) {
            $note['is_read'] = 1;
            $data['status'] = $this->user_notification_model->update_by("id",$_GET['note_id'], $note);
        }
        $data['count'] = $this->noteCount();
        echo json_encode($data);exit;
    }
}
