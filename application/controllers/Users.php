<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller
{
    // Stripe Configuration options
    private $stripeConfig = [
        'stripe_key_test_public' => 'pk_test_u6UyouBGDzUEWfVliFKP9zGk',
        'stripe_key_test_secret' => 'sk_test_8ZurwXcpCo7r2Sg9iWKbTgo5',
        'stripe_key_live_public' => 'pk_live_dSchXN2WjWt400PHbFx0HCLw',
        'stripe_key_live_secret' => 'sk_live_pX9QiNjLBN1kpoIT32jTkCGI',
        'stripe_test_mode'       => TRUE,
        'stripe_verify_ssl'      => FALSE,
    ];
    protected $models = array("user" , "project", "user_project", "card" , "bank" , "paypal" ,"file","token");
    protected $check_is_login = TRUE;


    public function __construct()
    {
        parent::__construct();
        $this->load->library('Stripe',$this->stripeConfig);
    }

    public function viewProfile() {
        $tokens = $this->token_model->get_by('user_id', $this->user_id);
        if(isset($tokens) && !empty($tokens)) {
            $data['api_key'] = $tokens->api_key;
            $data['secret_key'] = $tokens->secret_key;
        }
        else{
            $data['api_key'] = '';
            $data['secret_key'] = '';
        }
        $data['user_profile'] = $this->user_model->get_by("id", $this->user_id);
        $data['people'] = $this->project_model->getPeopleAndProjects($this->user_id);
        $count = count($data['people']);
        for($i=0; $i< $count; $i++){
            $data['people'][$i]['created_at'] = $this->timeAgo(strtotime($data['people'][$i]['created_at']));
        }
        $this->title = "Developer Escrow | Profile page";
        $this->page = "profile/profile";
        $this->data_template = $data;
        $this->render();
    }

    public function add_card(){
        $data['status'] = false;
        if($this->input->post()){
            $exists = $this->card_model->get_by('user_id', $this->user_id);
            if($exists){
                echo 'You have already added a card';
            } else {
                $data = $this->input->post();
                $data['user_id'] = $this->user_id;
                $add_card = $this->card_model->insert($data);
                if($add_card){
                    $data['status'] = true;
                    echo $data['status'];
                }
            }
        }
//       $this->viewProfile();
    }

    public function add_payment_information(){
        header('Content-Type: application/json');
        $data['status_paypal'] = false;
        $data['status_bank'] = false;
        if($this->input->post('payment_account')){
            $exists = $this->paypal_model->get_by('user_id', $this->user_id);
            if($exists){
                $data['error_paypal'] = 'You have already added a paypal account.';
            } else {
                $add_paypal['paypal_account'] = $this->input->post('payment_account');
                if (!filter_var($add_paypal['paypal_account'], FILTER_VALIDATE_EMAIL) === false) {
                    $add_paypal['user_id'] = $this->user_id;
                    $add_card = $this->paypal_model->insert($add_paypal);
                    if($add_card){
                        $data['status_paypal'] = true;
                    }
                } else {
                    $data['error_paypal'] = 'Invalid email address.';
                }
            }
        }
        if($this->input->post('bank_name')){
            $exists = $this->bank_model->get_by('user_id', $this->user_id);
            if($exists){
                $data['error_bank'] = 'You have already added a card.';
            } else {
                $add = $this->input->post();
                unset($add['payment_account']);
                $add['user_id'] = $this->user_id;
                $add_card = $this->bank_model->insert($add);
                if($add_card){
                    $data['status_bank'] = true;
                }
            }
        }
        echo json_encode($data);
        exit;
    }

    public function enable(){
        $data['status'] = false;
        if($this->input->post()){
            $type = $this->input->post('type');
            $change['status'] = 0;
            $model = $type."_model";
            $exist = $this->$model->get_by('user_id', $this->user_id);
            if($exist){
                $res = $this->$model->update_by('user_id', $this->user_id, $change);
                if($res){
                    $data['status'] = true;
                }
            }
        }
        echo $data['status'];
    }

    public function disable(){
        $data['status'] = false;
        if($this->input->post()){
            $type = $this->input->post('type');
            $change['status'] = 1;
            $model = $type."_model";
            $exist = $this->$model->get_by('user_id', $this->user_id);
            if($exist){
                $res = $this->$model->update_by('user_id', $this->user_id, $change);
                if($res){
                    $data['status'] = true;
                }
            }
        }
        echo $data['status'];
    }

    public function profile($user_id = null) {
        $data['user_profile'] = (array)$this->user_model->get_by("MD5(id)",$user_id);
        $this->title = "Developer Escrow | Profile page";
        $this->page = "profile/user_profile";
        $this->data_template = $data;
        $this->render();
    }
    /**
     * saved changes in user profile General settings
     */
    public function save_changes() {
        header('Content-Type: application/json');
        $success = false;
        $user = (array)$this->user_model->get_by("id", $this->user_id);
        $data = array();
        if(!empty($this->input->post())){
            $phone_number = $this->input->post("phone_number");
            $name = $this->input->post("name");
            $email = $this->input->post("email");
            if ($user['phone_number'] != $phone_number) {
                $user['phone_number'] = $phone_number;
            }
            if ($user['username'] != $name) {
                $user['username'] = $name;
            }
            if ($user['email'] != $email) {
                $user['email'] = $email;
            }
            $query = $this->user_model->update_by('id', (int)$user ['id'], $user);
            $success = true;
            if (!empty($this->input->post('old_password') && $this->input->post("new_password"))) {
                $old_password = md5($this->input->post("old_password"));
                $new_password = md5($this->input->post("new_password"));
                if ($old_password == $user['password']) {
                    $user ['password'] = $new_password;
                    $query = $this->user_model->update_by('id', (int)$user ['id'], $user);
                    $success = true;
                }
                else{
                    $success = false;
                }

            }

        }
        if (!empty($this->input->post("notification"))){
            $notification = $this->input->post("notification");
            if ($user['notifications'] != $notification) {
                $user['notifications'] = $notification;
                $query = $this->user_model->update_by('id', (int)$user ['id'], $user);
            }
        }
        echo json_encode($success); exit;
    }
    
    /* Generate Token */
    public function generate_token(){
        header('Content-Type: application/json');
        $token['api_key'] = $this->generateRandomString($lenght = 10);
        $token['secret_key'] = $this->generateRandomString($lenght = 15);
        $query = $this->token_model->update_by('user_id',$this->user_id, $token);
        echo json_encode($token);exit;
    }
    
    /*
     *  change user avatar photo ajax
    */
    public function updateUserAvatar(){
        header('Content-Type: application/json');
        if (!empty($_FILES)) {
            $file = $this->upload_file("img_file");
            $user = $this->user_model->get_by("id",$this->user_id);
            $userFile = $this->file_model->get_by("id",$user->profile_image_id);
            if (!isset($file["error"])) {
                if(!empty($userFile) && $userFile->id){
                    $this->file_model->update_by("id",$userFile->id, $file);
                    $file_id = $userFile->id;
                } else {
                    $file_id = $this->file_model->insert($file, FALSE);
                }
                if($file_id){
                    $data_user["profile_image_id"] = $file_id;
                    $this->user_model->update_by("id", $this->user_id, $data_user);

                    $file = (array)$this->file_model->get_by("id",$file_id);
                    $this->session->userdata['avatarPath'] = $file['path'];

                    echo json_encode(array('status' => true,"path"=> $file['path']));exit;
                }
            }
        }
        echo json_encode(array('status' => false));
        exit;
    }

}