<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Terms extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->title = "Developer Escrow | Terms of Service";
        $this->page = "terms/terms";
        $this->render();
    }



}