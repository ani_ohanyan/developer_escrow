<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->library('googlemaps');

        // Map One
        $config['center'] = '42.658516, -83.152725';
        $config['zoom'] = 14;
        $config['map_name'] = 'map_one';
        $config['map_div_id'] = 'map_canvas_one';
        $this->googlemaps->initialize($config);

        $marker = array();
        $marker['position'] = '42.658516, -83.152725';
        $marker['infowindow_content'] = "Troy";
        $this->googlemaps->add_marker($marker);

        $data['map_one'] = $this->googlemaps->create_map();

// Map Two
        $config['center'] = '53.3438289, -6.264057717';
        $config['zoom'] = 14;
        $config['map_name'] = 'map_two';
        $config['map_div_id'] = 'map_canvas_two';
        $this->googlemaps->initialize($config);

        $marker = array();
        $marker['position'] = '53.3438289, -6.264057717';
        $marker['infowindow_content'] = "Salford";
        $this->googlemaps->add_marker($marker);

        $data['map_two'] = $this->googlemaps->create_map();

        $this->data_template = $data;
        $this->title = "Developer Escrow | Contacts";
        $this->page = "contact/contact";
        $this->render();
    }

    function send_email()
    {
        $post = $this->input->post();
        $subject = $post['contact_name'];
        $email = $post['contact_email'];
        $message = $post['contact_message'];
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.sendgrid.net',
            'smtp_port' => 587,
            'smtp_user' => 'novp_ap',
            'smtp_pass' => '3rone!mE',
            'charset' => "utf-8",
            'mailtype' => "html",
            'newline' => "\r\n"
        );
        $this->load->library('email', $config);
        $this->email->from($email);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        if($this->email->send())
        {
            echo 'Email sent.';
        }
        else
        {
            show_error($this->email->print_debugger());
        }

    }



}