<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Comments extends MY_Controller
{
    /**
     * Checking is user logged
     */
    //protected $check_is_login = TRUE;

    protected $models = array('comment','dispute_comment', 'dispute', 'user_project','project','file','admin');

    public function __construct() {
        parent::__construct();
    }

    public function addCommentFile(){
        $data['id'] = [];
        $data['name'] = [];
        $data['path'] = [];
        if(!empty($_FILES)){
            $uploads = $this->upload_files($_FILES['project_attachment']);
            if(!isset($uploads["error"])) {
                foreach($uploads as $up) {
                    $up['user_id'] = $this->user_id;
                    $up['class'] = 'project_comment';
                    $up['attachment_id'] = $this->user_id;
                    $fileId = $this->file_model->insert($up);
                    array_push($data['id'], $fileId);
                    array_push($data['name'], $up['name']);
                    array_push($data['path'], $up['path']);
                }
            }
        }
        echo json_encode($data);exit;
    }

    public function addComment($model = '') {
        $data = $this->input->post();
        $data['user_id'] = $this->user_id;
        if($this->input->post('message_file')){
            $file = $this->input->post('message_file');
            $file_name = $this->input->post('message_file_name');
            $file_path = $this->input->post('message_file_path');
            unset($data['message_file']);
            unset($data['message_file_name']);
            unset($data['message_file_path']);
        }
        if(empty($model)){
            $notification['type'] = 'project';
            $a = (array)$this->project_model->get_by('id', $data['project_id']);
            if((int)$a['user_id'] == (int)$data['user_id']){
                $u_p = (array)$this->user_project_model->get_by('project_id', $data['project_id']);
                if($u_p){
                    $notification['user_id'] = $u_p['user_id'];
                }
            } else {
               $notification['user_id'] = $a['user_id'];
            }
            $notification['project_id'] = $data['project_id'];
            $notification['notification_id'] = 2;
            $notification['type_id'] = $data['project_id'];
        } else {
            $notification['type'] = str_replace("_","",$model);
            $receiver = (array)$this->dispute_model->get_by("id", $data['dispute_id']);
            if( $receiver['id'] != $data['user_id']){
                $notification['user_id'] =  $receiver['id'];
            } else {
                $proj = (array)$this->project_model->get_by("id", $receiver['project_id']);
                $notification['user_id'] =  $proj['user_id'];
            }
            $notification['project_id'] = $receiver['project_id'];
            $notification['notification_id'] = 3;
            $notification['type_id'] = $data['dispute_id'];
        }
        $model = $model."comment_model";
        $comment_id = $this->$model->insert($data,FALSE);

        if(isset($file)){
            $edit['attachment_id'] =  $comment_id;
            $this->file_model->update_by('id',$file, $edit);
            $data['file'] = $file;
            $data['file_name'] = $file_name;
            $data['file_path'] = $file_path;
        }
        $data['status'] = false;
        if($comment_id){
            $comment = $this->$model->getComment($comment_id);
            if($comment[0]['user_id'] == 0){
                $adminFile = $this->file_model->get_by("user_id",0)->path;
                $adminName = $this->admin_model->get_by("id",0)['username'];
                $comment[0]['username'] = $adminName;
                $comment[0]['path'] = $adminFile;
            }
            $data['comment'] = $comment[0];
            $data['status'] = true;
            //notificationification
            $notification['sender_id'] = $data['user_id'];
            if($notification['user_id']){
                $this->user_notification_model->insert($notification);
            }
        }
        if(!empty($model)){
            if(isset($data['comment'])){
                $data['comment']['date'] = $this->timeAgo(strtotime($data['comment']['date']));
            }
        }
        echo json_encode($data);exit;
    }

    public function delete_upload_file(){
        $status = false;
        if($this->input->post('delete_file')){
//            var_dump($this->input->post('delete_file_path')); die;
            unlink($this->input->post('delete_file_path'));
            $status = $this->file_model->delete_by('id',(int)$this->input->post('delete_file'));
        }
        echo $status;
    }
}