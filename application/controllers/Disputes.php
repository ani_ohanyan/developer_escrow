<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Disputes extends MY_Controller
{
    /**
     * Checking is user logged
     */
    protected $models = array("dispute", "user", "project", "comment", "user_notification", 'user_project', 'dispute_comment', 'admin', 'file');
    protected $check_is_login = TRUE;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->title = "Developer Escrow | Disputes";
        $user_profile = $this->user_model->get_by("id",$this->user_id);
        $projects = $this->project_model->getProjects($this->user_id,$this->session->userdata['user_type']);
        $disputes = $this->dispute_model->getAllDisputesById($this->user_id);
        for($i=0; $i<count($disputes); $i++){
            $disputes[$i]['date'] = $this->timeAgo(strtotime($disputes[$i]['date']));
        }
        $this->data_template = array(
            "user_profile" => $user_profile,
            "projects"     => $projects,
            "disputes"     => $disputes
        );
        $this->page = "disputes/disputes";
        $this->render();
    }

    public function create_dispute(){
        header('Content-Type: application/json');
        $user = (array)$this->user_model->get_by("id", $this->user_id);
        $project_id = $this->input->post("project_id");
        $project = (array)$this->project_model->get_by("id", $project_id);

        $project_user_id = $project['user_id'];
        $project_invite_id = $this->user_project_model->get_by("project_id", $project_id)->user_id;
        $data = array();
        if(!empty($project['id'])){
            $data['project_id'] = $project_id;
        } else{
            $this->errors("empty project name");
        }
        $data['user_id'] = $this->user_id;
        if(!empty($this->input->post("description"))) {
            $data['description'] = $this->input->post("description");
        } else {
            $this->errors("empty dispute description");
        }
        if(!empty($this->input->post("email"))){
            $receiver = $this->user_model->get_by("email", $this->input->post("email"));
            if(empty($receiver)){
                $message = [
                  'message' => 'This email does not exist',
                  'success' => false
                ];
                echo json_encode($message);die;
            }else{
                if($receiver->id == $project_user_id || $receiver->id == $project_invite_id){
                    $data['receiver_id'] = $receiver->id;
                }else{
                    $message = [
                        'message' => 'The email does not exist in this project. Enter an appropriate email',
                        'success' => false
                    ];
                    echo json_encode($message);die;
                }
            }

        }else {
            //$this->errors("empty other party email");
            $message = [
                'message' => 'The email field is required',
                'success' => false
            ];
            echo json_encode($message);die;
        }
        if(!empty($this->input->post("phone_number"))){
            $data['phone_number'] = $this->input->post("phone_number");
        }
        if(empty($this->user_model->get_by("email", $this->input->post("email")))){
            $this->errors("wrong user");
        }
        $data['status'] = 0;
        $this->sendmail($this->input->post("email"), $user['email'],"Dispute", $data['description']);
        $query = $this->dispute_model->insert($data);
        if($query){
            //notificationification
            $notification['sender_id'] = $this->user_id;
            $notification['user_id'] = $data['receiver_id'];
            $notification['notification_id'] = 1;
            $notification['type'] = 'dispute';
            $notification['project_id'] = $data['project_id'];
            $notification['type_id'] = $query;
            $this->user_notification_model->insert($notification);
        }

        $message = [
            'message' => 'success',
            'success' => true
        ];
        echo json_encode($message);die;
    }

    public function errors($error){
        echo $error;
        die();
    }

    public function view_dispute($disput_id) {
        $data['dispute'] = $this->dispute_model->getDisputesById($disput_id);
        $data['comments'] = $this->dispute_model->getDisputeComments($disput_id);
        foreach ($data['comments'] as &$comment){
            $comment_image = $this->file_model->get_by('user_id',$comment['user_id'])->path;
            $admin_name = $this->admin_model->get_by('id',$comment['user_id'])['username'];
            $comment['comment_image'] = $comment_image;
            if($comment['user_id'] == 0){
                $comment['username'] = $admin_name;
            }
        }
        $data['this_id'] = 0;
        $data['admin'] = $this->admin_model->get_by('id',0);
        $admin_image = $this->file_model->get_by('user_id',0)->path;
        $data['admin']['admin_image'] = $admin_image;
        $data['this_id'] = $this->user_id;
        $data['dispute']['admin_comment_date'] = $this->timeAgo(strtotime($data['dispute']['disp_date']));
        $this->page = "disputes/view_dispute";
        for($i=0; $i<count($data['comments']); $i++){
            $data['comments'][$i]['date'] = $this->timeAgo(strtotime($data['comments'][$i]['date']));
        }
        $this->data_template = $data;
        $this->render();
    }

    public function close_dispute(){
        $data = array();
        $data['id'] = $this->input->post("dispute_id");
        $data['status'] = 1;
        $d = $this->dispute_model->update_by("id",$data['id'],$data);
        if($d) {
            redirect(base_url('disputes'), 'refresh');
        }
    }
}