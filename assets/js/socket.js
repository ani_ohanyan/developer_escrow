/**
 * Websockets
 */

var wsUri = "ws://127.0.0.1:9000/al/server.php";
websocket = new WebSocket(wsUri);


websocket.onopen = function(ev) { // connection is open
    //alert('Connected!');// notify user
}

$('#newMsgFormId').on('submit',function(e){
    e.stopPropagation();
    var file_data = $("#msgFileId").prop('files')[0];
    //var i=0;
    //for(i=0; i<file_data.length; i++){
    //    console.log(file_data[i]);
    //}

    var form_data = new FormData();
    $("#newMsgFormId").attr('disabled','disabled');
    form_data.append('message_file', file_data);
    form_data.append('message_data',$(this).serialize() );

    $.ajax ({
        url: base_url+"messages/message",
        type: 'post',
        data: form_data,
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        success: function(data){
            var data = $.parseJSON(data);

            if(data.success){
                console.log(data.message_id);
                var msg = {
                    receiver_id:$("#receiver_id").val(),
                    message_id:data.message_id
                };
                websocket.send(JSON.stringify(msg));
            }
            $('#new_message_popup').modal('hide');
        },
        error: function(){
            //console.log("error");
        }
    });
    return false;
});

websocket.onmessage = function(ev) {

    var msg = JSON.parse(ev.data); //PHP sends Json data
    var type = msg.type;

    var receiver_id = msg.receiver_id;
    var message_id = msg.message_id;

    if(type == 'usermsg')
    {
        if($("#currentUserId").attr("data-id") == receiver_id ){

            $.ajax ({
                url: base_url+"messages/readmsg",
                type: 'post',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                success: function(data){
                    var data = $.parseJSON(data);
                    $("#round").html(data.count);
                },
                error: function(){
                }
            });

            $.ajax ({
                url: base_url+"messages/getMessageById",
                type: 'post',
                dataType: 'text',
                data:{msgId:message_id},
                success: function(data){
                    var data = $.parseJSON(data);
                    if(data.success) {
                        html = "<div class=\"col-lg-12 col-md-12  col-sm-12 col-xs-12 full-width-left table_content no-padding msgIsRead \">";
                        html+="<div class=\"col-lg-2 col-md-1 col-sm-1 col-xs-2 \"><p>"+data.message.full_name+"</p></div>";
                        html+="<div class=\"col-lg-4 col-md-5 col-sm-5 col-xs-6\"><p class=\"massage_title\">"+data.message.subject+"</p></div>";
                        html+="<div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-4\"><p class=\"time_stamp\">"+data.message.date+"</p></div>";
                        html+="<div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-12\"><a href=\""+base_url+"message/view/"+data.message.pm_id+"\">View Message <i class=\"fa fa-angle-right\"></i></a></div></div>";

                        $(".messageList").children(':nth-child(2)').before(html);
                        $(".messageList").children(':last-child').remove();
                    }
                },
                error: function(){
                }
            });
        }
    }
};

websocket.onerror	= function(ev){
    //$('#message_box').append("<div class=\"system_error\">Error Occurred - "+ev.data+"</div>");
};

websocket.onclose 	= function(ev){
    //$('#message_box').append("<div class=\"system_msg\">Connection Closed</div>");
};

