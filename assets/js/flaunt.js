/*
	Flaunt.js v1.0.0
	by Todd Motto: http://www.toddmotto.com
	Latest version: https://github.com/toddmotto/flaunt-js
	
	Copyright 2013 Todd Motto
	Licensed under the MIT license
	http://www.opensource.org/licenses/mit-license.php

	Flaunt JS, stylish responsive navigations with nested click to reveal.
*/
;
(function ($) {

    // DOM ready
    $(function () {

        // Append the mobile icon nav
        $('header').append($('<div class="nav-mobile"><i class="ion-navicon-round"></i></div>'));

        // Add a <span> to every .nav-item that has a <ul> inside
        $('.nav-item').has('ul').prepend('<span class="nav-click"><i class="nav-arrow"></i></span>');

        // Click to reveal the nav
        $('.nav-mobile').click(function () {
            $(this).toggleClass('active');
            $('.nav-list, .nav').toggle();
            $('.nav-list, .nav').toggleClass('active');
            $('.overlay-menu').toggleClass('active');
        });

        $('#open-sign-in').click(function () {
            $('.nav-list, .nav').toggle();
        });

        $('.nav-item').has('ul').mouseover(function () {
            $(this).addClass('active');
        });

        $('.nav-item').has('ul').mouseleave(function () {
            $(this).removeClass('active');
        });

        var current_width = $(window).width();
        if (current_width >= 1024) {
            $(".nav").addClass('display');
        } else {
            $(".nav").removeClass('display');
        };

        // Dynamic binding to on 'click'
        $('.nav-list').on('click', '.nav-click', function () {
            // Toggle the nested nav
            $(this).siblings('.nav-submenu').toggle();
            // Toggle the arrow using CSS3 transforms
            $(this).children('.nav-arrow').toggleClass('nav-rotate');
        });

    });

})(jQuery);