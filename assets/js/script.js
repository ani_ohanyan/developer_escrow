$(document).ready(function () {


    //if ($('.form input').length) {
    //    $('.form input').iCheck({
    //        checkboxClass: 'icheckbox_flat-green',
    //        radioClass: 'iradio_flat-green'
    //    });
    //
    //};

    $('a#open-pricing').click(function(){
        $('.pricing').fadeIn( "slow" );
        $('.nav, .nav-list').removeClass( "display" );
        $('.nav-mobile').removeClass( "active" );
    });

    $('a#tab-link-1').click(function(){
        $(this).addClass('active');
        $('a#tab-link-2, a#tab-link-3').removeClass('active');
        $('.tab-content-1').addClass('display');
        $('.tab-content-2, .tab-content-3').removeClass('display');
    });

    $('a#tab-link-2').click(function(){
        $(this).addClass('active');
        $('a#tab-link-1, a#tab-link-3').removeClass('active');
        $('.tab-content-2').addClass('display');
        $('.tab-content-1, .tab-content-3').removeClass('display');
    });

    $('a#tab-link-3').click(function(){
        $(this).addClass('active');
        $('a#tab-link-1, a#tab-link-2').removeClass('active');
        $('.tab-content-3').addClass('display');
        $('.tab-content-1, .tab-content-2').removeClass('display');
    });

    $('header .right-div ul li').click(function(){
        $(this).find('a').addClass('active');
        $(this).find('.dropdown').addClass('display');
        $(this).siblings('li').find('.dropdown').removeClass('display');
    });

    $('header .right-div ul li ul.dropdown').on('mouseleave', function(){
        $(this).removeClass('display');
        $(this).siblings('a').removeClass('active');
    });

    height1 = $(document).height() + $(window).scrollTop();
    $('.overlay').css({'height': height1 + 'px'});



    //$('header .right-div ul li').hover(
    //	function(){
    //		$(this).siblings().removeClass('display');
    //		$(this).find('.dropdown').addClass('display');
    //	 },
    //	function(){
    //		$(this).siblings().removeClass('display');
    //		$(this).find('.dropdown').removeClass('display');
    //
    //	 }
    //)




});


$('.popup a#close-it,.popup a#close-link, .refer-a-friend a#close-it').click(function(){
    $('.upload-files, .upload-link, .new-dispute').removeClass('display');
    $('.overlay').removeClass('display');
})


$('a#open-upload-files').click(function(){
    $('.upload-files').addClass('display');
    $('.overlay').addClass('display');
});

$('a#open-upload-link').click(function(){
    $('.upload-link').addClass('display');
    $('.overlay').addClass('display');
});

$('a#open-new-dispute').click(function(){
    $('.new-dispute').addClass('display');
    $('.overlay').addClass('display');
});





$("a.next-content").click(function() {
    $('html, body').animate({
        scrollTop: $(".what-you-get").offset().top
    }, 2000);
});
$(".btn_learn_more").click(function() {
    $('html, body').animate({
        scrollTop: $(".what-you-get").offset().top
    }, 2000);
});
$("a.scroll-on-top").click(function() {
    $('html, body').animate({
        scrollTop: $("header").offset().top
    }, 2000);
});

$("a#features").click(function() {
    $('html, body').animate({
        scrollTop: $(".what-you-get").offset().top
    }, 2000);
});

$("a#how-works").click(function() {
    $('html, body').animate({
        scrollTop: $(".how-it-works").offset().top
    }, 2000);
});

function resizeDiv() {



}

function resizeDivmobile() {

    var current_width = $(window).width();
    if (current_width >= 1084) {
        $('.nav-list, .nav').show();
    } else {
        if ($('.nav-list').hasClass('active')) {
            $('.nav-list, .nav').show();
        } else {
            $('.nav-list, .nav').hide();
        }
    }



}

jQuery(document).ready(function () {

    if (jQuery(this).width() > 1024) {

        //resizeDiv();

        window.onresize = function (event) {
            resizeDiv();
        }

        window.onload = function (event) {
            resizeDiv();
        }

    }

    if (jQuery(this).width() >= 768) {

        resizeDivmobile();

        window.onresize = function (event) {
            resizeDivmobile();
        }

        window.onload = function (event) {
            resizeDivmobile();
        }

    }

});


jQuery(window).load(function () {
    if (jQuery(this).width() > 1024) {
        resizeDiv();
    }
});


