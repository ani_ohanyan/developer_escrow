$(document).ready(function () {




		if ($('.form input').length) {
			$('.form input').iCheck({
				checkboxClass: 'icheckbox_flat-grey',
				radioClass: 'iradio_flat-grey'
			});

		};

		if ($('.found-div .payment input').length) {
			$('.found-div .payment input').iCheck({
				checkboxClass: 'icheckbox_flat-grey',
				radioClass: 'iradio_flat-grey'
			});

		};

		if ($('.cc-row input').length) {
			$('.cc-row input').iCheck({
				checkboxClass: 'icheckbox_flat-grey',
				radioClass: 'iradio_flat-grey'
			});

		};

		if ($('.milestone-row input').length) {
			$('.milestone-row input').iCheck({
				checkboxClass: 'icheckbox_flat-grey',
				radioClass: 'iradio_flat-grey'
			});

		};



		$('.cc-row input').on('ifUnchecked', function(){
			$('.found-client .ccv').removeClass( "display" );
			$(this).closest('.cc-row').find('.insert-card').removeClass( 'display' );

		});

		$('.cc-row input').on('ifChecked', function(){
			$(this).closest('.cc-row').find('.ccv').addClass( 'display' );
			$(this).closest('.cc-row').find('.insert-card').addClass( 'display' );
		});




		$('a#open-pricing').click(function(){
			$('.pricing').fadeIn( "slow" );
			$('.nav, .nav-list').removeClass( "display" );
			$('.nav-mobile').removeClass( "active" );
		});

		$('a#tab-link-1').click(function(){
			$(this).addClass('active');
			$('a#tab-link-2, a#tab-link-3').removeClass('active');
			$('.tab-content-1').addClass('display');
			$('.tab-content-2, .tab-content-3').removeClass('display');
		});

		$('a#tab-link-2').click(function(){
			$(this).addClass('active');
			$('a#tab-link-1, a#tab-link-3').removeClass('active');
			$('.tab-content-2').addClass('display');
			$('.tab-content-1, .tab-content-3').removeClass('display');
		});

		$('a#tab-link-3').click(function(){
			$(this).addClass('active');
			$('a#tab-link-1, a#tab-link-2').removeClass('active');
			$('.tab-content-3').addClass('display');
			$('.tab-content-1, .tab-content-2').removeClass('display');
		});

		$('header .right-div ul li').click(function(){
			$(this).find('a').addClass('active');
			$(this).find('.dropdown').addClass('display');
			$(this).siblings('li').find('.dropdown').removeClass('display');
		});

		$('header .right-div ul li ul.dropdown').on('mouseleave', function(){
			$(this).removeClass('display');
			$(this).siblings('a').removeClass('active');
		});

		height1 = $(document).height() + $(window).scrollTop();
		$('.overlay').css({'height': height1 + 'px'});



		//$('header .right-div ul li').hover(
		//	function(){
		//		$(this).siblings().removeClass('display');
		//		$(this).find('.dropdown').addClass('display');
		//	 },
		//	function(){
		//		$(this).siblings().removeClass('display');
		//		$(this).find('.dropdown').removeClass('display');
		//
		//	 }
		//)




	});


	$('.popup a#close-it, .refer-a-friend a#close-it').click(function(e) {
		e.preventDefault();
		$('.upload-files, .upload-link, .new-dispute, .withdrawal-ammount, .withdrawal-thanks, .request-funding-thanks, .request-funding, .request-funding-short, .request-release-thanks, .quick-payment-request').removeClass('display');
		$('.overlay').removeClass('display');
	})


	$('a#open-upload-files').click(function(e) {
		e.preventDefault();
		$('.upload-files').addClass('display');
		$('.overlay').addClass('display');
	});

	$('a#open-upload-link').click(function(e) {
		e.preventDefault();
		$('.upload-link').addClass('display');
		$('.overlay').addClass('display');
	});

	$('a#open-new-dispute').click(function(e) {
		e.preventDefault();
		$('.new-dispute').addClass('display');
		$('.overlay').addClass('display');
	});

	$('a#link-desc-client').click(function(e) {
		e.preventDefault();
		$(this).addClass('active');
		$('a#link-desc-me').removeClass('active');
		$('.desc-me').removeClass('display');
	});

	$('a#link-desc-me').click(function(e) {
		e.preventDefault();
		$(this).addClass('active');
		$('a#link-desc-client').removeClass('active');
		$('.desc-me').addClass('display');
	});

	$('a#open-withdrawal-ammount').click(function(e) {
		e.preventDefault();
		$('.withdrawal-ammount').addClass('display');
		$('.overlay').addClass('display');
	});


	$('a#open-found-thanks').click(function(e) {
		e.preventDefault();
		$('.found-client').removeClass('display');
		$('.found-thanks').addClass('display');
	});


	$('a#tab-link-1, a#tab-link-2, a#tab-link-3').click(function(e){
		e.preventDefault();
		$('.found-thanks').removeClass('display');
	});

	$('a#open-request-funding').click(function(e) {
		e.preventDefault();
		$('.request-funding').addClass('display');
		$('.overlay').addClass('display');
	});

	$('a#open-request-funding-thanks').click(function(e) {
		e.preventDefault();
		$('.request-funding').removeClass('display');
		$('.request-funding-thanks').addClass('display');
		$('.overlay').addClass('display');
	});

	$('a#open-quick-payment-request').click(function(e) {
		e.preventDefault();
		$('.quick-payment-request').addClass('display');
		$('.overlay').addClass('display');
	});



	function resizeDiv() {



	}

	function resizeDivmobile() {

		var current_width = $(window).width();
		if (current_width >= 1084) {
			$('.nav-list, .nav').show();
		} else {
			if ($('.nav-list').hasClass('active')) {
				$('.nav-list, .nav').show();
			} else {
				$('.nav-list, .nav').hide();
			}
		}



	}

	jQuery(document).ready(function () {

		if (jQuery(this).width() > 1024) {

			//resizeDiv();

			window.onresize = function (event) {
				resizeDiv();
			}

			window.onload = function (event) {
				resizeDiv();
			}

		}

		if (jQuery(this).width() >= 768) {

			resizeDivmobile();

			window.onresize = function (event) {
				resizeDivmobile();
			}

			window.onload = function (event) {
				resizeDivmobile();
			}

		}

	});


	jQuery(window).load(function () {
		if (jQuery(this).width() > 1024) {
			resizeDiv();
		}
	});
		if ($('.form input').length) {
			$('.form input').iCheck({
				checkboxClass: 'icheckbox_flat-green',
				radioClass: 'iradio_flat-green'
			});

		};


		$('a#open-pricing').click(function(){
			$('.pricing').fadeIn( "slow" );
			$('.nav, .nav-list').removeClass( "display" );
			$('.nav-mobile').removeClass( "active" );
		});

		$('label#tab-link-1').click(function(){
			$(this).addClass('active');
			$('a#tab-link-2').removeClass('active');
			$('.tab-content-1').addClass('display');
			$('.tab-content-2').removeClass('display');
		});

		$('label#tab-link-2').click(function(){
			$(this).addClass('active');
			$('a#tab-link-1').removeClass('active');
			$('.tab-content-2').addClass('display');
			$('.tab-content-1').removeClass('display');
		});

		$('header .right-div ul li').click(function(){
			$(this).find('a').addClass('active');
			$(this).find('.dropdown').addClass('display');
			$(this).siblings('li').find('.dropdown').removeClass('display');
		});

		$('header .right-div ul li ul.dropdown').on('mouseleave', function(){
		   	$(this).removeClass('display');
			$(this).siblings('a').removeClass('active');
		});

		height1 = $(document).height() + $(window).scrollTop();
		$('.overlay').css({'height': height1 + 'px'});


	$('.popup a#close-it, .refer-a-friend a#close-it, .new_project_file_button ,.discard_section').click(function(){
		$('.upload-files, .upload-link, .new-dispute').removeClass('display');
		$('.overlay').removeClass('display');
	})


	$('a#open-upload-files').click(function(){
		$('.upload-files').addClass('display');
		$('.overlay').addClass('display');
	});

	$('a#open-upload-link').click(function(){
		$('.upload-link').addClass('display');
		$('.overlay').addClass('display');
	});

	$('a#open-new-dispute').click(function(){
		$('.new-dispute').addClass('display');
		$('.overlay').addClass('display');
	});

	$("a.next-content").click(function() {
		$('html, body').animate({
			scrollTop: $(".what-you-get").offset().top
		}, 2000);
	});

	$("a.scroll-on-top").click(function() {
		$('html, body').animate({
			scrollTop: $("header").offset().top
		}, 2000);
	});





		$("a.next-content").click(function() {
			$('html, body').animate({
				scrollTop: $(".what-you-get").offset().top
			}, 2000);
		});

		$("a.scroll-on-top").click(function() {
			$('html, body').animate({
				scrollTop: $("header").offset().top
			}, 2000);
		});



function resizeDivmobile() {

	 var current_width = $(window).width();
		if (current_width >= 1084) {
			$('.nav-list, .nav').show();
		} else {
			if ($('.nav-list').hasClass('active')) {
			  $('.nav-list, .nav').show();
			} else {
			  $('.nav-list, .nav').hide();
			}
		}



}

jQuery(document).ready(function () {

  if (jQuery(this).width() > 1024) {
    //resizeDiv();
    window.onresize = function (event) {
      resizeDiv();
    }

    window.onload = function (event) {
      resizeDiv();
    }
  }

  if (jQuery(this).width() >= 768) {

    resizeDivmobile();

    window.onresize = function (event) {
      resizeDivmobile();
    }

    window.onload = function (event) {
      resizeDivmobile();
    }

  }

});


jQuery(window).load(function () {
  if (jQuery(this).width() > 1024) {
    resizeDiv();
  }
});


$("a[data-tab-destination]").on('click', function() {
	var tab = $(this).attr('data-tab-destination');
	$("#"+tab).click();
});

function stripeResponseHandler(status, response) {
	if (response.error) {
		// Show the errors on the form
		$('.withdrawal_amount_modal ').find('.funds_error').text(response.error.message);
		$('.withdrawal_amount_modal ').find('button').prop('disabled', false);
		$('.loading_content').hide();
	} else {
		// response contains id and card, which contains additional card details
		//console.log(response.id);
		var amount = $('.withdrawal_amount_modal ').find('.wa-input').val();
		$.ajax({
			url: base_url + "payments/add_available_by_card",
			type: 'post',
			data: {
				amount:amount,
				stripeToken:response.id
			},
			dataType: 'text',
			success: function (result){
				var result = $.parseJSON(result);
				if(result.status == true){
					$('.loading_content').hide();
					$('.withdrawal_amount_modal').modal('hide');
					$('.available').text('$'+result.available);
				} else {
					$('.funds_error').text(result.error);
					$('.loading_content').hide();
				}
			},
			error: function (result){
				$('.funds_error').text('error');
			}
		});
		return response.id;
	}
}
$('.withdrawal_amount_modal').find('.green').on('click', function(){
	$('.loading_content').show();
	var pay_by =  $('.withdrawal_amount_modal').find('.custom-paymentmethod').val();
	var amount =  $('.withdrawal_amount_modal').find('.wa-input').val();
	if($.trim(amount) && $.isNumeric( amount ) && pay_by == 'card'){  // payment by stripe
		Stripe.setPublishableKey('pk_test_eqsukeBAJlSnSn3XiVws1UyQ');
		$.ajax({
			url: base_url + "payments/create_stripe_token",
			type: 'post',
			dataType: 'text',
			success: function (result){
				if(result){
					var result = $.parseJSON(result);
					//console.log(result);
					var tokenS = Stripe.createToken({
						number: result.card_number,
						cvc: result.cvc,
						exp_month: result.expire_month,
						exp_year: result.expire_year
					}, stripeResponseHandler);
					//console.log(tokenS);
				} else {
					window.location(base_url+"/profile");
				}
				return false;
			},
			error: function (){
				$('.loading_content').hide();
			}
		});
	} else if(!$.trim(amount) && !$.isNumeric( amount ) && pay_by == 'card') {
		$('.loading_content').hide();
		$('.funds_error').text('Enter amount.');
	} else {

	}

	//if($.trim(amount) && $.isNumeric( amount ) && pay_by){
	//	$.ajax({
	//		url: base_url + "payments/add_available",
	//		type: 'post',
	//		data: {amount:amount},
	//		dataType: 'text',
	//		success: function (data){
	//			var data = $.parseJSON(data);
	//			if(data){
	//				$('.withdrawal_amount_modal').modal('hide');
	//				$('.available').text('$'+data.available);
	//			}
	//		}
	//	});
	//	return false;
	//}
})
